﻿using Napos.Core.Abstract;
using Napos.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Napos.Domain.Services.Base
{
    public class BaseDataService : BaseService, IScopedService, IDisposable
    {
        protected readonly DataContext Db;

        public BaseDataService(DataContext dataContext) : base()
        {
            Db = dataContext;
        }

        public virtual void Dispose()
        {

        }
    }
}
