﻿using Napos.Core.Helpers;
using System;
using System.Linq.Expressions;

namespace Napos.Core.Exceptions
{
    public class UserException : Exception
    {
        public string[] Paths { get; protected set; }
        public string[] Messages { get; protected set; }

        public UserException(string message) : base(message)
        {
            Paths = new[] { (string)null };
            Messages = new[] { message };
        }

        public UserException(string propertyPath, string message) : base(message + (!string.IsNullOrEmpty(propertyPath) ? " (" + propertyPath + ")" : ""))
        {
            Paths = new[] { propertyPath };
            Messages = new[] { message };
        }

        public UserException(string[] propertyPaths, string[] messages) : base(string.Join("; ", messages) + (propertyPaths != null && propertyPaths.Length > 0 ? " (" + string.Join(", ", propertyPaths) + ")" : ""))
        {
            Paths = propertyPaths;
            Messages = messages;
        }
    }

    public class UserException<TModel, TProperty> : UserException
    {
        public UserException(Expression<Func<TModel, TProperty>> propertyPath, string message) : base(message + (!string.IsNullOrEmpty(propertyPath.GetPropertyPath()) ? " (" + propertyPath.GetPropertyPath() + ")" : ""))
        {
            Paths = new[] { propertyPath.GetPropertyPath() };
            Messages = new[] { message };
        }
    }
}
