﻿using System;

namespace Napos.Core.Exceptions
{
    /// <summary>
    /// Only marked with [Api] Services and Methods (operations) are allowed. Othervise throw this exception. 
    /// </summary>
    public class ServiceNotFoundException : Exception
    {
        public ServiceNotFoundException(string service) : base($"Service with name '{service}' not found.")
        {

        }

        public ServiceNotFoundException(string service, string operation) : base($"Operation with name '{operation}' of the service '{service}' not found.")
        {

        }
    }
}
