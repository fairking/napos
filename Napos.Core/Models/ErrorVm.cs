﻿using Napos.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Napos.Core.Models
{
    [Api]
    public class ErrorVm
    {
        public string Path { get; set; }
        public string Message { get; set; }
    }
}
