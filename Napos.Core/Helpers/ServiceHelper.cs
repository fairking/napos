﻿using Microsoft.Extensions.DependencyInjection;
using Napos.Core.Abstract;
using Napos.Core.Attributes;
using System;
using System.Linq;
using System.Reflection;

namespace Napos.Core.Helpers
{
    public static class ServiceHelper
    {
        public static void RegisterAllServices(this IServiceCollection serviceCollection, params Assembly[] assemblies)
        {
            var allServices = GetAllServices(assemblies);

            foreach (var service in allServices)
            {
                if (typeof(ITransientService).IsAssignableFrom(service))
                {
                    var inter = service.GetInterfaces().SingleOrDefault(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(ITransientService<>));
                    if (inter != null)
                    {
                        if (!typeof(IMultiService).IsAssignableFrom(service))
                            serviceCollection.RemoveIfExists(inter);

                        serviceCollection.AddTransient(inter.GetGenericArguments()[0], service);
                    }
                    else
                    {
                        if (!typeof(IMultiService).IsAssignableFrom(service))
                            serviceCollection.RemoveIfExists(service);

                        serviceCollection.AddTransient(service);
                    }
                }
                else if (typeof(IScopedService).IsAssignableFrom(service))
                {
                    var inter = service.GetInterfaces().SingleOrDefault(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IScopedService<>));
                    if (inter != null)
                    {
                        if (!typeof(IMultiService).IsAssignableFrom(service))
                            serviceCollection.RemoveIfExists(inter);

                        serviceCollection.AddScoped(inter.GetGenericArguments()[0], service);
                    }
                    else
                    {
                        if (!typeof(IMultiService).IsAssignableFrom(service))
                            serviceCollection.RemoveIfExists(service);

                        serviceCollection.AddScoped(service);
                    }
                }
                else
                {
                    var inter = service.GetInterfaces().SingleOrDefault(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IService<>));
                    if (inter != null)
                    {
                        if (!typeof(IMultiService).IsAssignableFrom(service))
                            serviceCollection.RemoveIfExists(inter);

                        serviceCollection.AddSingleton(inter.GetGenericArguments()[0], service);
                    }
                    else
                    {
                        if (!typeof(IMultiService).IsAssignableFrom(service))
                            serviceCollection.RemoveIfExists(service);

                        serviceCollection.AddSingleton(service);
                    }
                }
            }
        }

        public static Type[] GetAllServices(Assembly[] assemblies)
        {
            var allAssemblies = new Assembly[] { typeof(ServiceHelper).Assembly }.Concat(assemblies).ToArray();

            var allServices = ReflectionHelper.GetAllConcreteTypesDerivedFrom<IService>(allAssemblies);

            return allServices;
        }

        public static Type[] GetAllApiServices(Assembly[] assemblies)
        {
            var services = GetAllServices(assemblies);

            return services.Where(x => x.GetCustomAttribute<ApiAttribute>() != null).ToArray();
        }

        public static void RemoveIfExists(this IServiceCollection serviceCollection, Type serviceType)
        {
            var existingServices = serviceCollection.Where(x => x.ServiceType == serviceType).ToList();

            foreach (var service in existingServices)
            {
                serviceCollection.Remove(service);
            }
        }
    }
}
