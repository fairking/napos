﻿using Microsoft.AspNetCore.Mvc;
using Napos.Core.Exceptions;
using Napos.Core.Helpers;
using Napos.Web.Filters;
using Napos.Web.Services;
using Newtonsoft.Json;

namespace Napos.Web.Controllers
{
    public class DefaultController : ControllerBase
    {
        private readonly ServiceExecutor _serviceExecuter;
        private readonly ILogger<DefaultController> _logger;

        public DefaultController(ServiceExecutor serviceExecuter, ILogger<DefaultController> logger)
        {
            _serviceExecuter = serviceExecuter;
            _logger = logger;
        }

        [HttpGet]
        [HttpPost]
        [DefaultException]
        public async Task<IActionResult> Index(string service, string operation, string? id)
        {
            return await _serviceExecuter.ExecuteService(this, service, operation);
        }
    }
}
