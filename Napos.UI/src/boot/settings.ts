import { boot } from 'quasar/wrappers';
import { SettingService, SettingsForm } from 'src/services/domain';
import { version } from '/package.json';

declare module '@vue/runtime-core' {
    interface ComponentCustomProperties {
        $settings: SettingsForm;
    }
}

export default boot(async ({ app }) => {
    const settings = await SettingService.get();
    app.config.globalProperties.$settings = settings;
    app.config.globalProperties.$version = version;
});
