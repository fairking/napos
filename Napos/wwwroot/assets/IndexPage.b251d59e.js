var __defProp = Object.defineProperty;
var __defProps = Object.defineProperties;
var __getOwnPropDescs = Object.getOwnPropertyDescriptors;
var __getOwnPropSymbols = Object.getOwnPropertySymbols;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __propIsEnum = Object.prototype.propertyIsEnumerable;
var __defNormalProp = (obj, key, value) => key in obj ? __defProp(obj, key, { enumerable: true, configurable: true, writable: true, value }) : obj[key] = value;
var __spreadValues = (a, b) => {
  for (var prop in b || (b = {}))
    if (__hasOwnProp.call(b, prop))
      __defNormalProp(a, prop, b[prop]);
  if (__getOwnPropSymbols)
    for (var prop of __getOwnPropSymbols(b)) {
      if (__propIsEnum.call(b, prop))
        __defNormalProp(a, prop, b[prop]);
    }
  return a;
};
var __spreadProps = (a, b) => __defProps(a, __getOwnPropDescs(b));
import { a as inject, w as watch, j as onBeforeUnmount, X as formKey, g as getCurrentInstance, c as computed, h, r as ref, Y as debounce, Z as injectProp, $ as onBeforeUpdate, s as stopAndPrevent, n as nextTick, a0 as onDeactivated, a1 as onActivated, o as onMounted, f as isRuntimeSsrPreHydration, x as prevent, V as Transition, a2 as shouldIgnoreKey, y as stop, _ as _export_sfc, I as defineComponent, J as openBlock, K as createBlock, L as withCtx, d as createVNode, U as createBaseVNode, M as createElementBlock, N as renderList, F as Fragment, R as toDisplayString } from "./index.b4b5ef26.js";
import { b as useSizeProps, a as useSize, Q as QIcon } from "./QIcon.58674f5b.js";
import { b as QSpinner, Q as QBtn } from "./QBtn.d038f5cf.js";
import { u as useDarkProps, a as useDark } from "./use-dark.a3ae9942.js";
import { c as createComponent, f as hMergeSlotSafely, h as hSlot } from "./render.c97de2b6.js";
import { b as between } from "./format.801e7424.js";
import { u as useFormProps, a as useFormInputNameAttr } from "./use-form.2420a798.js";
import { Q as QPage } from "./QPage.2bcd042a.js";
import { a as CreateStoreForm, b as StoreService } from "./domain.0d7d0906.js";
function useFormChild({ validate, resetValidation, requiresQForm }) {
  const $form = inject(formKey, false);
  if ($form !== false) {
    const { props, proxy } = getCurrentInstance();
    Object.assign(proxy, { validate, resetValidation });
    watch(() => props.disable, (val) => {
      if (val === true) {
        typeof resetValidation === "function" && resetValidation();
        $form.unbindComponent(proxy);
      } else {
        $form.bindComponent(proxy);
      }
    });
    props.disable !== true && $form.bindComponent(proxy);
    onBeforeUnmount(() => {
      props.disable !== true && $form.unbindComponent(proxy);
    });
  } else if (requiresQForm === true) {
    console.error("Parent QForm not found on useFormChild()!");
  }
}
const hex = /^#[0-9a-fA-F]{3}([0-9a-fA-F]{3})?$/, hexa = /^#[0-9a-fA-F]{4}([0-9a-fA-F]{4})?$/, hexOrHexa = /^#([0-9a-fA-F]{3}|[0-9a-fA-F]{4}|[0-9a-fA-F]{6}|[0-9a-fA-F]{8})$/, rgb = /^rgb\(((0|[1-9][\d]?|1[\d]{0,2}|2[\d]?|2[0-4][\d]|25[0-5]),){2}(0|[1-9][\d]?|1[\d]{0,2}|2[\d]?|2[0-4][\d]|25[0-5])\)$/, rgba = /^rgba\(((0|[1-9][\d]?|1[\d]{0,2}|2[\d]?|2[0-4][\d]|25[0-5]),){2}(0|[1-9][\d]?|1[\d]{0,2}|2[\d]?|2[0-4][\d]|25[0-5]),(0|0\.[0-9]+[1-9]|0\.[1-9]+|1)\)$/;
const testPattern = {
  date: (v) => /^-?[\d]+\/[0-1]\d\/[0-3]\d$/.test(v),
  time: (v) => /^([0-1]?\d|2[0-3]):[0-5]\d$/.test(v),
  fulltime: (v) => /^([0-1]?\d|2[0-3]):[0-5]\d:[0-5]\d$/.test(v),
  timeOrFulltime: (v) => /^([0-1]?\d|2[0-3]):[0-5]\d(:[0-5]\d)?$/.test(v),
  email: (v) => /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(v),
  hexColor: (v) => hex.test(v),
  hexaColor: (v) => hexa.test(v),
  hexOrHexaColor: (v) => hexOrHexa.test(v),
  rgbColor: (v) => rgb.test(v),
  rgbaColor: (v) => rgba.test(v),
  rgbOrRgbaColor: (v) => rgb.test(v) || rgba.test(v),
  hexOrRgbColor: (v) => hex.test(v) || rgb.test(v),
  hexaOrRgbaColor: (v) => hexa.test(v) || rgba.test(v),
  anyColor: (v) => hexOrHexa.test(v) || rgb.test(v) || rgba.test(v)
};
const class2type = {};
"Boolean Number String Function Array Date RegExp Object".split(" ").forEach((name) => {
  class2type["[object " + name + "]"] = name.toLowerCase();
});
const useCircularCommonProps = __spreadProps(__spreadValues({}, useSizeProps), {
  min: {
    type: Number,
    default: 0
  },
  max: {
    type: Number,
    default: 100
  },
  color: String,
  centerColor: String,
  trackColor: String,
  fontSize: String,
  thickness: {
    type: Number,
    default: 0.2,
    validator: (v) => v >= 0 && v <= 1
  },
  angle: {
    type: Number,
    default: 0
  },
  showValue: Boolean,
  reverse: Boolean,
  instantFeedback: Boolean
});
const radius = 50, diameter = 2 * radius, circumference = diameter * Math.PI, strokeDashArray = Math.round(circumference * 1e3) / 1e3;
createComponent({
  name: "QCircularProgress",
  props: __spreadProps(__spreadValues({}, useCircularCommonProps), {
    value: {
      type: Number,
      default: 0
    },
    animationSpeed: {
      type: [String, Number],
      default: 600
    },
    indeterminate: Boolean
  }),
  setup(props, { slots }) {
    const { proxy: { $q } } = getCurrentInstance();
    const sizeStyle = useSize(props);
    const svgStyle = computed(() => {
      const angle = ($q.lang.rtl === true ? -1 : 1) * props.angle;
      return {
        transform: props.reverse !== ($q.lang.rtl === true) ? `scale3d(-1, 1, 1) rotate3d(0, 0, 1, ${-90 - angle}deg)` : `rotate3d(0, 0, 1, ${angle - 90}deg)`
      };
    });
    const circleStyle = computed(() => props.instantFeedback !== true && props.indeterminate !== true ? { transition: `stroke-dashoffset ${props.animationSpeed}ms ease 0s, stroke ${props.animationSpeed}ms ease` } : "");
    const viewBox = computed(() => diameter / (1 - props.thickness / 2));
    const viewBoxAttr = computed(() => `${viewBox.value / 2} ${viewBox.value / 2} ${viewBox.value} ${viewBox.value}`);
    const normalized = computed(() => between(props.value, props.min, props.max));
    const strokeDashOffset = computed(() => circumference * (1 - (normalized.value - props.min) / (props.max - props.min)));
    const strokeWidth = computed(() => props.thickness / 2 * viewBox.value);
    function getCircle({ thickness, offset, color, cls }) {
      return h("circle", {
        class: "q-circular-progress__" + cls + (color !== void 0 ? ` text-${color}` : ""),
        style: circleStyle.value,
        fill: "transparent",
        stroke: "currentColor",
        "stroke-width": thickness,
        "stroke-dasharray": strokeDashArray,
        "stroke-dashoffset": offset,
        cx: viewBox.value,
        cy: viewBox.value,
        r: radius
      });
    }
    return () => {
      const svgChild = [];
      props.centerColor !== void 0 && props.centerColor !== "transparent" && svgChild.push(h("circle", {
        class: `q-circular-progress__center text-${props.centerColor}`,
        fill: "currentColor",
        r: radius - strokeWidth.value / 2,
        cx: viewBox.value,
        cy: viewBox.value
      }));
      props.trackColor !== void 0 && props.trackColor !== "transparent" && svgChild.push(getCircle({
        cls: "track",
        thickness: strokeWidth.value,
        offset: 0,
        color: props.trackColor
      }));
      svgChild.push(getCircle({
        cls: "circle",
        thickness: strokeWidth.value,
        offset: strokeDashOffset.value,
        color: props.color
      }));
      const child = [
        h("svg", {
          class: "q-circular-progress__svg",
          style: svgStyle.value,
          viewBox: viewBoxAttr.value,
          "aria-hidden": "true"
        }, svgChild)
      ];
      props.showValue === true && child.push(h("div", {
        class: "q-circular-progress__text absolute-full row flex-center content-center",
        style: { fontSize: props.fontSize }
      }, slots.default !== void 0 ? slots.default() : [h("div", normalized.value)]));
      return h("div", {
        class: `q-circular-progress q-circular-progress--${props.indeterminate === true ? "in" : ""}determinate`,
        style: sizeStyle.value,
        role: "progressbar",
        "aria-valuemin": props.min,
        "aria-valuemax": props.max,
        "aria-valuenow": props.indeterminate === true ? void 0 : normalized.value
      }, hMergeSlotSafely(slots.internal, child));
    };
  }
});
const useFileEmits = ["rejected"];
const coreEmits = [
  ...useFileEmits,
  "start",
  "finish",
  "added",
  "removed"
];
const trueFn = () => true;
function getEmitsObject(emitsArray) {
  const emitsObject = {};
  emitsArray.forEach((val) => {
    emitsObject[val] = trueFn;
  });
  return emitsObject;
}
getEmitsObject(coreEmits);
let buf, bufIdx = 0;
const hexBytes = new Array(256);
for (let i = 0; i < 256; i++) {
  hexBytes[i] = (i + 256).toString(16).substring(1);
}
const randomBytes = (() => {
  const lib = typeof crypto !== "undefined" ? crypto : typeof window !== "undefined" ? window.crypto || window.msCrypto : void 0;
  if (lib !== void 0) {
    if (lib.randomBytes !== void 0) {
      return lib.randomBytes;
    }
    if (lib.getRandomValues !== void 0) {
      return (n) => {
        const bytes = new Uint8Array(n);
        lib.getRandomValues(bytes);
        return bytes;
      };
    }
  }
  return (n) => {
    const r = [];
    for (let i = n; i > 0; i--) {
      r.push(Math.floor(Math.random() * 256));
    }
    return r;
  };
})();
const BUFFER_SIZE = 4096;
function uid() {
  if (buf === void 0 || bufIdx + 16 > BUFFER_SIZE) {
    bufIdx = 0;
    buf = randomBytes(BUFFER_SIZE);
  }
  const b = Array.prototype.slice.call(buf, bufIdx, bufIdx += 16);
  b[6] = b[6] & 15 | 64;
  b[8] = b[8] & 63 | 128;
  return hexBytes[b[0]] + hexBytes[b[1]] + hexBytes[b[2]] + hexBytes[b[3]] + "-" + hexBytes[b[4]] + hexBytes[b[5]] + "-" + hexBytes[b[6]] + hexBytes[b[7]] + "-" + hexBytes[b[8]] + hexBytes[b[9]] + "-" + hexBytes[b[10]] + hexBytes[b[11]] + hexBytes[b[12]] + hexBytes[b[13]] + hexBytes[b[14]] + hexBytes[b[15]];
}
const lazyRulesValues = [true, false, "ondemand"];
const useValidateProps = {
  modelValue: {},
  error: {
    type: Boolean,
    default: null
  },
  errorMessage: String,
  noErrorIcon: Boolean,
  rules: Array,
  reactiveRules: Boolean,
  lazyRules: {
    type: [Boolean, String],
    validator: (v) => lazyRulesValues.includes(v)
  }
};
function useValidate(focused, innerLoading) {
  const { props, proxy } = getCurrentInstance();
  const innerError = ref(false);
  const innerErrorMessage = ref(null);
  const isDirtyModel = ref(null);
  useFormChild({ validate, resetValidation });
  let validateIndex = 0, unwatchRules;
  const hasRules = computed(() => props.rules !== void 0 && props.rules !== null && props.rules.length > 0);
  const hasActiveRules = computed(() => props.disable !== true && hasRules.value === true);
  const hasError = computed(() => props.error === true || innerError.value === true);
  const errorMessage = computed(() => typeof props.errorMessage === "string" && props.errorMessage.length > 0 ? props.errorMessage : innerErrorMessage.value);
  watch(() => props.modelValue, () => {
    validateIfNeeded();
  });
  watch(() => props.reactiveRules, (val) => {
    if (val === true) {
      if (unwatchRules === void 0) {
        unwatchRules = watch(() => props.rules, () => {
          validateIfNeeded(true);
        });
      }
    } else if (unwatchRules !== void 0) {
      unwatchRules();
      unwatchRules = void 0;
    }
  }, { immediate: true });
  watch(focused, (val) => {
    if (val === true) {
      if (isDirtyModel.value === null) {
        isDirtyModel.value = false;
      }
    } else if (isDirtyModel.value === false) {
      isDirtyModel.value = true;
      if (hasActiveRules.value === true && props.lazyRules !== "ondemand" && innerLoading.value === false) {
        debouncedValidate();
      }
    }
  });
  function resetValidation() {
    validateIndex++;
    innerLoading.value = false;
    isDirtyModel.value = null;
    innerError.value = false;
    innerErrorMessage.value = null;
    debouncedValidate.cancel();
  }
  function validate(val = props.modelValue) {
    if (hasActiveRules.value !== true) {
      return true;
    }
    const index = ++validateIndex;
    if (innerLoading.value !== true && props.lazyRules !== true) {
      isDirtyModel.value = true;
    }
    const update = (err, msg) => {
      if (innerError.value !== err) {
        innerError.value = err;
      }
      const m = msg || void 0;
      if (innerErrorMessage.value !== m) {
        innerErrorMessage.value = m;
      }
      innerLoading.value = false;
    };
    const promises = [];
    for (let i = 0; i < props.rules.length; i++) {
      const rule = props.rules[i];
      let res;
      if (typeof rule === "function") {
        res = rule(val);
      } else if (typeof rule === "string" && testPattern[rule] !== void 0) {
        res = testPattern[rule](val);
      }
      if (res === false || typeof res === "string") {
        update(true, res);
        return false;
      } else if (res !== true && res !== void 0) {
        promises.push(res);
      }
    }
    if (promises.length === 0) {
      update(false);
      return true;
    }
    innerLoading.value = true;
    return Promise.all(promises).then((res) => {
      if (res === void 0 || Array.isArray(res) === false || res.length === 0) {
        index === validateIndex && update(false);
        return true;
      }
      const msg = res.find((r) => r === false || typeof r === "string");
      index === validateIndex && update(msg !== void 0, msg);
      return msg === void 0;
    }, (e) => {
      if (index === validateIndex) {
        console.error(e);
        update(true);
      }
      return false;
    });
  }
  function validateIfNeeded(changedRules) {
    if (hasActiveRules.value === true && props.lazyRules !== "ondemand" && (isDirtyModel.value === true || props.lazyRules !== true && changedRules !== true)) {
      debouncedValidate();
    }
  }
  const debouncedValidate = debounce(validate, 0);
  onBeforeUnmount(() => {
    unwatchRules !== void 0 && unwatchRules();
    debouncedValidate.cancel();
  });
  Object.assign(proxy, { resetValidation, validate });
  injectProp(proxy, "hasError", () => hasError.value);
  return {
    isDirtyModel,
    hasRules,
    hasError,
    errorMessage,
    validate,
    resetValidation
  };
}
const listenerRE = /^on[A-Z]/;
function useSplitAttrs(attrs, vnode) {
  const acc = {
    listeners: ref({}),
    attributes: ref({})
  };
  function update() {
    const attributes = {};
    const listeners = {};
    for (const key in attrs) {
      if (key !== "class" && key !== "style" && listenerRE.test(key) === false) {
        attributes[key] = attrs[key];
      }
    }
    for (const key in vnode.props) {
      if (listenerRE.test(key) === true) {
        listeners[key] = vnode.props[key];
      }
    }
    acc.attributes.value = attributes;
    acc.listeners.value = listeners;
  }
  onBeforeUpdate(update);
  update();
  return acc;
}
let queue = [];
let waitFlags = [];
function addFocusFn(fn) {
  if (waitFlags.length === 0) {
    fn();
  } else {
    queue.push(fn);
  }
}
function removeFocusFn(fn) {
  queue = queue.filter((entry) => entry !== fn);
}
function getTargetUid(val) {
  return val === void 0 ? `f_${uid()}` : val;
}
function fieldValueIsFilled(val) {
  return val !== void 0 && val !== null && ("" + val).length > 0;
}
const useFieldProps = __spreadProps(__spreadValues(__spreadValues({}, useDarkProps), useValidateProps), {
  label: String,
  stackLabel: Boolean,
  hint: String,
  hideHint: Boolean,
  prefix: String,
  suffix: String,
  labelColor: String,
  color: String,
  bgColor: String,
  filled: Boolean,
  outlined: Boolean,
  borderless: Boolean,
  standout: [Boolean, String],
  square: Boolean,
  loading: Boolean,
  labelSlot: Boolean,
  bottomSlots: Boolean,
  hideBottomSpace: Boolean,
  rounded: Boolean,
  dense: Boolean,
  itemAligned: Boolean,
  counter: Boolean,
  clearable: Boolean,
  clearIcon: String,
  disable: Boolean,
  readonly: Boolean,
  autofocus: Boolean,
  for: String,
  maxlength: [Number, String]
});
const useFieldEmits = ["update:modelValue", "clear", "focus", "blur", "popup-show", "popup-hide"];
function useFieldState() {
  const { props, attrs, proxy, vnode } = getCurrentInstance();
  const isDark = useDark(props, proxy.$q);
  return {
    isDark,
    editable: computed(() => props.disable !== true && props.readonly !== true),
    innerLoading: ref(false),
    focused: ref(false),
    hasPopupOpen: false,
    splitAttrs: useSplitAttrs(attrs, vnode),
    targetUid: ref(getTargetUid(props.for)),
    rootRef: ref(null),
    targetRef: ref(null),
    controlRef: ref(null)
  };
}
function useField(state) {
  const { props, emit, slots, attrs, proxy } = getCurrentInstance();
  const { $q } = proxy;
  let focusoutTimer;
  if (state.hasValue === void 0) {
    state.hasValue = computed(() => fieldValueIsFilled(props.modelValue));
  }
  if (state.emitValue === void 0) {
    state.emitValue = (value) => {
      emit("update:modelValue", value);
    };
  }
  if (state.controlEvents === void 0) {
    state.controlEvents = {
      onFocusin: onControlFocusin,
      onFocusout: onControlFocusout
    };
  }
  Object.assign(state, {
    clearValue,
    onControlFocusin,
    onControlFocusout,
    focus
  });
  if (state.computedCounter === void 0) {
    state.computedCounter = computed(() => {
      if (props.counter !== false) {
        const len = typeof props.modelValue === "string" || typeof props.modelValue === "number" ? ("" + props.modelValue).length : Array.isArray(props.modelValue) === true ? props.modelValue.length : 0;
        const max = props.maxlength !== void 0 ? props.maxlength : props.maxValues;
        return len + (max !== void 0 ? " / " + max : "");
      }
    });
  }
  const {
    isDirtyModel,
    hasRules,
    hasError,
    errorMessage,
    resetValidation
  } = useValidate(state.focused, state.innerLoading);
  const floatingLabel = state.floatingLabel !== void 0 ? computed(() => props.stackLabel === true || state.focused.value === true || state.floatingLabel.value === true) : computed(() => props.stackLabel === true || state.focused.value === true || state.hasValue.value === true);
  const shouldRenderBottom = computed(() => props.bottomSlots === true || props.hint !== void 0 || hasRules.value === true || props.counter === true || props.error !== null);
  const styleType = computed(() => {
    if (props.filled === true) {
      return "filled";
    }
    if (props.outlined === true) {
      return "outlined";
    }
    if (props.borderless === true) {
      return "borderless";
    }
    if (props.standout) {
      return "standout";
    }
    return "standard";
  });
  const classes = computed(() => `q-field row no-wrap items-start q-field--${styleType.value}` + (state.fieldClass !== void 0 ? ` ${state.fieldClass.value}` : "") + (props.rounded === true ? " q-field--rounded" : "") + (props.square === true ? " q-field--square" : "") + (floatingLabel.value === true ? " q-field--float" : "") + (hasLabel.value === true ? " q-field--labeled" : "") + (props.dense === true ? " q-field--dense" : "") + (props.itemAligned === true ? " q-field--item-aligned q-item-type" : "") + (state.isDark.value === true ? " q-field--dark" : "") + (state.getControl === void 0 ? " q-field--auto-height" : "") + (state.focused.value === true ? " q-field--focused" : "") + (hasError.value === true ? " q-field--error" : "") + (hasError.value === true || state.focused.value === true ? " q-field--highlighted" : "") + (props.hideBottomSpace !== true && shouldRenderBottom.value === true ? " q-field--with-bottom" : "") + (props.disable === true ? " q-field--disabled" : props.readonly === true ? " q-field--readonly" : ""));
  const contentClass = computed(() => "q-field__control relative-position row no-wrap" + (props.bgColor !== void 0 ? ` bg-${props.bgColor}` : "") + (hasError.value === true ? " text-negative" : typeof props.standout === "string" && props.standout.length > 0 && state.focused.value === true ? ` ${props.standout}` : props.color !== void 0 ? ` text-${props.color}` : ""));
  const hasLabel = computed(() => props.labelSlot === true || props.label !== void 0);
  const labelClass = computed(() => "q-field__label no-pointer-events absolute ellipsis" + (props.labelColor !== void 0 && hasError.value !== true ? ` text-${props.labelColor}` : ""));
  const controlSlotScope = computed(() => ({
    id: state.targetUid.value,
    editable: state.editable.value,
    focused: state.focused.value,
    floatingLabel: floatingLabel.value,
    modelValue: props.modelValue,
    emitValue: state.emitValue
  }));
  const attributes = computed(() => {
    const acc = {
      for: state.targetUid.value
    };
    if (props.disable === true) {
      acc["aria-disabled"] = "true";
    } else if (props.readonly === true) {
      acc["aria-readonly"] = "true";
    }
    return acc;
  });
  watch(() => props.for, (val) => {
    state.targetUid.value = getTargetUid(val);
  });
  function focusHandler() {
    const el = document.activeElement;
    let target = state.targetRef !== void 0 && state.targetRef.value;
    if (target && (el === null || el.id !== state.targetUid.value)) {
      target.hasAttribute("tabindex") === true || (target = target.querySelector("[tabindex]"));
      if (target && target !== el) {
        target.focus({ preventScroll: true });
      }
    }
  }
  function focus() {
    addFocusFn(focusHandler);
  }
  function blur() {
    removeFocusFn(focusHandler);
    const el = document.activeElement;
    if (el !== null && state.rootRef.value.contains(el)) {
      el.blur();
    }
  }
  function onControlFocusin(e) {
    clearTimeout(focusoutTimer);
    if (state.editable.value === true && state.focused.value === false) {
      state.focused.value = true;
      emit("focus", e);
    }
  }
  function onControlFocusout(e, then) {
    clearTimeout(focusoutTimer);
    focusoutTimer = setTimeout(() => {
      if (document.hasFocus() === true && (state.hasPopupOpen === true || state.controlRef === void 0 || state.controlRef.value === null || state.controlRef.value.contains(document.activeElement) !== false)) {
        return;
      }
      if (state.focused.value === true) {
        state.focused.value = false;
        emit("blur", e);
      }
      then !== void 0 && then();
    });
  }
  function clearValue(e) {
    stopAndPrevent(e);
    if ($q.platform.is.mobile !== true) {
      const el = state.targetRef !== void 0 && state.targetRef.value || state.rootRef.value;
      el.focus();
    } else if (state.rootRef.value.contains(document.activeElement) === true) {
      document.activeElement.blur();
    }
    if (props.type === "file") {
      state.inputRef.value.value = null;
    }
    emit("update:modelValue", null);
    emit("clear", props.modelValue);
    nextTick(() => {
      resetValidation();
      if ($q.platform.is.mobile !== true) {
        isDirtyModel.value = false;
      }
    });
  }
  function getContent() {
    const node = [];
    slots.prepend !== void 0 && node.push(h("div", {
      class: "q-field__prepend q-field__marginal row no-wrap items-center",
      key: "prepend",
      onClick: prevent
    }, slots.prepend()));
    node.push(h("div", {
      class: "q-field__control-container col relative-position row no-wrap q-anchor--skip"
    }, getControlContainer()));
    hasError.value === true && props.noErrorIcon === false && node.push(getInnerAppendNode("error", [
      h(QIcon, { name: $q.iconSet.field.error, color: "negative" })
    ]));
    if (props.loading === true || state.innerLoading.value === true) {
      node.push(getInnerAppendNode("inner-loading-append", slots.loading !== void 0 ? slots.loading() : [h(QSpinner, { color: props.color })]));
    } else if (props.clearable === true && state.hasValue.value === true && state.editable.value === true) {
      node.push(getInnerAppendNode("inner-clearable-append", [
        h(QIcon, {
          class: "q-field__focusable-action",
          tag: "button",
          name: props.clearIcon || $q.iconSet.field.clear,
          tabindex: 0,
          type: "button",
          "aria-hidden": null,
          role: null,
          onClick: clearValue
        })
      ]));
    }
    slots.append !== void 0 && node.push(h("div", {
      class: "q-field__append q-field__marginal row no-wrap items-center",
      key: "append",
      onClick: prevent
    }, slots.append()));
    state.getInnerAppend !== void 0 && node.push(getInnerAppendNode("inner-append", state.getInnerAppend()));
    state.getControlChild !== void 0 && node.push(state.getControlChild());
    return node;
  }
  function getControlContainer() {
    const node = [];
    props.prefix !== void 0 && props.prefix !== null && node.push(h("div", {
      class: "q-field__prefix no-pointer-events row items-center"
    }, props.prefix));
    if (state.getShadowControl !== void 0 && state.hasShadow.value === true) {
      node.push(state.getShadowControl());
    }
    if (state.getControl !== void 0) {
      node.push(state.getControl());
    } else if (slots.rawControl !== void 0) {
      node.push(slots.rawControl());
    } else if (slots.control !== void 0) {
      node.push(h("div", __spreadProps(__spreadValues({
        ref: state.targetRef,
        class: "q-field__native row"
      }, state.splitAttrs.attributes.value), {
        "data-autofocus": props.autofocus === true || void 0
      }), slots.control(controlSlotScope.value)));
    }
    hasLabel.value === true && node.push(h("div", {
      class: labelClass.value
    }, hSlot(slots.label, props.label)));
    props.suffix !== void 0 && props.suffix !== null && node.push(h("div", {
      class: "q-field__suffix no-pointer-events row items-center"
    }, props.suffix));
    return node.concat(hSlot(slots.default));
  }
  function getBottom() {
    let msg, key;
    if (hasError.value === true) {
      if (errorMessage.value !== null) {
        msg = [h("div", { role: "alert" }, errorMessage.value)];
        key = `q--slot-error-${errorMessage.value}`;
      } else {
        msg = hSlot(slots.error);
        key = "q--slot-error";
      }
    } else if (props.hideHint !== true || state.focused.value === true) {
      if (props.hint !== void 0) {
        msg = [h("div", props.hint)];
        key = `q--slot-hint-${props.hint}`;
      } else {
        msg = hSlot(slots.hint);
        key = "q--slot-hint";
      }
    }
    const hasCounter = props.counter === true || slots.counter !== void 0;
    if (props.hideBottomSpace === true && hasCounter === false && msg === void 0) {
      return;
    }
    const main = h("div", {
      key,
      class: "q-field__messages col"
    }, msg);
    return h("div", {
      class: "q-field__bottom row items-start q-field__bottom--" + (props.hideBottomSpace !== true ? "animated" : "stale")
    }, [
      props.hideBottomSpace === true ? main : h(Transition, { name: "q-transition--field-message" }, () => main),
      hasCounter === true ? h("div", {
        class: "q-field__counter"
      }, slots.counter !== void 0 ? slots.counter() : state.computedCounter.value) : null
    ]);
  }
  function getInnerAppendNode(key, content) {
    return content === null ? null : h("div", {
      key,
      class: "q-field__append q-field__marginal row no-wrap items-center q-anchor--skip"
    }, content);
  }
  Object.assign(proxy, { focus, blur });
  let shouldActivate = false;
  onDeactivated(() => {
    shouldActivate = true;
  });
  onActivated(() => {
    shouldActivate === true && props.autofocus === true && proxy.focus();
  });
  onMounted(() => {
    if (isRuntimeSsrPreHydration.value === true && props.for === void 0) {
      state.targetUid.value = getTargetUid();
    }
    props.autofocus === true && proxy.focus();
  });
  onBeforeUnmount(() => {
    clearTimeout(focusoutTimer);
  });
  return function renderField() {
    const labelAttrs = state.getControl === void 0 && slots.control === void 0 ? __spreadValues(__spreadProps(__spreadValues({}, state.splitAttrs.attributes.value), {
      "data-autofocus": props.autofocus
    }), attributes.value) : attributes.value;
    return h("label", __spreadValues({
      ref: state.rootRef,
      class: [
        classes.value,
        attrs.class
      ],
      style: attrs.style
    }, labelAttrs), [
      slots.before !== void 0 ? h("div", {
        class: "q-field__before q-field__marginal row no-wrap items-center",
        onClick: prevent
      }, slots.before()) : null,
      h("div", {
        class: "q-field__inner relative-position col self-stretch"
      }, [
        h("div", __spreadValues({
          ref: state.controlRef,
          class: contentClass.value,
          tabindex: -1
        }, state.controlEvents), getContent()),
        shouldRenderBottom.value === true ? getBottom() : null
      ]),
      slots.after !== void 0 ? h("div", {
        class: "q-field__after q-field__marginal row no-wrap items-center",
        onClick: prevent
      }, slots.after()) : null
    ]);
  };
}
const NAMED_MASKS = {
  date: "####/##/##",
  datetime: "####/##/## ##:##",
  time: "##:##",
  fulltime: "##:##:##",
  phone: "(###) ### - ####",
  card: "#### #### #### ####"
};
const TOKENS = {
  "#": { pattern: "[\\d]", negate: "[^\\d]" },
  S: { pattern: "[a-zA-Z]", negate: "[^a-zA-Z]" },
  N: { pattern: "[0-9a-zA-Z]", negate: "[^0-9a-zA-Z]" },
  A: { pattern: "[a-zA-Z]", negate: "[^a-zA-Z]", transform: (v) => v.toLocaleUpperCase() },
  a: { pattern: "[a-zA-Z]", negate: "[^a-zA-Z]", transform: (v) => v.toLocaleLowerCase() },
  X: { pattern: "[0-9a-zA-Z]", negate: "[^0-9a-zA-Z]", transform: (v) => v.toLocaleUpperCase() },
  x: { pattern: "[0-9a-zA-Z]", negate: "[^0-9a-zA-Z]", transform: (v) => v.toLocaleLowerCase() }
};
const KEYS = Object.keys(TOKENS);
KEYS.forEach((key) => {
  TOKENS[key].regex = new RegExp(TOKENS[key].pattern);
});
const tokenRegexMask = new RegExp("\\\\([^.*+?^${}()|([\\]])|([.*+?^${}()|[\\]])|([" + KEYS.join("") + "])|(.)", "g"), escRegex = /[.*+?^${}()|[\]\\]/g;
const MARKER = String.fromCharCode(1);
const useMaskProps = {
  mask: String,
  reverseFillMask: Boolean,
  fillMask: [Boolean, String],
  unmaskedValue: Boolean
};
function useMask(props, emit, emitValue, inputRef) {
  let maskMarked, maskReplaced, computedMask, computedUnmask;
  const hasMask = ref(null);
  const innerValue = ref(getInitialMaskedValue());
  function getIsTypeText() {
    return props.autogrow === true || ["textarea", "text", "search", "url", "tel", "password"].includes(props.type);
  }
  watch(() => props.type + props.autogrow, updateMaskInternals);
  watch(() => props.mask, (v) => {
    if (v !== void 0) {
      updateMaskValue(innerValue.value, true);
    } else {
      const val = unmaskValue(innerValue.value);
      updateMaskInternals();
      props.modelValue !== val && emit("update:modelValue", val);
    }
  });
  watch(() => props.fillMask + props.reverseFillMask, () => {
    hasMask.value === true && updateMaskValue(innerValue.value, true);
  });
  watch(() => props.unmaskedValue, () => {
    hasMask.value === true && updateMaskValue(innerValue.value);
  });
  function getInitialMaskedValue() {
    updateMaskInternals();
    if (hasMask.value === true) {
      const masked = maskValue(unmaskValue(props.modelValue));
      return props.fillMask !== false ? fillWithMask(masked) : masked;
    }
    return props.modelValue;
  }
  function getPaddedMaskMarked(size) {
    if (size < maskMarked.length) {
      return maskMarked.slice(-size);
    }
    let pad = "", localMaskMarked = maskMarked;
    const padPos = localMaskMarked.indexOf(MARKER);
    if (padPos > -1) {
      for (let i = size - localMaskMarked.length; i > 0; i--) {
        pad += MARKER;
      }
      localMaskMarked = localMaskMarked.slice(0, padPos) + pad + localMaskMarked.slice(padPos);
    }
    return localMaskMarked;
  }
  function updateMaskInternals() {
    hasMask.value = props.mask !== void 0 && props.mask.length > 0 && getIsTypeText();
    if (hasMask.value === false) {
      computedUnmask = void 0;
      maskMarked = "";
      maskReplaced = "";
      return;
    }
    const localComputedMask = NAMED_MASKS[props.mask] === void 0 ? props.mask : NAMED_MASKS[props.mask], fillChar = typeof props.fillMask === "string" && props.fillMask.length > 0 ? props.fillMask.slice(0, 1) : "_", fillCharEscaped = fillChar.replace(escRegex, "\\$&"), unmask = [], extract = [], mask = [];
    let firstMatch = props.reverseFillMask === true, unmaskChar = "", negateChar = "";
    localComputedMask.replace(tokenRegexMask, (_, char1, esc, token, char2) => {
      if (token !== void 0) {
        const c = TOKENS[token];
        mask.push(c);
        negateChar = c.negate;
        if (firstMatch === true) {
          extract.push("(?:" + negateChar + "+)?(" + c.pattern + "+)?(?:" + negateChar + "+)?(" + c.pattern + "+)?");
          firstMatch = false;
        }
        extract.push("(?:" + negateChar + "+)?(" + c.pattern + ")?");
      } else if (esc !== void 0) {
        unmaskChar = "\\" + (esc === "\\" ? "" : esc);
        mask.push(esc);
        unmask.push("([^" + unmaskChar + "]+)?" + unmaskChar + "?");
      } else {
        const c = char1 !== void 0 ? char1 : char2;
        unmaskChar = c === "\\" ? "\\\\\\\\" : c.replace(escRegex, "\\\\$&");
        mask.push(c);
        unmask.push("([^" + unmaskChar + "]+)?" + unmaskChar + "?");
      }
    });
    const unmaskMatcher = new RegExp("^" + unmask.join("") + "(" + (unmaskChar === "" ? "." : "[^" + unmaskChar + "]") + "+)?$"), extractLast = extract.length - 1, extractMatcher = extract.map((re, index) => {
      if (index === 0 && props.reverseFillMask === true) {
        return new RegExp("^" + fillCharEscaped + "*" + re);
      } else if (index === extractLast) {
        return new RegExp("^" + re + "(" + (negateChar === "" ? "." : negateChar) + "+)?" + (props.reverseFillMask === true ? "$" : fillCharEscaped + "*"));
      }
      return new RegExp("^" + re);
    });
    computedMask = mask;
    computedUnmask = (val) => {
      const unmaskMatch = unmaskMatcher.exec(val);
      if (unmaskMatch !== null) {
        val = unmaskMatch.slice(1).join("");
      }
      const extractMatch = [], extractMatcherLength = extractMatcher.length;
      for (let i = 0, str = val; i < extractMatcherLength; i++) {
        const m = extractMatcher[i].exec(str);
        if (m === null) {
          break;
        }
        str = str.slice(m.shift().length);
        extractMatch.push(...m);
      }
      if (extractMatch.length > 0) {
        return extractMatch.join("");
      }
      return val;
    };
    maskMarked = mask.map((v) => typeof v === "string" ? v : MARKER).join("");
    maskReplaced = maskMarked.split(MARKER).join(fillChar);
  }
  function updateMaskValue(rawVal, updateMaskInternalsFlag, inputType) {
    const inp = inputRef.value, end = inp.selectionEnd, endReverse = inp.value.length - end, unmasked = unmaskValue(rawVal);
    updateMaskInternalsFlag === true && updateMaskInternals();
    const preMasked = maskValue(unmasked), masked = props.fillMask !== false ? fillWithMask(preMasked) : preMasked, changed = innerValue.value !== masked;
    inp.value !== masked && (inp.value = masked);
    changed === true && (innerValue.value = masked);
    document.activeElement === inp && nextTick(() => {
      if (masked === maskReplaced) {
        const cursor = props.reverseFillMask === true ? maskReplaced.length : 0;
        inp.setSelectionRange(cursor, cursor, "forward");
        return;
      }
      if (inputType === "insertFromPaste" && props.reverseFillMask !== true) {
        const cursor = end - 1;
        moveCursor.right(inp, cursor, cursor);
        return;
      }
      if (["deleteContentBackward", "deleteContentForward"].indexOf(inputType) > -1) {
        const cursor = props.reverseFillMask === true ? end === 0 ? masked.length > preMasked.length ? 1 : 0 : Math.max(0, masked.length - (masked === maskReplaced ? 0 : Math.min(preMasked.length, endReverse) + 1)) + 1 : end;
        inp.setSelectionRange(cursor, cursor, "forward");
        return;
      }
      if (props.reverseFillMask === true) {
        if (changed === true) {
          const cursor = Math.max(0, masked.length - (masked === maskReplaced ? 0 : Math.min(preMasked.length, endReverse + 1)));
          if (cursor === 1 && end === 1) {
            inp.setSelectionRange(cursor, cursor, "forward");
          } else {
            moveCursor.rightReverse(inp, cursor, cursor);
          }
        } else {
          const cursor = masked.length - endReverse;
          inp.setSelectionRange(cursor, cursor, "backward");
        }
      } else {
        if (changed === true) {
          const cursor = Math.max(0, maskMarked.indexOf(MARKER), Math.min(preMasked.length, end) - 1);
          moveCursor.right(inp, cursor, cursor);
        } else {
          const cursor = end - 1;
          moveCursor.right(inp, cursor, cursor);
        }
      }
    });
    const val = props.unmaskedValue === true ? unmaskValue(masked) : masked;
    String(props.modelValue) !== val && emitValue(val, true);
  }
  function moveCursorForPaste(inp, start, end) {
    const preMasked = maskValue(unmaskValue(inp.value));
    start = Math.max(0, maskMarked.indexOf(MARKER), Math.min(preMasked.length, start));
    inp.setSelectionRange(start, end, "forward");
  }
  const moveCursor = {
    left(inp, start, end, selection) {
      const noMarkBefore = maskMarked.slice(start - 1).indexOf(MARKER) === -1;
      let i = Math.max(0, start - 1);
      for (; i >= 0; i--) {
        if (maskMarked[i] === MARKER) {
          start = i;
          noMarkBefore === true && start++;
          break;
        }
      }
      if (i < 0 && maskMarked[start] !== void 0 && maskMarked[start] !== MARKER) {
        return moveCursor.right(inp, 0, 0);
      }
      start >= 0 && inp.setSelectionRange(start, selection === true ? end : start, "backward");
    },
    right(inp, start, end, selection) {
      const limit = inp.value.length;
      let i = Math.min(limit, end + 1);
      for (; i <= limit; i++) {
        if (maskMarked[i] === MARKER) {
          end = i;
          break;
        } else if (maskMarked[i - 1] === MARKER) {
          end = i;
        }
      }
      if (i > limit && maskMarked[end - 1] !== void 0 && maskMarked[end - 1] !== MARKER) {
        return moveCursor.left(inp, limit, limit);
      }
      inp.setSelectionRange(selection ? start : end, end, "forward");
    },
    leftReverse(inp, start, end, selection) {
      const localMaskMarked = getPaddedMaskMarked(inp.value.length);
      let i = Math.max(0, start - 1);
      for (; i >= 0; i--) {
        if (localMaskMarked[i - 1] === MARKER) {
          start = i;
          break;
        } else if (localMaskMarked[i] === MARKER) {
          start = i;
          if (i === 0) {
            break;
          }
        }
      }
      if (i < 0 && localMaskMarked[start] !== void 0 && localMaskMarked[start] !== MARKER) {
        return moveCursor.rightReverse(inp, 0, 0);
      }
      start >= 0 && inp.setSelectionRange(start, selection === true ? end : start, "backward");
    },
    rightReverse(inp, start, end, selection) {
      const limit = inp.value.length, localMaskMarked = getPaddedMaskMarked(limit), noMarkBefore = localMaskMarked.slice(0, end + 1).indexOf(MARKER) === -1;
      let i = Math.min(limit, end + 1);
      for (; i <= limit; i++) {
        if (localMaskMarked[i - 1] === MARKER) {
          end = i;
          end > 0 && noMarkBefore === true && end--;
          break;
        }
      }
      if (i > limit && localMaskMarked[end - 1] !== void 0 && localMaskMarked[end - 1] !== MARKER) {
        return moveCursor.leftReverse(inp, limit, limit);
      }
      inp.setSelectionRange(selection === true ? start : end, end, "forward");
    }
  };
  function onMaskedKeydown(e) {
    if (shouldIgnoreKey(e) === true) {
      return;
    }
    const inp = inputRef.value, start = inp.selectionStart, end = inp.selectionEnd;
    if (e.keyCode === 37 || e.keyCode === 39) {
      const fn = moveCursor[(e.keyCode === 39 ? "right" : "left") + (props.reverseFillMask === true ? "Reverse" : "")];
      e.preventDefault();
      fn(inp, start, end, e.shiftKey);
    } else if (e.keyCode === 8 && props.reverseFillMask !== true && start === end) {
      moveCursor.left(inp, start, end, true);
    } else if (e.keyCode === 46 && props.reverseFillMask === true && start === end) {
      moveCursor.rightReverse(inp, start, end, true);
    }
  }
  function maskValue(val) {
    if (val === void 0 || val === null || val === "") {
      return "";
    }
    if (props.reverseFillMask === true) {
      return maskValueReverse(val);
    }
    const mask = computedMask;
    let valIndex = 0, output = "";
    for (let maskIndex = 0; maskIndex < mask.length; maskIndex++) {
      const valChar = val[valIndex], maskDef = mask[maskIndex];
      if (typeof maskDef === "string") {
        output += maskDef;
        valChar === maskDef && valIndex++;
      } else if (valChar !== void 0 && maskDef.regex.test(valChar)) {
        output += maskDef.transform !== void 0 ? maskDef.transform(valChar) : valChar;
        valIndex++;
      } else {
        return output;
      }
    }
    return output;
  }
  function maskValueReverse(val) {
    const mask = computedMask, firstTokenIndex = maskMarked.indexOf(MARKER);
    let valIndex = val.length - 1, output = "";
    for (let maskIndex = mask.length - 1; maskIndex >= 0 && valIndex > -1; maskIndex--) {
      const maskDef = mask[maskIndex];
      let valChar = val[valIndex];
      if (typeof maskDef === "string") {
        output = maskDef + output;
        valChar === maskDef && valIndex--;
      } else if (valChar !== void 0 && maskDef.regex.test(valChar)) {
        do {
          output = (maskDef.transform !== void 0 ? maskDef.transform(valChar) : valChar) + output;
          valIndex--;
          valChar = val[valIndex];
        } while (firstTokenIndex === maskIndex && valChar !== void 0 && maskDef.regex.test(valChar));
      } else {
        return output;
      }
    }
    return output;
  }
  function unmaskValue(val) {
    return typeof val !== "string" || computedUnmask === void 0 ? typeof val === "number" ? computedUnmask("" + val) : val : computedUnmask(val);
  }
  function fillWithMask(val) {
    if (maskReplaced.length - val.length <= 0) {
      return val;
    }
    return props.reverseFillMask === true && val.length > 0 ? maskReplaced.slice(0, -val.length) + val : val + maskReplaced.slice(val.length);
  }
  return {
    innerValue,
    hasMask,
    moveCursorForPaste,
    updateMaskValue,
    onMaskedKeydown
  };
}
function useFileFormDomProps(props, typeGuard) {
  function getFormDomProps() {
    const model = props.modelValue;
    try {
      const dt = "DataTransfer" in window ? new DataTransfer() : "ClipboardEvent" in window ? new ClipboardEvent("").clipboardData : void 0;
      if (Object(model) === model) {
        ("length" in model ? Array.from(model) : [model]).forEach((file) => {
          dt.items.add(file);
        });
      }
      return {
        files: dt.files
      };
    } catch (e) {
      return {
        files: void 0
      };
    }
  }
  return typeGuard === true ? computed(() => {
    if (props.type !== "file") {
      return;
    }
    return getFormDomProps();
  }) : computed(getFormDomProps);
}
const isJapanese = /[\u3000-\u303f\u3040-\u309f\u30a0-\u30ff\uff00-\uff9f\u4e00-\u9faf\u3400-\u4dbf]/;
const isChinese = /[\u4e00-\u9fff\u3400-\u4dbf\u{20000}-\u{2a6df}\u{2a700}-\u{2b73f}\u{2b740}-\u{2b81f}\u{2b820}-\u{2ceaf}\uf900-\ufaff\u3300-\u33ff\ufe30-\ufe4f\uf900-\ufaff\u{2f800}-\u{2fa1f}]/u;
const isKorean = /[\u3131-\u314e\u314f-\u3163\uac00-\ud7a3]/;
function useKeyComposition(onInput) {
  return function onComposition(e) {
    if (e.type === "compositionend" || e.type === "change") {
      if (e.target.composing !== true) {
        return;
      }
      e.target.composing = false;
      onInput(e);
    } else if (e.type === "compositionupdate") {
      if (typeof e.data === "string" && isJapanese.test(e.data) === false && isChinese.test(e.data) === false && isKorean.test(e.data) === false) {
        e.target.composing = false;
      }
    } else {
      e.target.composing = true;
    }
  };
}
var QInput = createComponent({
  name: "QInput",
  inheritAttrs: false,
  props: __spreadProps(__spreadValues(__spreadValues(__spreadValues({}, useFieldProps), useMaskProps), useFormProps), {
    modelValue: { required: false },
    shadowText: String,
    type: {
      type: String,
      default: "text"
    },
    debounce: [String, Number],
    autogrow: Boolean,
    inputClass: [Array, String, Object],
    inputStyle: [Array, String, Object]
  }),
  emits: [
    ...useFieldEmits,
    "paste",
    "change"
  ],
  setup(props, { emit, attrs }) {
    const temp = {};
    let emitCachedValue = NaN, typedNumber, stopValueWatcher, emitTimer, emitValueFn;
    const inputRef = ref(null);
    const nameProp = useFormInputNameAttr(props);
    const {
      innerValue,
      hasMask,
      moveCursorForPaste,
      updateMaskValue,
      onMaskedKeydown
    } = useMask(props, emit, emitValue, inputRef);
    const formDomProps = useFileFormDomProps(props, true);
    const hasValue = computed(() => fieldValueIsFilled(innerValue.value));
    const onComposition = useKeyComposition(onInput);
    const state = useFieldState();
    const isTextarea = computed(() => props.type === "textarea" || props.autogrow === true);
    const isTypeText = computed(() => isTextarea.value === true || ["text", "search", "url", "tel", "password"].includes(props.type));
    const onEvents = computed(() => {
      const evt = __spreadProps(__spreadValues({}, state.splitAttrs.listeners.value), {
        onInput,
        onPaste,
        onChange,
        onBlur: onFinishEditing,
        onFocus: stop
      });
      evt.onCompositionstart = evt.onCompositionupdate = evt.onCompositionend = onComposition;
      if (hasMask.value === true) {
        evt.onKeydown = onMaskedKeydown;
      }
      if (props.autogrow === true) {
        evt.onAnimationend = adjustHeight;
      }
      return evt;
    });
    const inputAttrs = computed(() => {
      const attrs2 = __spreadProps(__spreadValues({
        tabindex: 0,
        "data-autofocus": props.autofocus === true || void 0,
        rows: props.type === "textarea" ? 6 : void 0,
        "aria-label": props.label,
        name: nameProp.value
      }, state.splitAttrs.attributes.value), {
        id: state.targetUid.value,
        maxlength: props.maxlength,
        disabled: props.disable === true,
        readonly: props.readonly === true
      });
      if (isTextarea.value === false) {
        attrs2.type = props.type;
      }
      if (props.autogrow === true) {
        attrs2.rows = 1;
      }
      return attrs2;
    });
    watch(() => props.type, () => {
      if (inputRef.value) {
        inputRef.value.value = props.modelValue;
      }
    });
    watch(() => props.modelValue, (v) => {
      if (hasMask.value === true) {
        if (stopValueWatcher === true) {
          stopValueWatcher = false;
          if (String(v) === emitCachedValue) {
            return;
          }
        }
        updateMaskValue(v);
      } else if (innerValue.value !== v) {
        innerValue.value = v;
        if (props.type === "number" && temp.hasOwnProperty("value") === true) {
          if (typedNumber === true) {
            typedNumber = false;
          } else {
            delete temp.value;
          }
        }
      }
      props.autogrow === true && nextTick(adjustHeight);
    });
    watch(() => props.autogrow, (val) => {
      if (val === true) {
        nextTick(adjustHeight);
      } else if (inputRef.value !== null && attrs.rows > 0) {
        inputRef.value.style.height = "auto";
      }
    });
    watch(() => props.dense, () => {
      props.autogrow === true && nextTick(adjustHeight);
    });
    function focus() {
      addFocusFn(() => {
        const el = document.activeElement;
        if (inputRef.value !== null && inputRef.value !== el && (el === null || el.id !== state.targetUid.value)) {
          inputRef.value.focus({ preventScroll: true });
        }
      });
    }
    function select() {
      inputRef.value !== null && inputRef.value.select();
    }
    function onPaste(e) {
      if (hasMask.value === true && props.reverseFillMask !== true) {
        const inp = e.target;
        moveCursorForPaste(inp, inp.selectionStart, inp.selectionEnd);
      }
      emit("paste", e);
    }
    function onInput(e) {
      if (!e || !e.target || e.target.composing === true) {
        return;
      }
      if (props.type === "file") {
        emit("update:modelValue", e.target.files);
        return;
      }
      const val = e.target.value;
      if (hasMask.value === true) {
        updateMaskValue(val, false, e.inputType);
      } else {
        emitValue(val);
        if (isTypeText.value === true && e.target === document.activeElement) {
          const { selectionStart, selectionEnd } = e.target;
          if (selectionStart !== void 0 && selectionEnd !== void 0) {
            nextTick(() => {
              if (e.target === document.activeElement && val.indexOf(e.target.value) === 0) {
                e.target.setSelectionRange(selectionStart, selectionEnd);
              }
            });
          }
        }
      }
      props.autogrow === true && adjustHeight();
    }
    function emitValue(val, stopWatcher) {
      emitValueFn = () => {
        if (props.type !== "number" && temp.hasOwnProperty("value") === true) {
          delete temp.value;
        }
        if (props.modelValue !== val && emitCachedValue !== val) {
          stopWatcher === true && (stopValueWatcher = true);
          emit("update:modelValue", val);
          nextTick(() => {
            emitCachedValue === val && (emitCachedValue = NaN);
          });
        }
        emitValueFn = void 0;
      };
      if (props.type === "number") {
        typedNumber = true;
        temp.value = val;
      }
      if (props.debounce !== void 0) {
        clearTimeout(emitTimer);
        temp.value = val;
        emitTimer = setTimeout(emitValueFn, props.debounce);
      } else {
        emitValueFn();
      }
    }
    function adjustHeight() {
      const inp = inputRef.value;
      if (inp !== null) {
        const parentStyle = inp.parentNode.style;
        parentStyle.marginBottom = inp.scrollHeight - 1 + "px";
        inp.style.height = "1px";
        inp.style.height = inp.scrollHeight + "px";
        parentStyle.marginBottom = "";
      }
    }
    function onChange(e) {
      onComposition(e);
      clearTimeout(emitTimer);
      emitValueFn !== void 0 && emitValueFn();
      emit("change", e.target.value);
    }
    function onFinishEditing(e) {
      e !== void 0 && stop(e);
      clearTimeout(emitTimer);
      emitValueFn !== void 0 && emitValueFn();
      typedNumber = false;
      stopValueWatcher = false;
      delete temp.value;
      props.type !== "file" && setTimeout(() => {
        if (inputRef.value !== null) {
          inputRef.value.value = innerValue.value !== void 0 ? innerValue.value : "";
        }
      });
    }
    function getCurValue() {
      return temp.hasOwnProperty("value") === true ? temp.value : innerValue.value !== void 0 ? innerValue.value : "";
    }
    onBeforeUnmount(() => {
      onFinishEditing();
    });
    onMounted(() => {
      props.autogrow === true && adjustHeight();
    });
    Object.assign(state, {
      innerValue,
      fieldClass: computed(() => `q-${isTextarea.value === true ? "textarea" : "input"}` + (props.autogrow === true ? " q-textarea--autogrow" : "")),
      hasShadow: computed(() => props.type !== "file" && typeof props.shadowText === "string" && props.shadowText.length > 0),
      inputRef,
      emitValue,
      hasValue,
      floatingLabel: computed(() => hasValue.value === true || fieldValueIsFilled(props.displayValue)),
      getControl: () => {
        return h(isTextarea.value === true ? "textarea" : "input", __spreadValues(__spreadValues(__spreadValues({
          ref: inputRef,
          class: [
            "q-field__native q-placeholder",
            props.inputClass
          ],
          style: props.inputStyle
        }, inputAttrs.value), onEvents.value), props.type !== "file" ? { value: getCurValue() } : formDomProps.value));
      },
      getShadowControl: () => {
        return h("div", {
          class: "q-field__native q-field__shadow absolute-bottom no-pointer-events" + (isTextarea.value === true ? "" : " text-no-wrap")
        }, [
          h("span", { class: "invisible" }, getCurValue()),
          h("span", props.shadowText)
        ]);
      }
    });
    const renderFn = useField(state);
    const vm = getCurrentInstance();
    Object.assign(vm.proxy, {
      focus,
      select,
      getNativeElement: () => inputRef.value
    });
    return renderFn;
  }
});
const _sfc_main = defineComponent({
  name: "IndexPage",
  data() {
    return {
      item: new CreateStoreForm(),
      list: Array()
    };
  },
  methods: {
    createStore() {
      StoreService.create({ body: this.item }).then(() => {
        this.item = new CreateStoreForm();
      });
    },
    loadStores() {
      StoreService.getList().then((result) => {
        this.list.length = 0;
        this.list.push(...result);
      });
    },
    loadEnvs() {
      console.log(process.env);
    }
  }
});
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createBlock(QPage, { class: "row items-center justify-evenly" }, {
    default: withCtx(() => [
      createVNode(QInput, {
        modelValue: _ctx.item.name,
        "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.item.name = $event),
        label: "Name: "
      }, null, 8, ["modelValue"]),
      createVNode(QBtn, {
        onClick: _ctx.createStore,
        label: "Create"
      }, null, 8, ["onClick"]),
      createVNode(QBtn, {
        onClick: _ctx.loadStores,
        label: "Load"
      }, null, 8, ["onClick"]),
      createBaseVNode("ul", null, [
        (openBlock(true), createElementBlock(Fragment, null, renderList(_ctx.list, (itm) => {
          return openBlock(), createElementBlock("li", {
            key: itm.id
          }, toDisplayString(itm.name), 1);
        }), 128))
      ])
    ]),
    _: 1
  });
}
var IndexPage = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "IndexPage.vue"]]);
export { IndexPage as default };
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSW5kZXhQYWdlLmIyNTFkNTllLmpzIiwic291cmNlcyI6WyIuLi8uLi8uLi9ub2RlX21vZHVsZXMvcXVhc2FyL3NyYy9jb21wb3NhYmxlcy91c2UtZm9ybS1jaGlsZC5qcyIsIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9xdWFzYXIvc3JjL3V0aWxzL3BhdHRlcm5zLmpzIiwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3F1YXNhci9zcmMvdXRpbHMvZXh0ZW5kLmpzIiwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3F1YXNhci9zcmMvY29tcG9uZW50cy9jaXJjdWxhci1wcm9ncmVzcy91c2UtY2lyY3VsYXItcHJvZ3Jlc3MuanMiLCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvcXVhc2FyL3NyYy9jb21wb25lbnRzL2NpcmN1bGFyLXByb2dyZXNzL1FDaXJjdWxhclByb2dyZXNzLmpzIiwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3F1YXNhci9zcmMvY29tcG9zYWJsZXMvcHJpdmF0ZS91c2UtZmlsZS5qcyIsIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9xdWFzYXIvc3JjL2NvbXBvbmVudHMvdXBsb2FkZXIvdXBsb2FkZXItY29yZS5qcyIsIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9xdWFzYXIvc3JjL3V0aWxzL3ByaXZhdGUvZ2V0LWVtaXRzLW9iamVjdC5qcyIsIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9xdWFzYXIvc3JjL3V0aWxzL2NyZWF0ZS11cGxvYWRlci1jb21wb25lbnQuanMiLCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvcXVhc2FyL3NyYy91dGlscy91aWQuanMiLCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvcXVhc2FyL3NyYy9jb21wb3NhYmxlcy9wcml2YXRlL3VzZS12YWxpZGF0ZS5qcyIsIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9xdWFzYXIvc3JjL2NvbXBvc2FibGVzL3ByaXZhdGUvdXNlLXNwbGl0LWF0dHJzLmpzIiwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3F1YXNhci9zcmMvdXRpbHMvcHJpdmF0ZS9mb2N1cy1tYW5hZ2VyLmpzIiwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3F1YXNhci9zcmMvY29tcG9zYWJsZXMvcHJpdmF0ZS91c2UtZmllbGQuanMiLCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvcXVhc2FyL3NyYy9jb21wb25lbnRzL2lucHV0L3VzZS1tYXNrLmpzIiwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3F1YXNhci9zcmMvY29tcG9zYWJsZXMvcHJpdmF0ZS91c2UtZmlsZS1kb20tcHJvcHMuanMiLCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvcXVhc2FyL3NyYy9jb21wb3NhYmxlcy9wcml2YXRlL3VzZS1rZXktY29tcG9zaXRpb24uanMiLCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvcXVhc2FyL3NyYy9jb21wb25lbnRzL2lucHV0L1FJbnB1dC5qcyIsIi4uLy4uLy4uL3NyYy9wYWdlcy9JbmRleFBhZ2UudnVlIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGluamVjdCwgd2F0Y2gsIG9uQmVmb3JlVW5tb3VudCwgZ2V0Q3VycmVudEluc3RhbmNlIH0gZnJvbSAndnVlJ1xuXG5pbXBvcnQgeyBmb3JtS2V5IH0gZnJvbSAnLi4vdXRpbHMvcHJpdmF0ZS9zeW1ib2xzLmpzJ1xuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiAoeyB2YWxpZGF0ZSwgcmVzZXRWYWxpZGF0aW9uLCByZXF1aXJlc1FGb3JtIH0pIHtcbiAgY29uc3QgJGZvcm0gPSBpbmplY3QoZm9ybUtleSwgZmFsc2UpXG5cbiAgaWYgKCRmb3JtICE9PSBmYWxzZSkge1xuICAgIGNvbnN0IHsgcHJvcHMsIHByb3h5IH0gPSBnZXRDdXJyZW50SW5zdGFuY2UoKVxuXG4gICAgLy8gZXhwb3J0IHB1YmxpYyBtZXRob2QgKHNvIGl0IGNhbiBiZSB1c2VkIGluIFFGb3JtKVxuICAgIE9iamVjdC5hc3NpZ24ocHJveHksIHsgdmFsaWRhdGUsIHJlc2V0VmFsaWRhdGlvbiB9KVxuXG4gICAgd2F0Y2goKCkgPT4gcHJvcHMuZGlzYWJsZSwgdmFsID0+IHtcbiAgICAgIGlmICh2YWwgPT09IHRydWUpIHtcbiAgICAgICAgdHlwZW9mIHJlc2V0VmFsaWRhdGlvbiA9PT0gJ2Z1bmN0aW9uJyAmJiByZXNldFZhbGlkYXRpb24oKVxuICAgICAgICAkZm9ybS51bmJpbmRDb21wb25lbnQocHJveHkpXG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgJGZvcm0uYmluZENvbXBvbmVudChwcm94eSlcbiAgICAgIH1cbiAgICB9KVxuXG4gICAgLy8gcmVnaXN0ZXIgY29tcG9uZW50IHRvIHBhcmVudCBRRm9ybVxuICAgIHByb3BzLmRpc2FibGUgIT09IHRydWUgJiYgJGZvcm0uYmluZENvbXBvbmVudChwcm94eSlcblxuICAgIG9uQmVmb3JlVW5tb3VudCgoKSA9PiB7XG4gICAgICAvLyB1bnJlZ2lzdGVyIGNvbXBvbmVudFxuICAgICAgcHJvcHMuZGlzYWJsZSAhPT0gdHJ1ZSAmJiAkZm9ybS51bmJpbmRDb21wb25lbnQocHJveHkpXG4gICAgfSlcbiAgfVxuICBlbHNlIGlmIChyZXF1aXJlc1FGb3JtID09PSB0cnVlKSB7XG4gICAgY29uc29sZS5lcnJvcignUGFyZW50IFFGb3JtIG5vdCBmb3VuZCBvbiB1c2VGb3JtQ2hpbGQoKSEnKVxuICB9XG59XG4iLCIvLyBmaWxlIHJlZmVyZW5jZWQgZnJvbSBkb2NzXG5cbmNvbnN0XG4gIGhleCA9IC9eI1swLTlhLWZBLUZdezN9KFswLTlhLWZBLUZdezN9KT8kLyxcbiAgaGV4YSA9IC9eI1swLTlhLWZBLUZdezR9KFswLTlhLWZBLUZdezR9KT8kLyxcbiAgaGV4T3JIZXhhID0gL14jKFswLTlhLWZBLUZdezN9fFswLTlhLWZBLUZdezR9fFswLTlhLWZBLUZdezZ9fFswLTlhLWZBLUZdezh9KSQvLFxuICByZ2IgPSAvXnJnYlxcKCgoMHxbMS05XVtcXGRdP3wxW1xcZF17MCwyfXwyW1xcZF0/fDJbMC00XVtcXGRdfDI1WzAtNV0pLCl7Mn0oMHxbMS05XVtcXGRdP3wxW1xcZF17MCwyfXwyW1xcZF0/fDJbMC00XVtcXGRdfDI1WzAtNV0pXFwpJC8sXG4gIHJnYmEgPSAvXnJnYmFcXCgoKDB8WzEtOV1bXFxkXT98MVtcXGRdezAsMn18MltcXGRdP3wyWzAtNF1bXFxkXXwyNVswLTVdKSwpezJ9KDB8WzEtOV1bXFxkXT98MVtcXGRdezAsMn18MltcXGRdP3wyWzAtNF1bXFxkXXwyNVswLTVdKSwoMHwwXFwuWzAtOV0rWzEtOV18MFxcLlsxLTldK3wxKVxcKSQvXG5cbi8vIEtlZXAgaW4gc3luYyB3aXRoIHVpL3R5cGVzL2FwaS92YWxpZGF0aW9uLmQudHNcbmV4cG9ydCBjb25zdCB0ZXN0UGF0dGVybiA9IHtcbiAgZGF0ZTogdiA9PiAvXi0/W1xcZF0rXFwvWzAtMV1cXGRcXC9bMC0zXVxcZCQvLnRlc3QodiksXG4gIHRpbWU6IHYgPT4gL14oWzAtMV0/XFxkfDJbMC0zXSk6WzAtNV1cXGQkLy50ZXN0KHYpLFxuICBmdWxsdGltZTogdiA9PiAvXihbMC0xXT9cXGR8MlswLTNdKTpbMC01XVxcZDpbMC01XVxcZCQvLnRlc3QodiksXG4gIHRpbWVPckZ1bGx0aW1lOiB2ID0+IC9eKFswLTFdP1xcZHwyWzAtM10pOlswLTVdXFxkKDpbMC01XVxcZCk/JC8udGVzdCh2KSxcblxuICAvLyAtLSBSRkMgNTMyMiAtLVxuICAvLyAtLSBBZGRlZCBpbiB2Mi42LjYgLS1cbiAgLy8gVGhpcyBpcyBhIGJhc2ljIGhlbHBlciB2YWxpZGF0aW9uLlxuICAvLyBGb3Igc29tZXRoaW5nIG1vcmUgY29tcGxleCAobGlrZSBSRkMgODIyKSB5b3Ugc2hvdWxkIHdyaXRlIGFuZCB1c2UgeW91ciBvd24gcnVsZS5cbiAgLy8gV2Ugd29uJ3QgYmUgYWNjZXB0aW5nIFBScyB0byBlbmhhbmNlIHRoZSBvbmUgYmVsb3cgYmVjYXVzZSBvZiB0aGUgcmVhc29uIGFib3ZlLlxuICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmVcbiAgZW1haWw6IHYgPT4gL14oKFtePD4oKVxcW1xcXVxcXFwuLDs6XFxzQFwiXSsoXFwuW148PigpXFxbXFxdXFxcXC4sOzpcXHNAXCJdKykqKXwoXCIuK1wiKSlAKChcXFtbMC05XXsxLDN9XFwuWzAtOV17MSwzfVxcLlswLTldezEsM31cXC5bMC05XXsxLDN9XSl8KChbYS16QS1aXFwtMC05XStcXC4pK1thLXpBLVpdezIsfSkpJC8udGVzdCh2KSxcblxuICBoZXhDb2xvcjogdiA9PiBoZXgudGVzdCh2KSxcbiAgaGV4YUNvbG9yOiB2ID0+IGhleGEudGVzdCh2KSxcbiAgaGV4T3JIZXhhQ29sb3I6IHYgPT4gaGV4T3JIZXhhLnRlc3QodiksXG5cbiAgcmdiQ29sb3I6IHYgPT4gcmdiLnRlc3QodiksXG4gIHJnYmFDb2xvcjogdiA9PiByZ2JhLnRlc3QodiksXG4gIHJnYk9yUmdiYUNvbG9yOiB2ID0+IHJnYi50ZXN0KHYpIHx8IHJnYmEudGVzdCh2KSxcblxuICBoZXhPclJnYkNvbG9yOiB2ID0+IGhleC50ZXN0KHYpIHx8IHJnYi50ZXN0KHYpLFxuICBoZXhhT3JSZ2JhQ29sb3I6IHYgPT4gaGV4YS50ZXN0KHYpIHx8IHJnYmEudGVzdCh2KSxcbiAgYW55Q29sb3I6IHYgPT4gaGV4T3JIZXhhLnRlc3QodikgfHwgcmdiLnRlc3QodikgfHwgcmdiYS50ZXN0KHYpXG59XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgdGVzdFBhdHRlcm5cbn1cbiIsImNvbnN0XG4gIHRvU3RyaW5nID0gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZyxcbiAgaGFzT3duID0gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eSxcbiAgY2xhc3MydHlwZSA9IHt9XG5cbidCb29sZWFuIE51bWJlciBTdHJpbmcgRnVuY3Rpb24gQXJyYXkgRGF0ZSBSZWdFeHAgT2JqZWN0Jy5zcGxpdCgnICcpLmZvckVhY2gobmFtZSA9PiB7XG4gIGNsYXNzMnR5cGVbICdbb2JqZWN0ICcgKyBuYW1lICsgJ10nIF0gPSBuYW1lLnRvTG93ZXJDYXNlKClcbn0pXG5cbmZ1bmN0aW9uIHR5cGUgKG9iaikge1xuICByZXR1cm4gb2JqID09PSBudWxsID8gU3RyaW5nKG9iaikgOiBjbGFzczJ0eXBlWyB0b1N0cmluZy5jYWxsKG9iaikgXSB8fCAnb2JqZWN0J1xufVxuXG5mdW5jdGlvbiBpc1BsYWluT2JqZWN0IChvYmopIHtcbiAgaWYgKCFvYmogfHwgdHlwZShvYmopICE9PSAnb2JqZWN0Jykge1xuICAgIHJldHVybiBmYWxzZVxuICB9XG5cbiAgaWYgKG9iai5jb25zdHJ1Y3RvclxuICAgICYmICFoYXNPd24uY2FsbChvYmosICdjb25zdHJ1Y3RvcicpXG4gICAgJiYgIWhhc093bi5jYWxsKG9iai5jb25zdHJ1Y3Rvci5wcm90b3R5cGUsICdpc1Byb3RvdHlwZU9mJykpIHtcbiAgICByZXR1cm4gZmFsc2VcbiAgfVxuXG4gIGxldCBrZXlcbiAgZm9yIChrZXkgaW4gb2JqKSB7fSAvLyBlc2xpbnQtZGlzYWJsZS1saW5lXG5cbiAgcmV0dXJuIGtleSA9PT0gdm9pZCAwIHx8IGhhc093bi5jYWxsKG9iaiwga2V5KVxufVxuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBleHRlbmQgKCkge1xuICBsZXRcbiAgICBvcHRpb25zLCBuYW1lLCBzcmMsIGNvcHksIGNvcHlJc0FycmF5LCBjbG9uZSxcbiAgICB0YXJnZXQgPSBhcmd1bWVudHNbIDAgXSB8fCB7fSxcbiAgICBpID0gMSxcbiAgICBkZWVwID0gZmFsc2VcbiAgY29uc3QgbGVuZ3RoID0gYXJndW1lbnRzLmxlbmd0aFxuXG4gIGlmICh0eXBlb2YgdGFyZ2V0ID09PSAnYm9vbGVhbicpIHtcbiAgICBkZWVwID0gdGFyZ2V0XG4gICAgdGFyZ2V0ID0gYXJndW1lbnRzWyAxIF0gfHwge31cbiAgICBpID0gMlxuICB9XG5cbiAgaWYgKE9iamVjdCh0YXJnZXQpICE9PSB0YXJnZXQgJiYgdHlwZSh0YXJnZXQpICE9PSAnZnVuY3Rpb24nKSB7XG4gICAgdGFyZ2V0ID0ge31cbiAgfVxuXG4gIGlmIChsZW5ndGggPT09IGkpIHtcbiAgICB0YXJnZXQgPSB0aGlzXG4gICAgaS0tXG4gIH1cblxuICBmb3IgKDsgaSA8IGxlbmd0aDsgaSsrKSB7XG4gICAgaWYgKChvcHRpb25zID0gYXJndW1lbnRzWyBpIF0pICE9PSBudWxsKSB7XG4gICAgICBmb3IgKG5hbWUgaW4gb3B0aW9ucykge1xuICAgICAgICBzcmMgPSB0YXJnZXRbIG5hbWUgXVxuICAgICAgICBjb3B5ID0gb3B0aW9uc1sgbmFtZSBdXG5cbiAgICAgICAgaWYgKHRhcmdldCA9PT0gY29weSkge1xuICAgICAgICAgIGNvbnRpbnVlXG4gICAgICAgIH1cblxuICAgICAgICBpZiAoZGVlcCAmJiBjb3B5ICYmIChpc1BsYWluT2JqZWN0KGNvcHkpIHx8IChjb3B5SXNBcnJheSA9IHR5cGUoY29weSkgPT09ICdhcnJheScpKSkge1xuICAgICAgICAgIGlmIChjb3B5SXNBcnJheSkge1xuICAgICAgICAgICAgY29weUlzQXJyYXkgPSBmYWxzZVxuICAgICAgICAgICAgY2xvbmUgPSBzcmMgJiYgdHlwZShzcmMpID09PSAnYXJyYXknID8gc3JjIDogW11cbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBjbG9uZSA9IHNyYyAmJiBpc1BsYWluT2JqZWN0KHNyYykgPyBzcmMgOiB7fVxuICAgICAgICAgIH1cblxuICAgICAgICAgIHRhcmdldFsgbmFtZSBdID0gZXh0ZW5kKGRlZXAsIGNsb25lLCBjb3B5KVxuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKGNvcHkgIT09IHZvaWQgMCkge1xuICAgICAgICAgIHRhcmdldFsgbmFtZSBdID0gY29weVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHRhcmdldFxufVxuIiwiaW1wb3J0IHsgdXNlU2l6ZVByb3BzIH0gZnJvbSAnLi4vLi4vY29tcG9zYWJsZXMvcHJpdmF0ZS91c2Utc2l6ZS5qcydcblxuLy8gYWxzbyB1c2VkIGJ5IFFLbm9iXG5leHBvcnQgY29uc3QgdXNlQ2lyY3VsYXJDb21tb25Qcm9wcyA9IHtcbiAgLi4udXNlU2l6ZVByb3BzLFxuXG4gIG1pbjoge1xuICAgIHR5cGU6IE51bWJlcixcbiAgICBkZWZhdWx0OiAwXG4gIH0sXG4gIG1heDoge1xuICAgIHR5cGU6IE51bWJlcixcbiAgICBkZWZhdWx0OiAxMDBcbiAgfSxcblxuICBjb2xvcjogU3RyaW5nLFxuICBjZW50ZXJDb2xvcjogU3RyaW5nLFxuICB0cmFja0NvbG9yOiBTdHJpbmcsXG5cbiAgZm9udFNpemU6IFN0cmluZyxcblxuICAvLyByYXRpb1xuICB0aGlja25lc3M6IHtcbiAgICB0eXBlOiBOdW1iZXIsXG4gICAgZGVmYXVsdDogMC4yLFxuICAgIHZhbGlkYXRvcjogdiA9PiB2ID49IDAgJiYgdiA8PSAxXG4gIH0sXG5cbiAgYW5nbGU6IHtcbiAgICB0eXBlOiBOdW1iZXIsXG4gICAgZGVmYXVsdDogMFxuICB9LFxuXG4gIHNob3dWYWx1ZTogQm9vbGVhbixcbiAgcmV2ZXJzZTogQm9vbGVhbixcblxuICBpbnN0YW50RmVlZGJhY2s6IEJvb2xlYW5cbn1cbiIsImltcG9ydCB7IGgsIGNvbXB1dGVkLCBnZXRDdXJyZW50SW5zdGFuY2UgfSBmcm9tICd2dWUnXG5cbmltcG9ydCB1c2VTaXplIGZyb20gJy4uLy4uL2NvbXBvc2FibGVzL3ByaXZhdGUvdXNlLXNpemUuanMnXG5pbXBvcnQgeyB1c2VDaXJjdWxhckNvbW1vblByb3BzIH0gZnJvbSAnLi91c2UtY2lyY3VsYXItcHJvZ3Jlc3MuanMnXG5cbmltcG9ydCB7IGNyZWF0ZUNvbXBvbmVudCB9IGZyb20gJy4uLy4uL3V0aWxzL3ByaXZhdGUvY3JlYXRlLmpzJ1xuaW1wb3J0IHsgaE1lcmdlU2xvdFNhZmVseSB9IGZyb20gJy4uLy4uL3V0aWxzL3ByaXZhdGUvcmVuZGVyLmpzJ1xuaW1wb3J0IHsgYmV0d2VlbiB9IGZyb20gJy4uLy4uL3V0aWxzL2Zvcm1hdC5qcydcblxuY29uc3RcbiAgcmFkaXVzID0gNTAsXG4gIGRpYW1ldGVyID0gMiAqIHJhZGl1cyxcbiAgY2lyY3VtZmVyZW5jZSA9IGRpYW1ldGVyICogTWF0aC5QSSxcbiAgc3Ryb2tlRGFzaEFycmF5ID0gTWF0aC5yb3VuZChjaXJjdW1mZXJlbmNlICogMTAwMCkgLyAxMDAwXG5cbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZUNvbXBvbmVudCh7XG4gIG5hbWU6ICdRQ2lyY3VsYXJQcm9ncmVzcycsXG5cbiAgcHJvcHM6IHtcbiAgICAuLi51c2VDaXJjdWxhckNvbW1vblByb3BzLFxuXG4gICAgdmFsdWU6IHtcbiAgICAgIHR5cGU6IE51bWJlcixcbiAgICAgIGRlZmF1bHQ6IDBcbiAgICB9LFxuXG4gICAgYW5pbWF0aW9uU3BlZWQ6IHtcbiAgICAgIHR5cGU6IFsgU3RyaW5nLCBOdW1iZXIgXSxcbiAgICAgIGRlZmF1bHQ6IDYwMFxuICAgIH0sXG5cbiAgICBpbmRldGVybWluYXRlOiBCb29sZWFuXG4gIH0sXG5cbiAgc2V0dXAgKHByb3BzLCB7IHNsb3RzIH0pIHtcbiAgICBjb25zdCB7IHByb3h5OiB7ICRxIH0gfSA9IGdldEN1cnJlbnRJbnN0YW5jZSgpXG4gICAgY29uc3Qgc2l6ZVN0eWxlID0gdXNlU2l6ZShwcm9wcylcblxuICAgIGNvbnN0IHN2Z1N0eWxlID0gY29tcHV0ZWQoKCkgPT4ge1xuICAgICAgY29uc3QgYW5nbGUgPSAoJHEubGFuZy5ydGwgPT09IHRydWUgPyAtMSA6IDEpICogcHJvcHMuYW5nbGVcblxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgdHJhbnNmb3JtOiBwcm9wcy5yZXZlcnNlICE9PSAoJHEubGFuZy5ydGwgPT09IHRydWUpXG4gICAgICAgICAgPyBgc2NhbGUzZCgtMSwgMSwgMSkgcm90YXRlM2QoMCwgMCwgMSwgJHsgLTkwIC0gYW5nbGUgfWRlZylgXG4gICAgICAgICAgOiBgcm90YXRlM2QoMCwgMCwgMSwgJHsgYW5nbGUgLSA5MCB9ZGVnKWBcbiAgICAgIH1cbiAgICB9KVxuXG4gICAgY29uc3QgY2lyY2xlU3R5bGUgPSBjb21wdXRlZCgoKSA9PiAoXG4gICAgICBwcm9wcy5pbnN0YW50RmVlZGJhY2sgIT09IHRydWUgJiYgcHJvcHMuaW5kZXRlcm1pbmF0ZSAhPT0gdHJ1ZVxuICAgICAgICA/IHsgdHJhbnNpdGlvbjogYHN0cm9rZS1kYXNob2Zmc2V0ICR7IHByb3BzLmFuaW1hdGlvblNwZWVkIH1tcyBlYXNlIDBzLCBzdHJva2UgJHsgcHJvcHMuYW5pbWF0aW9uU3BlZWQgfW1zIGVhc2VgIH1cbiAgICAgICAgOiAnJ1xuICAgICkpXG5cbiAgICBjb25zdCB2aWV3Qm94ID0gY29tcHV0ZWQoKCkgPT4gZGlhbWV0ZXIgLyAoMSAtIHByb3BzLnRoaWNrbmVzcyAvIDIpKVxuXG4gICAgY29uc3Qgdmlld0JveEF0dHIgPSBjb21wdXRlZCgoKSA9PlxuICAgICAgYCR7IHZpZXdCb3gudmFsdWUgLyAyIH0gJHsgdmlld0JveC52YWx1ZSAvIDIgfSAkeyB2aWV3Qm94LnZhbHVlIH0gJHsgdmlld0JveC52YWx1ZSB9YFxuICAgIClcblxuICAgIGNvbnN0IG5vcm1hbGl6ZWQgPSBjb21wdXRlZCgoKSA9PiBiZXR3ZWVuKHByb3BzLnZhbHVlLCBwcm9wcy5taW4sIHByb3BzLm1heCkpXG5cbiAgICBjb25zdCBzdHJva2VEYXNoT2Zmc2V0ID0gY29tcHV0ZWQoKCkgPT4gY2lyY3VtZmVyZW5jZSAqIChcbiAgICAgIDEgLSAobm9ybWFsaXplZC52YWx1ZSAtIHByb3BzLm1pbikgLyAocHJvcHMubWF4IC0gcHJvcHMubWluKVxuICAgICkpXG5cbiAgICBjb25zdCBzdHJva2VXaWR0aCA9IGNvbXB1dGVkKCgpID0+IHByb3BzLnRoaWNrbmVzcyAvIDIgKiB2aWV3Qm94LnZhbHVlKVxuXG4gICAgZnVuY3Rpb24gZ2V0Q2lyY2xlICh7IHRoaWNrbmVzcywgb2Zmc2V0LCBjb2xvciwgY2xzIH0pIHtcbiAgICAgIHJldHVybiBoKCdjaXJjbGUnLCB7XG4gICAgICAgIGNsYXNzOiAncS1jaXJjdWxhci1wcm9ncmVzc19fJyArIGNscyArIChjb2xvciAhPT0gdm9pZCAwID8gYCB0ZXh0LSR7IGNvbG9yIH1gIDogJycpLFxuICAgICAgICBzdHlsZTogY2lyY2xlU3R5bGUudmFsdWUsXG4gICAgICAgIGZpbGw6ICd0cmFuc3BhcmVudCcsXG4gICAgICAgIHN0cm9rZTogJ2N1cnJlbnRDb2xvcicsXG4gICAgICAgICdzdHJva2Utd2lkdGgnOiB0aGlja25lc3MsXG4gICAgICAgICdzdHJva2UtZGFzaGFycmF5Jzogc3Ryb2tlRGFzaEFycmF5LFxuICAgICAgICAnc3Ryb2tlLWRhc2hvZmZzZXQnOiBvZmZzZXQsXG4gICAgICAgIGN4OiB2aWV3Qm94LnZhbHVlLFxuICAgICAgICBjeTogdmlld0JveC52YWx1ZSxcbiAgICAgICAgcjogcmFkaXVzXG4gICAgICB9KVxuICAgIH1cblxuICAgIHJldHVybiAoKSA9PiB7XG4gICAgICBjb25zdCBzdmdDaGlsZCA9IFtdXG5cbiAgICAgIHByb3BzLmNlbnRlckNvbG9yICE9PSB2b2lkIDAgJiYgcHJvcHMuY2VudGVyQ29sb3IgIT09ICd0cmFuc3BhcmVudCcgJiYgc3ZnQ2hpbGQucHVzaChcbiAgICAgICAgaCgnY2lyY2xlJywge1xuICAgICAgICAgIGNsYXNzOiBgcS1jaXJjdWxhci1wcm9ncmVzc19fY2VudGVyIHRleHQtJHsgcHJvcHMuY2VudGVyQ29sb3IgfWAsXG4gICAgICAgICAgZmlsbDogJ2N1cnJlbnRDb2xvcicsXG4gICAgICAgICAgcjogcmFkaXVzIC0gc3Ryb2tlV2lkdGgudmFsdWUgLyAyLFxuICAgICAgICAgIGN4OiB2aWV3Qm94LnZhbHVlLFxuICAgICAgICAgIGN5OiB2aWV3Qm94LnZhbHVlXG4gICAgICAgIH0pXG4gICAgICApXG5cbiAgICAgIHByb3BzLnRyYWNrQ29sb3IgIT09IHZvaWQgMCAmJiBwcm9wcy50cmFja0NvbG9yICE9PSAndHJhbnNwYXJlbnQnICYmIHN2Z0NoaWxkLnB1c2goXG4gICAgICAgIGdldENpcmNsZSh7XG4gICAgICAgICAgY2xzOiAndHJhY2snLFxuICAgICAgICAgIHRoaWNrbmVzczogc3Ryb2tlV2lkdGgudmFsdWUsXG4gICAgICAgICAgb2Zmc2V0OiAwLFxuICAgICAgICAgIGNvbG9yOiBwcm9wcy50cmFja0NvbG9yXG4gICAgICAgIH0pXG4gICAgICApXG5cbiAgICAgIHN2Z0NoaWxkLnB1c2goXG4gICAgICAgIGdldENpcmNsZSh7XG4gICAgICAgICAgY2xzOiAnY2lyY2xlJyxcbiAgICAgICAgICB0aGlja25lc3M6IHN0cm9rZVdpZHRoLnZhbHVlLFxuICAgICAgICAgIG9mZnNldDogc3Ryb2tlRGFzaE9mZnNldC52YWx1ZSxcbiAgICAgICAgICBjb2xvcjogcHJvcHMuY29sb3JcbiAgICAgICAgfSlcbiAgICAgIClcblxuICAgICAgY29uc3QgY2hpbGQgPSBbXG4gICAgICAgIGgoJ3N2ZycsIHtcbiAgICAgICAgICBjbGFzczogJ3EtY2lyY3VsYXItcHJvZ3Jlc3NfX3N2ZycsXG4gICAgICAgICAgc3R5bGU6IHN2Z1N0eWxlLnZhbHVlLFxuICAgICAgICAgIHZpZXdCb3g6IHZpZXdCb3hBdHRyLnZhbHVlLFxuICAgICAgICAgICdhcmlhLWhpZGRlbic6ICd0cnVlJ1xuICAgICAgICB9LCBzdmdDaGlsZClcbiAgICAgIF1cblxuICAgICAgcHJvcHMuc2hvd1ZhbHVlID09PSB0cnVlICYmIGNoaWxkLnB1c2goXG4gICAgICAgIGgoJ2RpdicsIHtcbiAgICAgICAgICBjbGFzczogJ3EtY2lyY3VsYXItcHJvZ3Jlc3NfX3RleHQgYWJzb2x1dGUtZnVsbCByb3cgZmxleC1jZW50ZXIgY29udGVudC1jZW50ZXInLFxuICAgICAgICAgIHN0eWxlOiB7IGZvbnRTaXplOiBwcm9wcy5mb250U2l6ZSB9XG4gICAgICAgIH0sIHNsb3RzLmRlZmF1bHQgIT09IHZvaWQgMCA/IHNsb3RzLmRlZmF1bHQoKSA6IFsgaCgnZGl2Jywgbm9ybWFsaXplZC52YWx1ZSkgXSlcbiAgICAgIClcblxuICAgICAgcmV0dXJuIGgoJ2RpdicsIHtcbiAgICAgICAgY2xhc3M6IGBxLWNpcmN1bGFyLXByb2dyZXNzIHEtY2lyY3VsYXItcHJvZ3Jlc3MtLSR7IHByb3BzLmluZGV0ZXJtaW5hdGUgPT09IHRydWUgPyAnaW4nIDogJycgfWRldGVybWluYXRlYCxcbiAgICAgICAgc3R5bGU6IHNpemVTdHlsZS52YWx1ZSxcbiAgICAgICAgcm9sZTogJ3Byb2dyZXNzYmFyJyxcbiAgICAgICAgJ2FyaWEtdmFsdWVtaW4nOiBwcm9wcy5taW4sXG4gICAgICAgICdhcmlhLXZhbHVlbWF4JzogcHJvcHMubWF4LFxuICAgICAgICAnYXJpYS12YWx1ZW5vdyc6IHByb3BzLmluZGV0ZXJtaW5hdGUgPT09IHRydWUgPyB2b2lkIDAgOiBub3JtYWxpemVkLnZhbHVlXG4gICAgICB9LCBoTWVyZ2VTbG90U2FmZWx5KHNsb3RzLmludGVybmFsLCBjaGlsZCkpIC8vIFwiaW50ZXJuYWxcIiBpcyB1c2VkIGJ5IFFLbm9iXG4gICAgfVxuICB9XG59KVxuIiwiaW1wb3J0IHsgaCwgcmVmLCBjb21wdXRlZCwgZ2V0Q3VycmVudEluc3RhbmNlIH0gZnJvbSAndnVlJ1xuXG5pbXBvcnQgeyBzdG9wLCBzdG9wQW5kUHJldmVudCB9IGZyb20gJy4uLy4uL3V0aWxzL2V2ZW50LmpzJ1xuXG5mdW5jdGlvbiBmaWx0ZXJGaWxlcyAoZmlsZXMsIHJlamVjdGVkRmlsZXMsIGZhaWxlZFByb3BWYWxpZGF0aW9uLCBmaWx0ZXJGbikge1xuICBjb25zdCBhY2NlcHRlZEZpbGVzID0gW11cblxuICBmaWxlcy5mb3JFYWNoKGZpbGUgPT4ge1xuICAgIGlmIChmaWx0ZXJGbihmaWxlKSA9PT0gdHJ1ZSkge1xuICAgICAgYWNjZXB0ZWRGaWxlcy5wdXNoKGZpbGUpXG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgcmVqZWN0ZWRGaWxlcy5wdXNoKHsgZmFpbGVkUHJvcFZhbGlkYXRpb24sIGZpbGUgfSlcbiAgICB9XG4gIH0pXG5cbiAgcmV0dXJuIGFjY2VwdGVkRmlsZXNcbn1cblxuZnVuY3Rpb24gc3RvcEFuZFByZXZlbnREcmFnIChlKSB7XG4gIGUgJiYgZS5kYXRhVHJhbnNmZXIgJiYgKGUuZGF0YVRyYW5zZmVyLmRyb3BFZmZlY3QgPSAnY29weScpXG4gIHN0b3BBbmRQcmV2ZW50KGUpXG59XG5cbmV4cG9ydCBjb25zdCB1c2VGaWxlUHJvcHMgPSB7XG4gIG11bHRpcGxlOiBCb29sZWFuLFxuICBhY2NlcHQ6IFN0cmluZyxcbiAgY2FwdHVyZTogU3RyaW5nLFxuICBtYXhGaWxlU2l6ZTogWyBOdW1iZXIsIFN0cmluZyBdLFxuICBtYXhUb3RhbFNpemU6IFsgTnVtYmVyLCBTdHJpbmcgXSxcbiAgbWF4RmlsZXM6IFsgTnVtYmVyLCBTdHJpbmcgXSxcbiAgZmlsdGVyOiBGdW5jdGlvblxufVxuXG5leHBvcnQgY29uc3QgdXNlRmlsZUVtaXRzID0gWyAncmVqZWN0ZWQnIF1cblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gKHtcbiAgZWRpdGFibGUsXG4gIGRuZCxcbiAgZ2V0RmlsZUlucHV0LFxuICBhZGRGaWxlc1RvUXVldWVcbn0pIHtcbiAgY29uc3QgeyBwcm9wcywgZW1pdCwgcHJveHkgfSA9IGdldEN1cnJlbnRJbnN0YW5jZSgpXG5cbiAgY29uc3QgZG5kUmVmID0gcmVmKG51bGwpXG5cbiAgY29uc3QgZXh0ZW5zaW9ucyA9IGNvbXB1dGVkKCgpID0+IChcbiAgICBwcm9wcy5hY2NlcHQgIT09IHZvaWQgMFxuICAgICAgPyBwcm9wcy5hY2NlcHQuc3BsaXQoJywnKS5tYXAoZXh0ID0+IHtcbiAgICAgICAgZXh0ID0gZXh0LnRyaW0oKVxuICAgICAgICBpZiAoZXh0ID09PSAnKicpIHsgLy8gc3VwcG9ydCBcIipcIlxuICAgICAgICAgIHJldHVybiAnKi8nXG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAoZXh0LmVuZHNXaXRoKCcvKicpKSB7IC8vIHN1cHBvcnQgXCJpbWFnZS8qXCIgb3IgXCIqLypcIlxuICAgICAgICAgIGV4dCA9IGV4dC5zbGljZSgwLCBleHQubGVuZ3RoIC0gMSlcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZXh0LnRvVXBwZXJDYXNlKClcbiAgICAgIH0pXG4gICAgICA6IG51bGxcbiAgKSlcblxuICBjb25zdCBtYXhGaWxlc051bWJlciA9IGNvbXB1dGVkKCgpID0+IHBhcnNlSW50KHByb3BzLm1heEZpbGVzLCAxMCkpXG4gIGNvbnN0IG1heFRvdGFsU2l6ZU51bWJlciA9IGNvbXB1dGVkKCgpID0+IHBhcnNlSW50KHByb3BzLm1heFRvdGFsU2l6ZSwgMTApKVxuXG4gIGZ1bmN0aW9uIHBpY2tGaWxlcyAoZSkge1xuICAgIGlmIChlZGl0YWJsZS52YWx1ZSkge1xuICAgICAgaWYgKGUgIT09IE9iamVjdChlKSkge1xuICAgICAgICBlID0geyB0YXJnZXQ6IG51bGwgfVxuICAgICAgfVxuXG4gICAgICBpZiAoZS50YXJnZXQgIT09IG51bGwgJiYgZS50YXJnZXQubWF0Y2hlcygnaW5wdXRbdHlwZT1cImZpbGVcIl0nKSA9PT0gdHJ1ZSkge1xuICAgICAgICAvLyBzdG9wIHByb3BhZ2F0aW9uIGlmIGl0J3Mgbm90IGEgcmVhbCBwb2ludGVyIGV2ZW50XG4gICAgICAgIGUuY2xpZW50WCA9PT0gMCAmJiBlLmNsaWVudFkgPT09IDAgJiYgc3RvcChlKVxuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIGNvbnN0IGlucHV0ID0gZ2V0RmlsZUlucHV0KClcbiAgICAgICAgaW5wdXQgJiYgaW5wdXQgIT09IGUudGFyZ2V0ICYmIGlucHV0LmNsaWNrKGUpXG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gYWRkRmlsZXMgKGZpbGVzKSB7XG4gICAgaWYgKGVkaXRhYmxlLnZhbHVlICYmIGZpbGVzKSB7XG4gICAgICBhZGRGaWxlc1RvUXVldWUobnVsbCwgZmlsZXMpXG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gcHJvY2Vzc0ZpbGVzIChlLCBmaWxlc1RvUHJvY2VzcywgY3VycmVudEZpbGVMaXN0LCBhcHBlbmQpIHtcbiAgICBsZXQgZmlsZXMgPSBBcnJheS5mcm9tKGZpbGVzVG9Qcm9jZXNzIHx8IGUudGFyZ2V0LmZpbGVzKVxuICAgIGNvbnN0IHJlamVjdGVkRmlsZXMgPSBbXVxuXG4gICAgY29uc3QgZG9uZSA9ICgpID0+IHtcbiAgICAgIGlmIChyZWplY3RlZEZpbGVzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgZW1pdCgncmVqZWN0ZWQnLCByZWplY3RlZEZpbGVzKVxuICAgICAgfVxuICAgIH1cblxuICAgIC8vIGZpbHRlciBmaWxlIHR5cGVzXG4gICAgaWYgKHByb3BzLmFjY2VwdCAhPT0gdm9pZCAwICYmIGV4dGVuc2lvbnMudmFsdWUuaW5kZXhPZignKi8nKSA9PT0gLTEpIHtcbiAgICAgIGZpbGVzID0gZmlsdGVyRmlsZXMoZmlsZXMsIHJlamVjdGVkRmlsZXMsICdhY2NlcHQnLCBmaWxlID0+IHtcbiAgICAgICAgcmV0dXJuIGV4dGVuc2lvbnMudmFsdWUuc29tZShleHQgPT4gKFxuICAgICAgICAgIGZpbGUudHlwZS50b1VwcGVyQ2FzZSgpLnN0YXJ0c1dpdGgoZXh0KVxuICAgICAgICAgIHx8IGZpbGUubmFtZS50b1VwcGVyQ2FzZSgpLmVuZHNXaXRoKGV4dClcbiAgICAgICAgKSlcbiAgICAgIH0pXG5cbiAgICAgIGlmIChmaWxlcy5sZW5ndGggPT09IDApIHsgcmV0dXJuIGRvbmUoKSB9XG4gICAgfVxuXG4gICAgLy8gZmlsdGVyIG1heCBmaWxlIHNpemVcbiAgICBpZiAocHJvcHMubWF4RmlsZVNpemUgIT09IHZvaWQgMCkge1xuICAgICAgY29uc3QgbWF4RmlsZVNpemUgPSBwYXJzZUludChwcm9wcy5tYXhGaWxlU2l6ZSwgMTApXG4gICAgICBmaWxlcyA9IGZpbHRlckZpbGVzKGZpbGVzLCByZWplY3RlZEZpbGVzLCAnbWF4LWZpbGUtc2l6ZScsIGZpbGUgPT4ge1xuICAgICAgICByZXR1cm4gZmlsZS5zaXplIDw9IG1heEZpbGVTaXplXG4gICAgICB9KVxuXG4gICAgICBpZiAoZmlsZXMubGVuZ3RoID09PSAwKSB7IHJldHVybiBkb25lKCkgfVxuICAgIH1cblxuICAgIC8vIENvcmRvdmEvaU9TIGFsbG93cyBzZWxlY3RpbmcgbXVsdGlwbGUgZmlsZXMgZXZlbiB3aGVuIHRoZVxuICAgIC8vIG11bHRpcGxlIGF0dHJpYnV0ZSBpcyBub3Qgc3BlY2lmaWVkLiBXZSBhbHNvIG5vcm1hbGl6ZSBkcmFnJ24nZHJvcHBlZFxuICAgIC8vIGZpbGVzIGhlcmU6XG4gICAgaWYgKHByb3BzLm11bHRpcGxlICE9PSB0cnVlICYmIGZpbGVzLmxlbmd0aCA+IDApIHtcbiAgICAgIGZpbGVzID0gWyBmaWxlc1sgMCBdIF1cbiAgICB9XG5cbiAgICAvLyBDb21wdXRlIGtleSB0byB1c2UgZm9yIGVhY2ggZmlsZVxuICAgIGZpbGVzLmZvckVhY2goZmlsZSA9PiB7XG4gICAgICBmaWxlLl9fa2V5ID0gZmlsZS53ZWJraXRSZWxhdGl2ZVBhdGggKyBmaWxlLmxhc3RNb2RpZmllZCArIGZpbGUubmFtZSArIGZpbGUuc2l6ZVxuICAgIH0pXG5cbiAgICAvLyBBdm9pZCBkdXBsaWNhdGUgZmlsZXNcbiAgICBjb25zdCBmaWxlbmFtZU1hcCA9IGN1cnJlbnRGaWxlTGlzdC5tYXAoZW50cnkgPT4gZW50cnkuX19rZXkpXG4gICAgZmlsZXMgPSBmaWx0ZXJGaWxlcyhmaWxlcywgcmVqZWN0ZWRGaWxlcywgJ2R1cGxpY2F0ZScsIGZpbGUgPT4ge1xuICAgICAgcmV0dXJuIGZpbGVuYW1lTWFwLmluY2x1ZGVzKGZpbGUuX19rZXkpID09PSBmYWxzZVxuICAgIH0pXG5cbiAgICBpZiAoZmlsZXMubGVuZ3RoID09PSAwKSB7IHJldHVybiBkb25lKCkgfVxuXG4gICAgaWYgKHByb3BzLm1heFRvdGFsU2l6ZSAhPT0gdm9pZCAwKSB7XG4gICAgICBsZXQgc2l6ZSA9IGFwcGVuZCA9PT0gdHJ1ZVxuICAgICAgICA/IGN1cnJlbnRGaWxlTGlzdC5yZWR1Y2UoKHRvdGFsLCBmaWxlKSA9PiB0b3RhbCArIGZpbGUuc2l6ZSwgMClcbiAgICAgICAgOiAwXG5cbiAgICAgIGZpbGVzID0gZmlsdGVyRmlsZXMoZmlsZXMsIHJlamVjdGVkRmlsZXMsICdtYXgtdG90YWwtc2l6ZScsIGZpbGUgPT4ge1xuICAgICAgICBzaXplICs9IGZpbGUuc2l6ZVxuICAgICAgICByZXR1cm4gc2l6ZSA8PSBtYXhUb3RhbFNpemVOdW1iZXIudmFsdWVcbiAgICAgIH0pXG5cbiAgICAgIGlmIChmaWxlcy5sZW5ndGggPT09IDApIHsgcmV0dXJuIGRvbmUoKSB9XG4gICAgfVxuXG4gICAgLy8gZG8gd2UgaGF2ZSBjdXN0b20gZmlsdGVyIGZ1bmN0aW9uP1xuICAgIGlmICh0eXBlb2YgcHJvcHMuZmlsdGVyID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICBjb25zdCBmaWx0ZXJlZEZpbGVzID0gcHJvcHMuZmlsdGVyKGZpbGVzKVxuICAgICAgZmlsZXMgPSBmaWx0ZXJGaWxlcyhmaWxlcywgcmVqZWN0ZWRGaWxlcywgJ2ZpbHRlcicsIGZpbGUgPT4ge1xuICAgICAgICByZXR1cm4gZmlsdGVyZWRGaWxlcy5pbmNsdWRlcyhmaWxlKVxuICAgICAgfSlcbiAgICB9XG5cbiAgICBpZiAocHJvcHMubWF4RmlsZXMgIT09IHZvaWQgMCkge1xuICAgICAgbGV0IGZpbGVzTnVtYmVyID0gYXBwZW5kID09PSB0cnVlXG4gICAgICAgID8gY3VycmVudEZpbGVMaXN0Lmxlbmd0aFxuICAgICAgICA6IDBcblxuICAgICAgZmlsZXMgPSBmaWx0ZXJGaWxlcyhmaWxlcywgcmVqZWN0ZWRGaWxlcywgJ21heC1maWxlcycsICgpID0+IHtcbiAgICAgICAgZmlsZXNOdW1iZXIrK1xuICAgICAgICByZXR1cm4gZmlsZXNOdW1iZXIgPD0gbWF4RmlsZXNOdW1iZXIudmFsdWVcbiAgICAgIH0pXG5cbiAgICAgIGlmIChmaWxlcy5sZW5ndGggPT09IDApIHsgcmV0dXJuIGRvbmUoKSB9XG4gICAgfVxuXG4gICAgZG9uZSgpXG5cbiAgICBpZiAoZmlsZXMubGVuZ3RoID4gMCkge1xuICAgICAgcmV0dXJuIGZpbGVzXG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gb25EcmFnb3ZlciAoZSkge1xuICAgIHN0b3BBbmRQcmV2ZW50RHJhZyhlKVxuICAgIGRuZC52YWx1ZSAhPT0gdHJ1ZSAmJiAoZG5kLnZhbHVlID0gdHJ1ZSlcbiAgfVxuXG4gIGZ1bmN0aW9uIG9uRHJhZ2xlYXZlIChlKSB7XG4gICAgc3RvcEFuZFByZXZlbnQoZSlcbiAgICBlLnJlbGF0ZWRUYXJnZXQgIT09IGRuZFJlZi52YWx1ZSAmJiAoZG5kLnZhbHVlID0gZmFsc2UpXG4gIH1cblxuICBmdW5jdGlvbiBvbkRyb3AgKGUpIHtcbiAgICBzdG9wQW5kUHJldmVudERyYWcoZSlcbiAgICBjb25zdCBmaWxlcyA9IGUuZGF0YVRyYW5zZmVyLmZpbGVzXG5cbiAgICBpZiAoZmlsZXMubGVuZ3RoID4gMCkge1xuICAgICAgYWRkRmlsZXNUb1F1ZXVlKG51bGwsIGZpbGVzKVxuICAgIH1cblxuICAgIGRuZC52YWx1ZSA9IGZhbHNlXG4gIH1cblxuICBmdW5jdGlvbiBnZXREbmROb2RlICh0eXBlKSB7XG4gICAgaWYgKGRuZC52YWx1ZSA9PT0gdHJ1ZSkge1xuICAgICAgcmV0dXJuIGgoJ2RpdicsIHtcbiAgICAgICAgcmVmOiBkbmRSZWYsXG4gICAgICAgIGNsYXNzOiBgcS0keyB0eXBlIH1fX2RuZCBhYnNvbHV0ZS1mdWxsYCxcbiAgICAgICAgb25EcmFnZW50ZXI6IHN0b3BBbmRQcmV2ZW50RHJhZyxcbiAgICAgICAgb25EcmFnb3Zlcjogc3RvcEFuZFByZXZlbnREcmFnLFxuICAgICAgICBvbkRyYWdsZWF2ZSxcbiAgICAgICAgb25Ecm9wXG4gICAgICB9KVxuICAgIH1cbiAgfVxuXG4gIC8vIGV4cG9zZSBwdWJsaWMgbWV0aG9kc1xuICBPYmplY3QuYXNzaWduKHByb3h5LCB7IHBpY2tGaWxlcywgYWRkRmlsZXMgfSlcblxuICByZXR1cm4ge1xuICAgIHBpY2tGaWxlcyxcbiAgICBhZGRGaWxlcyxcbiAgICBvbkRyYWdvdmVyLFxuICAgIHByb2Nlc3NGaWxlcyxcbiAgICBnZXREbmROb2RlLFxuICAgIG1heEZpbGVzTnVtYmVyLFxuICAgIG1heFRvdGFsU2l6ZU51bWJlclxuICB9XG59XG4iLCJpbXBvcnQgeyBoLCByZWYsIGlzUmVmLCBjb21wdXRlZCwgd2F0Y2gsIHByb3ZpZGUsIG9uQmVmb3JlVW5tb3VudCwgZ2V0Q3VycmVudEluc3RhbmNlIH0gZnJvbSAndnVlJ1xuXG5pbXBvcnQgUUJ0biBmcm9tICcuLi9idG4vUUJ0bi5qcydcbmltcG9ydCBRSWNvbiBmcm9tICcuLi9pY29uL1FJY29uLmpzJ1xuaW1wb3J0IFFTcGlubmVyIGZyb20gJy4uL3NwaW5uZXIvUVNwaW5uZXIuanMnXG5pbXBvcnQgUUNpcmN1bGFyUHJvZ3Jlc3MgZnJvbSAnLi4vY2lyY3VsYXItcHJvZ3Jlc3MvUUNpcmN1bGFyUHJvZ3Jlc3MuanMnXG5cbmltcG9ydCB1c2VEYXJrLCB7IHVzZURhcmtQcm9wcyB9IGZyb20gJy4uLy4uL2NvbXBvc2FibGVzL3ByaXZhdGUvdXNlLWRhcmsuanMnXG5pbXBvcnQgdXNlRmlsZSwgeyB1c2VGaWxlUHJvcHMsIHVzZUZpbGVFbWl0cyB9IGZyb20gJy4uLy4uL2NvbXBvc2FibGVzL3ByaXZhdGUvdXNlLWZpbGUuanMnXG5cbmltcG9ydCB7IHN0b3AgfSBmcm9tICcuLi8uLi91dGlscy9ldmVudC5qcydcbmltcG9ydCB7IGh1bWFuU3RvcmFnZVNpemUgfSBmcm9tICcuLi8uLi91dGlscy9mb3JtYXQuanMnXG5pbXBvcnQgeyB1cGxvYWRlcktleSB9IGZyb20gJy4uLy4uL3V0aWxzL3ByaXZhdGUvc3ltYm9scy5qcydcblxuZnVuY3Rpb24gZ2V0UHJvZ3Jlc3NMYWJlbCAocCkge1xuICByZXR1cm4gKHAgKiAxMDApLnRvRml4ZWQoMikgKyAnJSdcbn1cblxuZXhwb3J0IGNvbnN0IGNvcmVQcm9wcyA9IHtcbiAgLi4udXNlRGFya1Byb3BzLFxuICAuLi51c2VGaWxlUHJvcHMsXG5cbiAgbGFiZWw6IFN0cmluZyxcblxuICBjb2xvcjogU3RyaW5nLFxuICB0ZXh0Q29sb3I6IFN0cmluZyxcblxuICBzcXVhcmU6IEJvb2xlYW4sXG4gIGZsYXQ6IEJvb2xlYW4sXG4gIGJvcmRlcmVkOiBCb29sZWFuLFxuXG4gIG5vVGh1bWJuYWlsczogQm9vbGVhbixcbiAgYXV0b1VwbG9hZDogQm9vbGVhbixcbiAgaGlkZVVwbG9hZEJ0bjogQm9vbGVhbixcblxuICBkaXNhYmxlOiBCb29sZWFuLFxuICByZWFkb25seTogQm9vbGVhblxufVxuXG5leHBvcnQgY29uc3QgY29yZUVtaXRzID0gW1xuICAuLi51c2VGaWxlRW1pdHMsXG4gICdzdGFydCcsICdmaW5pc2gnLCAnYWRkZWQnLCAncmVtb3ZlZCdcbl1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldFJlbmRlcmVyIChnZXRQbHVnaW4pIHtcbiAgY29uc3Qgdm0gPSBnZXRDdXJyZW50SW5zdGFuY2UoKVxuICBjb25zdCB7IHByb3BzLCBzbG90cywgZW1pdCwgcHJveHkgfSA9IHZtXG4gIGNvbnN0IHsgJHEgfSA9IHByb3h5XG5cbiAgY29uc3QgaXNEYXJrID0gdXNlRGFyayhwcm9wcywgJHEpXG5cbiAgZnVuY3Rpb24gdXBkYXRlRmlsZVN0YXR1cyAoZmlsZSwgc3RhdHVzLCB1cGxvYWRlZFNpemUpIHtcbiAgICBmaWxlLl9fc3RhdHVzID0gc3RhdHVzXG5cbiAgICBpZiAoc3RhdHVzID09PSAnaWRsZScpIHtcbiAgICAgIGZpbGUuX191cGxvYWRlZCA9IDBcbiAgICAgIGZpbGUuX19wcm9ncmVzcyA9IDBcbiAgICAgIGZpbGUuX19zaXplTGFiZWwgPSBodW1hblN0b3JhZ2VTaXplKGZpbGUuc2l6ZSlcbiAgICAgIGZpbGUuX19wcm9ncmVzc0xhYmVsID0gJzAuMDAlJ1xuICAgICAgcmV0dXJuXG4gICAgfVxuICAgIGlmIChzdGF0dXMgPT09ICdmYWlsZWQnKSB7XG4gICAgICBwcm94eS4kZm9yY2VVcGRhdGUoKVxuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgZmlsZS5fX3VwbG9hZGVkID0gc3RhdHVzID09PSAndXBsb2FkZWQnXG4gICAgICA/IGZpbGUuc2l6ZVxuICAgICAgOiB1cGxvYWRlZFNpemVcblxuICAgIGZpbGUuX19wcm9ncmVzcyA9IHN0YXR1cyA9PT0gJ3VwbG9hZGVkJ1xuICAgICAgPyAxXG4gICAgICA6IE1hdGgubWluKDAuOTk5OSwgZmlsZS5fX3VwbG9hZGVkIC8gZmlsZS5zaXplKVxuXG4gICAgZmlsZS5fX3Byb2dyZXNzTGFiZWwgPSBnZXRQcm9ncmVzc0xhYmVsKGZpbGUuX19wcm9ncmVzcylcbiAgICBwcm94eS4kZm9yY2VVcGRhdGUoKVxuICB9XG5cbiAgY29uc3Qgc3RhdGUgPSB7XG4gICAgZmlsZXM6IHJlZihbXSksXG4gICAgcXVldWVkRmlsZXM6IHJlZihbXSksXG4gICAgdXBsb2FkZWRGaWxlczogcmVmKFtdKSxcbiAgICB1cGxvYWRlZFNpemU6IHJlZigwKSxcblxuICAgIHVwZGF0ZUZpbGVTdGF0dXMsXG4gICAgaXNBbGl2ZSAoKSB7XG4gICAgICByZXR1cm4gdm0uaXNEZWFjdGl2YXRlZCAhPT0gdHJ1ZSAmJiB2bS5pc1VubW91bnRlZCAhPT0gdHJ1ZVxuICAgIH1cbiAgfVxuXG4gIE9iamVjdC5hc3NpZ24oc3RhdGUsIGdldFBsdWdpbih7IHByb3BzLCBzbG90cywgZW1pdCwgaGVscGVyczogc3RhdGUgfSkpXG5cbiAgY29uc3QgdXBsb2FkU2l6ZSA9IHJlZigwKVxuICBjb25zdCBlZGl0YWJsZSA9IGNvbXB1dGVkKCgpID0+IHByb3BzLmRpc2FibGUgIT09IHRydWUgJiYgcHJvcHMucmVhZG9ubHkgIT09IHRydWUpXG5cbiAgaWYgKHN0YXRlLmlzQnVzeSA9PT0gdm9pZCAwKSB7XG4gICAgc3RhdGUuaXNCdXN5ID0gcmVmKGZhbHNlKVxuICB9XG5cbiAgY29uc3QgZG5kID0gcmVmKGZhbHNlKVxuXG4gIGNvbnN0IHJvb3RSZWYgPSByZWYobnVsbClcbiAgY29uc3QgaW5wdXRSZWYgPSByZWYobnVsbClcblxuICBwcm92aWRlKHVwbG9hZGVyS2V5LCByZW5kZXJJbnB1dClcblxuICBjb25zdCB7XG4gICAgcGlja0ZpbGVzLFxuICAgIGFkZEZpbGVzLFxuICAgIG9uRHJhZ292ZXIsXG4gICAgb25EcmFnbGVhdmUsXG4gICAgcHJvY2Vzc0ZpbGVzLFxuICAgIGdldERuZE5vZGUsXG4gICAgbWF4RmlsZXNOdW1iZXIsXG4gICAgbWF4VG90YWxTaXplTnVtYmVyXG4gIH0gPSB1c2VGaWxlKHsgZWRpdGFibGUsIGRuZCwgZ2V0RmlsZUlucHV0LCBhZGRGaWxlc1RvUXVldWUgfSlcblxuICBjb25zdCBjYW5BZGRGaWxlcyA9IGNvbXB1dGVkKCgpID0+XG4gICAgZWRpdGFibGUudmFsdWUgPT09IHRydWVcbiAgICAmJiBzdGF0ZS5pc1VwbG9hZGluZy52YWx1ZSAhPT0gdHJ1ZVxuICAgIC8vIGlmIHNpbmdsZSBzZWxlY3Rpb24gYW5kIG5vIGZpbGVzIGFyZSBxdWV1ZWQ6XG4gICAgJiYgKHByb3BzLm11bHRpcGxlID09PSB0cnVlIHx8IHN0YXRlLnF1ZXVlZEZpbGVzLnZhbHVlLmxlbmd0aCA9PT0gMClcbiAgICAvLyBpZiBtYXgtZmlsZXMgaXMgc2V0IGFuZCBjdXJyZW50IG51bWJlciBvZiBmaWxlcyBkb2VzIG5vdCBleGNlZWRzIGl0OlxuICAgICYmIChwcm9wcy5tYXhGaWxlcyA9PT0gdm9pZCAwIHx8IHN0YXRlLmZpbGVzLnZhbHVlLmxlbmd0aCA8IG1heEZpbGVzTnVtYmVyLnZhbHVlKVxuICAgIC8vIGlmIG1heC10b3RhbC1zaXplIGlzIHNldCBhbmQgY3VycmVudCB1cGxvYWQgc2l6ZSBkb2VzIG5vdCBleGNlZWRzIGl0OlxuICAgICYmIChwcm9wcy5tYXhUb3RhbFNpemUgPT09IHZvaWQgMCB8fCB1cGxvYWRTaXplLnZhbHVlIDwgbWF4VG90YWxTaXplTnVtYmVyLnZhbHVlKVxuICApXG5cbiAgY29uc3QgY2FuVXBsb2FkID0gY29tcHV0ZWQoKCkgPT5cbiAgICBlZGl0YWJsZS52YWx1ZSA9PT0gdHJ1ZVxuICAgICYmIHN0YXRlLmlzQnVzeS52YWx1ZSAhPT0gdHJ1ZVxuICAgICYmIHN0YXRlLmlzVXBsb2FkaW5nLnZhbHVlICE9PSB0cnVlXG4gICAgJiYgc3RhdGUucXVldWVkRmlsZXMudmFsdWUubGVuZ3RoID4gMFxuICApXG5cbiAgY29uc3QgdXBsb2FkUHJvZ3Jlc3MgPSBjb21wdXRlZCgoKSA9PiAoXG4gICAgdXBsb2FkU2l6ZS52YWx1ZSA9PT0gMFxuICAgICAgPyAwXG4gICAgICA6IHN0YXRlLnVwbG9hZGVkU2l6ZS52YWx1ZSAvIHVwbG9hZFNpemUudmFsdWVcbiAgKSlcblxuICBjb25zdCB1cGxvYWRQcm9ncmVzc0xhYmVsID0gY29tcHV0ZWQoKCkgPT4gZ2V0UHJvZ3Jlc3NMYWJlbCh1cGxvYWRQcm9ncmVzcy52YWx1ZSkpXG4gIGNvbnN0IHVwbG9hZFNpemVMYWJlbCA9IGNvbXB1dGVkKCgpID0+IGh1bWFuU3RvcmFnZVNpemUodXBsb2FkU2l6ZS52YWx1ZSkpXG5cbiAgY29uc3QgY2xhc3NlcyA9IGNvbXB1dGVkKCgpID0+XG4gICAgJ3EtdXBsb2FkZXIgY29sdW1uIG5vLXdyYXAnXG4gICAgKyAoaXNEYXJrLnZhbHVlID09PSB0cnVlID8gJyBxLXVwbG9hZGVyLS1kYXJrIHEtZGFyaycgOiAnJylcbiAgICArIChwcm9wcy5ib3JkZXJlZCA9PT0gdHJ1ZSA/ICcgcS11cGxvYWRlci0tYm9yZGVyZWQnIDogJycpXG4gICAgKyAocHJvcHMuc3F1YXJlID09PSB0cnVlID8gJyBxLXVwbG9hZGVyLS1zcXVhcmUgbm8tYm9yZGVyLXJhZGl1cycgOiAnJylcbiAgICArIChwcm9wcy5mbGF0ID09PSB0cnVlID8gJyBxLXVwbG9hZGVyLS1mbGF0IG5vLXNoYWRvdycgOiAnJylcbiAgICArIChwcm9wcy5kaXNhYmxlID09PSB0cnVlID8gJyBkaXNhYmxlZCBxLXVwbG9hZGVyLS1kaXNhYmxlJyA6ICcnKVxuICApXG5cbiAgY29uc3QgY29sb3JDbGFzcyA9IGNvbXB1dGVkKCgpID0+XG4gICAgJ3EtdXBsb2FkZXJfX2hlYWRlcidcbiAgICArIChwcm9wcy5jb2xvciAhPT0gdm9pZCAwID8gYCBiZy0keyBwcm9wcy5jb2xvciB9YCA6ICcnKVxuICAgICsgKHByb3BzLnRleHRDb2xvciAhPT0gdm9pZCAwID8gYCB0ZXh0LSR7IHByb3BzLnRleHRDb2xvciB9YCA6ICcnKVxuICApXG5cbiAgd2F0Y2goc3RhdGUuaXNVcGxvYWRpbmcsIChuZXdWYWwsIG9sZFZhbCkgPT4ge1xuICAgIGlmIChvbGRWYWwgPT09IGZhbHNlICYmIG5ld1ZhbCA9PT0gdHJ1ZSkge1xuICAgICAgZW1pdCgnc3RhcnQnKVxuICAgIH1cbiAgICBlbHNlIGlmIChvbGRWYWwgPT09IHRydWUgJiYgbmV3VmFsID09PSBmYWxzZSkge1xuICAgICAgZW1pdCgnZmluaXNoJylcbiAgICB9XG4gIH0pXG5cbiAgZnVuY3Rpb24gcmVzZXQgKCkge1xuICAgIGlmIChwcm9wcy5kaXNhYmxlID09PSBmYWxzZSkge1xuICAgICAgc3RhdGUuYWJvcnQoKVxuICAgICAgc3RhdGUudXBsb2FkZWRTaXplLnZhbHVlID0gMFxuICAgICAgdXBsb2FkU2l6ZS52YWx1ZSA9IDBcbiAgICAgIHJldm9rZUltZ1VSTHMoKVxuICAgICAgc3RhdGUuZmlsZXMudmFsdWUgPSBbXVxuICAgICAgc3RhdGUucXVldWVkRmlsZXMudmFsdWUgPSBbXVxuICAgICAgc3RhdGUudXBsb2FkZWRGaWxlcy52YWx1ZSA9IFtdXG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gcmVtb3ZlVXBsb2FkZWRGaWxlcyAoKSB7XG4gICAgaWYgKHByb3BzLmRpc2FibGUgPT09IGZhbHNlKSB7XG4gICAgICBiYXRjaFJlbW92ZUZpbGVzKFsgJ3VwbG9hZGVkJyBdLCAoKSA9PiB7XG4gICAgICAgIHN0YXRlLnVwbG9hZGVkRmlsZXMudmFsdWUgPSBbXVxuICAgICAgfSlcbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiByZW1vdmVRdWV1ZWRGaWxlcyAoKSB7XG4gICAgYmF0Y2hSZW1vdmVGaWxlcyhbICdpZGxlJywgJ2ZhaWxlZCcgXSwgKHsgc2l6ZSB9KSA9PiB7XG4gICAgICB1cGxvYWRTaXplLnZhbHVlIC09IHNpemVcbiAgICAgIHN0YXRlLnF1ZXVlZEZpbGVzLnZhbHVlID0gW11cbiAgICB9KVxuICB9XG5cbiAgZnVuY3Rpb24gYmF0Y2hSZW1vdmVGaWxlcyAoc3RhdHVzTGlzdCwgY2IpIHtcbiAgICBpZiAocHJvcHMuZGlzYWJsZSA9PT0gdHJ1ZSkge1xuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgY29uc3QgcmVtb3ZlZCA9IHtcbiAgICAgIGZpbGVzOiBbXSxcbiAgICAgIHNpemU6IDBcbiAgICB9XG5cbiAgICBjb25zdCBsb2NhbEZpbGVzID0gc3RhdGUuZmlsZXMudmFsdWUuZmlsdGVyKGYgPT4ge1xuICAgICAgaWYgKHN0YXR1c0xpc3QuaW5kZXhPZihmLl9fc3RhdHVzKSA9PT0gLTEpIHtcbiAgICAgICAgcmV0dXJuIHRydWVcbiAgICAgIH1cblxuICAgICAgcmVtb3ZlZC5zaXplICs9IGYuc2l6ZVxuICAgICAgcmVtb3ZlZC5maWxlcy5wdXNoKGYpXG5cbiAgICAgIGYuX19pbWcgIT09IHZvaWQgMCAmJiB3aW5kb3cuVVJMLnJldm9rZU9iamVjdFVSTChmLl9faW1nLnNyYylcblxuICAgICAgcmV0dXJuIGZhbHNlXG4gICAgfSlcblxuICAgIGlmIChyZW1vdmVkLmZpbGVzLmxlbmd0aCA+IDApIHtcbiAgICAgIHN0YXRlLmZpbGVzLnZhbHVlID0gbG9jYWxGaWxlc1xuICAgICAgY2IocmVtb3ZlZClcbiAgICAgIGVtaXQoJ3JlbW92ZWQnLCByZW1vdmVkLmZpbGVzKVxuICAgIH1cbiAgfVxuXG4gIGZ1bmN0aW9uIHJlbW92ZUZpbGUgKGZpbGUpIHtcbiAgICBpZiAocHJvcHMuZGlzYWJsZSkgeyByZXR1cm4gfVxuXG4gICAgaWYgKGZpbGUuX19zdGF0dXMgPT09ICd1cGxvYWRlZCcpIHtcbiAgICAgIHN0YXRlLnVwbG9hZGVkRmlsZXMudmFsdWUgPSBzdGF0ZS51cGxvYWRlZEZpbGVzLnZhbHVlLmZpbHRlcihmID0+IGYuX19rZXkgIT09IGZpbGUuX19rZXkpXG4gICAgfVxuICAgIGVsc2UgaWYgKGZpbGUuX19zdGF0dXMgPT09ICd1cGxvYWRpbmcnKSB7XG4gICAgICBmaWxlLl9fYWJvcnQoKVxuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgIHVwbG9hZFNpemUudmFsdWUgLT0gZmlsZS5zaXplXG4gICAgfVxuXG4gICAgc3RhdGUuZmlsZXMudmFsdWUgPSBzdGF0ZS5maWxlcy52YWx1ZS5maWx0ZXIoZiA9PiB7XG4gICAgICBpZiAoZi5fX2tleSAhPT0gZmlsZS5fX2tleSkge1xuICAgICAgICByZXR1cm4gdHJ1ZVxuICAgICAgfVxuXG4gICAgICBmLl9faW1nICE9PSB2b2lkIDAgJiYgd2luZG93LlVSTC5yZXZva2VPYmplY3RVUkwoZi5fX2ltZy5zcmMpXG5cbiAgICAgIHJldHVybiBmYWxzZVxuICAgIH0pXG5cbiAgICBzdGF0ZS5xdWV1ZWRGaWxlcy52YWx1ZSA9IHN0YXRlLnF1ZXVlZEZpbGVzLnZhbHVlLmZpbHRlcihmID0+IGYuX19rZXkgIT09IGZpbGUuX19rZXkpXG4gICAgZW1pdCgncmVtb3ZlZCcsIFsgZmlsZSBdKVxuICB9XG5cbiAgZnVuY3Rpb24gcmV2b2tlSW1nVVJMcyAoKSB7XG4gICAgc3RhdGUuZmlsZXMudmFsdWUuZm9yRWFjaChmID0+IHtcbiAgICAgIGYuX19pbWcgIT09IHZvaWQgMCAmJiB3aW5kb3cuVVJMLnJldm9rZU9iamVjdFVSTChmLl9faW1nLnNyYylcbiAgICB9KVxuICB9XG5cbiAgZnVuY3Rpb24gZ2V0RmlsZUlucHV0ICgpIHtcbiAgICByZXR1cm4gaW5wdXRSZWYudmFsdWVcbiAgICAgIHx8IHJvb3RSZWYudmFsdWUuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgncS11cGxvYWRlcl9faW5wdXQnKVsgMCBdXG4gIH1cblxuICBmdW5jdGlvbiBhZGRGaWxlc1RvUXVldWUgKGUsIGZpbGVMaXN0KSB7XG4gICAgY29uc3QgbG9jYWxGaWxlcyA9IHByb2Nlc3NGaWxlcyhlLCBmaWxlTGlzdCwgc3RhdGUuZmlsZXMudmFsdWUsIHRydWUpXG5cbiAgICBpZiAobG9jYWxGaWxlcyA9PT0gdm9pZCAwKSB7IHJldHVybiB9XG5cbiAgICBjb25zdCBmaWxlSW5wdXQgPSBnZXRGaWxlSW5wdXQoKVxuICAgIGlmIChmaWxlSW5wdXQgIT09IHZvaWQgMCAmJiBmaWxlSW5wdXQgIT09IG51bGwpIHtcbiAgICAgIGZpbGVJbnB1dC52YWx1ZSA9ICcnXG4gICAgfVxuXG4gICAgbG9jYWxGaWxlcy5mb3JFYWNoKGZpbGUgPT4ge1xuICAgICAgc3RhdGUudXBkYXRlRmlsZVN0YXR1cyhmaWxlLCAnaWRsZScpXG4gICAgICB1cGxvYWRTaXplLnZhbHVlICs9IGZpbGUuc2l6ZVxuXG4gICAgICBpZiAocHJvcHMubm9UaHVtYm5haWxzICE9PSB0cnVlICYmIGZpbGUudHlwZS50b1VwcGVyQ2FzZSgpLnN0YXJ0c1dpdGgoJ0lNQUdFJykpIHtcbiAgICAgICAgY29uc3QgaW1nID0gbmV3IEltYWdlKClcbiAgICAgICAgaW1nLnNyYyA9IHdpbmRvdy5VUkwuY3JlYXRlT2JqZWN0VVJMKGZpbGUpXG4gICAgICAgIGZpbGUuX19pbWcgPSBpbWdcbiAgICAgIH1cbiAgICB9KVxuXG4gICAgc3RhdGUuZmlsZXMudmFsdWUgPSBzdGF0ZS5maWxlcy52YWx1ZS5jb25jYXQobG9jYWxGaWxlcylcbiAgICBzdGF0ZS5xdWV1ZWRGaWxlcy52YWx1ZSA9IHN0YXRlLnF1ZXVlZEZpbGVzLnZhbHVlLmNvbmNhdChsb2NhbEZpbGVzKVxuICAgIGVtaXQoJ2FkZGVkJywgbG9jYWxGaWxlcylcbiAgICBwcm9wcy5hdXRvVXBsb2FkID09PSB0cnVlICYmIHN0YXRlLnVwbG9hZCgpXG4gIH1cblxuICBmdW5jdGlvbiB1cGxvYWQgKCkge1xuICAgIGNhblVwbG9hZC52YWx1ZSA9PT0gdHJ1ZSAmJiBzdGF0ZS51cGxvYWQoKVxuICB9XG5cbiAgZnVuY3Rpb24gZ2V0QnRuIChzaG93LCBpY29uLCBmbikge1xuICAgIGlmIChzaG93ID09PSB0cnVlKSB7XG4gICAgICBjb25zdCBkYXRhID0ge1xuICAgICAgICB0eXBlOiAnYScsXG4gICAgICAgIGtleTogaWNvbixcbiAgICAgICAgaWNvbjogJHEuaWNvblNldC51cGxvYWRlclsgaWNvbiBdLFxuICAgICAgICBmbGF0OiB0cnVlLFxuICAgICAgICBkZW5zZTogdHJ1ZVxuICAgICAgfVxuXG4gICAgICBsZXQgY2hpbGQgPSB2b2lkIDBcblxuICAgICAgaWYgKGljb24gPT09ICdhZGQnKSB7XG4gICAgICAgIGRhdGEub25DbGljayA9IHBpY2tGaWxlc1xuICAgICAgICBjaGlsZCA9IHJlbmRlcklucHV0XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgZGF0YS5vbkNsaWNrID0gZm5cbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGgoUUJ0biwgZGF0YSwgY2hpbGQpXG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gcmVuZGVySW5wdXQgKCkge1xuICAgIHJldHVybiBoKCdpbnB1dCcsIHtcbiAgICAgIHJlZjogaW5wdXRSZWYsXG4gICAgICBjbGFzczogJ3EtdXBsb2FkZXJfX2lucHV0IG92ZXJmbG93LWhpZGRlbiBhYnNvbHV0ZS1mdWxsJyxcbiAgICAgIHRhYmluZGV4OiAtMSxcbiAgICAgIHR5cGU6ICdmaWxlJyxcbiAgICAgIHRpdGxlOiAnJywgLy8gdHJ5IHRvIHJlbW92ZSBkZWZhdWx0IHRvb2x0aXBcbiAgICAgIGFjY2VwdDogcHJvcHMuYWNjZXB0LFxuICAgICAgbXVsdGlwbGU6IHByb3BzLm11bHRpcGxlID09PSB0cnVlID8gJ211bHRpcGxlJyA6IHZvaWQgMCxcbiAgICAgIGNhcHR1cmU6IHByb3BzLmNhcHR1cmUsXG4gICAgICBvbk1vdXNlZG93bjogc3RvcCwgLy8gbmVlZCB0byBzdG9wIHJlZm9jdXMgZnJvbSBRQnRuXG4gICAgICBvbkNsaWNrOiBwaWNrRmlsZXMsXG4gICAgICBvbkNoYW5nZTogYWRkRmlsZXNUb1F1ZXVlXG4gICAgfSlcbiAgfVxuXG4gIGZ1bmN0aW9uIGdldEhlYWRlciAoKSB7XG4gICAgaWYgKHNsb3RzLmhlYWRlciAhPT0gdm9pZCAwKSB7XG4gICAgICByZXR1cm4gc2xvdHMuaGVhZGVyKHNsb3RTY29wZS52YWx1ZSlcbiAgICB9XG5cbiAgICByZXR1cm4gW1xuICAgICAgaCgnZGl2Jywge1xuICAgICAgICBjbGFzczogJ3EtdXBsb2FkZXJfX2hlYWRlci1jb250ZW50IGZsZXggZmxleC1jZW50ZXIgbm8td3JhcCBxLWd1dHRlci14cydcbiAgICAgIH0sIFtcbiAgICAgICAgZ2V0QnRuKHN0YXRlLnF1ZXVlZEZpbGVzLnZhbHVlLmxlbmd0aCA+IDAsICdyZW1vdmVRdWV1ZScsIHJlbW92ZVF1ZXVlZEZpbGVzKSxcbiAgICAgICAgZ2V0QnRuKHN0YXRlLnVwbG9hZGVkRmlsZXMudmFsdWUubGVuZ3RoID4gMCwgJ3JlbW92ZVVwbG9hZGVkJywgcmVtb3ZlVXBsb2FkZWRGaWxlcyksXG5cbiAgICAgICAgc3RhdGUuaXNVcGxvYWRpbmcudmFsdWUgPT09IHRydWVcbiAgICAgICAgICA/IGgoUVNwaW5uZXIsIHsgY2xhc3M6ICdxLXVwbG9hZGVyX19zcGlubmVyJyB9KVxuICAgICAgICAgIDogbnVsbCxcblxuICAgICAgICBoKCdkaXYnLCB7IGNsYXNzOiAnY29sIGNvbHVtbiBqdXN0aWZ5LWNlbnRlcicgfSwgW1xuICAgICAgICAgIHByb3BzLmxhYmVsICE9PSB2b2lkIDBcbiAgICAgICAgICAgID8gaCgnZGl2JywgeyBjbGFzczogJ3EtdXBsb2FkZXJfX3RpdGxlJyB9LCBbIHByb3BzLmxhYmVsIF0pXG4gICAgICAgICAgICA6IG51bGwsXG5cbiAgICAgICAgICBoKCdkaXYnLCB7IGNsYXNzOiAncS11cGxvYWRlcl9fc3VidGl0bGUnIH0sIFtcbiAgICAgICAgICAgIHVwbG9hZFNpemVMYWJlbC52YWx1ZSArICcgLyAnICsgdXBsb2FkUHJvZ3Jlc3NMYWJlbC52YWx1ZVxuICAgICAgICAgIF0pXG4gICAgICAgIF0pLFxuXG4gICAgICAgIGdldEJ0bihjYW5BZGRGaWxlcy52YWx1ZSwgJ2FkZCcpLFxuICAgICAgICBnZXRCdG4ocHJvcHMuaGlkZVVwbG9hZEJ0biA9PT0gZmFsc2UgJiYgY2FuVXBsb2FkLnZhbHVlID09PSB0cnVlLCAndXBsb2FkJywgc3RhdGUudXBsb2FkKSxcbiAgICAgICAgZ2V0QnRuKHN0YXRlLmlzVXBsb2FkaW5nLnZhbHVlLCAnY2xlYXInLCBzdGF0ZS5hYm9ydClcbiAgICAgIF0pXG4gICAgXVxuICB9XG5cbiAgZnVuY3Rpb24gZ2V0TGlzdCAoKSB7XG4gICAgaWYgKHNsb3RzLmxpc3QgIT09IHZvaWQgMCkge1xuICAgICAgcmV0dXJuIHNsb3RzLmxpc3Qoc2xvdFNjb3BlLnZhbHVlKVxuICAgIH1cblxuICAgIHJldHVybiBzdGF0ZS5maWxlcy52YWx1ZS5tYXAoZmlsZSA9PiBoKCdkaXYnLCB7XG4gICAgICBrZXk6IGZpbGUuX19rZXksXG4gICAgICBjbGFzczogJ3EtdXBsb2FkZXJfX2ZpbGUgcmVsYXRpdmUtcG9zaXRpb24nXG4gICAgICAgICsgKHByb3BzLm5vVGh1bWJuYWlscyAhPT0gdHJ1ZSAmJiBmaWxlLl9faW1nICE9PSB2b2lkIDAgPyAnIHEtdXBsb2FkZXJfX2ZpbGUtLWltZycgOiAnJylcbiAgICAgICAgKyAoXG4gICAgICAgICAgZmlsZS5fX3N0YXR1cyA9PT0gJ2ZhaWxlZCdcbiAgICAgICAgICAgID8gJyBxLXVwbG9hZGVyX19maWxlLS1mYWlsZWQnXG4gICAgICAgICAgICA6IChmaWxlLl9fc3RhdHVzID09PSAndXBsb2FkZWQnID8gJyBxLXVwbG9hZGVyX19maWxlLS11cGxvYWRlZCcgOiAnJylcbiAgICAgICAgKSxcbiAgICAgIHN0eWxlOiBwcm9wcy5ub1RodW1ibmFpbHMgIT09IHRydWUgJiYgZmlsZS5fX2ltZyAhPT0gdm9pZCAwXG4gICAgICAgID8geyBiYWNrZ3JvdW5kSW1hZ2U6ICd1cmwoXCInICsgZmlsZS5fX2ltZy5zcmMgKyAnXCIpJyB9XG4gICAgICAgIDogbnVsbFxuICAgIH0sIFtcbiAgICAgIGgoJ2RpdicsIHtcbiAgICAgICAgY2xhc3M6ICdxLXVwbG9hZGVyX19maWxlLWhlYWRlciByb3cgZmxleC1jZW50ZXIgbm8td3JhcCdcbiAgICAgIH0sIFtcbiAgICAgICAgZmlsZS5fX3N0YXR1cyA9PT0gJ2ZhaWxlZCdcbiAgICAgICAgICA/IGgoUUljb24sIHtcbiAgICAgICAgICAgIGNsYXNzOiAncS11cGxvYWRlcl9fZmlsZS1zdGF0dXMnLFxuICAgICAgICAgICAgbmFtZTogJHEuaWNvblNldC50eXBlLm5lZ2F0aXZlLFxuICAgICAgICAgICAgY29sb3I6ICduZWdhdGl2ZSdcbiAgICAgICAgICB9KVxuICAgICAgICAgIDogbnVsbCxcblxuICAgICAgICBoKCdkaXYnLCB7IGNsYXNzOiAncS11cGxvYWRlcl9fZmlsZS1oZWFkZXItY29udGVudCBjb2wnIH0sIFtcbiAgICAgICAgICBoKCdkaXYnLCB7IGNsYXNzOiAncS11cGxvYWRlcl9fdGl0bGUnIH0sIFsgZmlsZS5uYW1lIF0pLFxuICAgICAgICAgIGgoJ2RpdicsIHtcbiAgICAgICAgICAgIGNsYXNzOiAncS11cGxvYWRlcl9fc3VidGl0bGUgcm93IGl0ZW1zLWNlbnRlciBuby13cmFwJ1xuICAgICAgICAgIH0sIFtcbiAgICAgICAgICAgIGZpbGUuX19zaXplTGFiZWwgKyAnIC8gJyArIGZpbGUuX19wcm9ncmVzc0xhYmVsXG4gICAgICAgICAgXSlcbiAgICAgICAgXSksXG5cbiAgICAgICAgZmlsZS5fX3N0YXR1cyA9PT0gJ3VwbG9hZGluZydcbiAgICAgICAgICA/IGgoUUNpcmN1bGFyUHJvZ3Jlc3MsIHtcbiAgICAgICAgICAgIHZhbHVlOiBmaWxlLl9fcHJvZ3Jlc3MsXG4gICAgICAgICAgICBtaW46IDAsXG4gICAgICAgICAgICBtYXg6IDEsXG4gICAgICAgICAgICBpbmRldGVybWluYXRlOiBmaWxlLl9fcHJvZ3Jlc3MgPT09IDBcbiAgICAgICAgICB9KVxuICAgICAgICAgIDogaChRQnRuLCB7XG4gICAgICAgICAgICByb3VuZDogdHJ1ZSxcbiAgICAgICAgICAgIGRlbnNlOiB0cnVlLFxuICAgICAgICAgICAgZmxhdDogdHJ1ZSxcbiAgICAgICAgICAgIGljb246ICRxLmljb25TZXQudXBsb2FkZXJbIGZpbGUuX19zdGF0dXMgPT09ICd1cGxvYWRlZCcgPyAnZG9uZScgOiAnY2xlYXInIF0sXG4gICAgICAgICAgICBvbkNsaWNrOiAoKSA9PiB7IHJlbW92ZUZpbGUoZmlsZSkgfVxuICAgICAgICAgIH0pXG4gICAgICBdKVxuICAgIF0pKVxuICB9XG5cbiAgb25CZWZvcmVVbm1vdW50KCgpID0+IHtcbiAgICBzdGF0ZS5pc1VwbG9hZGluZy52YWx1ZSA9PT0gdHJ1ZSAmJiBzdGF0ZS5hYm9ydCgpXG4gICAgc3RhdGUuZmlsZXMudmFsdWUubGVuZ3RoID4gMCAmJiByZXZva2VJbWdVUkxzKClcbiAgfSlcblxuICBjb25zdCBwdWJsaWNNZXRob2RzID0ge1xuICAgIHBpY2tGaWxlcyxcbiAgICBhZGRGaWxlcyxcbiAgICByZXNldCxcbiAgICByZW1vdmVVcGxvYWRlZEZpbGVzLFxuICAgIHJlbW92ZVF1ZXVlZEZpbGVzLFxuICAgIHJlbW92ZUZpbGUsXG4gICAgdXBsb2FkLFxuICAgIGFib3J0OiBzdGF0ZS5hYm9ydFxuICB9XG5cbiAgLy8gVE9ETzogdGhlIHJlc3VsdCBvZiB0aGlzIGNvbXB1dGVkLCBlc3BlY2lhbGx5IHRoZSBkeW5hbWljIHBhcnQsIGlzbid0IGN1cnJlbnRseSB0eXBlZFxuICAvLyBUaGlzIHJlc3VsdCBpbiBhbiBlcnJvciB3aXRoIFZvbGFyIHdoZW4gYWNjZXNzaW5nIHRoZSBzdGF0ZSAoZWcuIGZpbGVzIGFycmF5KVxuICBjb25zdCBzbG90U2NvcGUgPSBjb21wdXRlZCgoKSA9PiB7XG4gICAgY29uc3QgYWNjID0ge1xuICAgICAgY2FuQWRkRmlsZXM6IGNhbkFkZEZpbGVzLnZhbHVlLFxuICAgICAgY2FuVXBsb2FkOiBjYW5VcGxvYWQudmFsdWUsXG4gICAgICB1cGxvYWRTaXplTGFiZWw6IHVwbG9hZFNpemVMYWJlbC52YWx1ZSxcbiAgICAgIHVwbG9hZFByb2dyZXNzTGFiZWw6IHVwbG9hZFByb2dyZXNzTGFiZWwudmFsdWVcbiAgICB9XG5cbiAgICBmb3IgKGNvbnN0IGtleSBpbiBzdGF0ZSkge1xuICAgICAgYWNjWyBrZXkgXSA9IGlzUmVmKHN0YXRlWyBrZXkgXSkgPT09IHRydWVcbiAgICAgICAgPyBzdGF0ZVsga2V5IF0udmFsdWVcbiAgICAgICAgOiBzdGF0ZVsga2V5IF1cbiAgICB9XG5cbiAgICAvLyBUT0RPOiAoUXYzKSBQdXQgdGhlIFFVcGxvYWRlciBpbnN0YW5jZSB1bmRlciBgcmVmYFxuICAgIC8vIHByb3BlcnR5IGZvciBjb25zaXN0ZW5jeSBhbmQgZmxleGliaWxpdHlcbiAgICAvLyByZXR1cm4geyByZWY6IHsgLi4uYWNjLCAuLi5wdWJsaWNNZXRob2RzIH0gfVxuICAgIHJldHVybiB7IC4uLmFjYywgLi4ucHVibGljTWV0aG9kcyB9XG4gIH0pXG5cbiAgLy8gZXhwb3NlIHB1YmxpYyBtZXRob2RzXG4gIE9iamVjdC5hc3NpZ24ocHJveHksIHB1YmxpY01ldGhvZHMpXG5cbiAgcmV0dXJuICgpID0+IHtcbiAgICBjb25zdCBjaGlsZHJlbiA9IFtcbiAgICAgIGgoJ2RpdicsIHsgY2xhc3M6IGNvbG9yQ2xhc3MudmFsdWUgfSwgZ2V0SGVhZGVyKCkpLFxuICAgICAgaCgnZGl2JywgeyBjbGFzczogJ3EtdXBsb2FkZXJfX2xpc3Qgc2Nyb2xsJyB9LCBnZXRMaXN0KCkpLFxuICAgICAgZ2V0RG5kTm9kZSgndXBsb2FkZXInKVxuICAgIF1cblxuICAgIHN0YXRlLmlzQnVzeS52YWx1ZSA9PT0gdHJ1ZSAmJiBjaGlsZHJlbi5wdXNoKFxuICAgICAgaCgnZGl2Jywge1xuICAgICAgICBjbGFzczogJ3EtdXBsb2FkZXJfX292ZXJsYXkgYWJzb2x1dGUtZnVsbCBmbGV4IGZsZXgtY2VudGVyJ1xuICAgICAgfSwgWyBoKFFTcGlubmVyKSBdKVxuICAgIClcblxuICAgIGNvbnN0IGRhdGEgPSB7IHJlZjogcm9vdFJlZiwgY2xhc3M6IGNsYXNzZXMudmFsdWUgfVxuXG4gICAgaWYgKGNhbkFkZEZpbGVzLnZhbHVlID09PSB0cnVlKSB7XG4gICAgICBPYmplY3QuYXNzaWduKGRhdGEsIHsgb25EcmFnb3Zlciwgb25EcmFnbGVhdmUgfSlcbiAgICB9XG5cbiAgICByZXR1cm4gaCgnZGl2JywgZGF0YSwgY2hpbGRyZW4pXG4gIH1cbn1cbiIsImNvbnN0IHRydWVGbiA9ICgpID0+IHRydWVcblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gKGVtaXRzQXJyYXkpIHtcbiAgY29uc3QgZW1pdHNPYmplY3QgPSB7fVxuXG4gIGVtaXRzQXJyYXkuZm9yRWFjaCh2YWwgPT4ge1xuICAgIGVtaXRzT2JqZWN0WyB2YWwgXSA9IHRydWVGblxuICB9KVxuXG4gIHJldHVybiBlbWl0c09iamVjdFxufVxuIiwiaW1wb3J0IHsgY29yZVByb3BzLCBjb3JlRW1pdHMsIGdldFJlbmRlcmVyIH0gZnJvbSAnLi4vY29tcG9uZW50cy91cGxvYWRlci91cGxvYWRlci1jb3JlLmpzJ1xuXG5pbXBvcnQgeyBjcmVhdGVDb21wb25lbnQgfSBmcm9tICcuL3ByaXZhdGUvY3JlYXRlLmpzJ1xuaW1wb3J0IGdldEVtaXRzT2JqZWN0IGZyb20gJy4vcHJpdmF0ZS9nZXQtZW1pdHMtb2JqZWN0LmpzJ1xuaW1wb3J0IHsgaXNPYmplY3QgfSBmcm9tICcuL3ByaXZhdGUvaXMuanMnXG5cbmNvbnN0IGNvcmVFbWl0c09iamVjdCA9IGdldEVtaXRzT2JqZWN0KGNvcmVFbWl0cylcblxuZXhwb3J0IGRlZmF1bHQgKHsgbmFtZSwgcHJvcHMsIGVtaXRzLCBpbmplY3RQbHVnaW4gfSkgPT4gY3JlYXRlQ29tcG9uZW50KHtcbiAgbmFtZSxcblxuICBwcm9wczoge1xuICAgIC4uLmNvcmVQcm9wcyxcbiAgICAuLi5wcm9wc1xuICB9LFxuXG4gIGVtaXRzOiBpc09iamVjdChlbWl0cykgPT09IHRydWVcbiAgICA/IHsgLi4uY29yZUVtaXRzT2JqZWN0LCAuLi5lbWl0cyB9XG4gICAgOiBbIC4uLmNvcmVFbWl0cywgLi4uZW1pdHMgXSxcblxuICBzZXR1cCAoKSB7XG4gICAgcmV0dXJuIGdldFJlbmRlcmVyKGluamVjdFBsdWdpbilcbiAgfVxufSlcbiIsIi8qKlxuICogQmFzZWQgb24gdGhlIHdvcmsgb2YgaHR0cHM6Ly9naXRodWIuY29tL2pjaG9vay91dWlkLXJhbmRvbVxuICovXG5cbmxldFxuICBidWYsXG4gIGJ1ZklkeCA9IDBcbmNvbnN0IGhleEJ5dGVzID0gbmV3IEFycmF5KDI1NilcblxuLy8gUHJlLWNhbGN1bGF0ZSB0b1N0cmluZygxNikgZm9yIHNwZWVkXG5mb3IgKGxldCBpID0gMDsgaSA8IDI1NjsgaSsrKSB7XG4gIGhleEJ5dGVzWyBpIF0gPSAoaSArIDB4MTAwKS50b1N0cmluZygxNikuc3Vic3RyaW5nKDEpXG59XG5cbi8vIFVzZSBiZXN0IGF2YWlsYWJsZSBQUk5HXG5jb25zdCByYW5kb21CeXRlcyA9ICgoKSA9PiB7XG4gIC8vIE5vZGUgJiBCcm93c2VyIHN1cHBvcnRcbiAgY29uc3QgbGliID0gdHlwZW9mIGNyeXB0byAhPT0gJ3VuZGVmaW5lZCdcbiAgICA/IGNyeXB0b1xuICAgIDogKFxuICAgICAgICB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJ1xuICAgICAgICAgID8gd2luZG93LmNyeXB0byB8fCB3aW5kb3cubXNDcnlwdG9cbiAgICAgICAgICA6IHZvaWQgMFxuICAgICAgKVxuXG4gIGlmIChsaWIgIT09IHZvaWQgMCkge1xuICAgIGlmIChsaWIucmFuZG9tQnl0ZXMgIT09IHZvaWQgMCkge1xuICAgICAgcmV0dXJuIGxpYi5yYW5kb21CeXRlc1xuICAgIH1cbiAgICBpZiAobGliLmdldFJhbmRvbVZhbHVlcyAhPT0gdm9pZCAwKSB7XG4gICAgICByZXR1cm4gbiA9PiB7XG4gICAgICAgIGNvbnN0IGJ5dGVzID0gbmV3IFVpbnQ4QXJyYXkobilcbiAgICAgICAgbGliLmdldFJhbmRvbVZhbHVlcyhieXRlcylcbiAgICAgICAgcmV0dXJuIGJ5dGVzXG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIG4gPT4ge1xuICAgIGNvbnN0IHIgPSBbXVxuICAgIGZvciAobGV0IGkgPSBuOyBpID4gMDsgaS0tKSB7XG4gICAgICByLnB1c2goTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogMjU2KSlcbiAgICB9XG4gICAgcmV0dXJuIHJcbiAgfVxufSkoKVxuXG4vLyBCdWZmZXIgcmFuZG9tIG51bWJlcnMgZm9yIHNwZWVkXG4vLyBSZWR1Y2UgbWVtb3J5IHVzYWdlIGJ5IGRlY3JlYXNpbmcgdGhpcyBudW1iZXIgKG1pbiAxNilcbi8vIG9yIGltcHJvdmUgc3BlZWQgYnkgaW5jcmVhc2luZyB0aGlzIG51bWJlciAodHJ5IDE2Mzg0KVxuY29uc3QgQlVGRkVSX1NJWkUgPSA0MDk2XG5cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uICgpIHtcbiAgLy8gQnVmZmVyIHNvbWUgcmFuZG9tIGJ5dGVzIGZvciBzcGVlZFxuICBpZiAoYnVmID09PSB2b2lkIDAgfHwgKGJ1ZklkeCArIDE2ID4gQlVGRkVSX1NJWkUpKSB7XG4gICAgYnVmSWR4ID0gMFxuICAgIGJ1ZiA9IHJhbmRvbUJ5dGVzKEJVRkZFUl9TSVpFKVxuICB9XG5cbiAgY29uc3QgYiA9IEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGJ1ZiwgYnVmSWR4LCAoYnVmSWR4ICs9IDE2KSlcbiAgYlsgNiBdID0gKGJbIDYgXSAmIDB4MGYpIHwgMHg0MFxuICBiWyA4IF0gPSAoYlsgOCBdICYgMHgzZikgfCAweDgwXG5cbiAgcmV0dXJuIGhleEJ5dGVzWyBiWyAwIF0gXSArIGhleEJ5dGVzWyBiWyAxIF0gXVxuICAgICsgaGV4Qnl0ZXNbIGJbIDIgXSBdICsgaGV4Qnl0ZXNbIGJbIDMgXSBdICsgJy0nXG4gICAgKyBoZXhCeXRlc1sgYlsgNCBdIF0gKyBoZXhCeXRlc1sgYlsgNSBdIF0gKyAnLSdcbiAgICArIGhleEJ5dGVzWyBiWyA2IF0gXSArIGhleEJ5dGVzWyBiWyA3IF0gXSArICctJ1xuICAgICsgaGV4Qnl0ZXNbIGJbIDggXSBdICsgaGV4Qnl0ZXNbIGJbIDkgXSBdICsgJy0nXG4gICAgKyBoZXhCeXRlc1sgYlsgMTAgXSBdICsgaGV4Qnl0ZXNbIGJbIDExIF0gXVxuICAgICsgaGV4Qnl0ZXNbIGJbIDEyIF0gXSArIGhleEJ5dGVzWyBiWyAxMyBdIF1cbiAgICArIGhleEJ5dGVzWyBiWyAxNCBdIF0gKyBoZXhCeXRlc1sgYlsgMTUgXSBdXG59XG4iLCJpbXBvcnQgeyByZWYsIGNvbXB1dGVkLCB3YXRjaCwgb25CZWZvcmVVbm1vdW50LCBnZXRDdXJyZW50SW5zdGFuY2UgfSBmcm9tICd2dWUnXG5cbmltcG9ydCB1c2VGb3JtQ2hpbGQgZnJvbSAnLi4vdXNlLWZvcm0tY2hpbGQuanMnXG5pbXBvcnQgeyB0ZXN0UGF0dGVybiB9IGZyb20gJy4uLy4uL3V0aWxzL3BhdHRlcm5zLmpzJ1xuaW1wb3J0IHsgZGVib3VuY2UgfSBmcm9tICcuLi8uLi91dGlscy5qcydcbmltcG9ydCB7IGluamVjdFByb3AgfSBmcm9tICcuLi8uLi91dGlscy9wcml2YXRlL2luamVjdC1vYmotcHJvcC5qcydcblxuY29uc3QgbGF6eVJ1bGVzVmFsdWVzID0gWyB0cnVlLCBmYWxzZSwgJ29uZGVtYW5kJyBdXG5cbmV4cG9ydCBjb25zdCB1c2VWYWxpZGF0ZVByb3BzID0ge1xuICBtb2RlbFZhbHVlOiB7fSxcblxuICBlcnJvcjoge1xuICAgIHR5cGU6IEJvb2xlYW4sXG4gICAgZGVmYXVsdDogbnVsbFxuICB9LFxuICBlcnJvck1lc3NhZ2U6IFN0cmluZyxcbiAgbm9FcnJvckljb246IEJvb2xlYW4sXG5cbiAgcnVsZXM6IEFycmF5LFxuICByZWFjdGl2ZVJ1bGVzOiBCb29sZWFuLFxuICBsYXp5UnVsZXM6IHtcbiAgICB0eXBlOiBbIEJvb2xlYW4sIFN0cmluZyBdLFxuICAgIHZhbGlkYXRvcjogdiA9PiBsYXp5UnVsZXNWYWx1ZXMuaW5jbHVkZXModilcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiAoZm9jdXNlZCwgaW5uZXJMb2FkaW5nKSB7XG4gIGNvbnN0IHsgcHJvcHMsIHByb3h5IH0gPSBnZXRDdXJyZW50SW5zdGFuY2UoKVxuXG4gIGNvbnN0IGlubmVyRXJyb3IgPSByZWYoZmFsc2UpXG4gIGNvbnN0IGlubmVyRXJyb3JNZXNzYWdlID0gcmVmKG51bGwpXG4gIGNvbnN0IGlzRGlydHlNb2RlbCA9IHJlZihudWxsKVxuXG4gIHVzZUZvcm1DaGlsZCh7IHZhbGlkYXRlLCByZXNldFZhbGlkYXRpb24gfSlcblxuICBsZXQgdmFsaWRhdGVJbmRleCA9IDAsIHVud2F0Y2hSdWxlc1xuXG4gIGNvbnN0IGhhc1J1bGVzID0gY29tcHV0ZWQoKCkgPT5cbiAgICBwcm9wcy5ydWxlcyAhPT0gdm9pZCAwXG4gICAgJiYgcHJvcHMucnVsZXMgIT09IG51bGxcbiAgICAmJiBwcm9wcy5ydWxlcy5sZW5ndGggPiAwXG4gIClcblxuICBjb25zdCBoYXNBY3RpdmVSdWxlcyA9IGNvbXB1dGVkKCgpID0+XG4gICAgcHJvcHMuZGlzYWJsZSAhPT0gdHJ1ZVxuICAgICYmIGhhc1J1bGVzLnZhbHVlID09PSB0cnVlXG4gIClcblxuICBjb25zdCBoYXNFcnJvciA9IGNvbXB1dGVkKCgpID0+XG4gICAgcHJvcHMuZXJyb3IgPT09IHRydWUgfHwgaW5uZXJFcnJvci52YWx1ZSA9PT0gdHJ1ZVxuICApXG5cbiAgY29uc3QgZXJyb3JNZXNzYWdlID0gY29tcHV0ZWQoKCkgPT4gKFxuICAgIHR5cGVvZiBwcm9wcy5lcnJvck1lc3NhZ2UgPT09ICdzdHJpbmcnICYmIHByb3BzLmVycm9yTWVzc2FnZS5sZW5ndGggPiAwXG4gICAgICA/IHByb3BzLmVycm9yTWVzc2FnZVxuICAgICAgOiBpbm5lckVycm9yTWVzc2FnZS52YWx1ZVxuICApKVxuXG4gIHdhdGNoKCgpID0+IHByb3BzLm1vZGVsVmFsdWUsICgpID0+IHtcbiAgICB2YWxpZGF0ZUlmTmVlZGVkKClcbiAgfSlcblxuICB3YXRjaCgoKSA9PiBwcm9wcy5yZWFjdGl2ZVJ1bGVzLCB2YWwgPT4ge1xuICAgIGlmICh2YWwgPT09IHRydWUpIHtcbiAgICAgIGlmICh1bndhdGNoUnVsZXMgPT09IHZvaWQgMCkge1xuICAgICAgICB1bndhdGNoUnVsZXMgPSB3YXRjaCgoKSA9PiBwcm9wcy5ydWxlcywgKCkgPT4ge1xuICAgICAgICAgIHZhbGlkYXRlSWZOZWVkZWQodHJ1ZSlcbiAgICAgICAgfSlcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSBpZiAodW53YXRjaFJ1bGVzICE9PSB2b2lkIDApIHtcbiAgICAgIHVud2F0Y2hSdWxlcygpXG4gICAgICB1bndhdGNoUnVsZXMgPSB2b2lkIDBcbiAgICB9XG4gIH0sIHsgaW1tZWRpYXRlOiB0cnVlIH0pXG5cbiAgd2F0Y2goZm9jdXNlZCwgdmFsID0+IHtcbiAgICBpZiAodmFsID09PSB0cnVlKSB7XG4gICAgICBpZiAoaXNEaXJ0eU1vZGVsLnZhbHVlID09PSBudWxsKSB7XG4gICAgICAgIGlzRGlydHlNb2RlbC52YWx1ZSA9IGZhbHNlXG4gICAgICB9XG4gICAgfVxuICAgIGVsc2UgaWYgKGlzRGlydHlNb2RlbC52YWx1ZSA9PT0gZmFsc2UpIHtcbiAgICAgIGlzRGlydHlNb2RlbC52YWx1ZSA9IHRydWVcblxuICAgICAgaWYgKFxuICAgICAgICBoYXNBY3RpdmVSdWxlcy52YWx1ZSA9PT0gdHJ1ZVxuICAgICAgICAmJiBwcm9wcy5sYXp5UnVsZXMgIT09ICdvbmRlbWFuZCdcbiAgICAgICAgLy8gRG9uJ3QgcmUtdHJpZ2dlciBpZiBpdCdzIGFscmVhZHkgaW4gcHJvZ3Jlc3M7XG4gICAgICAgIC8vIEl0IG1pZ2h0IG1lYW4gdGhhdCBmb2N1cyBzd2l0Y2hlZCB0byBzdWJtaXQgYnRuIGFuZFxuICAgICAgICAvLyBRRm9ybSdzIHN1Ym1pdCgpIGhhcyBiZWVuIGNhbGxlZCBhbHJlYWR5IChFTlRFUiBrZXkpXG4gICAgICAgICYmIGlubmVyTG9hZGluZy52YWx1ZSA9PT0gZmFsc2VcbiAgICAgICkge1xuICAgICAgICBkZWJvdW5jZWRWYWxpZGF0ZSgpXG4gICAgICB9XG4gICAgfVxuICB9KVxuXG4gIGZ1bmN0aW9uIHJlc2V0VmFsaWRhdGlvbiAoKSB7XG4gICAgdmFsaWRhdGVJbmRleCsrXG4gICAgaW5uZXJMb2FkaW5nLnZhbHVlID0gZmFsc2VcbiAgICBpc0RpcnR5TW9kZWwudmFsdWUgPSBudWxsXG4gICAgaW5uZXJFcnJvci52YWx1ZSA9IGZhbHNlXG4gICAgaW5uZXJFcnJvck1lc3NhZ2UudmFsdWUgPSBudWxsXG4gICAgZGVib3VuY2VkVmFsaWRhdGUuY2FuY2VsKClcbiAgfVxuXG4gIC8qXG4gICAqIFJldHVybiB2YWx1ZVxuICAgKiAgIC0gdHJ1ZSAodmFsaWRhdGlvbiBzdWNjZWVkZWQpXG4gICAqICAgLSBmYWxzZSAodmFsaWRhdGlvbiBmYWlsZWQpXG4gICAqICAgLSBQcm9taXNlIChwZW5kaW5nIGFzeW5jIHZhbGlkYXRpb24pXG4gICAqL1xuICBmdW5jdGlvbiB2YWxpZGF0ZSAodmFsID0gcHJvcHMubW9kZWxWYWx1ZSkge1xuICAgIGlmIChoYXNBY3RpdmVSdWxlcy52YWx1ZSAhPT0gdHJ1ZSkge1xuICAgICAgcmV0dXJuIHRydWVcbiAgICB9XG5cbiAgICBjb25zdCBpbmRleCA9ICsrdmFsaWRhdGVJbmRleFxuXG4gICAgaWYgKGlubmVyTG9hZGluZy52YWx1ZSAhPT0gdHJ1ZSAmJiBwcm9wcy5sYXp5UnVsZXMgIT09IHRydWUpIHtcbiAgICAgIGlzRGlydHlNb2RlbC52YWx1ZSA9IHRydWVcbiAgICB9XG5cbiAgICBjb25zdCB1cGRhdGUgPSAoZXJyLCBtc2cpID0+IHtcbiAgICAgIGlmIChpbm5lckVycm9yLnZhbHVlICE9PSBlcnIpIHtcbiAgICAgICAgaW5uZXJFcnJvci52YWx1ZSA9IGVyclxuICAgICAgfVxuXG4gICAgICBjb25zdCBtID0gbXNnIHx8IHZvaWQgMFxuXG4gICAgICBpZiAoaW5uZXJFcnJvck1lc3NhZ2UudmFsdWUgIT09IG0pIHtcbiAgICAgICAgaW5uZXJFcnJvck1lc3NhZ2UudmFsdWUgPSBtXG4gICAgICB9XG5cbiAgICAgIGlubmVyTG9hZGluZy52YWx1ZSA9IGZhbHNlXG4gICAgfVxuXG4gICAgY29uc3QgcHJvbWlzZXMgPSBbXVxuXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBwcm9wcy5ydWxlcy5sZW5ndGg7IGkrKykge1xuICAgICAgY29uc3QgcnVsZSA9IHByb3BzLnJ1bGVzWyBpIF1cbiAgICAgIGxldCByZXNcblxuICAgICAgaWYgKHR5cGVvZiBydWxlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIHJlcyA9IHJ1bGUodmFsKVxuICAgICAgfVxuICAgICAgZWxzZSBpZiAodHlwZW9mIHJ1bGUgPT09ICdzdHJpbmcnICYmIHRlc3RQYXR0ZXJuWyBydWxlIF0gIT09IHZvaWQgMCkge1xuICAgICAgICByZXMgPSB0ZXN0UGF0dGVyblsgcnVsZSBdKHZhbClcbiAgICAgIH1cblxuICAgICAgaWYgKHJlcyA9PT0gZmFsc2UgfHwgdHlwZW9mIHJlcyA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgdXBkYXRlKHRydWUsIHJlcylcbiAgICAgICAgcmV0dXJuIGZhbHNlXG4gICAgICB9XG4gICAgICBlbHNlIGlmIChyZXMgIT09IHRydWUgJiYgcmVzICE9PSB2b2lkIDApIHtcbiAgICAgICAgcHJvbWlzZXMucHVzaChyZXMpXG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKHByb21pc2VzLmxlbmd0aCA9PT0gMCkge1xuICAgICAgdXBkYXRlKGZhbHNlKVxuICAgICAgcmV0dXJuIHRydWVcbiAgICB9XG5cbiAgICBpbm5lckxvYWRpbmcudmFsdWUgPSB0cnVlXG5cbiAgICByZXR1cm4gUHJvbWlzZS5hbGwocHJvbWlzZXMpLnRoZW4oXG4gICAgICByZXMgPT4ge1xuICAgICAgICBpZiAocmVzID09PSB2b2lkIDAgfHwgQXJyYXkuaXNBcnJheShyZXMpID09PSBmYWxzZSB8fCByZXMubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgaW5kZXggPT09IHZhbGlkYXRlSW5kZXggJiYgdXBkYXRlKGZhbHNlKVxuICAgICAgICAgIHJldHVybiB0cnVlXG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBtc2cgPSByZXMuZmluZChyID0+IHIgPT09IGZhbHNlIHx8IHR5cGVvZiByID09PSAnc3RyaW5nJylcbiAgICAgICAgaW5kZXggPT09IHZhbGlkYXRlSW5kZXggJiYgdXBkYXRlKG1zZyAhPT0gdm9pZCAwLCBtc2cpXG4gICAgICAgIHJldHVybiBtc2cgPT09IHZvaWQgMFxuICAgICAgfSxcbiAgICAgIGUgPT4ge1xuICAgICAgICBpZiAoaW5kZXggPT09IHZhbGlkYXRlSW5kZXgpIHtcbiAgICAgICAgICBjb25zb2xlLmVycm9yKGUpXG4gICAgICAgICAgdXBkYXRlKHRydWUpXG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gZmFsc2VcbiAgICAgIH1cbiAgICApXG4gIH1cblxuICBmdW5jdGlvbiB2YWxpZGF0ZUlmTmVlZGVkIChjaGFuZ2VkUnVsZXMpIHtcbiAgICBpZiAoXG4gICAgICBoYXNBY3RpdmVSdWxlcy52YWx1ZSA9PT0gdHJ1ZVxuICAgICAgJiYgcHJvcHMubGF6eVJ1bGVzICE9PSAnb25kZW1hbmQnXG4gICAgICAmJiAoaXNEaXJ0eU1vZGVsLnZhbHVlID09PSB0cnVlIHx8IChwcm9wcy5sYXp5UnVsZXMgIT09IHRydWUgJiYgY2hhbmdlZFJ1bGVzICE9PSB0cnVlKSlcbiAgICApIHtcbiAgICAgIGRlYm91bmNlZFZhbGlkYXRlKClcbiAgICB9XG4gIH1cblxuICBjb25zdCBkZWJvdW5jZWRWYWxpZGF0ZSA9IGRlYm91bmNlKHZhbGlkYXRlLCAwKVxuXG4gIG9uQmVmb3JlVW5tb3VudCgoKSA9PiB7XG4gICAgdW53YXRjaFJ1bGVzICE9PSB2b2lkIDAgJiYgdW53YXRjaFJ1bGVzKClcbiAgICBkZWJvdW5jZWRWYWxpZGF0ZS5jYW5jZWwoKVxuICB9KVxuXG4gIC8vIGV4cG9zZSBwdWJsaWMgbWV0aG9kcyAmIHByb3BzXG4gIE9iamVjdC5hc3NpZ24ocHJveHksIHsgcmVzZXRWYWxpZGF0aW9uLCB2YWxpZGF0ZSB9KVxuICBpbmplY3RQcm9wKHByb3h5LCAnaGFzRXJyb3InLCAoKSA9PiBoYXNFcnJvci52YWx1ZSlcblxuICByZXR1cm4ge1xuICAgIGlzRGlydHlNb2RlbCxcbiAgICBoYXNSdWxlcyxcbiAgICBoYXNFcnJvcixcbiAgICBlcnJvck1lc3NhZ2UsXG5cbiAgICB2YWxpZGF0ZSxcbiAgICByZXNldFZhbGlkYXRpb25cbiAgfVxufVxuIiwiaW1wb3J0IHsgcmVmLCBvbkJlZm9yZVVwZGF0ZSB9IGZyb20gJ3Z1ZSdcblxuY29uc3QgbGlzdGVuZXJSRSA9IC9eb25bQS1aXS9cblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gKGF0dHJzLCB2bm9kZSkge1xuICBjb25zdCBhY2MgPSB7XG4gICAgbGlzdGVuZXJzOiByZWYoe30pLFxuICAgIGF0dHJpYnV0ZXM6IHJlZih7fSlcbiAgfVxuXG4gIGZ1bmN0aW9uIHVwZGF0ZSAoKSB7XG4gICAgY29uc3QgYXR0cmlidXRlcyA9IHt9XG4gICAgY29uc3QgbGlzdGVuZXJzID0ge31cblxuICAgIGZvciAoY29uc3Qga2V5IGluIGF0dHJzKSB7XG4gICAgICBpZiAoa2V5ICE9PSAnY2xhc3MnICYmIGtleSAhPT0gJ3N0eWxlJyAmJiBsaXN0ZW5lclJFLnRlc3Qoa2V5KSA9PT0gZmFsc2UpIHtcbiAgICAgICAgYXR0cmlidXRlc1sga2V5IF0gPSBhdHRyc1sga2V5IF1cbiAgICAgIH1cbiAgICB9XG5cbiAgICBmb3IgKGNvbnN0IGtleSBpbiB2bm9kZS5wcm9wcykge1xuICAgICAgaWYgKGxpc3RlbmVyUkUudGVzdChrZXkpID09PSB0cnVlKSB7XG4gICAgICAgIGxpc3RlbmVyc1sga2V5IF0gPSB2bm9kZS5wcm9wc1sga2V5IF1cbiAgICAgIH1cbiAgICB9XG5cbiAgICBhY2MuYXR0cmlidXRlcy52YWx1ZSA9IGF0dHJpYnV0ZXNcbiAgICBhY2MubGlzdGVuZXJzLnZhbHVlID0gbGlzdGVuZXJzXG4gIH1cblxuICBvbkJlZm9yZVVwZGF0ZSh1cGRhdGUpXG5cbiAgdXBkYXRlKClcblxuICByZXR1cm4gYWNjXG59XG4iLCJsZXQgcXVldWUgPSBbXVxubGV0IHdhaXRGbGFncyA9IFtdXG5cbmZ1bmN0aW9uIGNsZWFyRmxhZyAoZmxhZykge1xuICB3YWl0RmxhZ3MgPSB3YWl0RmxhZ3MuZmlsdGVyKGVudHJ5ID0+IGVudHJ5ICE9PSBmbGFnKVxufVxuXG5leHBvcnQgZnVuY3Rpb24gYWRkRm9jdXNXYWl0RmxhZyAoZmxhZykge1xuICBjbGVhckZsYWcoZmxhZylcbiAgd2FpdEZsYWdzLnB1c2goZmxhZylcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHJlbW92ZUZvY3VzV2FpdEZsYWcgKGZsYWcpIHtcbiAgY2xlYXJGbGFnKGZsYWcpXG5cbiAgaWYgKHdhaXRGbGFncy5sZW5ndGggPT09IDAgJiYgcXVldWUubGVuZ3RoID4gMCkge1xuICAgIC8vIG9ubHkgY2FsbCBsYXN0IGZvY3VzIGhhbmRsZXIgKGNhbid0IGZvY3VzIG11bHRpcGxlIHRoaW5ncyBhdCBvbmNlKVxuICAgIHF1ZXVlWyBxdWV1ZS5sZW5ndGggLSAxIF0oKVxuICAgIHF1ZXVlID0gW11cbiAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gYWRkRm9jdXNGbiAoZm4pIHtcbiAgaWYgKHdhaXRGbGFncy5sZW5ndGggPT09IDApIHtcbiAgICBmbigpXG4gIH1cbiAgZWxzZSB7XG4gICAgcXVldWUucHVzaChmbilcbiAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gcmVtb3ZlRm9jdXNGbiAoZm4pIHtcbiAgcXVldWUgPSBxdWV1ZS5maWx0ZXIoZW50cnkgPT4gZW50cnkgIT09IGZuKVxufVxuIiwiaW1wb3J0IHsgaCwgcmVmLCBjb21wdXRlZCwgd2F0Y2gsIFRyYW5zaXRpb24sIG5leHRUaWNrLCBvbkFjdGl2YXRlZCwgb25EZWFjdGl2YXRlZCwgb25CZWZvcmVVbm1vdW50LCBvbk1vdW50ZWQsIGdldEN1cnJlbnRJbnN0YW5jZSB9IGZyb20gJ3Z1ZSdcblxuaW1wb3J0IHsgaXNSdW50aW1lU3NyUHJlSHlkcmF0aW9uIH0gZnJvbSAnLi4vLi4vcGx1Z2lucy9QbGF0Zm9ybS5qcydcblxuaW1wb3J0IFFJY29uIGZyb20gJy4uLy4uL2NvbXBvbmVudHMvaWNvbi9RSWNvbi5qcydcbmltcG9ydCBRU3Bpbm5lciBmcm9tICcuLi8uLi9jb21wb25lbnRzL3NwaW5uZXIvUVNwaW5uZXIuanMnXG5cbmltcG9ydCB1c2VEYXJrLCB7IHVzZURhcmtQcm9wcyB9IGZyb20gJy4uLy4uL2NvbXBvc2FibGVzL3ByaXZhdGUvdXNlLWRhcmsuanMnXG5pbXBvcnQgdXNlVmFsaWRhdGUsIHsgdXNlVmFsaWRhdGVQcm9wcyB9IGZyb20gJy4vdXNlLXZhbGlkYXRlLmpzJ1xuaW1wb3J0IHVzZVNwbGl0QXR0cnMgZnJvbSAnLi91c2Utc3BsaXQtYXR0cnMuanMnXG5cbmltcG9ydCB7IGhTbG90IH0gZnJvbSAnLi4vLi4vdXRpbHMvcHJpdmF0ZS9yZW5kZXIuanMnXG5pbXBvcnQgdWlkIGZyb20gJy4uLy4uL3V0aWxzL3VpZC5qcydcbmltcG9ydCB7IHByZXZlbnQsIHN0b3BBbmRQcmV2ZW50IH0gZnJvbSAnLi4vLi4vdXRpbHMvZXZlbnQuanMnXG5pbXBvcnQgeyBhZGRGb2N1c0ZuLCByZW1vdmVGb2N1c0ZuIH0gZnJvbSAnLi4vLi4vdXRpbHMvcHJpdmF0ZS9mb2N1cy1tYW5hZ2VyLmpzJ1xuXG5mdW5jdGlvbiBnZXRUYXJnZXRVaWQgKHZhbCkge1xuICByZXR1cm4gdmFsID09PSB2b2lkIDAgPyBgZl8keyB1aWQoKSB9YCA6IHZhbFxufVxuXG5leHBvcnQgZnVuY3Rpb24gZmllbGRWYWx1ZUlzRmlsbGVkICh2YWwpIHtcbiAgcmV0dXJuIHZhbCAhPT0gdm9pZCAwXG4gICAgJiYgdmFsICE9PSBudWxsXG4gICAgJiYgKCcnICsgdmFsKS5sZW5ndGggPiAwXG59XG5cbmV4cG9ydCBjb25zdCB1c2VGaWVsZFByb3BzID0ge1xuICAuLi51c2VEYXJrUHJvcHMsXG4gIC4uLnVzZVZhbGlkYXRlUHJvcHMsXG5cbiAgbGFiZWw6IFN0cmluZyxcbiAgc3RhY2tMYWJlbDogQm9vbGVhbixcbiAgaGludDogU3RyaW5nLFxuICBoaWRlSGludDogQm9vbGVhbixcbiAgcHJlZml4OiBTdHJpbmcsXG4gIHN1ZmZpeDogU3RyaW5nLFxuXG4gIGxhYmVsQ29sb3I6IFN0cmluZyxcbiAgY29sb3I6IFN0cmluZyxcbiAgYmdDb2xvcjogU3RyaW5nLFxuXG4gIGZpbGxlZDogQm9vbGVhbixcbiAgb3V0bGluZWQ6IEJvb2xlYW4sXG4gIGJvcmRlcmxlc3M6IEJvb2xlYW4sXG4gIHN0YW5kb3V0OiBbIEJvb2xlYW4sIFN0cmluZyBdLFxuXG4gIHNxdWFyZTogQm9vbGVhbixcblxuICBsb2FkaW5nOiBCb29sZWFuLFxuXG4gIGxhYmVsU2xvdDogQm9vbGVhbixcblxuICBib3R0b21TbG90czogQm9vbGVhbixcbiAgaGlkZUJvdHRvbVNwYWNlOiBCb29sZWFuLFxuXG4gIHJvdW5kZWQ6IEJvb2xlYW4sXG4gIGRlbnNlOiBCb29sZWFuLFxuICBpdGVtQWxpZ25lZDogQm9vbGVhbixcblxuICBjb3VudGVyOiBCb29sZWFuLFxuXG4gIGNsZWFyYWJsZTogQm9vbGVhbixcbiAgY2xlYXJJY29uOiBTdHJpbmcsXG5cbiAgZGlzYWJsZTogQm9vbGVhbixcbiAgcmVhZG9ubHk6IEJvb2xlYW4sXG5cbiAgYXV0b2ZvY3VzOiBCb29sZWFuLFxuXG4gIGZvcjogU3RyaW5nLFxuXG4gIG1heGxlbmd0aDogWyBOdW1iZXIsIFN0cmluZyBdXG59XG5cbmV4cG9ydCBjb25zdCB1c2VGaWVsZEVtaXRzID0gWyAndXBkYXRlOm1vZGVsVmFsdWUnLCAnY2xlYXInLCAnZm9jdXMnLCAnYmx1cicsICdwb3B1cC1zaG93JywgJ3BvcHVwLWhpZGUnIF1cblxuZXhwb3J0IGZ1bmN0aW9uIHVzZUZpZWxkU3RhdGUgKCkge1xuICBjb25zdCB7IHByb3BzLCBhdHRycywgcHJveHksIHZub2RlIH0gPSBnZXRDdXJyZW50SW5zdGFuY2UoKVxuXG4gIGNvbnN0IGlzRGFyayA9IHVzZURhcmsocHJvcHMsIHByb3h5LiRxKVxuXG4gIHJldHVybiB7XG4gICAgaXNEYXJrLFxuXG4gICAgZWRpdGFibGU6IGNvbXB1dGVkKCgpID0+XG4gICAgICBwcm9wcy5kaXNhYmxlICE9PSB0cnVlICYmIHByb3BzLnJlYWRvbmx5ICE9PSB0cnVlXG4gICAgKSxcblxuICAgIGlubmVyTG9hZGluZzogcmVmKGZhbHNlKSxcbiAgICBmb2N1c2VkOiByZWYoZmFsc2UpLFxuICAgIGhhc1BvcHVwT3BlbjogZmFsc2UsXG5cbiAgICBzcGxpdEF0dHJzOiB1c2VTcGxpdEF0dHJzKGF0dHJzLCB2bm9kZSksXG4gICAgdGFyZ2V0VWlkOiByZWYoZ2V0VGFyZ2V0VWlkKHByb3BzLmZvcikpLFxuXG4gICAgcm9vdFJlZjogcmVmKG51bGwpLFxuICAgIHRhcmdldFJlZjogcmVmKG51bGwpLFxuICAgIGNvbnRyb2xSZWY6IHJlZihudWxsKVxuXG4gICAgLyoqXG4gICAgICogdXNlciBzdXBwbGllZCBhZGRpdGlvbmFsczpcblxuICAgICAqIGlubmVyVmFsdWUgLSBjb21wdXRlZFxuICAgICAqIGZsb2F0aW5nTGFiZWwgLSBjb21wdXRlZFxuICAgICAqIGlucHV0UmVmIC0gY29tcHV0ZWRcblxuICAgICAqIGZpZWxkQ2xhc3MgLSBjb21wdXRlZFxuICAgICAqIGhhc1NoYWRvdyAtIGNvbXB1dGVkXG5cbiAgICAgKiBjb250cm9sRXZlbnRzIC0gT2JqZWN0IHdpdGggZm4oZSlcblxuICAgICAqIGdldENvbnRyb2wgLSBmblxuICAgICAqIGdldElubmVyQXBwZW5kIC0gZm5cbiAgICAgKiBnZXRDb250cm9sQ2hpbGQgLSBmblxuICAgICAqIGdldFNoYWRvd0NvbnRyb2wgLSBmblxuICAgICAqIHNob3dQb3B1cCAtIGZuXG4gICAgICovXG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gKHN0YXRlKSB7XG4gIGNvbnN0IHsgcHJvcHMsIGVtaXQsIHNsb3RzLCBhdHRycywgcHJveHkgfSA9IGdldEN1cnJlbnRJbnN0YW5jZSgpXG4gIGNvbnN0IHsgJHEgfSA9IHByb3h5XG5cbiAgbGV0IGZvY3Vzb3V0VGltZXJcblxuICBpZiAoc3RhdGUuaGFzVmFsdWUgPT09IHZvaWQgMCkge1xuICAgIHN0YXRlLmhhc1ZhbHVlID0gY29tcHV0ZWQoKCkgPT4gZmllbGRWYWx1ZUlzRmlsbGVkKHByb3BzLm1vZGVsVmFsdWUpKVxuICB9XG5cbiAgaWYgKHN0YXRlLmVtaXRWYWx1ZSA9PT0gdm9pZCAwKSB7XG4gICAgc3RhdGUuZW1pdFZhbHVlID0gdmFsdWUgPT4ge1xuICAgICAgZW1pdCgndXBkYXRlOm1vZGVsVmFsdWUnLCB2YWx1ZSlcbiAgICB9XG4gIH1cblxuICBpZiAoc3RhdGUuY29udHJvbEV2ZW50cyA9PT0gdm9pZCAwKSB7XG4gICAgc3RhdGUuY29udHJvbEV2ZW50cyA9IHtcbiAgICAgIG9uRm9jdXNpbjogb25Db250cm9sRm9jdXNpbixcbiAgICAgIG9uRm9jdXNvdXQ6IG9uQ29udHJvbEZvY3Vzb3V0XG4gICAgfVxuICB9XG5cbiAgT2JqZWN0LmFzc2lnbihzdGF0ZSwge1xuICAgIGNsZWFyVmFsdWUsXG4gICAgb25Db250cm9sRm9jdXNpbixcbiAgICBvbkNvbnRyb2xGb2N1c291dCxcbiAgICBmb2N1c1xuICB9KVxuXG4gIGlmIChzdGF0ZS5jb21wdXRlZENvdW50ZXIgPT09IHZvaWQgMCkge1xuICAgIHN0YXRlLmNvbXB1dGVkQ291bnRlciA9IGNvbXB1dGVkKCgpID0+IHtcbiAgICAgIGlmIChwcm9wcy5jb3VudGVyICE9PSBmYWxzZSkge1xuICAgICAgICBjb25zdCBsZW4gPSB0eXBlb2YgcHJvcHMubW9kZWxWYWx1ZSA9PT0gJ3N0cmluZycgfHwgdHlwZW9mIHByb3BzLm1vZGVsVmFsdWUgPT09ICdudW1iZXInXG4gICAgICAgICAgPyAoJycgKyBwcm9wcy5tb2RlbFZhbHVlKS5sZW5ndGhcbiAgICAgICAgICA6IChBcnJheS5pc0FycmF5KHByb3BzLm1vZGVsVmFsdWUpID09PSB0cnVlID8gcHJvcHMubW9kZWxWYWx1ZS5sZW5ndGggOiAwKVxuXG4gICAgICAgIGNvbnN0IG1heCA9IHByb3BzLm1heGxlbmd0aCAhPT0gdm9pZCAwXG4gICAgICAgICAgPyBwcm9wcy5tYXhsZW5ndGhcbiAgICAgICAgICA6IHByb3BzLm1heFZhbHVlc1xuXG4gICAgICAgIHJldHVybiBsZW4gKyAobWF4ICE9PSB2b2lkIDAgPyAnIC8gJyArIG1heCA6ICcnKVxuICAgICAgfVxuICAgIH0pXG4gIH1cblxuICBjb25zdCB7XG4gICAgaXNEaXJ0eU1vZGVsLFxuICAgIGhhc1J1bGVzLFxuICAgIGhhc0Vycm9yLFxuICAgIGVycm9yTWVzc2FnZSxcbiAgICByZXNldFZhbGlkYXRpb25cbiAgfSA9IHVzZVZhbGlkYXRlKHN0YXRlLmZvY3VzZWQsIHN0YXRlLmlubmVyTG9hZGluZylcblxuICBjb25zdCBmbG9hdGluZ0xhYmVsID0gc3RhdGUuZmxvYXRpbmdMYWJlbCAhPT0gdm9pZCAwXG4gICAgPyBjb21wdXRlZCgoKSA9PiBwcm9wcy5zdGFja0xhYmVsID09PSB0cnVlIHx8IHN0YXRlLmZvY3VzZWQudmFsdWUgPT09IHRydWUgfHwgc3RhdGUuZmxvYXRpbmdMYWJlbC52YWx1ZSA9PT0gdHJ1ZSlcbiAgICA6IGNvbXB1dGVkKCgpID0+IHByb3BzLnN0YWNrTGFiZWwgPT09IHRydWUgfHwgc3RhdGUuZm9jdXNlZC52YWx1ZSA9PT0gdHJ1ZSB8fCBzdGF0ZS5oYXNWYWx1ZS52YWx1ZSA9PT0gdHJ1ZSlcblxuICBjb25zdCBzaG91bGRSZW5kZXJCb3R0b20gPSBjb21wdXRlZCgoKSA9PlxuICAgIHByb3BzLmJvdHRvbVNsb3RzID09PSB0cnVlXG4gICAgfHwgcHJvcHMuaGludCAhPT0gdm9pZCAwXG4gICAgfHwgaGFzUnVsZXMudmFsdWUgPT09IHRydWVcbiAgICB8fCBwcm9wcy5jb3VudGVyID09PSB0cnVlXG4gICAgfHwgcHJvcHMuZXJyb3IgIT09IG51bGxcbiAgKVxuXG4gIGNvbnN0IHN0eWxlVHlwZSA9IGNvbXB1dGVkKCgpID0+IHtcbiAgICBpZiAocHJvcHMuZmlsbGVkID09PSB0cnVlKSB7IHJldHVybiAnZmlsbGVkJyB9XG4gICAgaWYgKHByb3BzLm91dGxpbmVkID09PSB0cnVlKSB7IHJldHVybiAnb3V0bGluZWQnIH1cbiAgICBpZiAocHJvcHMuYm9yZGVybGVzcyA9PT0gdHJ1ZSkgeyByZXR1cm4gJ2JvcmRlcmxlc3MnIH1cbiAgICBpZiAocHJvcHMuc3RhbmRvdXQpIHsgcmV0dXJuICdzdGFuZG91dCcgfVxuICAgIHJldHVybiAnc3RhbmRhcmQnXG4gIH0pXG5cbiAgY29uc3QgY2xhc3NlcyA9IGNvbXB1dGVkKCgpID0+XG4gICAgYHEtZmllbGQgcm93IG5vLXdyYXAgaXRlbXMtc3RhcnQgcS1maWVsZC0tJHsgc3R5bGVUeXBlLnZhbHVlIH1gXG4gICAgKyAoc3RhdGUuZmllbGRDbGFzcyAhPT0gdm9pZCAwID8gYCAkeyBzdGF0ZS5maWVsZENsYXNzLnZhbHVlIH1gIDogJycpXG4gICAgKyAocHJvcHMucm91bmRlZCA9PT0gdHJ1ZSA/ICcgcS1maWVsZC0tcm91bmRlZCcgOiAnJylcbiAgICArIChwcm9wcy5zcXVhcmUgPT09IHRydWUgPyAnIHEtZmllbGQtLXNxdWFyZScgOiAnJylcbiAgICArIChmbG9hdGluZ0xhYmVsLnZhbHVlID09PSB0cnVlID8gJyBxLWZpZWxkLS1mbG9hdCcgOiAnJylcbiAgICArIChoYXNMYWJlbC52YWx1ZSA9PT0gdHJ1ZSA/ICcgcS1maWVsZC0tbGFiZWxlZCcgOiAnJylcbiAgICArIChwcm9wcy5kZW5zZSA9PT0gdHJ1ZSA/ICcgcS1maWVsZC0tZGVuc2UnIDogJycpXG4gICAgKyAocHJvcHMuaXRlbUFsaWduZWQgPT09IHRydWUgPyAnIHEtZmllbGQtLWl0ZW0tYWxpZ25lZCBxLWl0ZW0tdHlwZScgOiAnJylcbiAgICArIChzdGF0ZS5pc0RhcmsudmFsdWUgPT09IHRydWUgPyAnIHEtZmllbGQtLWRhcmsnIDogJycpXG4gICAgKyAoc3RhdGUuZ2V0Q29udHJvbCA9PT0gdm9pZCAwID8gJyBxLWZpZWxkLS1hdXRvLWhlaWdodCcgOiAnJylcbiAgICArIChzdGF0ZS5mb2N1c2VkLnZhbHVlID09PSB0cnVlID8gJyBxLWZpZWxkLS1mb2N1c2VkJyA6ICcnKVxuICAgICsgKGhhc0Vycm9yLnZhbHVlID09PSB0cnVlID8gJyBxLWZpZWxkLS1lcnJvcicgOiAnJylcbiAgICArIChoYXNFcnJvci52YWx1ZSA9PT0gdHJ1ZSB8fCBzdGF0ZS5mb2N1c2VkLnZhbHVlID09PSB0cnVlID8gJyBxLWZpZWxkLS1oaWdobGlnaHRlZCcgOiAnJylcbiAgICArIChwcm9wcy5oaWRlQm90dG9tU3BhY2UgIT09IHRydWUgJiYgc2hvdWxkUmVuZGVyQm90dG9tLnZhbHVlID09PSB0cnVlID8gJyBxLWZpZWxkLS13aXRoLWJvdHRvbScgOiAnJylcbiAgICArIChwcm9wcy5kaXNhYmxlID09PSB0cnVlID8gJyBxLWZpZWxkLS1kaXNhYmxlZCcgOiAocHJvcHMucmVhZG9ubHkgPT09IHRydWUgPyAnIHEtZmllbGQtLXJlYWRvbmx5JyA6ICcnKSlcbiAgKVxuXG4gIGNvbnN0IGNvbnRlbnRDbGFzcyA9IGNvbXB1dGVkKCgpID0+XG4gICAgJ3EtZmllbGRfX2NvbnRyb2wgcmVsYXRpdmUtcG9zaXRpb24gcm93IG5vLXdyYXAnXG4gICAgKyAocHJvcHMuYmdDb2xvciAhPT0gdm9pZCAwID8gYCBiZy0keyBwcm9wcy5iZ0NvbG9yIH1gIDogJycpXG4gICAgKyAoXG4gICAgICBoYXNFcnJvci52YWx1ZSA9PT0gdHJ1ZVxuICAgICAgICA/ICcgdGV4dC1uZWdhdGl2ZSdcbiAgICAgICAgOiAoXG4gICAgICAgICAgICB0eXBlb2YgcHJvcHMuc3RhbmRvdXQgPT09ICdzdHJpbmcnICYmIHByb3BzLnN0YW5kb3V0Lmxlbmd0aCA+IDAgJiYgc3RhdGUuZm9jdXNlZC52YWx1ZSA9PT0gdHJ1ZVxuICAgICAgICAgICAgICA/IGAgJHsgcHJvcHMuc3RhbmRvdXQgfWBcbiAgICAgICAgICAgICAgOiAocHJvcHMuY29sb3IgIT09IHZvaWQgMCA/IGAgdGV4dC0keyBwcm9wcy5jb2xvciB9YCA6ICcnKVxuICAgICAgICAgIClcbiAgICApXG4gIClcblxuICBjb25zdCBoYXNMYWJlbCA9IGNvbXB1dGVkKCgpID0+XG4gICAgcHJvcHMubGFiZWxTbG90ID09PSB0cnVlIHx8IHByb3BzLmxhYmVsICE9PSB2b2lkIDBcbiAgKVxuXG4gIGNvbnN0IGxhYmVsQ2xhc3MgPSBjb21wdXRlZCgoKSA9PlxuICAgICdxLWZpZWxkX19sYWJlbCBuby1wb2ludGVyLWV2ZW50cyBhYnNvbHV0ZSBlbGxpcHNpcydcbiAgICArIChwcm9wcy5sYWJlbENvbG9yICE9PSB2b2lkIDAgJiYgaGFzRXJyb3IudmFsdWUgIT09IHRydWUgPyBgIHRleHQtJHsgcHJvcHMubGFiZWxDb2xvciB9YCA6ICcnKVxuICApXG5cbiAgY29uc3QgY29udHJvbFNsb3RTY29wZSA9IGNvbXB1dGVkKCgpID0+ICh7XG4gICAgaWQ6IHN0YXRlLnRhcmdldFVpZC52YWx1ZSxcbiAgICBlZGl0YWJsZTogc3RhdGUuZWRpdGFibGUudmFsdWUsXG4gICAgZm9jdXNlZDogc3RhdGUuZm9jdXNlZC52YWx1ZSxcbiAgICBmbG9hdGluZ0xhYmVsOiBmbG9hdGluZ0xhYmVsLnZhbHVlLFxuICAgIG1vZGVsVmFsdWU6IHByb3BzLm1vZGVsVmFsdWUsXG4gICAgZW1pdFZhbHVlOiBzdGF0ZS5lbWl0VmFsdWVcbiAgfSkpXG5cbiAgY29uc3QgYXR0cmlidXRlcyA9IGNvbXB1dGVkKCgpID0+IHtcbiAgICBjb25zdCBhY2MgPSB7XG4gICAgICBmb3I6IHN0YXRlLnRhcmdldFVpZC52YWx1ZVxuICAgIH1cblxuICAgIGlmIChwcm9wcy5kaXNhYmxlID09PSB0cnVlKSB7XG4gICAgICBhY2NbICdhcmlhLWRpc2FibGVkJyBdID0gJ3RydWUnXG4gICAgfVxuICAgIGVsc2UgaWYgKHByb3BzLnJlYWRvbmx5ID09PSB0cnVlKSB7XG4gICAgICBhY2NbICdhcmlhLXJlYWRvbmx5JyBdID0gJ3RydWUnXG4gICAgfVxuXG4gICAgcmV0dXJuIGFjY1xuICB9KVxuXG4gIHdhdGNoKCgpID0+IHByb3BzLmZvciwgdmFsID0+IHtcbiAgICAvLyBkb24ndCB0cmFuc2Zvcm0gdGFyZ2V0VWlkIGludG8gYSBjb21wdXRlZFxuICAgIC8vIHByb3AgYXMgaXQgd2lsbCBicmVhayBTU1JcbiAgICBzdGF0ZS50YXJnZXRVaWQudmFsdWUgPSBnZXRUYXJnZXRVaWQodmFsKVxuICB9KVxuXG4gIGZ1bmN0aW9uIGZvY3VzSGFuZGxlciAoKSB7XG4gICAgY29uc3QgZWwgPSBkb2N1bWVudC5hY3RpdmVFbGVtZW50XG4gICAgbGV0IHRhcmdldCA9IHN0YXRlLnRhcmdldFJlZiAhPT0gdm9pZCAwICYmIHN0YXRlLnRhcmdldFJlZi52YWx1ZVxuXG4gICAgaWYgKHRhcmdldCAmJiAoZWwgPT09IG51bGwgfHwgZWwuaWQgIT09IHN0YXRlLnRhcmdldFVpZC52YWx1ZSkpIHtcbiAgICAgIHRhcmdldC5oYXNBdHRyaWJ1dGUoJ3RhYmluZGV4JykgPT09IHRydWUgfHwgKHRhcmdldCA9IHRhcmdldC5xdWVyeVNlbGVjdG9yKCdbdGFiaW5kZXhdJykpXG4gICAgICBpZiAodGFyZ2V0ICYmIHRhcmdldCAhPT0gZWwpIHtcbiAgICAgICAgdGFyZ2V0LmZvY3VzKHsgcHJldmVudFNjcm9sbDogdHJ1ZSB9KVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIGZ1bmN0aW9uIGZvY3VzICgpIHtcbiAgICBhZGRGb2N1c0ZuKGZvY3VzSGFuZGxlcilcbiAgfVxuXG4gIGZ1bmN0aW9uIGJsdXIgKCkge1xuICAgIHJlbW92ZUZvY3VzRm4oZm9jdXNIYW5kbGVyKVxuICAgIGNvbnN0IGVsID0gZG9jdW1lbnQuYWN0aXZlRWxlbWVudFxuICAgIGlmIChlbCAhPT0gbnVsbCAmJiBzdGF0ZS5yb290UmVmLnZhbHVlLmNvbnRhaW5zKGVsKSkge1xuICAgICAgZWwuYmx1cigpXG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gb25Db250cm9sRm9jdXNpbiAoZSkge1xuICAgIGNsZWFyVGltZW91dChmb2N1c291dFRpbWVyKVxuICAgIGlmIChzdGF0ZS5lZGl0YWJsZS52YWx1ZSA9PT0gdHJ1ZSAmJiBzdGF0ZS5mb2N1c2VkLnZhbHVlID09PSBmYWxzZSkge1xuICAgICAgc3RhdGUuZm9jdXNlZC52YWx1ZSA9IHRydWVcbiAgICAgIGVtaXQoJ2ZvY3VzJywgZSlcbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiBvbkNvbnRyb2xGb2N1c291dCAoZSwgdGhlbikge1xuICAgIGNsZWFyVGltZW91dChmb2N1c291dFRpbWVyKVxuICAgIGZvY3Vzb3V0VGltZXIgPSBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgIGlmIChcbiAgICAgICAgZG9jdW1lbnQuaGFzRm9jdXMoKSA9PT0gdHJ1ZSAmJiAoXG4gICAgICAgICAgc3RhdGUuaGFzUG9wdXBPcGVuID09PSB0cnVlXG4gICAgICAgICAgfHwgc3RhdGUuY29udHJvbFJlZiA9PT0gdm9pZCAwXG4gICAgICAgICAgfHwgc3RhdGUuY29udHJvbFJlZi52YWx1ZSA9PT0gbnVsbFxuICAgICAgICAgIHx8IHN0YXRlLmNvbnRyb2xSZWYudmFsdWUuY29udGFpbnMoZG9jdW1lbnQuYWN0aXZlRWxlbWVudCkgIT09IGZhbHNlXG4gICAgICAgIClcbiAgICAgICkge1xuICAgICAgICByZXR1cm5cbiAgICAgIH1cblxuICAgICAgaWYgKHN0YXRlLmZvY3VzZWQudmFsdWUgPT09IHRydWUpIHtcbiAgICAgICAgc3RhdGUuZm9jdXNlZC52YWx1ZSA9IGZhbHNlXG4gICAgICAgIGVtaXQoJ2JsdXInLCBlKVxuICAgICAgfVxuXG4gICAgICB0aGVuICE9PSB2b2lkIDAgJiYgdGhlbigpXG4gICAgfSlcbiAgfVxuXG4gIGZ1bmN0aW9uIGNsZWFyVmFsdWUgKGUpIHtcbiAgICAvLyBwcmV2ZW50IGFjdGl2YXRpbmcgdGhlIGZpZWxkIGJ1dCBrZWVwIGZvY3VzIG9uIGRlc2t0b3BcbiAgICBzdG9wQW5kUHJldmVudChlKVxuXG4gICAgaWYgKCRxLnBsYXRmb3JtLmlzLm1vYmlsZSAhPT0gdHJ1ZSkge1xuICAgICAgY29uc3QgZWwgPSAoc3RhdGUudGFyZ2V0UmVmICE9PSB2b2lkIDAgJiYgc3RhdGUudGFyZ2V0UmVmLnZhbHVlKSB8fCBzdGF0ZS5yb290UmVmLnZhbHVlXG4gICAgICBlbC5mb2N1cygpXG4gICAgfVxuICAgIGVsc2UgaWYgKHN0YXRlLnJvb3RSZWYudmFsdWUuY29udGFpbnMoZG9jdW1lbnQuYWN0aXZlRWxlbWVudCkgPT09IHRydWUpIHtcbiAgICAgIGRvY3VtZW50LmFjdGl2ZUVsZW1lbnQuYmx1cigpXG4gICAgfVxuXG4gICAgaWYgKHByb3BzLnR5cGUgPT09ICdmaWxlJykgeyAvLyBUT0RPIHZ1ZTNcbiAgICAgIC8vIGRvIG5vdCBsZXQgZm9jdXMgYmUgdHJpZ2dlcmVkXG4gICAgICAvLyBhcyBpdCB3aWxsIG1ha2UgdGhlIG5hdGl2ZSBmaWxlIGRpYWxvZ1xuICAgICAgLy8gYXBwZWFyIGZvciBhbm90aGVyIHNlbGVjdGlvblxuICAgICAgc3RhdGUuaW5wdXRSZWYudmFsdWUudmFsdWUgPSBudWxsXG4gICAgfVxuXG4gICAgZW1pdCgndXBkYXRlOm1vZGVsVmFsdWUnLCBudWxsKVxuICAgIGVtaXQoJ2NsZWFyJywgcHJvcHMubW9kZWxWYWx1ZSlcblxuICAgIG5leHRUaWNrKCgpID0+IHtcbiAgICAgIHJlc2V0VmFsaWRhdGlvbigpXG5cbiAgICAgIGlmICgkcS5wbGF0Zm9ybS5pcy5tb2JpbGUgIT09IHRydWUpIHtcbiAgICAgICAgaXNEaXJ0eU1vZGVsLnZhbHVlID0gZmFsc2VcbiAgICAgIH1cbiAgICB9KVxuICB9XG5cbiAgZnVuY3Rpb24gZ2V0Q29udGVudCAoKSB7XG4gICAgY29uc3Qgbm9kZSA9IFtdXG5cbiAgICBzbG90cy5wcmVwZW5kICE9PSB2b2lkIDAgJiYgbm9kZS5wdXNoKFxuICAgICAgaCgnZGl2Jywge1xuICAgICAgICBjbGFzczogJ3EtZmllbGRfX3ByZXBlbmQgcS1maWVsZF9fbWFyZ2luYWwgcm93IG5vLXdyYXAgaXRlbXMtY2VudGVyJyxcbiAgICAgICAga2V5OiAncHJlcGVuZCcsXG4gICAgICAgIG9uQ2xpY2s6IHByZXZlbnRcbiAgICAgIH0sIHNsb3RzLnByZXBlbmQoKSlcbiAgICApXG5cbiAgICBub2RlLnB1c2goXG4gICAgICBoKCdkaXYnLCB7XG4gICAgICAgIGNsYXNzOiAncS1maWVsZF9fY29udHJvbC1jb250YWluZXIgY29sIHJlbGF0aXZlLXBvc2l0aW9uIHJvdyBuby13cmFwIHEtYW5jaG9yLS1za2lwJ1xuICAgICAgfSwgZ2V0Q29udHJvbENvbnRhaW5lcigpKVxuICAgIClcblxuICAgIGhhc0Vycm9yLnZhbHVlID09PSB0cnVlICYmIHByb3BzLm5vRXJyb3JJY29uID09PSBmYWxzZSAmJiBub2RlLnB1c2goXG4gICAgICBnZXRJbm5lckFwcGVuZE5vZGUoJ2Vycm9yJywgW1xuICAgICAgICBoKFFJY29uLCB7IG5hbWU6ICRxLmljb25TZXQuZmllbGQuZXJyb3IsIGNvbG9yOiAnbmVnYXRpdmUnIH0pXG4gICAgICBdKVxuICAgIClcblxuICAgIGlmIChwcm9wcy5sb2FkaW5nID09PSB0cnVlIHx8IHN0YXRlLmlubmVyTG9hZGluZy52YWx1ZSA9PT0gdHJ1ZSkge1xuICAgICAgbm9kZS5wdXNoKFxuICAgICAgICBnZXRJbm5lckFwcGVuZE5vZGUoXG4gICAgICAgICAgJ2lubmVyLWxvYWRpbmctYXBwZW5kJyxcbiAgICAgICAgICBzbG90cy5sb2FkaW5nICE9PSB2b2lkIDBcbiAgICAgICAgICAgID8gc2xvdHMubG9hZGluZygpXG4gICAgICAgICAgICA6IFsgaChRU3Bpbm5lciwgeyBjb2xvcjogcHJvcHMuY29sb3IgfSkgXVxuICAgICAgICApXG4gICAgICApXG4gICAgfVxuICAgIGVsc2UgaWYgKHByb3BzLmNsZWFyYWJsZSA9PT0gdHJ1ZSAmJiBzdGF0ZS5oYXNWYWx1ZS52YWx1ZSA9PT0gdHJ1ZSAmJiBzdGF0ZS5lZGl0YWJsZS52YWx1ZSA9PT0gdHJ1ZSkge1xuICAgICAgbm9kZS5wdXNoKFxuICAgICAgICBnZXRJbm5lckFwcGVuZE5vZGUoJ2lubmVyLWNsZWFyYWJsZS1hcHBlbmQnLCBbXG4gICAgICAgICAgaChRSWNvbiwge1xuICAgICAgICAgICAgY2xhc3M6ICdxLWZpZWxkX19mb2N1c2FibGUtYWN0aW9uJyxcbiAgICAgICAgICAgIHRhZzogJ2J1dHRvbicsXG4gICAgICAgICAgICBuYW1lOiBwcm9wcy5jbGVhckljb24gfHwgJHEuaWNvblNldC5maWVsZC5jbGVhcixcbiAgICAgICAgICAgIHRhYmluZGV4OiAwLFxuICAgICAgICAgICAgdHlwZTogJ2J1dHRvbicsXG4gICAgICAgICAgICAnYXJpYS1oaWRkZW4nOiBudWxsLFxuICAgICAgICAgICAgcm9sZTogbnVsbCxcbiAgICAgICAgICAgIG9uQ2xpY2s6IGNsZWFyVmFsdWVcbiAgICAgICAgICB9KVxuICAgICAgICBdKVxuICAgICAgKVxuICAgIH1cblxuICAgIHNsb3RzLmFwcGVuZCAhPT0gdm9pZCAwICYmIG5vZGUucHVzaChcbiAgICAgIGgoJ2RpdicsIHtcbiAgICAgICAgY2xhc3M6ICdxLWZpZWxkX19hcHBlbmQgcS1maWVsZF9fbWFyZ2luYWwgcm93IG5vLXdyYXAgaXRlbXMtY2VudGVyJyxcbiAgICAgICAga2V5OiAnYXBwZW5kJyxcbiAgICAgICAgb25DbGljazogcHJldmVudFxuICAgICAgfSwgc2xvdHMuYXBwZW5kKCkpXG4gICAgKVxuXG4gICAgc3RhdGUuZ2V0SW5uZXJBcHBlbmQgIT09IHZvaWQgMCAmJiBub2RlLnB1c2goXG4gICAgICBnZXRJbm5lckFwcGVuZE5vZGUoJ2lubmVyLWFwcGVuZCcsIHN0YXRlLmdldElubmVyQXBwZW5kKCkpXG4gICAgKVxuXG4gICAgc3RhdGUuZ2V0Q29udHJvbENoaWxkICE9PSB2b2lkIDAgJiYgbm9kZS5wdXNoKFxuICAgICAgc3RhdGUuZ2V0Q29udHJvbENoaWxkKClcbiAgICApXG5cbiAgICByZXR1cm4gbm9kZVxuICB9XG5cbiAgZnVuY3Rpb24gZ2V0Q29udHJvbENvbnRhaW5lciAoKSB7XG4gICAgY29uc3Qgbm9kZSA9IFtdXG5cbiAgICBwcm9wcy5wcmVmaXggIT09IHZvaWQgMCAmJiBwcm9wcy5wcmVmaXggIT09IG51bGwgJiYgbm9kZS5wdXNoKFxuICAgICAgaCgnZGl2Jywge1xuICAgICAgICBjbGFzczogJ3EtZmllbGRfX3ByZWZpeCBuby1wb2ludGVyLWV2ZW50cyByb3cgaXRlbXMtY2VudGVyJ1xuICAgICAgfSwgcHJvcHMucHJlZml4KVxuICAgIClcblxuICAgIGlmIChzdGF0ZS5nZXRTaGFkb3dDb250cm9sICE9PSB2b2lkIDAgJiYgc3RhdGUuaGFzU2hhZG93LnZhbHVlID09PSB0cnVlKSB7XG4gICAgICBub2RlLnB1c2goXG4gICAgICAgIHN0YXRlLmdldFNoYWRvd0NvbnRyb2woKVxuICAgICAgKVxuICAgIH1cblxuICAgIGlmIChzdGF0ZS5nZXRDb250cm9sICE9PSB2b2lkIDApIHtcbiAgICAgIG5vZGUucHVzaChzdGF0ZS5nZXRDb250cm9sKCkpXG4gICAgfVxuICAgIC8vIGludGVybmFsIHVzYWdlIG9ubHk6XG4gICAgZWxzZSBpZiAoc2xvdHMucmF3Q29udHJvbCAhPT0gdm9pZCAwKSB7XG4gICAgICBub2RlLnB1c2goc2xvdHMucmF3Q29udHJvbCgpKVxuICAgIH1cbiAgICBlbHNlIGlmIChzbG90cy5jb250cm9sICE9PSB2b2lkIDApIHtcbiAgICAgIG5vZGUucHVzaChcbiAgICAgICAgaCgnZGl2Jywge1xuICAgICAgICAgIHJlZjogc3RhdGUudGFyZ2V0UmVmLFxuICAgICAgICAgIGNsYXNzOiAncS1maWVsZF9fbmF0aXZlIHJvdycsXG4gICAgICAgICAgLi4uc3RhdGUuc3BsaXRBdHRycy5hdHRyaWJ1dGVzLnZhbHVlLFxuICAgICAgICAgICdkYXRhLWF1dG9mb2N1cyc6IHByb3BzLmF1dG9mb2N1cyA9PT0gdHJ1ZSB8fCB2b2lkIDBcbiAgICAgICAgfSwgc2xvdHMuY29udHJvbChjb250cm9sU2xvdFNjb3BlLnZhbHVlKSlcbiAgICAgIClcbiAgICB9XG5cbiAgICBoYXNMYWJlbC52YWx1ZSA9PT0gdHJ1ZSAmJiBub2RlLnB1c2goXG4gICAgICBoKCdkaXYnLCB7XG4gICAgICAgIGNsYXNzOiBsYWJlbENsYXNzLnZhbHVlXG4gICAgICB9LCBoU2xvdChzbG90cy5sYWJlbCwgcHJvcHMubGFiZWwpKVxuICAgIClcblxuICAgIHByb3BzLnN1ZmZpeCAhPT0gdm9pZCAwICYmIHByb3BzLnN1ZmZpeCAhPT0gbnVsbCAmJiBub2RlLnB1c2goXG4gICAgICBoKCdkaXYnLCB7XG4gICAgICAgIGNsYXNzOiAncS1maWVsZF9fc3VmZml4IG5vLXBvaW50ZXItZXZlbnRzIHJvdyBpdGVtcy1jZW50ZXInXG4gICAgICB9LCBwcm9wcy5zdWZmaXgpXG4gICAgKVxuXG4gICAgcmV0dXJuIG5vZGUuY29uY2F0KGhTbG90KHNsb3RzLmRlZmF1bHQpKVxuICB9XG5cbiAgZnVuY3Rpb24gZ2V0Qm90dG9tICgpIHtcbiAgICBsZXQgbXNnLCBrZXlcblxuICAgIGlmIChoYXNFcnJvci52YWx1ZSA9PT0gdHJ1ZSkge1xuICAgICAgaWYgKGVycm9yTWVzc2FnZS52YWx1ZSAhPT0gbnVsbCkge1xuICAgICAgICBtc2cgPSBbIGgoJ2RpdicsIHsgcm9sZTogJ2FsZXJ0JyB9LCBlcnJvck1lc3NhZ2UudmFsdWUpIF1cbiAgICAgICAga2V5ID0gYHEtLXNsb3QtZXJyb3ItJHsgZXJyb3JNZXNzYWdlLnZhbHVlIH1gXG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgbXNnID0gaFNsb3Qoc2xvdHMuZXJyb3IpXG4gICAgICAgIGtleSA9ICdxLS1zbG90LWVycm9yJ1xuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIGlmIChwcm9wcy5oaWRlSGludCAhPT0gdHJ1ZSB8fCBzdGF0ZS5mb2N1c2VkLnZhbHVlID09PSB0cnVlKSB7XG4gICAgICBpZiAocHJvcHMuaGludCAhPT0gdm9pZCAwKSB7XG4gICAgICAgIG1zZyA9IFsgaCgnZGl2JywgcHJvcHMuaGludCkgXVxuICAgICAgICBrZXkgPSBgcS0tc2xvdC1oaW50LSR7IHByb3BzLmhpbnQgfWBcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICBtc2cgPSBoU2xvdChzbG90cy5oaW50KVxuICAgICAgICBrZXkgPSAncS0tc2xvdC1oaW50J1xuICAgICAgfVxuICAgIH1cblxuICAgIGNvbnN0IGhhc0NvdW50ZXIgPSBwcm9wcy5jb3VudGVyID09PSB0cnVlIHx8IHNsb3RzLmNvdW50ZXIgIT09IHZvaWQgMFxuXG4gICAgaWYgKHByb3BzLmhpZGVCb3R0b21TcGFjZSA9PT0gdHJ1ZSAmJiBoYXNDb3VudGVyID09PSBmYWxzZSAmJiBtc2cgPT09IHZvaWQgMCkge1xuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgY29uc3QgbWFpbiA9IGgoJ2RpdicsIHtcbiAgICAgIGtleSxcbiAgICAgIGNsYXNzOiAncS1maWVsZF9fbWVzc2FnZXMgY29sJ1xuICAgIH0sIG1zZylcblxuICAgIHJldHVybiBoKCdkaXYnLCB7XG4gICAgICBjbGFzczogJ3EtZmllbGRfX2JvdHRvbSByb3cgaXRlbXMtc3RhcnQgcS1maWVsZF9fYm90dG9tLS0nXG4gICAgICAgICsgKHByb3BzLmhpZGVCb3R0b21TcGFjZSAhPT0gdHJ1ZSA/ICdhbmltYXRlZCcgOiAnc3RhbGUnKVxuICAgIH0sIFtcbiAgICAgIHByb3BzLmhpZGVCb3R0b21TcGFjZSA9PT0gdHJ1ZVxuICAgICAgICA/IG1haW5cbiAgICAgICAgOiBoKFRyYW5zaXRpb24sIHsgbmFtZTogJ3EtdHJhbnNpdGlvbi0tZmllbGQtbWVzc2FnZScgfSwgKCkgPT4gbWFpbiksXG5cbiAgICAgIGhhc0NvdW50ZXIgPT09IHRydWVcbiAgICAgICAgPyBoKCdkaXYnLCB7XG4gICAgICAgICAgY2xhc3M6ICdxLWZpZWxkX19jb3VudGVyJ1xuICAgICAgICB9LCBzbG90cy5jb3VudGVyICE9PSB2b2lkIDAgPyBzbG90cy5jb3VudGVyKCkgOiBzdGF0ZS5jb21wdXRlZENvdW50ZXIudmFsdWUpXG4gICAgICAgIDogbnVsbFxuICAgIF0pXG4gIH1cblxuICBmdW5jdGlvbiBnZXRJbm5lckFwcGVuZE5vZGUgKGtleSwgY29udGVudCkge1xuICAgIHJldHVybiBjb250ZW50ID09PSBudWxsXG4gICAgICA/IG51bGxcbiAgICAgIDogaCgnZGl2Jywge1xuICAgICAgICBrZXksXG4gICAgICAgIGNsYXNzOiAncS1maWVsZF9fYXBwZW5kIHEtZmllbGRfX21hcmdpbmFsIHJvdyBuby13cmFwIGl0ZW1zLWNlbnRlciBxLWFuY2hvci0tc2tpcCdcbiAgICAgIH0sIGNvbnRlbnQpXG4gIH1cblxuICAvLyBleHBvc2UgcHVibGljIG1ldGhvZHNcbiAgT2JqZWN0LmFzc2lnbihwcm94eSwgeyBmb2N1cywgYmx1ciB9KVxuXG4gIGxldCBzaG91bGRBY3RpdmF0ZSA9IGZhbHNlXG5cbiAgb25EZWFjdGl2YXRlZCgoKSA9PiB7XG4gICAgc2hvdWxkQWN0aXZhdGUgPSB0cnVlXG4gIH0pXG5cbiAgb25BY3RpdmF0ZWQoKCkgPT4ge1xuICAgIHNob3VsZEFjdGl2YXRlID09PSB0cnVlICYmIHByb3BzLmF1dG9mb2N1cyA9PT0gdHJ1ZSAmJiBwcm94eS5mb2N1cygpXG4gIH0pXG5cbiAgb25Nb3VudGVkKCgpID0+IHtcbiAgICBpZiAoaXNSdW50aW1lU3NyUHJlSHlkcmF0aW9uLnZhbHVlID09PSB0cnVlICYmIHByb3BzLmZvciA9PT0gdm9pZCAwKSB7XG4gICAgICBzdGF0ZS50YXJnZXRVaWQudmFsdWUgPSBnZXRUYXJnZXRVaWQoKVxuICAgIH1cblxuICAgIHByb3BzLmF1dG9mb2N1cyA9PT0gdHJ1ZSAmJiBwcm94eS5mb2N1cygpXG4gIH0pXG5cbiAgb25CZWZvcmVVbm1vdW50KCgpID0+IHtcbiAgICBjbGVhclRpbWVvdXQoZm9jdXNvdXRUaW1lcilcbiAgfSlcblxuICByZXR1cm4gZnVuY3Rpb24gcmVuZGVyRmllbGQgKCkge1xuICAgIGNvbnN0IGxhYmVsQXR0cnMgPSBzdGF0ZS5nZXRDb250cm9sID09PSB2b2lkIDAgJiYgc2xvdHMuY29udHJvbCA9PT0gdm9pZCAwXG4gICAgICA/IHtcbiAgICAgICAgICAuLi5zdGF0ZS5zcGxpdEF0dHJzLmF0dHJpYnV0ZXMudmFsdWUsXG4gICAgICAgICAgJ2RhdGEtYXV0b2ZvY3VzJzogcHJvcHMuYXV0b2ZvY3VzLFxuICAgICAgICAgIC4uLmF0dHJpYnV0ZXMudmFsdWVcbiAgICAgICAgfVxuICAgICAgOiBhdHRyaWJ1dGVzLnZhbHVlXG5cbiAgICByZXR1cm4gaCgnbGFiZWwnLCB7XG4gICAgICByZWY6IHN0YXRlLnJvb3RSZWYsXG4gICAgICBjbGFzczogW1xuICAgICAgICBjbGFzc2VzLnZhbHVlLFxuICAgICAgICBhdHRycy5jbGFzc1xuICAgICAgXSxcbiAgICAgIHN0eWxlOiBhdHRycy5zdHlsZSxcbiAgICAgIC4uLmxhYmVsQXR0cnNcbiAgICB9LCBbXG4gICAgICBzbG90cy5iZWZvcmUgIT09IHZvaWQgMFxuICAgICAgICA/IGgoJ2RpdicsIHtcbiAgICAgICAgICBjbGFzczogJ3EtZmllbGRfX2JlZm9yZSBxLWZpZWxkX19tYXJnaW5hbCByb3cgbm8td3JhcCBpdGVtcy1jZW50ZXInLFxuICAgICAgICAgIG9uQ2xpY2s6IHByZXZlbnRcbiAgICAgICAgfSwgc2xvdHMuYmVmb3JlKCkpXG4gICAgICAgIDogbnVsbCxcblxuICAgICAgaCgnZGl2Jywge1xuICAgICAgICBjbGFzczogJ3EtZmllbGRfX2lubmVyIHJlbGF0aXZlLXBvc2l0aW9uIGNvbCBzZWxmLXN0cmV0Y2gnXG4gICAgICB9LCBbXG4gICAgICAgIGgoJ2RpdicsIHtcbiAgICAgICAgICByZWY6IHN0YXRlLmNvbnRyb2xSZWYsXG4gICAgICAgICAgY2xhc3M6IGNvbnRlbnRDbGFzcy52YWx1ZSxcbiAgICAgICAgICB0YWJpbmRleDogLTEsXG4gICAgICAgICAgLi4uc3RhdGUuY29udHJvbEV2ZW50c1xuICAgICAgICB9LCBnZXRDb250ZW50KCkpLFxuXG4gICAgICAgIHNob3VsZFJlbmRlckJvdHRvbS52YWx1ZSA9PT0gdHJ1ZVxuICAgICAgICAgID8gZ2V0Qm90dG9tKClcbiAgICAgICAgICA6IG51bGxcbiAgICAgIF0pLFxuXG4gICAgICBzbG90cy5hZnRlciAhPT0gdm9pZCAwXG4gICAgICAgID8gaCgnZGl2Jywge1xuICAgICAgICAgIGNsYXNzOiAncS1maWVsZF9fYWZ0ZXIgcS1maWVsZF9fbWFyZ2luYWwgcm93IG5vLXdyYXAgaXRlbXMtY2VudGVyJyxcbiAgICAgICAgICBvbkNsaWNrOiBwcmV2ZW50XG4gICAgICAgIH0sIHNsb3RzLmFmdGVyKCkpXG4gICAgICAgIDogbnVsbFxuICAgIF0pXG4gIH1cbn1cbiIsImltcG9ydCB7IHJlZiwgd2F0Y2gsIG5leHRUaWNrIH0gZnJvbSAndnVlJ1xuXG5pbXBvcnQgeyBzaG91bGRJZ25vcmVLZXkgfSBmcm9tICcuLi8uLi91dGlscy9wcml2YXRlL2tleS1jb21wb3NpdGlvbi5qcydcblxuLy8gbGVhdmUgTkFNRURfTUFTS1MgYXQgdG9wIG9mIGZpbGUgKGNvZGUgcmVmZXJlbmNlZCBmcm9tIGRvY3MpXG5jb25zdCBOQU1FRF9NQVNLUyA9IHtcbiAgZGF0ZTogJyMjIyMvIyMvIyMnLFxuICBkYXRldGltZTogJyMjIyMvIyMvIyMgIyM6IyMnLFxuICB0aW1lOiAnIyM6IyMnLFxuICBmdWxsdGltZTogJyMjOiMjOiMjJyxcbiAgcGhvbmU6ICcoIyMjKSAjIyMgLSAjIyMjJyxcbiAgY2FyZDogJyMjIyMgIyMjIyAjIyMjICMjIyMnXG59XG5cbmNvbnN0IFRPS0VOUyA9IHtcbiAgJyMnOiB7IHBhdHRlcm46ICdbXFxcXGRdJywgbmVnYXRlOiAnW15cXFxcZF0nIH0sXG5cbiAgUzogeyBwYXR0ZXJuOiAnW2EtekEtWl0nLCBuZWdhdGU6ICdbXmEtekEtWl0nIH0sXG4gIE46IHsgcGF0dGVybjogJ1swLTlhLXpBLVpdJywgbmVnYXRlOiAnW14wLTlhLXpBLVpdJyB9LFxuXG4gIEE6IHsgcGF0dGVybjogJ1thLXpBLVpdJywgbmVnYXRlOiAnW15hLXpBLVpdJywgdHJhbnNmb3JtOiB2ID0+IHYudG9Mb2NhbGVVcHBlckNhc2UoKSB9LFxuICBhOiB7IHBhdHRlcm46ICdbYS16QS1aXScsIG5lZ2F0ZTogJ1teYS16QS1aXScsIHRyYW5zZm9ybTogdiA9PiB2LnRvTG9jYWxlTG93ZXJDYXNlKCkgfSxcblxuICBYOiB7IHBhdHRlcm46ICdbMC05YS16QS1aXScsIG5lZ2F0ZTogJ1teMC05YS16QS1aXScsIHRyYW5zZm9ybTogdiA9PiB2LnRvTG9jYWxlVXBwZXJDYXNlKCkgfSxcbiAgeDogeyBwYXR0ZXJuOiAnWzAtOWEtekEtWl0nLCBuZWdhdGU6ICdbXjAtOWEtekEtWl0nLCB0cmFuc2Zvcm06IHYgPT4gdi50b0xvY2FsZUxvd2VyQ2FzZSgpIH1cbn1cblxuY29uc3QgS0VZUyA9IE9iamVjdC5rZXlzKFRPS0VOUylcbktFWVMuZm9yRWFjaChrZXkgPT4ge1xuICBUT0tFTlNbIGtleSBdLnJlZ2V4ID0gbmV3IFJlZ0V4cChUT0tFTlNbIGtleSBdLnBhdHRlcm4pXG59KVxuXG5jb25zdFxuICB0b2tlblJlZ2V4TWFzayA9IG5ldyBSZWdFeHAoJ1xcXFxcXFxcKFteLiorP14ke30oKXwoW1xcXFxdXSl8KFsuKis/XiR7fSgpfFtcXFxcXV0pfChbJyArIEtFWVMuam9pbignJykgKyAnXSl8KC4pJywgJ2cnKSxcbiAgZXNjUmVnZXggPSAvWy4qKz9eJHt9KCl8W1xcXVxcXFxdL2dcblxuY29uc3QgTUFSS0VSID0gU3RyaW5nLmZyb21DaGFyQ29kZSgxKVxuXG5leHBvcnQgY29uc3QgdXNlTWFza1Byb3BzID0ge1xuICBtYXNrOiBTdHJpbmcsXG4gIHJldmVyc2VGaWxsTWFzazogQm9vbGVhbixcbiAgZmlsbE1hc2s6IFsgQm9vbGVhbiwgU3RyaW5nIF0sXG4gIHVubWFza2VkVmFsdWU6IEJvb2xlYW5cbn1cblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gKHByb3BzLCBlbWl0LCBlbWl0VmFsdWUsIGlucHV0UmVmKSB7XG4gIGxldCBtYXNrTWFya2VkLCBtYXNrUmVwbGFjZWQsIGNvbXB1dGVkTWFzaywgY29tcHV0ZWRVbm1hc2tcblxuICBjb25zdCBoYXNNYXNrID0gcmVmKG51bGwpXG4gIGNvbnN0IGlubmVyVmFsdWUgPSByZWYoZ2V0SW5pdGlhbE1hc2tlZFZhbHVlKCkpXG5cbiAgZnVuY3Rpb24gZ2V0SXNUeXBlVGV4dCAoKSB7XG4gICAgcmV0dXJuIHByb3BzLmF1dG9ncm93ID09PSB0cnVlXG4gICAgICB8fCBbICd0ZXh0YXJlYScsICd0ZXh0JywgJ3NlYXJjaCcsICd1cmwnLCAndGVsJywgJ3Bhc3N3b3JkJyBdLmluY2x1ZGVzKHByb3BzLnR5cGUpXG4gIH1cblxuICB3YXRjaCgoKSA9PiBwcm9wcy50eXBlICsgcHJvcHMuYXV0b2dyb3csIHVwZGF0ZU1hc2tJbnRlcm5hbHMpXG5cbiAgd2F0Y2goKCkgPT4gcHJvcHMubWFzaywgdiA9PiB7XG4gICAgaWYgKHYgIT09IHZvaWQgMCkge1xuICAgICAgdXBkYXRlTWFza1ZhbHVlKGlubmVyVmFsdWUudmFsdWUsIHRydWUpXG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgY29uc3QgdmFsID0gdW5tYXNrVmFsdWUoaW5uZXJWYWx1ZS52YWx1ZSlcbiAgICAgIHVwZGF0ZU1hc2tJbnRlcm5hbHMoKVxuICAgICAgcHJvcHMubW9kZWxWYWx1ZSAhPT0gdmFsICYmIGVtaXQoJ3VwZGF0ZTptb2RlbFZhbHVlJywgdmFsKVxuICAgIH1cbiAgfSlcblxuICB3YXRjaCgoKSA9PiBwcm9wcy5maWxsTWFzayArIHByb3BzLnJldmVyc2VGaWxsTWFzaywgKCkgPT4ge1xuICAgIGhhc01hc2sudmFsdWUgPT09IHRydWUgJiYgdXBkYXRlTWFza1ZhbHVlKGlubmVyVmFsdWUudmFsdWUsIHRydWUpXG4gIH0pXG5cbiAgd2F0Y2goKCkgPT4gcHJvcHMudW5tYXNrZWRWYWx1ZSwgKCkgPT4ge1xuICAgIGhhc01hc2sudmFsdWUgPT09IHRydWUgJiYgdXBkYXRlTWFza1ZhbHVlKGlubmVyVmFsdWUudmFsdWUpXG4gIH0pXG5cbiAgZnVuY3Rpb24gZ2V0SW5pdGlhbE1hc2tlZFZhbHVlICgpIHtcbiAgICB1cGRhdGVNYXNrSW50ZXJuYWxzKClcblxuICAgIGlmIChoYXNNYXNrLnZhbHVlID09PSB0cnVlKSB7XG4gICAgICBjb25zdCBtYXNrZWQgPSBtYXNrVmFsdWUodW5tYXNrVmFsdWUocHJvcHMubW9kZWxWYWx1ZSkpXG5cbiAgICAgIHJldHVybiBwcm9wcy5maWxsTWFzayAhPT0gZmFsc2VcbiAgICAgICAgPyBmaWxsV2l0aE1hc2sobWFza2VkKVxuICAgICAgICA6IG1hc2tlZFxuICAgIH1cblxuICAgIHJldHVybiBwcm9wcy5tb2RlbFZhbHVlXG4gIH1cblxuICBmdW5jdGlvbiBnZXRQYWRkZWRNYXNrTWFya2VkIChzaXplKSB7XG4gICAgaWYgKHNpemUgPCBtYXNrTWFya2VkLmxlbmd0aCkge1xuICAgICAgcmV0dXJuIG1hc2tNYXJrZWQuc2xpY2UoLXNpemUpXG4gICAgfVxuXG4gICAgbGV0IHBhZCA9ICcnLCBsb2NhbE1hc2tNYXJrZWQgPSBtYXNrTWFya2VkXG4gICAgY29uc3QgcGFkUG9zID0gbG9jYWxNYXNrTWFya2VkLmluZGV4T2YoTUFSS0VSKVxuXG4gICAgaWYgKHBhZFBvcyA+IC0xKSB7XG4gICAgICBmb3IgKGxldCBpID0gc2l6ZSAtIGxvY2FsTWFza01hcmtlZC5sZW5ndGg7IGkgPiAwOyBpLS0pIHtcbiAgICAgICAgcGFkICs9IE1BUktFUlxuICAgICAgfVxuXG4gICAgICBsb2NhbE1hc2tNYXJrZWQgPSBsb2NhbE1hc2tNYXJrZWQuc2xpY2UoMCwgcGFkUG9zKSArIHBhZCArIGxvY2FsTWFza01hcmtlZC5zbGljZShwYWRQb3MpXG4gICAgfVxuXG4gICAgcmV0dXJuIGxvY2FsTWFza01hcmtlZFxuICB9XG5cbiAgZnVuY3Rpb24gdXBkYXRlTWFza0ludGVybmFscyAoKSB7XG4gICAgaGFzTWFzay52YWx1ZSA9IHByb3BzLm1hc2sgIT09IHZvaWQgMFxuICAgICAgJiYgcHJvcHMubWFzay5sZW5ndGggPiAwXG4gICAgICAmJiBnZXRJc1R5cGVUZXh0KClcblxuICAgIGlmIChoYXNNYXNrLnZhbHVlID09PSBmYWxzZSkge1xuICAgICAgY29tcHV0ZWRVbm1hc2sgPSB2b2lkIDBcbiAgICAgIG1hc2tNYXJrZWQgPSAnJ1xuICAgICAgbWFza1JlcGxhY2VkID0gJydcbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIGNvbnN0XG4gICAgICBsb2NhbENvbXB1dGVkTWFzayA9IE5BTUVEX01BU0tTWyBwcm9wcy5tYXNrIF0gPT09IHZvaWQgMFxuICAgICAgICA/IHByb3BzLm1hc2tcbiAgICAgICAgOiBOQU1FRF9NQVNLU1sgcHJvcHMubWFzayBdLFxuICAgICAgZmlsbENoYXIgPSB0eXBlb2YgcHJvcHMuZmlsbE1hc2sgPT09ICdzdHJpbmcnICYmIHByb3BzLmZpbGxNYXNrLmxlbmd0aCA+IDBcbiAgICAgICAgPyBwcm9wcy5maWxsTWFzay5zbGljZSgwLCAxKVxuICAgICAgICA6ICdfJyxcbiAgICAgIGZpbGxDaGFyRXNjYXBlZCA9IGZpbGxDaGFyLnJlcGxhY2UoZXNjUmVnZXgsICdcXFxcJCYnKSxcbiAgICAgIHVubWFzayA9IFtdLFxuICAgICAgZXh0cmFjdCA9IFtdLFxuICAgICAgbWFzayA9IFtdXG5cbiAgICBsZXRcbiAgICAgIGZpcnN0TWF0Y2ggPSBwcm9wcy5yZXZlcnNlRmlsbE1hc2sgPT09IHRydWUsXG4gICAgICB1bm1hc2tDaGFyID0gJycsXG4gICAgICBuZWdhdGVDaGFyID0gJydcblxuICAgIGxvY2FsQ29tcHV0ZWRNYXNrLnJlcGxhY2UodG9rZW5SZWdleE1hc2ssIChfLCBjaGFyMSwgZXNjLCB0b2tlbiwgY2hhcjIpID0+IHtcbiAgICAgIGlmICh0b2tlbiAhPT0gdm9pZCAwKSB7XG4gICAgICAgIGNvbnN0IGMgPSBUT0tFTlNbIHRva2VuIF1cbiAgICAgICAgbWFzay5wdXNoKGMpXG4gICAgICAgIG5lZ2F0ZUNoYXIgPSBjLm5lZ2F0ZVxuICAgICAgICBpZiAoZmlyc3RNYXRjaCA9PT0gdHJ1ZSkge1xuICAgICAgICAgIGV4dHJhY3QucHVzaCgnKD86JyArIG5lZ2F0ZUNoYXIgKyAnKyk/KCcgKyBjLnBhdHRlcm4gKyAnKyk/KD86JyArIG5lZ2F0ZUNoYXIgKyAnKyk/KCcgKyBjLnBhdHRlcm4gKyAnKyk/JylcbiAgICAgICAgICBmaXJzdE1hdGNoID0gZmFsc2VcbiAgICAgICAgfVxuICAgICAgICBleHRyYWN0LnB1c2goJyg/OicgKyBuZWdhdGVDaGFyICsgJyspPygnICsgYy5wYXR0ZXJuICsgJyk/JylcbiAgICAgIH1cbiAgICAgIGVsc2UgaWYgKGVzYyAhPT0gdm9pZCAwKSB7XG4gICAgICAgIHVubWFza0NoYXIgPSAnXFxcXCcgKyAoZXNjID09PSAnXFxcXCcgPyAnJyA6IGVzYylcbiAgICAgICAgbWFzay5wdXNoKGVzYylcbiAgICAgICAgdW5tYXNrLnB1c2goJyhbXicgKyB1bm1hc2tDaGFyICsgJ10rKT8nICsgdW5tYXNrQ2hhciArICc/JylcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICBjb25zdCBjID0gY2hhcjEgIT09IHZvaWQgMCA/IGNoYXIxIDogY2hhcjJcbiAgICAgICAgdW5tYXNrQ2hhciA9IGMgPT09ICdcXFxcJyA/ICdcXFxcXFxcXFxcXFxcXFxcJyA6IGMucmVwbGFjZShlc2NSZWdleCwgJ1xcXFxcXFxcJCYnKVxuICAgICAgICBtYXNrLnB1c2goYylcbiAgICAgICAgdW5tYXNrLnB1c2goJyhbXicgKyB1bm1hc2tDaGFyICsgJ10rKT8nICsgdW5tYXNrQ2hhciArICc/JylcbiAgICAgIH1cbiAgICB9KVxuXG4gICAgY29uc3RcbiAgICAgIHVubWFza01hdGNoZXIgPSBuZXcgUmVnRXhwKFxuICAgICAgICAnXidcbiAgICAgICAgKyB1bm1hc2suam9pbignJylcbiAgICAgICAgKyAnKCcgKyAodW5tYXNrQ2hhciA9PT0gJycgPyAnLicgOiAnW14nICsgdW5tYXNrQ2hhciArICddJykgKyAnKyk/J1xuICAgICAgICArICckJ1xuICAgICAgKSxcbiAgICAgIGV4dHJhY3RMYXN0ID0gZXh0cmFjdC5sZW5ndGggLSAxLFxuICAgICAgZXh0cmFjdE1hdGNoZXIgPSBleHRyYWN0Lm1hcCgocmUsIGluZGV4KSA9PiB7XG4gICAgICAgIGlmIChpbmRleCA9PT0gMCAmJiBwcm9wcy5yZXZlcnNlRmlsbE1hc2sgPT09IHRydWUpIHtcbiAgICAgICAgICByZXR1cm4gbmV3IFJlZ0V4cCgnXicgKyBmaWxsQ2hhckVzY2FwZWQgKyAnKicgKyByZSlcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmIChpbmRleCA9PT0gZXh0cmFjdExhc3QpIHtcbiAgICAgICAgICByZXR1cm4gbmV3IFJlZ0V4cChcbiAgICAgICAgICAgICdeJyArIHJlXG4gICAgICAgICAgICArICcoJyArIChuZWdhdGVDaGFyID09PSAnJyA/ICcuJyA6IG5lZ2F0ZUNoYXIpICsgJyspPydcbiAgICAgICAgICAgICsgKHByb3BzLnJldmVyc2VGaWxsTWFzayA9PT0gdHJ1ZSA/ICckJyA6IGZpbGxDaGFyRXNjYXBlZCArICcqJylcbiAgICAgICAgICApXG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gbmV3IFJlZ0V4cCgnXicgKyByZSlcbiAgICAgIH0pXG5cbiAgICBjb21wdXRlZE1hc2sgPSBtYXNrXG4gICAgY29tcHV0ZWRVbm1hc2sgPSB2YWwgPT4ge1xuICAgICAgY29uc3QgdW5tYXNrTWF0Y2ggPSB1bm1hc2tNYXRjaGVyLmV4ZWModmFsKVxuICAgICAgaWYgKHVubWFza01hdGNoICE9PSBudWxsKSB7XG4gICAgICAgIHZhbCA9IHVubWFza01hdGNoLnNsaWNlKDEpLmpvaW4oJycpXG4gICAgICB9XG5cbiAgICAgIGNvbnN0XG4gICAgICAgIGV4dHJhY3RNYXRjaCA9IFtdLFxuICAgICAgICBleHRyYWN0TWF0Y2hlckxlbmd0aCA9IGV4dHJhY3RNYXRjaGVyLmxlbmd0aFxuXG4gICAgICBmb3IgKGxldCBpID0gMCwgc3RyID0gdmFsOyBpIDwgZXh0cmFjdE1hdGNoZXJMZW5ndGg7IGkrKykge1xuICAgICAgICBjb25zdCBtID0gZXh0cmFjdE1hdGNoZXJbIGkgXS5leGVjKHN0cilcblxuICAgICAgICBpZiAobSA9PT0gbnVsbCkge1xuICAgICAgICAgIGJyZWFrXG4gICAgICAgIH1cblxuICAgICAgICBzdHIgPSBzdHIuc2xpY2UobS5zaGlmdCgpLmxlbmd0aClcbiAgICAgICAgZXh0cmFjdE1hdGNoLnB1c2goLi4ubSlcbiAgICAgIH1cbiAgICAgIGlmIChleHRyYWN0TWF0Y2gubGVuZ3RoID4gMCkge1xuICAgICAgICByZXR1cm4gZXh0cmFjdE1hdGNoLmpvaW4oJycpXG4gICAgICB9XG5cbiAgICAgIHJldHVybiB2YWxcbiAgICB9XG4gICAgbWFza01hcmtlZCA9IG1hc2subWFwKHYgPT4gKHR5cGVvZiB2ID09PSAnc3RyaW5nJyA/IHYgOiBNQVJLRVIpKS5qb2luKCcnKVxuICAgIG1hc2tSZXBsYWNlZCA9IG1hc2tNYXJrZWQuc3BsaXQoTUFSS0VSKS5qb2luKGZpbGxDaGFyKVxuICB9XG5cbiAgZnVuY3Rpb24gdXBkYXRlTWFza1ZhbHVlIChyYXdWYWwsIHVwZGF0ZU1hc2tJbnRlcm5hbHNGbGFnLCBpbnB1dFR5cGUpIHtcbiAgICBjb25zdFxuICAgICAgaW5wID0gaW5wdXRSZWYudmFsdWUsXG4gICAgICBlbmQgPSBpbnAuc2VsZWN0aW9uRW5kLFxuICAgICAgZW5kUmV2ZXJzZSA9IGlucC52YWx1ZS5sZW5ndGggLSBlbmQsXG4gICAgICB1bm1hc2tlZCA9IHVubWFza1ZhbHVlKHJhd1ZhbClcblxuICAgIC8vIFVwZGF0ZSBoZXJlIHNvIHVubWFzayB1c2VzIHRoZSBvcmlnaW5hbCBmaWxsQ2hhclxuICAgIHVwZGF0ZU1hc2tJbnRlcm5hbHNGbGFnID09PSB0cnVlICYmIHVwZGF0ZU1hc2tJbnRlcm5hbHMoKVxuXG4gICAgY29uc3RcbiAgICAgIHByZU1hc2tlZCA9IG1hc2tWYWx1ZSh1bm1hc2tlZCksXG4gICAgICBtYXNrZWQgPSBwcm9wcy5maWxsTWFzayAhPT0gZmFsc2VcbiAgICAgICAgPyBmaWxsV2l0aE1hc2socHJlTWFza2VkKVxuICAgICAgICA6IHByZU1hc2tlZCxcbiAgICAgIGNoYW5nZWQgPSBpbm5lclZhbHVlLnZhbHVlICE9PSBtYXNrZWRcblxuICAgIC8vIFdlIHdhbnQgdG8gYXZvaWQgXCJmbGlja2VyaW5nXCIgc28gd2Ugc2V0IHZhbHVlIGltbWVkaWF0ZWx5XG4gICAgaW5wLnZhbHVlICE9PSBtYXNrZWQgJiYgKGlucC52YWx1ZSA9IG1hc2tlZClcblxuICAgIGNoYW5nZWQgPT09IHRydWUgJiYgKGlubmVyVmFsdWUudmFsdWUgPSBtYXNrZWQpXG5cbiAgICBkb2N1bWVudC5hY3RpdmVFbGVtZW50ID09PSBpbnAgJiYgbmV4dFRpY2soKCkgPT4ge1xuICAgICAgaWYgKG1hc2tlZCA9PT0gbWFza1JlcGxhY2VkKSB7XG4gICAgICAgIGNvbnN0IGN1cnNvciA9IHByb3BzLnJldmVyc2VGaWxsTWFzayA9PT0gdHJ1ZSA/IG1hc2tSZXBsYWNlZC5sZW5ndGggOiAwXG4gICAgICAgIGlucC5zZXRTZWxlY3Rpb25SYW5nZShjdXJzb3IsIGN1cnNvciwgJ2ZvcndhcmQnKVxuXG4gICAgICAgIHJldHVyblxuICAgICAgfVxuXG4gICAgICBpZiAoaW5wdXRUeXBlID09PSAnaW5zZXJ0RnJvbVBhc3RlJyAmJiBwcm9wcy5yZXZlcnNlRmlsbE1hc2sgIT09IHRydWUpIHtcbiAgICAgICAgY29uc3QgY3Vyc29yID0gZW5kIC0gMVxuICAgICAgICBtb3ZlQ3Vyc29yLnJpZ2h0KGlucCwgY3Vyc29yLCBjdXJzb3IpXG5cbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG5cbiAgICAgIGlmIChbICdkZWxldGVDb250ZW50QmFja3dhcmQnLCAnZGVsZXRlQ29udGVudEZvcndhcmQnIF0uaW5kZXhPZihpbnB1dFR5cGUpID4gLTEpIHtcbiAgICAgICAgY29uc3QgY3Vyc29yID0gcHJvcHMucmV2ZXJzZUZpbGxNYXNrID09PSB0cnVlXG4gICAgICAgICAgPyAoXG4gICAgICAgICAgICAgIGVuZCA9PT0gMFxuICAgICAgICAgICAgICAgID8gKG1hc2tlZC5sZW5ndGggPiBwcmVNYXNrZWQubGVuZ3RoID8gMSA6IDApXG4gICAgICAgICAgICAgICAgOiBNYXRoLm1heCgwLCBtYXNrZWQubGVuZ3RoIC0gKG1hc2tlZCA9PT0gbWFza1JlcGxhY2VkID8gMCA6IE1hdGgubWluKHByZU1hc2tlZC5sZW5ndGgsIGVuZFJldmVyc2UpICsgMSkpICsgMVxuICAgICAgICAgICAgKVxuICAgICAgICAgIDogZW5kXG5cbiAgICAgICAgaW5wLnNldFNlbGVjdGlvblJhbmdlKGN1cnNvciwgY3Vyc29yLCAnZm9yd2FyZCcpXG4gICAgICAgIHJldHVyblxuICAgICAgfVxuXG4gICAgICBpZiAocHJvcHMucmV2ZXJzZUZpbGxNYXNrID09PSB0cnVlKSB7XG4gICAgICAgIGlmIChjaGFuZ2VkID09PSB0cnVlKSB7XG4gICAgICAgICAgY29uc3QgY3Vyc29yID0gTWF0aC5tYXgoMCwgbWFza2VkLmxlbmd0aCAtIChtYXNrZWQgPT09IG1hc2tSZXBsYWNlZCA/IDAgOiBNYXRoLm1pbihwcmVNYXNrZWQubGVuZ3RoLCBlbmRSZXZlcnNlICsgMSkpKVxuXG4gICAgICAgICAgaWYgKGN1cnNvciA9PT0gMSAmJiBlbmQgPT09IDEpIHtcbiAgICAgICAgICAgIGlucC5zZXRTZWxlY3Rpb25SYW5nZShjdXJzb3IsIGN1cnNvciwgJ2ZvcndhcmQnKVxuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIG1vdmVDdXJzb3IucmlnaHRSZXZlcnNlKGlucCwgY3Vyc29yLCBjdXJzb3IpXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIGNvbnN0IGN1cnNvciA9IG1hc2tlZC5sZW5ndGggLSBlbmRSZXZlcnNlXG4gICAgICAgICAgaW5wLnNldFNlbGVjdGlvblJhbmdlKGN1cnNvciwgY3Vyc29yLCAnYmFja3dhcmQnKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgaWYgKGNoYW5nZWQgPT09IHRydWUpIHtcbiAgICAgICAgICBjb25zdCBjdXJzb3IgPSBNYXRoLm1heCgwLCBtYXNrTWFya2VkLmluZGV4T2YoTUFSS0VSKSwgTWF0aC5taW4ocHJlTWFza2VkLmxlbmd0aCwgZW5kKSAtIDEpXG4gICAgICAgICAgbW92ZUN1cnNvci5yaWdodChpbnAsIGN1cnNvciwgY3Vyc29yKVxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIGNvbnN0IGN1cnNvciA9IGVuZCAtIDFcbiAgICAgICAgICBtb3ZlQ3Vyc29yLnJpZ2h0KGlucCwgY3Vyc29yLCBjdXJzb3IpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KVxuXG4gICAgY29uc3QgdmFsID0gcHJvcHMudW5tYXNrZWRWYWx1ZSA9PT0gdHJ1ZVxuICAgICAgPyB1bm1hc2tWYWx1ZShtYXNrZWQpXG4gICAgICA6IG1hc2tlZFxuXG4gICAgU3RyaW5nKHByb3BzLm1vZGVsVmFsdWUpICE9PSB2YWwgJiYgZW1pdFZhbHVlKHZhbCwgdHJ1ZSlcbiAgfVxuXG4gIGZ1bmN0aW9uIG1vdmVDdXJzb3JGb3JQYXN0ZSAoaW5wLCBzdGFydCwgZW5kKSB7XG4gICAgY29uc3QgcHJlTWFza2VkID0gbWFza1ZhbHVlKHVubWFza1ZhbHVlKGlucC52YWx1ZSkpXG5cbiAgICBzdGFydCA9IE1hdGgubWF4KDAsIG1hc2tNYXJrZWQuaW5kZXhPZihNQVJLRVIpLCBNYXRoLm1pbihwcmVNYXNrZWQubGVuZ3RoLCBzdGFydCkpXG5cbiAgICBpbnAuc2V0U2VsZWN0aW9uUmFuZ2Uoc3RhcnQsIGVuZCwgJ2ZvcndhcmQnKVxuICB9XG5cbiAgY29uc3QgbW92ZUN1cnNvciA9IHtcbiAgICBsZWZ0IChpbnAsIHN0YXJ0LCBlbmQsIHNlbGVjdGlvbikge1xuICAgICAgY29uc3Qgbm9NYXJrQmVmb3JlID0gbWFza01hcmtlZC5zbGljZShzdGFydCAtIDEpLmluZGV4T2YoTUFSS0VSKSA9PT0gLTFcbiAgICAgIGxldCBpID0gTWF0aC5tYXgoMCwgc3RhcnQgLSAxKVxuXG4gICAgICBmb3IgKDsgaSA+PSAwOyBpLS0pIHtcbiAgICAgICAgaWYgKG1hc2tNYXJrZWRbIGkgXSA9PT0gTUFSS0VSKSB7XG4gICAgICAgICAgc3RhcnQgPSBpXG4gICAgICAgICAgbm9NYXJrQmVmb3JlID09PSB0cnVlICYmIHN0YXJ0KytcbiAgICAgICAgICBicmVha1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmIChcbiAgICAgICAgaSA8IDBcbiAgICAgICAgJiYgbWFza01hcmtlZFsgc3RhcnQgXSAhPT0gdm9pZCAwXG4gICAgICAgICYmIG1hc2tNYXJrZWRbIHN0YXJ0IF0gIT09IE1BUktFUlxuICAgICAgKSB7XG4gICAgICAgIHJldHVybiBtb3ZlQ3Vyc29yLnJpZ2h0KGlucCwgMCwgMClcbiAgICAgIH1cblxuICAgICAgc3RhcnQgPj0gMCAmJiBpbnAuc2V0U2VsZWN0aW9uUmFuZ2UoXG4gICAgICAgIHN0YXJ0LFxuICAgICAgICBzZWxlY3Rpb24gPT09IHRydWUgPyBlbmQgOiBzdGFydCwgJ2JhY2t3YXJkJ1xuICAgICAgKVxuICAgIH0sXG5cbiAgICByaWdodCAoaW5wLCBzdGFydCwgZW5kLCBzZWxlY3Rpb24pIHtcbiAgICAgIGNvbnN0IGxpbWl0ID0gaW5wLnZhbHVlLmxlbmd0aFxuICAgICAgbGV0IGkgPSBNYXRoLm1pbihsaW1pdCwgZW5kICsgMSlcblxuICAgICAgZm9yICg7IGkgPD0gbGltaXQ7IGkrKykge1xuICAgICAgICBpZiAobWFza01hcmtlZFsgaSBdID09PSBNQVJLRVIpIHtcbiAgICAgICAgICBlbmQgPSBpXG4gICAgICAgICAgYnJlYWtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmIChtYXNrTWFya2VkWyBpIC0gMSBdID09PSBNQVJLRVIpIHtcbiAgICAgICAgICBlbmQgPSBpXG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKFxuICAgICAgICBpID4gbGltaXRcbiAgICAgICAgJiYgbWFza01hcmtlZFsgZW5kIC0gMSBdICE9PSB2b2lkIDBcbiAgICAgICAgJiYgbWFza01hcmtlZFsgZW5kIC0gMSBdICE9PSBNQVJLRVJcbiAgICAgICkge1xuICAgICAgICByZXR1cm4gbW92ZUN1cnNvci5sZWZ0KGlucCwgbGltaXQsIGxpbWl0KVxuICAgICAgfVxuXG4gICAgICBpbnAuc2V0U2VsZWN0aW9uUmFuZ2Uoc2VsZWN0aW9uID8gc3RhcnQgOiBlbmQsIGVuZCwgJ2ZvcndhcmQnKVxuICAgIH0sXG5cbiAgICBsZWZ0UmV2ZXJzZSAoaW5wLCBzdGFydCwgZW5kLCBzZWxlY3Rpb24pIHtcbiAgICAgIGNvbnN0XG4gICAgICAgIGxvY2FsTWFza01hcmtlZCA9IGdldFBhZGRlZE1hc2tNYXJrZWQoaW5wLnZhbHVlLmxlbmd0aClcbiAgICAgIGxldCBpID0gTWF0aC5tYXgoMCwgc3RhcnQgLSAxKVxuXG4gICAgICBmb3IgKDsgaSA+PSAwOyBpLS0pIHtcbiAgICAgICAgaWYgKGxvY2FsTWFza01hcmtlZFsgaSAtIDEgXSA9PT0gTUFSS0VSKSB7XG4gICAgICAgICAgc3RhcnQgPSBpXG4gICAgICAgICAgYnJlYWtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmIChsb2NhbE1hc2tNYXJrZWRbIGkgXSA9PT0gTUFSS0VSKSB7XG4gICAgICAgICAgc3RhcnQgPSBpXG4gICAgICAgICAgaWYgKGkgPT09IDApIHtcbiAgICAgICAgICAgIGJyZWFrXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmIChcbiAgICAgICAgaSA8IDBcbiAgICAgICAgJiYgbG9jYWxNYXNrTWFya2VkWyBzdGFydCBdICE9PSB2b2lkIDBcbiAgICAgICAgJiYgbG9jYWxNYXNrTWFya2VkWyBzdGFydCBdICE9PSBNQVJLRVJcbiAgICAgICkge1xuICAgICAgICByZXR1cm4gbW92ZUN1cnNvci5yaWdodFJldmVyc2UoaW5wLCAwLCAwKVxuICAgICAgfVxuXG4gICAgICBzdGFydCA+PSAwICYmIGlucC5zZXRTZWxlY3Rpb25SYW5nZShcbiAgICAgICAgc3RhcnQsXG4gICAgICAgIHNlbGVjdGlvbiA9PT0gdHJ1ZSA/IGVuZCA6IHN0YXJ0LCAnYmFja3dhcmQnXG4gICAgICApXG4gICAgfSxcblxuICAgIHJpZ2h0UmV2ZXJzZSAoaW5wLCBzdGFydCwgZW5kLCBzZWxlY3Rpb24pIHtcbiAgICAgIGNvbnN0XG4gICAgICAgIGxpbWl0ID0gaW5wLnZhbHVlLmxlbmd0aCxcbiAgICAgICAgbG9jYWxNYXNrTWFya2VkID0gZ2V0UGFkZGVkTWFza01hcmtlZChsaW1pdCksXG4gICAgICAgIG5vTWFya0JlZm9yZSA9IGxvY2FsTWFza01hcmtlZC5zbGljZSgwLCBlbmQgKyAxKS5pbmRleE9mKE1BUktFUikgPT09IC0xXG4gICAgICBsZXQgaSA9IE1hdGgubWluKGxpbWl0LCBlbmQgKyAxKVxuXG4gICAgICBmb3IgKDsgaSA8PSBsaW1pdDsgaSsrKSB7XG4gICAgICAgIGlmIChsb2NhbE1hc2tNYXJrZWRbIGkgLSAxIF0gPT09IE1BUktFUikge1xuICAgICAgICAgIGVuZCA9IGlcbiAgICAgICAgICBlbmQgPiAwICYmIG5vTWFya0JlZm9yZSA9PT0gdHJ1ZSAmJiBlbmQtLVxuICAgICAgICAgIGJyZWFrXG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKFxuICAgICAgICBpID4gbGltaXRcbiAgICAgICAgJiYgbG9jYWxNYXNrTWFya2VkWyBlbmQgLSAxIF0gIT09IHZvaWQgMFxuICAgICAgICAmJiBsb2NhbE1hc2tNYXJrZWRbIGVuZCAtIDEgXSAhPT0gTUFSS0VSXG4gICAgICApIHtcbiAgICAgICAgcmV0dXJuIG1vdmVDdXJzb3IubGVmdFJldmVyc2UoaW5wLCBsaW1pdCwgbGltaXQpXG4gICAgICB9XG5cbiAgICAgIGlucC5zZXRTZWxlY3Rpb25SYW5nZShzZWxlY3Rpb24gPT09IHRydWUgPyBzdGFydCA6IGVuZCwgZW5kLCAnZm9yd2FyZCcpXG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gb25NYXNrZWRLZXlkb3duIChlKSB7XG4gICAgaWYgKHNob3VsZElnbm9yZUtleShlKSA9PT0gdHJ1ZSkge1xuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgY29uc3RcbiAgICAgIGlucCA9IGlucHV0UmVmLnZhbHVlLFxuICAgICAgc3RhcnQgPSBpbnAuc2VsZWN0aW9uU3RhcnQsXG4gICAgICBlbmQgPSBpbnAuc2VsZWN0aW9uRW5kXG5cbiAgICBpZiAoZS5rZXlDb2RlID09PSAzNyB8fCBlLmtleUNvZGUgPT09IDM5KSB7IC8vIExlZnQgLyBSaWdodFxuICAgICAgY29uc3QgZm4gPSBtb3ZlQ3Vyc29yWyAoZS5rZXlDb2RlID09PSAzOSA/ICdyaWdodCcgOiAnbGVmdCcpICsgKHByb3BzLnJldmVyc2VGaWxsTWFzayA9PT0gdHJ1ZSA/ICdSZXZlcnNlJyA6ICcnKSBdXG5cbiAgICAgIGUucHJldmVudERlZmF1bHQoKVxuICAgICAgZm4oaW5wLCBzdGFydCwgZW5kLCBlLnNoaWZ0S2V5KVxuICAgIH1cbiAgICBlbHNlIGlmIChcbiAgICAgIGUua2V5Q29kZSA9PT0gOCAvLyBCYWNrc3BhY2VcbiAgICAgICYmIHByb3BzLnJldmVyc2VGaWxsTWFzayAhPT0gdHJ1ZVxuICAgICAgJiYgc3RhcnQgPT09IGVuZFxuICAgICkge1xuICAgICAgbW92ZUN1cnNvci5sZWZ0KGlucCwgc3RhcnQsIGVuZCwgdHJ1ZSlcbiAgICB9XG4gICAgZWxzZSBpZiAoXG4gICAgICBlLmtleUNvZGUgPT09IDQ2IC8vIERlbGV0ZVxuICAgICAgJiYgcHJvcHMucmV2ZXJzZUZpbGxNYXNrID09PSB0cnVlXG4gICAgICAmJiBzdGFydCA9PT0gZW5kXG4gICAgKSB7XG4gICAgICBtb3ZlQ3Vyc29yLnJpZ2h0UmV2ZXJzZShpbnAsIHN0YXJ0LCBlbmQsIHRydWUpXG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gbWFza1ZhbHVlICh2YWwpIHtcbiAgICBpZiAodmFsID09PSB2b2lkIDAgfHwgdmFsID09PSBudWxsIHx8IHZhbCA9PT0gJycpIHsgcmV0dXJuICcnIH1cblxuICAgIGlmIChwcm9wcy5yZXZlcnNlRmlsbE1hc2sgPT09IHRydWUpIHtcbiAgICAgIHJldHVybiBtYXNrVmFsdWVSZXZlcnNlKHZhbClcbiAgICB9XG5cbiAgICBjb25zdCBtYXNrID0gY29tcHV0ZWRNYXNrXG5cbiAgICBsZXQgdmFsSW5kZXggPSAwLCBvdXRwdXQgPSAnJ1xuXG4gICAgZm9yIChsZXQgbWFza0luZGV4ID0gMDsgbWFza0luZGV4IDwgbWFzay5sZW5ndGg7IG1hc2tJbmRleCsrKSB7XG4gICAgICBjb25zdFxuICAgICAgICB2YWxDaGFyID0gdmFsWyB2YWxJbmRleCBdLFxuICAgICAgICBtYXNrRGVmID0gbWFza1sgbWFza0luZGV4IF1cblxuICAgICAgaWYgKHR5cGVvZiBtYXNrRGVmID09PSAnc3RyaW5nJykge1xuICAgICAgICBvdXRwdXQgKz0gbWFza0RlZlxuICAgICAgICB2YWxDaGFyID09PSBtYXNrRGVmICYmIHZhbEluZGV4KytcbiAgICAgIH1cbiAgICAgIGVsc2UgaWYgKHZhbENoYXIgIT09IHZvaWQgMCAmJiBtYXNrRGVmLnJlZ2V4LnRlc3QodmFsQ2hhcikpIHtcbiAgICAgICAgb3V0cHV0ICs9IG1hc2tEZWYudHJhbnNmb3JtICE9PSB2b2lkIDBcbiAgICAgICAgICA/IG1hc2tEZWYudHJhbnNmb3JtKHZhbENoYXIpXG4gICAgICAgICAgOiB2YWxDaGFyXG4gICAgICAgIHZhbEluZGV4KytcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICByZXR1cm4gb3V0cHV0XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIG91dHB1dFxuICB9XG5cbiAgZnVuY3Rpb24gbWFza1ZhbHVlUmV2ZXJzZSAodmFsKSB7XG4gICAgY29uc3RcbiAgICAgIG1hc2sgPSBjb21wdXRlZE1hc2ssXG4gICAgICBmaXJzdFRva2VuSW5kZXggPSBtYXNrTWFya2VkLmluZGV4T2YoTUFSS0VSKVxuXG4gICAgbGV0IHZhbEluZGV4ID0gdmFsLmxlbmd0aCAtIDEsIG91dHB1dCA9ICcnXG5cbiAgICBmb3IgKGxldCBtYXNrSW5kZXggPSBtYXNrLmxlbmd0aCAtIDE7IG1hc2tJbmRleCA+PSAwICYmIHZhbEluZGV4ID4gLTE7IG1hc2tJbmRleC0tKSB7XG4gICAgICBjb25zdCBtYXNrRGVmID0gbWFza1sgbWFza0luZGV4IF1cblxuICAgICAgbGV0IHZhbENoYXIgPSB2YWxbIHZhbEluZGV4IF1cblxuICAgICAgaWYgKHR5cGVvZiBtYXNrRGVmID09PSAnc3RyaW5nJykge1xuICAgICAgICBvdXRwdXQgPSBtYXNrRGVmICsgb3V0cHV0XG4gICAgICAgIHZhbENoYXIgPT09IG1hc2tEZWYgJiYgdmFsSW5kZXgtLVxuICAgICAgfVxuICAgICAgZWxzZSBpZiAodmFsQ2hhciAhPT0gdm9pZCAwICYmIG1hc2tEZWYucmVnZXgudGVzdCh2YWxDaGFyKSkge1xuICAgICAgICBkbyB7XG4gICAgICAgICAgb3V0cHV0ID0gKG1hc2tEZWYudHJhbnNmb3JtICE9PSB2b2lkIDAgPyBtYXNrRGVmLnRyYW5zZm9ybSh2YWxDaGFyKSA6IHZhbENoYXIpICsgb3V0cHV0XG4gICAgICAgICAgdmFsSW5kZXgtLVxuICAgICAgICAgIHZhbENoYXIgPSB2YWxbIHZhbEluZGV4IF1cbiAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXVubW9kaWZpZWQtbG9vcC1jb25kaXRpb25cbiAgICAgICAgfSB3aGlsZSAoZmlyc3RUb2tlbkluZGV4ID09PSBtYXNrSW5kZXggJiYgdmFsQ2hhciAhPT0gdm9pZCAwICYmIG1hc2tEZWYucmVnZXgudGVzdCh2YWxDaGFyKSlcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICByZXR1cm4gb3V0cHV0XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIG91dHB1dFxuICB9XG5cbiAgZnVuY3Rpb24gdW5tYXNrVmFsdWUgKHZhbCkge1xuICAgIHJldHVybiB0eXBlb2YgdmFsICE9PSAnc3RyaW5nJyB8fCBjb21wdXRlZFVubWFzayA9PT0gdm9pZCAwXG4gICAgICA/ICh0eXBlb2YgdmFsID09PSAnbnVtYmVyJyA/IGNvbXB1dGVkVW5tYXNrKCcnICsgdmFsKSA6IHZhbClcbiAgICAgIDogY29tcHV0ZWRVbm1hc2sodmFsKVxuICB9XG5cbiAgZnVuY3Rpb24gZmlsbFdpdGhNYXNrICh2YWwpIHtcbiAgICBpZiAobWFza1JlcGxhY2VkLmxlbmd0aCAtIHZhbC5sZW5ndGggPD0gMCkge1xuICAgICAgcmV0dXJuIHZhbFxuICAgIH1cblxuICAgIHJldHVybiBwcm9wcy5yZXZlcnNlRmlsbE1hc2sgPT09IHRydWUgJiYgdmFsLmxlbmd0aCA+IDBcbiAgICAgID8gbWFza1JlcGxhY2VkLnNsaWNlKDAsIC12YWwubGVuZ3RoKSArIHZhbFxuICAgICAgOiB2YWwgKyBtYXNrUmVwbGFjZWQuc2xpY2UodmFsLmxlbmd0aClcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgaW5uZXJWYWx1ZSxcbiAgICBoYXNNYXNrLFxuICAgIG1vdmVDdXJzb3JGb3JQYXN0ZSxcbiAgICB1cGRhdGVNYXNrVmFsdWUsXG4gICAgb25NYXNrZWRLZXlkb3duXG4gIH1cbn1cbiIsImltcG9ydCB7IGNvbXB1dGVkIH0gZnJvbSAndnVlJ1xuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiAocHJvcHMsIHR5cGVHdWFyZCkge1xuICBmdW5jdGlvbiBnZXRGb3JtRG9tUHJvcHMgKCkge1xuICAgIGNvbnN0IG1vZGVsID0gcHJvcHMubW9kZWxWYWx1ZVxuXG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IGR0ID0gJ0RhdGFUcmFuc2ZlcicgaW4gd2luZG93XG4gICAgICAgID8gbmV3IERhdGFUcmFuc2ZlcigpXG4gICAgICAgIDogKCdDbGlwYm9hcmRFdmVudCcgaW4gd2luZG93XG4gICAgICAgICAgICA/IG5ldyBDbGlwYm9hcmRFdmVudCgnJykuY2xpcGJvYXJkRGF0YVxuICAgICAgICAgICAgOiB2b2lkIDBcbiAgICAgICAgICApXG5cbiAgICAgIGlmIChPYmplY3QobW9kZWwpID09PSBtb2RlbCkge1xuICAgICAgICAoJ2xlbmd0aCcgaW4gbW9kZWxcbiAgICAgICAgICA/IEFycmF5LmZyb20obW9kZWwpXG4gICAgICAgICAgOiBbIG1vZGVsIF1cbiAgICAgICAgKS5mb3JFYWNoKGZpbGUgPT4ge1xuICAgICAgICAgIGR0Lml0ZW1zLmFkZChmaWxlKVxuICAgICAgICB9KVxuICAgICAgfVxuXG4gICAgICByZXR1cm4ge1xuICAgICAgICBmaWxlczogZHQuZmlsZXNcbiAgICAgIH1cbiAgICB9XG4gICAgY2F0Y2ggKGUpIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGZpbGVzOiB2b2lkIDBcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICByZXR1cm4gdHlwZUd1YXJkID09PSB0cnVlXG4gICAgPyBjb21wdXRlZCgoKSA9PiB7XG4gICAgICBpZiAocHJvcHMudHlwZSAhPT0gJ2ZpbGUnKSB7XG4gICAgICAgIHJldHVyblxuICAgICAgfVxuXG4gICAgICByZXR1cm4gZ2V0Rm9ybURvbVByb3BzKClcbiAgICB9KVxuICAgIDogY29tcHV0ZWQoZ2V0Rm9ybURvbVByb3BzKVxufVxuIiwiY29uc3QgaXNKYXBhbmVzZSA9IC9bXFx1MzAwMC1cXHUzMDNmXFx1MzA0MC1cXHUzMDlmXFx1MzBhMC1cXHUzMGZmXFx1ZmYwMC1cXHVmZjlmXFx1NGUwMC1cXHU5ZmFmXFx1MzQwMC1cXHU0ZGJmXS9cbmNvbnN0IGlzQ2hpbmVzZSA9IC9bXFx1NGUwMC1cXHU5ZmZmXFx1MzQwMC1cXHU0ZGJmXFx1ezIwMDAwfS1cXHV7MmE2ZGZ9XFx1ezJhNzAwfS1cXHV7MmI3M2Z9XFx1ezJiNzQwfS1cXHV7MmI4MWZ9XFx1ezJiODIwfS1cXHV7MmNlYWZ9XFx1ZjkwMC1cXHVmYWZmXFx1MzMwMC1cXHUzM2ZmXFx1ZmUzMC1cXHVmZTRmXFx1ZjkwMC1cXHVmYWZmXFx1ezJmODAwfS1cXHV7MmZhMWZ9XS91XG5jb25zdCBpc0tvcmVhbiA9IC9bXFx1MzEzMS1cXHUzMTRlXFx1MzE0Zi1cXHUzMTYzXFx1YWMwMC1cXHVkN2EzXS9cblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gKG9uSW5wdXQpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIG9uQ29tcG9zaXRpb24gKGUpIHtcbiAgICBpZiAoZS50eXBlID09PSAnY29tcG9zaXRpb25lbmQnIHx8IGUudHlwZSA9PT0gJ2NoYW5nZScpIHtcbiAgICAgIGlmIChlLnRhcmdldC5jb21wb3NpbmcgIT09IHRydWUpIHsgcmV0dXJuIH1cbiAgICAgIGUudGFyZ2V0LmNvbXBvc2luZyA9IGZhbHNlXG4gICAgICBvbklucHV0KGUpXG4gICAgfVxuICAgIGVsc2UgaWYgKGUudHlwZSA9PT0gJ2NvbXBvc2l0aW9udXBkYXRlJykge1xuICAgICAgaWYgKFxuICAgICAgICB0eXBlb2YgZS5kYXRhID09PSAnc3RyaW5nJ1xuICAgICAgICAmJiBpc0phcGFuZXNlLnRlc3QoZS5kYXRhKSA9PT0gZmFsc2VcbiAgICAgICAgJiYgaXNDaGluZXNlLnRlc3QoZS5kYXRhKSA9PT0gZmFsc2VcbiAgICAgICAgJiYgaXNLb3JlYW4udGVzdChlLmRhdGEpID09PSBmYWxzZVxuICAgICAgKSB7XG4gICAgICAgIGUudGFyZ2V0LmNvbXBvc2luZyA9IGZhbHNlXG4gICAgICB9XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgZS50YXJnZXQuY29tcG9zaW5nID0gdHJ1ZVxuICAgIH1cbiAgfVxufVxuIiwiaW1wb3J0IHsgaCwgcmVmLCBjb21wdXRlZCwgd2F0Y2gsIG9uQmVmb3JlVW5tb3VudCwgb25Nb3VudGVkLCBuZXh0VGljaywgZ2V0Q3VycmVudEluc3RhbmNlIH0gZnJvbSAndnVlJ1xuXG5pbXBvcnQgdXNlRmllbGQsIHsgdXNlRmllbGRTdGF0ZSwgdXNlRmllbGRQcm9wcywgdXNlRmllbGRFbWl0cywgZmllbGRWYWx1ZUlzRmlsbGVkIH0gZnJvbSAnLi4vLi4vY29tcG9zYWJsZXMvcHJpdmF0ZS91c2UtZmllbGQuanMnXG5pbXBvcnQgdXNlTWFzaywgeyB1c2VNYXNrUHJvcHMgfSBmcm9tICcuL3VzZS1tYXNrLmpzJ1xuaW1wb3J0IHsgdXNlRm9ybVByb3BzLCB1c2VGb3JtSW5wdXROYW1lQXR0ciB9IGZyb20gJy4uLy4uL2NvbXBvc2FibGVzL3ByaXZhdGUvdXNlLWZvcm0uanMnXG5pbXBvcnQgdXNlRmlsZUZvcm1Eb21Qcm9wcyBmcm9tICcuLi8uLi9jb21wb3NhYmxlcy9wcml2YXRlL3VzZS1maWxlLWRvbS1wcm9wcy5qcydcbmltcG9ydCB1c2VLZXlDb21wb3NpdGlvbiBmcm9tICcuLi8uLi9jb21wb3NhYmxlcy9wcml2YXRlL3VzZS1rZXktY29tcG9zaXRpb24uanMnXG5cbmltcG9ydCB7IGNyZWF0ZUNvbXBvbmVudCB9IGZyb20gJy4uLy4uL3V0aWxzL3ByaXZhdGUvY3JlYXRlLmpzJ1xuaW1wb3J0IHsgc3RvcCB9IGZyb20gJy4uLy4uL3V0aWxzL2V2ZW50LmpzJ1xuaW1wb3J0IHsgYWRkRm9jdXNGbiB9IGZyb20gJy4uLy4uL3V0aWxzL3ByaXZhdGUvZm9jdXMtbWFuYWdlci5qcydcblxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlQ29tcG9uZW50KHtcbiAgbmFtZTogJ1FJbnB1dCcsXG5cbiAgaW5oZXJpdEF0dHJzOiBmYWxzZSxcblxuICBwcm9wczoge1xuICAgIC4uLnVzZUZpZWxkUHJvcHMsXG4gICAgLi4udXNlTWFza1Byb3BzLFxuICAgIC4uLnVzZUZvcm1Qcm9wcyxcblxuICAgIG1vZGVsVmFsdWU6IHsgcmVxdWlyZWQ6IGZhbHNlIH0sXG5cbiAgICBzaGFkb3dUZXh0OiBTdHJpbmcsXG5cbiAgICB0eXBlOiB7XG4gICAgICB0eXBlOiBTdHJpbmcsXG4gICAgICBkZWZhdWx0OiAndGV4dCdcbiAgICB9LFxuXG4gICAgZGVib3VuY2U6IFsgU3RyaW5nLCBOdW1iZXIgXSxcblxuICAgIGF1dG9ncm93OiBCb29sZWFuLCAvLyBtYWtlcyBhIHRleHRhcmVhXG5cbiAgICBpbnB1dENsYXNzOiBbIEFycmF5LCBTdHJpbmcsIE9iamVjdCBdLFxuICAgIGlucHV0U3R5bGU6IFsgQXJyYXksIFN0cmluZywgT2JqZWN0IF1cbiAgfSxcblxuICBlbWl0czogW1xuICAgIC4uLnVzZUZpZWxkRW1pdHMsXG4gICAgJ3Bhc3RlJywgJ2NoYW5nZSdcbiAgXSxcblxuICBzZXR1cCAocHJvcHMsIHsgZW1pdCwgYXR0cnMgfSkge1xuICAgIGNvbnN0IHRlbXAgPSB7fVxuICAgIGxldCBlbWl0Q2FjaGVkVmFsdWUgPSBOYU4sIHR5cGVkTnVtYmVyLCBzdG9wVmFsdWVXYXRjaGVyLCBlbWl0VGltZXIsIGVtaXRWYWx1ZUZuXG5cbiAgICBjb25zdCBpbnB1dFJlZiA9IHJlZihudWxsKVxuICAgIGNvbnN0IG5hbWVQcm9wID0gdXNlRm9ybUlucHV0TmFtZUF0dHIocHJvcHMpXG5cbiAgICBjb25zdCB7XG4gICAgICBpbm5lclZhbHVlLFxuICAgICAgaGFzTWFzayxcbiAgICAgIG1vdmVDdXJzb3JGb3JQYXN0ZSxcbiAgICAgIHVwZGF0ZU1hc2tWYWx1ZSxcbiAgICAgIG9uTWFza2VkS2V5ZG93blxuICAgIH0gPSB1c2VNYXNrKHByb3BzLCBlbWl0LCBlbWl0VmFsdWUsIGlucHV0UmVmKVxuXG4gICAgY29uc3QgZm9ybURvbVByb3BzID0gdXNlRmlsZUZvcm1Eb21Qcm9wcyhwcm9wcywgLyogdHlwZSBndWFyZCAqLyB0cnVlKVxuICAgIGNvbnN0IGhhc1ZhbHVlID0gY29tcHV0ZWQoKCkgPT4gZmllbGRWYWx1ZUlzRmlsbGVkKGlubmVyVmFsdWUudmFsdWUpKVxuXG4gICAgY29uc3Qgb25Db21wb3NpdGlvbiA9IHVzZUtleUNvbXBvc2l0aW9uKG9uSW5wdXQpXG5cbiAgICBjb25zdCBzdGF0ZSA9IHVzZUZpZWxkU3RhdGUoKVxuXG4gICAgY29uc3QgaXNUZXh0YXJlYSA9IGNvbXB1dGVkKCgpID0+XG4gICAgICBwcm9wcy50eXBlID09PSAndGV4dGFyZWEnIHx8IHByb3BzLmF1dG9ncm93ID09PSB0cnVlXG4gICAgKVxuXG4gICAgY29uc3QgaXNUeXBlVGV4dCA9IGNvbXB1dGVkKCgpID0+XG4gICAgICBpc1RleHRhcmVhLnZhbHVlID09PSB0cnVlXG4gICAgICB8fCBbICd0ZXh0JywgJ3NlYXJjaCcsICd1cmwnLCAndGVsJywgJ3Bhc3N3b3JkJyBdLmluY2x1ZGVzKHByb3BzLnR5cGUpXG4gICAgKVxuXG4gICAgY29uc3Qgb25FdmVudHMgPSBjb21wdXRlZCgoKSA9PiB7XG4gICAgICBjb25zdCBldnQgPSB7XG4gICAgICAgIC4uLnN0YXRlLnNwbGl0QXR0cnMubGlzdGVuZXJzLnZhbHVlLFxuICAgICAgICBvbklucHV0LFxuICAgICAgICBvblBhc3RlLFxuICAgICAgICAvLyBTYWZhcmkgPCAxMC4yICYgVUlXZWJWaWV3IGRvZXNuJ3QgZmlyZSBjb21wb3NpdGlvbmVuZCB3aGVuXG4gICAgICAgIC8vIHN3aXRjaGluZyBmb2N1cyBiZWZvcmUgY29uZmlybWluZyBjb21wb3NpdGlvbiBjaG9pY2VcbiAgICAgICAgLy8gdGhpcyBhbHNvIGZpeGVzIHRoZSBpc3N1ZSB3aGVyZSBzb21lIGJyb3dzZXJzIGUuZy4gaU9TIENocm9tZVxuICAgICAgICAvLyBmaXJlcyBcImNoYW5nZVwiIGluc3RlYWQgb2YgXCJpbnB1dFwiIG9uIGF1dG9jb21wbGV0ZS5cbiAgICAgICAgb25DaGFuZ2UsXG4gICAgICAgIG9uQmx1cjogb25GaW5pc2hFZGl0aW5nLFxuICAgICAgICBvbkZvY3VzOiBzdG9wXG4gICAgICB9XG5cbiAgICAgIGV2dC5vbkNvbXBvc2l0aW9uc3RhcnQgPSBldnQub25Db21wb3NpdGlvbnVwZGF0ZSA9IGV2dC5vbkNvbXBvc2l0aW9uZW5kID0gb25Db21wb3NpdGlvblxuXG4gICAgICBpZiAoaGFzTWFzay52YWx1ZSA9PT0gdHJ1ZSkge1xuICAgICAgICBldnQub25LZXlkb3duID0gb25NYXNrZWRLZXlkb3duXG4gICAgICB9XG5cbiAgICAgIGlmIChwcm9wcy5hdXRvZ3JvdyA9PT0gdHJ1ZSkge1xuICAgICAgICBldnQub25BbmltYXRpb25lbmQgPSBhZGp1c3RIZWlnaHRcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGV2dFxuICAgIH0pXG5cbiAgICBjb25zdCBpbnB1dEF0dHJzID0gY29tcHV0ZWQoKCkgPT4ge1xuICAgICAgY29uc3QgYXR0cnMgPSB7XG4gICAgICAgIHRhYmluZGV4OiAwLFxuICAgICAgICAnZGF0YS1hdXRvZm9jdXMnOiBwcm9wcy5hdXRvZm9jdXMgPT09IHRydWUgfHwgdm9pZCAwLFxuICAgICAgICByb3dzOiBwcm9wcy50eXBlID09PSAndGV4dGFyZWEnID8gNiA6IHZvaWQgMCxcbiAgICAgICAgJ2FyaWEtbGFiZWwnOiBwcm9wcy5sYWJlbCxcbiAgICAgICAgbmFtZTogbmFtZVByb3AudmFsdWUsXG4gICAgICAgIC4uLnN0YXRlLnNwbGl0QXR0cnMuYXR0cmlidXRlcy52YWx1ZSxcbiAgICAgICAgaWQ6IHN0YXRlLnRhcmdldFVpZC52YWx1ZSxcbiAgICAgICAgbWF4bGVuZ3RoOiBwcm9wcy5tYXhsZW5ndGgsXG4gICAgICAgIGRpc2FibGVkOiBwcm9wcy5kaXNhYmxlID09PSB0cnVlLFxuICAgICAgICByZWFkb25seTogcHJvcHMucmVhZG9ubHkgPT09IHRydWVcbiAgICAgIH1cblxuICAgICAgaWYgKGlzVGV4dGFyZWEudmFsdWUgPT09IGZhbHNlKSB7XG4gICAgICAgIGF0dHJzLnR5cGUgPSBwcm9wcy50eXBlXG4gICAgICB9XG5cbiAgICAgIGlmIChwcm9wcy5hdXRvZ3JvdyA9PT0gdHJ1ZSkge1xuICAgICAgICBhdHRycy5yb3dzID0gMVxuICAgICAgfVxuXG4gICAgICByZXR1cm4gYXR0cnNcbiAgICB9KVxuXG4gICAgLy8gc29tZSBicm93c2VycyBsb3NlIHRoZSBuYXRpdmUgaW5wdXQgdmFsdWVcbiAgICAvLyBzbyB3ZSBuZWVkIHRvIHJlYXR0YWNoIGl0IGR5bmFtaWNhbGx5XG4gICAgLy8gKGxpa2UgdHlwZT1cInBhc3N3b3JkXCIgPC0+IHR5cGU9XCJ0ZXh0XCI7IHNlZSAjMTIwNzgpXG4gICAgd2F0Y2goKCkgPT4gcHJvcHMudHlwZSwgKCkgPT4ge1xuICAgICAgaWYgKGlucHV0UmVmLnZhbHVlKSB7XG4gICAgICAgIGlucHV0UmVmLnZhbHVlLnZhbHVlID0gcHJvcHMubW9kZWxWYWx1ZVxuICAgICAgfVxuICAgIH0pXG5cbiAgICB3YXRjaCgoKSA9PiBwcm9wcy5tb2RlbFZhbHVlLCB2ID0+IHtcbiAgICAgIGlmIChoYXNNYXNrLnZhbHVlID09PSB0cnVlKSB7XG4gICAgICAgIGlmIChzdG9wVmFsdWVXYXRjaGVyID09PSB0cnVlKSB7XG4gICAgICAgICAgc3RvcFZhbHVlV2F0Y2hlciA9IGZhbHNlXG5cbiAgICAgICAgICBpZiAoU3RyaW5nKHYpID09PSBlbWl0Q2FjaGVkVmFsdWUpIHtcbiAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHVwZGF0ZU1hc2tWYWx1ZSh2KVxuICAgICAgfVxuICAgICAgZWxzZSBpZiAoaW5uZXJWYWx1ZS52YWx1ZSAhPT0gdikge1xuICAgICAgICBpbm5lclZhbHVlLnZhbHVlID0gdlxuXG4gICAgICAgIGlmIChcbiAgICAgICAgICBwcm9wcy50eXBlID09PSAnbnVtYmVyJ1xuICAgICAgICAgICYmIHRlbXAuaGFzT3duUHJvcGVydHkoJ3ZhbHVlJykgPT09IHRydWVcbiAgICAgICAgKSB7XG4gICAgICAgICAgaWYgKHR5cGVkTnVtYmVyID09PSB0cnVlKSB7XG4gICAgICAgICAgICB0eXBlZE51bWJlciA9IGZhbHNlXG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgZGVsZXRlIHRlbXAudmFsdWVcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgLy8gdGV4dGFyZWEgb25seVxuICAgICAgcHJvcHMuYXV0b2dyb3cgPT09IHRydWUgJiYgbmV4dFRpY2soYWRqdXN0SGVpZ2h0KVxuICAgIH0pXG5cbiAgICB3YXRjaCgoKSA9PiBwcm9wcy5hdXRvZ3JvdywgdmFsID0+IHtcbiAgICAgIC8vIHRleHRhcmVhIG9ubHlcbiAgICAgIGlmICh2YWwgPT09IHRydWUpIHtcbiAgICAgICAgbmV4dFRpY2soYWRqdXN0SGVpZ2h0KVxuICAgICAgfVxuICAgICAgLy8gaWYgaXQgaGFzIGEgbnVtYmVyIG9mIHJvd3Mgc2V0IHJlc3BlY3QgaXRcbiAgICAgIGVsc2UgaWYgKGlucHV0UmVmLnZhbHVlICE9PSBudWxsICYmIGF0dHJzLnJvd3MgPiAwKSB7XG4gICAgICAgIGlucHV0UmVmLnZhbHVlLnN0eWxlLmhlaWdodCA9ICdhdXRvJ1xuICAgICAgfVxuICAgIH0pXG5cbiAgICB3YXRjaCgoKSA9PiBwcm9wcy5kZW5zZSwgKCkgPT4ge1xuICAgICAgcHJvcHMuYXV0b2dyb3cgPT09IHRydWUgJiYgbmV4dFRpY2soYWRqdXN0SGVpZ2h0KVxuICAgIH0pXG5cbiAgICBmdW5jdGlvbiBmb2N1cyAoKSB7XG4gICAgICBhZGRGb2N1c0ZuKCgpID0+IHtcbiAgICAgICAgY29uc3QgZWwgPSBkb2N1bWVudC5hY3RpdmVFbGVtZW50XG4gICAgICAgIGlmIChcbiAgICAgICAgICBpbnB1dFJlZi52YWx1ZSAhPT0gbnVsbFxuICAgICAgICAgICYmIGlucHV0UmVmLnZhbHVlICE9PSBlbFxuICAgICAgICAgICYmIChlbCA9PT0gbnVsbCB8fCBlbC5pZCAhPT0gc3RhdGUudGFyZ2V0VWlkLnZhbHVlKVxuICAgICAgICApIHtcbiAgICAgICAgICBpbnB1dFJlZi52YWx1ZS5mb2N1cyh7IHByZXZlbnRTY3JvbGw6IHRydWUgfSlcbiAgICAgICAgfVxuICAgICAgfSlcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBzZWxlY3QgKCkge1xuICAgICAgaW5wdXRSZWYudmFsdWUgIT09IG51bGwgJiYgaW5wdXRSZWYudmFsdWUuc2VsZWN0KClcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBvblBhc3RlIChlKSB7XG4gICAgICBpZiAoaGFzTWFzay52YWx1ZSA9PT0gdHJ1ZSAmJiBwcm9wcy5yZXZlcnNlRmlsbE1hc2sgIT09IHRydWUpIHtcbiAgICAgICAgY29uc3QgaW5wID0gZS50YXJnZXRcbiAgICAgICAgbW92ZUN1cnNvckZvclBhc3RlKGlucCwgaW5wLnNlbGVjdGlvblN0YXJ0LCBpbnAuc2VsZWN0aW9uRW5kKVxuICAgICAgfVxuXG4gICAgICBlbWl0KCdwYXN0ZScsIGUpXG4gICAgfVxuXG4gICAgZnVuY3Rpb24gb25JbnB1dCAoZSkge1xuICAgICAgaWYgKCFlIHx8ICFlLnRhcmdldCB8fCBlLnRhcmdldC5jb21wb3NpbmcgPT09IHRydWUpIHtcbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG5cbiAgICAgIGlmIChwcm9wcy50eXBlID09PSAnZmlsZScpIHtcbiAgICAgICAgZW1pdCgndXBkYXRlOm1vZGVsVmFsdWUnLCBlLnRhcmdldC5maWxlcylcbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG5cbiAgICAgIGNvbnN0IHZhbCA9IGUudGFyZ2V0LnZhbHVlXG5cbiAgICAgIGlmIChoYXNNYXNrLnZhbHVlID09PSB0cnVlKSB7XG4gICAgICAgIHVwZGF0ZU1hc2tWYWx1ZSh2YWwsIGZhbHNlLCBlLmlucHV0VHlwZSlcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICBlbWl0VmFsdWUodmFsKVxuXG4gICAgICAgIGlmIChpc1R5cGVUZXh0LnZhbHVlID09PSB0cnVlICYmIGUudGFyZ2V0ID09PSBkb2N1bWVudC5hY3RpdmVFbGVtZW50KSB7XG4gICAgICAgICAgY29uc3QgeyBzZWxlY3Rpb25TdGFydCwgc2VsZWN0aW9uRW5kIH0gPSBlLnRhcmdldFxuXG4gICAgICAgICAgaWYgKHNlbGVjdGlvblN0YXJ0ICE9PSB2b2lkIDAgJiYgc2VsZWN0aW9uRW5kICE9PSB2b2lkIDApIHtcbiAgICAgICAgICAgIG5leHRUaWNrKCgpID0+IHtcbiAgICAgICAgICAgICAgaWYgKGUudGFyZ2V0ID09PSBkb2N1bWVudC5hY3RpdmVFbGVtZW50ICYmIHZhbC5pbmRleE9mKGUudGFyZ2V0LnZhbHVlKSA9PT0gMCkge1xuICAgICAgICAgICAgICAgIGUudGFyZ2V0LnNldFNlbGVjdGlvblJhbmdlKHNlbGVjdGlvblN0YXJ0LCBzZWxlY3Rpb25FbmQpXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIC8vIHdlIG5lZWQgdG8gdHJpZ2dlciBpdCBpbW1lZGlhdGVseSB0b28sXG4gICAgICAvLyB0byBhdm9pZCBcImZsaWNrZXJpbmdcIlxuICAgICAgcHJvcHMuYXV0b2dyb3cgPT09IHRydWUgJiYgYWRqdXN0SGVpZ2h0KClcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBlbWl0VmFsdWUgKHZhbCwgc3RvcFdhdGNoZXIpIHtcbiAgICAgIGVtaXRWYWx1ZUZuID0gKCkgPT4ge1xuICAgICAgICBpZiAoXG4gICAgICAgICAgcHJvcHMudHlwZSAhPT0gJ251bWJlcidcbiAgICAgICAgICAmJiB0ZW1wLmhhc093blByb3BlcnR5KCd2YWx1ZScpID09PSB0cnVlXG4gICAgICAgICkge1xuICAgICAgICAgIGRlbGV0ZSB0ZW1wLnZhbHVlXG4gICAgICAgIH1cblxuICAgICAgICBpZiAocHJvcHMubW9kZWxWYWx1ZSAhPT0gdmFsICYmIGVtaXRDYWNoZWRWYWx1ZSAhPT0gdmFsKSB7XG4gICAgICAgICAgc3RvcFdhdGNoZXIgPT09IHRydWUgJiYgKHN0b3BWYWx1ZVdhdGNoZXIgPSB0cnVlKVxuICAgICAgICAgIGVtaXQoJ3VwZGF0ZTptb2RlbFZhbHVlJywgdmFsKVxuXG4gICAgICAgICAgbmV4dFRpY2soKCkgPT4ge1xuICAgICAgICAgICAgZW1pdENhY2hlZFZhbHVlID09PSB2YWwgJiYgKGVtaXRDYWNoZWRWYWx1ZSA9IE5hTilcbiAgICAgICAgICB9KVxuICAgICAgICB9XG5cbiAgICAgICAgZW1pdFZhbHVlRm4gPSB2b2lkIDBcbiAgICAgIH1cblxuICAgICAgaWYgKHByb3BzLnR5cGUgPT09ICdudW1iZXInKSB7XG4gICAgICAgIHR5cGVkTnVtYmVyID0gdHJ1ZVxuICAgICAgICB0ZW1wLnZhbHVlID0gdmFsXG4gICAgICB9XG5cbiAgICAgIGlmIChwcm9wcy5kZWJvdW5jZSAhPT0gdm9pZCAwKSB7XG4gICAgICAgIGNsZWFyVGltZW91dChlbWl0VGltZXIpXG4gICAgICAgIHRlbXAudmFsdWUgPSB2YWxcbiAgICAgICAgZW1pdFRpbWVyID0gc2V0VGltZW91dChlbWl0VmFsdWVGbiwgcHJvcHMuZGVib3VuY2UpXG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgZW1pdFZhbHVlRm4oKVxuICAgICAgfVxuICAgIH1cblxuICAgIC8vIHRleHRhcmVhIG9ubHlcbiAgICBmdW5jdGlvbiBhZGp1c3RIZWlnaHQgKCkge1xuICAgICAgY29uc3QgaW5wID0gaW5wdXRSZWYudmFsdWVcbiAgICAgIGlmIChpbnAgIT09IG51bGwpIHtcbiAgICAgICAgY29uc3QgcGFyZW50U3R5bGUgPSBpbnAucGFyZW50Tm9kZS5zdHlsZVxuXG4gICAgICAgIC8vIHJlc2V0IGhlaWdodCBvZiB0ZXh0YXJlYSB0byBhIHNtYWxsIHNpemUgdG8gZGV0ZWN0IHRoZSByZWFsIGhlaWdodFxuICAgICAgICAvLyBidXQga2VlcCB0aGUgdG90YWwgY29udHJvbCBzaXplIHRoZSBzYW1lXG4gICAgICAgIHBhcmVudFN0eWxlLm1hcmdpbkJvdHRvbSA9IChpbnAuc2Nyb2xsSGVpZ2h0IC0gMSkgKyAncHgnXG4gICAgICAgIGlucC5zdHlsZS5oZWlnaHQgPSAnMXB4J1xuXG4gICAgICAgIGlucC5zdHlsZS5oZWlnaHQgPSBpbnAuc2Nyb2xsSGVpZ2h0ICsgJ3B4J1xuICAgICAgICBwYXJlbnRTdHlsZS5tYXJnaW5Cb3R0b20gPSAnJ1xuICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIG9uQ2hhbmdlIChlKSB7XG4gICAgICBvbkNvbXBvc2l0aW9uKGUpXG5cbiAgICAgIGNsZWFyVGltZW91dChlbWl0VGltZXIpXG4gICAgICBlbWl0VmFsdWVGbiAhPT0gdm9pZCAwICYmIGVtaXRWYWx1ZUZuKClcblxuICAgICAgZW1pdCgnY2hhbmdlJywgZS50YXJnZXQudmFsdWUpXG4gICAgfVxuXG4gICAgZnVuY3Rpb24gb25GaW5pc2hFZGl0aW5nIChlKSB7XG4gICAgICBlICE9PSB2b2lkIDAgJiYgc3RvcChlKVxuXG4gICAgICBjbGVhclRpbWVvdXQoZW1pdFRpbWVyKVxuICAgICAgZW1pdFZhbHVlRm4gIT09IHZvaWQgMCAmJiBlbWl0VmFsdWVGbigpXG5cbiAgICAgIHR5cGVkTnVtYmVyID0gZmFsc2VcbiAgICAgIHN0b3BWYWx1ZVdhdGNoZXIgPSBmYWxzZVxuICAgICAgZGVsZXRlIHRlbXAudmFsdWVcblxuICAgICAgLy8gd2UgbmVlZCB0byB1c2Ugc2V0VGltZW91dCBpbnN0ZWFkIG9mIHRoaXMuJG5leHRUaWNrXG4gICAgICAvLyB0byBhdm9pZCBhIGJ1ZyB3aGVyZSBmb2N1c291dCBpcyBub3QgZW1pdHRlZCBmb3IgdHlwZSBkYXRlL3RpbWUvd2Vlay8uLi5cbiAgICAgIHByb3BzLnR5cGUgIT09ICdmaWxlJyAmJiBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgaWYgKGlucHV0UmVmLnZhbHVlICE9PSBudWxsKSB7XG4gICAgICAgICAgaW5wdXRSZWYudmFsdWUudmFsdWUgPSBpbm5lclZhbHVlLnZhbHVlICE9PSB2b2lkIDAgPyBpbm5lclZhbHVlLnZhbHVlIDogJydcbiAgICAgICAgfVxuICAgICAgfSlcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZXRDdXJWYWx1ZSAoKSB7XG4gICAgICByZXR1cm4gdGVtcC5oYXNPd25Qcm9wZXJ0eSgndmFsdWUnKSA9PT0gdHJ1ZVxuICAgICAgICA/IHRlbXAudmFsdWVcbiAgICAgICAgOiAoaW5uZXJWYWx1ZS52YWx1ZSAhPT0gdm9pZCAwID8gaW5uZXJWYWx1ZS52YWx1ZSA6ICcnKVxuICAgIH1cblxuICAgIG9uQmVmb3JlVW5tb3VudCgoKSA9PiB7XG4gICAgICBvbkZpbmlzaEVkaXRpbmcoKVxuICAgIH0pXG5cbiAgICBvbk1vdW50ZWQoKCkgPT4ge1xuICAgICAgLy8gdGV4dGFyZWEgb25seVxuICAgICAgcHJvcHMuYXV0b2dyb3cgPT09IHRydWUgJiYgYWRqdXN0SGVpZ2h0KClcbiAgICB9KVxuXG4gICAgT2JqZWN0LmFzc2lnbihzdGF0ZSwge1xuICAgICAgaW5uZXJWYWx1ZSxcblxuICAgICAgZmllbGRDbGFzczogY29tcHV0ZWQoKCkgPT5cbiAgICAgICAgYHEtJHsgaXNUZXh0YXJlYS52YWx1ZSA9PT0gdHJ1ZSA/ICd0ZXh0YXJlYScgOiAnaW5wdXQnIH1gXG4gICAgICAgICsgKHByb3BzLmF1dG9ncm93ID09PSB0cnVlID8gJyBxLXRleHRhcmVhLS1hdXRvZ3JvdycgOiAnJylcbiAgICAgICksXG5cbiAgICAgIGhhc1NoYWRvdzogY29tcHV0ZWQoKCkgPT5cbiAgICAgICAgcHJvcHMudHlwZSAhPT0gJ2ZpbGUnXG4gICAgICAgICYmIHR5cGVvZiBwcm9wcy5zaGFkb3dUZXh0ID09PSAnc3RyaW5nJ1xuICAgICAgICAmJiBwcm9wcy5zaGFkb3dUZXh0Lmxlbmd0aCA+IDBcbiAgICAgICksXG5cbiAgICAgIGlucHV0UmVmLFxuXG4gICAgICBlbWl0VmFsdWUsXG5cbiAgICAgIGhhc1ZhbHVlLFxuXG4gICAgICBmbG9hdGluZ0xhYmVsOiBjb21wdXRlZCgoKSA9PlxuICAgICAgICBoYXNWYWx1ZS52YWx1ZSA9PT0gdHJ1ZVxuICAgICAgICB8fCBmaWVsZFZhbHVlSXNGaWxsZWQocHJvcHMuZGlzcGxheVZhbHVlKVxuICAgICAgKSxcblxuICAgICAgZ2V0Q29udHJvbDogKCkgPT4ge1xuICAgICAgICByZXR1cm4gaChpc1RleHRhcmVhLnZhbHVlID09PSB0cnVlID8gJ3RleHRhcmVhJyA6ICdpbnB1dCcsIHtcbiAgICAgICAgICByZWY6IGlucHV0UmVmLFxuICAgICAgICAgIGNsYXNzOiBbXG4gICAgICAgICAgICAncS1maWVsZF9fbmF0aXZlIHEtcGxhY2Vob2xkZXInLFxuICAgICAgICAgICAgcHJvcHMuaW5wdXRDbGFzc1xuICAgICAgICAgIF0sXG4gICAgICAgICAgc3R5bGU6IHByb3BzLmlucHV0U3R5bGUsXG4gICAgICAgICAgLi4uaW5wdXRBdHRycy52YWx1ZSxcbiAgICAgICAgICAuLi5vbkV2ZW50cy52YWx1ZSxcbiAgICAgICAgICAuLi4oXG4gICAgICAgICAgICBwcm9wcy50eXBlICE9PSAnZmlsZSdcbiAgICAgICAgICAgICAgPyB7IHZhbHVlOiBnZXRDdXJWYWx1ZSgpIH1cbiAgICAgICAgICAgICAgOiBmb3JtRG9tUHJvcHMudmFsdWVcbiAgICAgICAgICApXG4gICAgICAgIH0pXG4gICAgICB9LFxuXG4gICAgICBnZXRTaGFkb3dDb250cm9sOiAoKSA9PiB7XG4gICAgICAgIHJldHVybiBoKCdkaXYnLCB7XG4gICAgICAgICAgY2xhc3M6ICdxLWZpZWxkX19uYXRpdmUgcS1maWVsZF9fc2hhZG93IGFic29sdXRlLWJvdHRvbSBuby1wb2ludGVyLWV2ZW50cydcbiAgICAgICAgICAgICsgKGlzVGV4dGFyZWEudmFsdWUgPT09IHRydWUgPyAnJyA6ICcgdGV4dC1uby13cmFwJylcbiAgICAgICAgfSwgW1xuICAgICAgICAgIGgoJ3NwYW4nLCB7IGNsYXNzOiAnaW52aXNpYmxlJyB9LCBnZXRDdXJWYWx1ZSgpKSxcbiAgICAgICAgICBoKCdzcGFuJywgcHJvcHMuc2hhZG93VGV4dClcbiAgICAgICAgXSlcbiAgICAgIH1cbiAgICB9KVxuXG4gICAgY29uc3QgcmVuZGVyRm4gPSB1c2VGaWVsZChzdGF0ZSlcblxuICAgIC8vIGV4cG9zZSBwdWJsaWMgbWV0aG9kc1xuICAgIGNvbnN0IHZtID0gZ2V0Q3VycmVudEluc3RhbmNlKClcbiAgICBPYmplY3QuYXNzaWduKHZtLnByb3h5LCB7XG4gICAgICBmb2N1cyxcbiAgICAgIHNlbGVjdCxcbiAgICAgIGdldE5hdGl2ZUVsZW1lbnQ6ICgpID0+IGlucHV0UmVmLnZhbHVlXG4gICAgfSlcblxuICAgIHJldHVybiByZW5kZXJGblxuICB9XG59KVxuIiwiPHRlbXBsYXRlPlxuICA8cS1wYWdlIGNsYXNzPVwicm93IGl0ZW1zLWNlbnRlciBqdXN0aWZ5LWV2ZW5seVwiPlxuICAgIDxxLWlucHV0IHYtbW9kZWw9XCJpdGVtLm5hbWVcIiBsYWJlbD1cIk5hbWU6IFwiIC8+XG4gICAgPHEtYnRuIEBjbGljaz1cImNyZWF0ZVN0b3JlXCIgbGFiZWw9XCJDcmVhdGVcIiAvPlxuICAgIDxxLWJ0biBAY2xpY2s9XCJsb2FkU3RvcmVzXCIgbGFiZWw9XCJMb2FkXCIgLz5cbiAgICA8dWw+XG4gICAgICA8bGkgdi1mb3I9XCJpdG0gaW4gbGlzdFwiIDprZXk9XCJpdG0uaWRcIj57eyBpdG0ubmFtZSB9fTwvbGk+XG4gICAgPC91bD5cbiAgPC9xLXBhZ2U+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0IGxhbmc9XCJ0c1wiPlxuICBpbXBvcnQgeyBkZWZpbmVDb21wb25lbnQgfSBmcm9tICd2dWUnO1xuICBpbXBvcnQgeyBDcmVhdGVTdG9yZUZvcm0sIFN0b3JlSXRlbSwgU3RvcmVTZXJ2aWNlIH0gZnJvbSAnc3JjL3NlcnZpY2VzL2RvbWFpbic7XG5cbmV4cG9ydCBkZWZhdWx0IGRlZmluZUNvbXBvbmVudCh7XG4gIG5hbWU6ICdJbmRleFBhZ2UnLFxuICBkYXRhICgpIHtcbiAgICByZXR1cm4ge1xuICAgICAgaXRlbTogbmV3IENyZWF0ZVN0b3JlRm9ybSgpLFxuICAgICAgbGlzdDogQXJyYXk8U3RvcmVJdGVtPigpLFxuICAgIH07XG4gIH0sXG4gIG1ldGhvZHM6IHtcbiAgICBjcmVhdGVTdG9yZSgpIHtcbiAgICAgIFN0b3JlU2VydmljZS5jcmVhdGUoeyBib2R5OiB0aGlzLml0ZW0gfSkudGhlbigoKSA9PiB7XG4gICAgICAgIHRoaXMuaXRlbSA9IG5ldyBDcmVhdGVTdG9yZUZvcm0oKTtcbiAgICAgIH0pO1xuICAgIH0sXG4gICAgbG9hZFN0b3JlcygpIHtcbiAgICAgIFN0b3JlU2VydmljZS5nZXRMaXN0KCkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICB0aGlzLmxpc3QubGVuZ3RoID0gMDtcbiAgICAgICAgdGhpcy5saXN0LnB1c2goLi4ucmVzdWx0KTtcbiAgICAgIH0pO1xuICAgIH0sXG4gICAgbG9hZEVudnMoKSB7XG4gICAgICBjb25zb2xlLmxvZyhwcm9jZXNzLmVudik7XG4gICAgfVxuICB9XG59KTtcbjwvc2NyaXB0PlxuIl0sIm5hbWVzIjpbIl9jcmVhdGVCbG9jayIsIl93aXRoQ3R4IiwiX2NyZWF0ZVZOb2RlIiwiX2NyZWF0ZUVsZW1lbnRWTm9kZSIsIl9vcGVuQmxvY2siLCJfY3JlYXRlRWxlbWVudEJsb2NrIiwiX0ZyYWdtZW50IiwiX3JlbmRlckxpc3QiLCJfdG9EaXNwbGF5U3RyaW5nIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBSWUsc0JBQVUsRUFBRSxVQUFVLGlCQUFpQixpQkFBaUI7QUFDckUsUUFBTSxRQUFRLE9BQU8sU0FBUyxLQUFLO0FBRW5DLE1BQUksVUFBVSxPQUFPO0FBQ25CLFVBQU0sRUFBRSxPQUFPLFVBQVUsbUJBQW9CO0FBRzdDLFdBQU8sT0FBTyxPQUFPLEVBQUUsVUFBVSxnQkFBZSxDQUFFO0FBRWxELFVBQU0sTUFBTSxNQUFNLFNBQVMsU0FBTztBQUNoQyxVQUFJLFFBQVEsTUFBTTtBQUNoQixlQUFPLG9CQUFvQixjQUFjLGdCQUFpQjtBQUMxRCxjQUFNLGdCQUFnQixLQUFLO0FBQUEsTUFDNUIsT0FDSTtBQUNILGNBQU0sY0FBYyxLQUFLO0FBQUEsTUFDMUI7QUFBQSxJQUNQLENBQUs7QUFHRCxVQUFNLFlBQVksUUFBUSxNQUFNLGNBQWMsS0FBSztBQUVuRCxvQkFBZ0IsTUFBTTtBQUVwQixZQUFNLFlBQVksUUFBUSxNQUFNLGdCQUFnQixLQUFLO0FBQUEsSUFDM0QsQ0FBSztBQUFBLEVBQ0YsV0FDUSxrQkFBa0IsTUFBTTtBQUMvQixZQUFRLE1BQU0sMkNBQTJDO0FBQUEsRUFDMUQ7QUFDSDtBQ2hDQSxNQUNFLE1BQU0sc0NBQ04sT0FBTyxzQ0FDUCxZQUFZLG9FQUNaLE1BQU0seUhBQ04sT0FBTztBQUdGLE1BQU0sY0FBYztBQUFBLEVBQ3pCLE1BQU0sT0FBSyw4QkFBOEIsS0FBSyxDQUFDO0FBQUEsRUFDL0MsTUFBTSxPQUFLLDhCQUE4QixLQUFLLENBQUM7QUFBQSxFQUMvQyxVQUFVLE9BQUssc0NBQXNDLEtBQUssQ0FBQztBQUFBLEVBQzNELGdCQUFnQixPQUFLLHlDQUF5QyxLQUFLLENBQUM7QUFBQSxFQVFwRSxPQUFPLE9BQUsseUpBQXlKLEtBQUssQ0FBQztBQUFBLEVBRTNLLFVBQVUsT0FBSyxJQUFJLEtBQUssQ0FBQztBQUFBLEVBQ3pCLFdBQVcsT0FBSyxLQUFLLEtBQUssQ0FBQztBQUFBLEVBQzNCLGdCQUFnQixPQUFLLFVBQVUsS0FBSyxDQUFDO0FBQUEsRUFFckMsVUFBVSxPQUFLLElBQUksS0FBSyxDQUFDO0FBQUEsRUFDekIsV0FBVyxPQUFLLEtBQUssS0FBSyxDQUFDO0FBQUEsRUFDM0IsZ0JBQWdCLE9BQUssSUFBSSxLQUFLLENBQUMsS0FBSyxLQUFLLEtBQUssQ0FBQztBQUFBLEVBRS9DLGVBQWUsT0FBSyxJQUFJLEtBQUssQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDO0FBQUEsRUFDN0MsaUJBQWlCLE9BQUssS0FBSyxLQUFLLENBQUMsS0FBSyxLQUFLLEtBQUssQ0FBQztBQUFBLEVBQ2pELFVBQVUsT0FBSyxVQUFVLEtBQUssQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLEtBQUssS0FBSyxLQUFLLENBQUM7QUFDaEU7QUNsQ0MsTUFFQyxhQUFhLENBQUU7QUFFakIsMERBQTBELE1BQU0sR0FBRyxFQUFFLFFBQVEsVUFBUTtBQUNuRixhQUFZLGFBQWEsT0FBTyxPQUFRLEtBQUssWUFBYTtBQUM1RCxDQUFDO0FDSk0sTUFBTSx5QkFBeUIsaUNBQ2pDLGVBRGlDO0FBQUEsRUFHcEMsS0FBSztBQUFBLElBQ0gsTUFBTTtBQUFBLElBQ04sU0FBUztBQUFBLEVBQ1Y7QUFBQSxFQUNELEtBQUs7QUFBQSxJQUNILE1BQU07QUFBQSxJQUNOLFNBQVM7QUFBQSxFQUNWO0FBQUEsRUFFRCxPQUFPO0FBQUEsRUFDUCxhQUFhO0FBQUEsRUFDYixZQUFZO0FBQUEsRUFFWixVQUFVO0FBQUEsRUFHVixXQUFXO0FBQUEsSUFDVCxNQUFNO0FBQUEsSUFDTixTQUFTO0FBQUEsSUFDVCxXQUFXLE9BQUssS0FBSyxLQUFLLEtBQUs7QUFBQSxFQUNoQztBQUFBLEVBRUQsT0FBTztBQUFBLElBQ0wsTUFBTTtBQUFBLElBQ04sU0FBUztBQUFBLEVBQ1Y7QUFBQSxFQUVELFdBQVc7QUFBQSxFQUNYLFNBQVM7QUFBQSxFQUVULGlCQUFpQjtBQUNuQjtBQzVCQSxNQUNFLFNBQVMsSUFDVCxXQUFXLElBQUksUUFDZixnQkFBZ0IsV0FBVyxLQUFLLElBQ2hDLGtCQUFrQixLQUFLLE1BQU0sZ0JBQWdCLEdBQUksSUFBSTtBQUV4QyxnQkFBZ0I7QUFBQSxFQUM3QixNQUFNO0FBQUEsRUFFTixPQUFPLGlDQUNGLHlCQURFO0FBQUEsSUFHTCxPQUFPO0FBQUEsTUFDTCxNQUFNO0FBQUEsTUFDTixTQUFTO0FBQUEsSUFDVjtBQUFBLElBRUQsZ0JBQWdCO0FBQUEsTUFDZCxNQUFNLENBQUUsUUFBUSxNQUFRO0FBQUEsTUFDeEIsU0FBUztBQUFBLElBQ1Y7QUFBQSxJQUVELGVBQWU7QUFBQSxFQUNoQjtBQUFBLEVBRUQsTUFBTyxPQUFPLEVBQUUsU0FBUztBQUN2QixVQUFNLEVBQUUsT0FBTyxFQUFFLFNBQVMsbUJBQW9CO0FBQzlDLFVBQU0sWUFBWSxRQUFRLEtBQUs7QUFFL0IsVUFBTSxXQUFXLFNBQVMsTUFBTTtBQUM5QixZQUFNLFFBQVMsSUFBRyxLQUFLLFFBQVEsT0FBTyxLQUFLLEtBQUssTUFBTTtBQUV0RCxhQUFPO0FBQUEsUUFDTCxXQUFXLE1BQU0sWUFBYSxJQUFHLEtBQUssUUFBUSxRQUMxQyx1Q0FBd0MsTUFBTSxjQUM5QyxxQkFBc0IsUUFBUTtBQUFBLE1BQ25DO0FBQUEsSUFDUCxDQUFLO0FBRUQsVUFBTSxjQUFjLFNBQVMsTUFDM0IsTUFBTSxvQkFBb0IsUUFBUSxNQUFNLGtCQUFrQixPQUN0RCxFQUFFLFlBQVkscUJBQXNCLE1BQU0sb0NBQXNDLE1BQU0sd0JBQTBCLElBQ2hILEVBQ0w7QUFFRCxVQUFNLFVBQVUsU0FBUyxNQUFNLFdBQVksS0FBSSxNQUFNLFlBQVksRUFBRTtBQUVuRSxVQUFNLGNBQWMsU0FBUyxNQUMzQixHQUFJLFFBQVEsUUFBUSxLQUFPLFFBQVEsUUFBUSxLQUFPLFFBQVEsU0FBVyxRQUFRLE9BQzlFO0FBRUQsVUFBTSxhQUFhLFNBQVMsTUFBTSxRQUFRLE1BQU0sT0FBTyxNQUFNLEtBQUssTUFBTSxHQUFHLENBQUM7QUFFNUUsVUFBTSxtQkFBbUIsU0FBUyxNQUFNLGdCQUN0QyxLQUFLLFlBQVcsUUFBUSxNQUFNLE9BQVEsT0FBTSxNQUFNLE1BQU0sS0FDekQ7QUFFRCxVQUFNLGNBQWMsU0FBUyxNQUFNLE1BQU0sWUFBWSxJQUFJLFFBQVEsS0FBSztBQUV0RSx1QkFBb0IsRUFBRSxXQUFXLFFBQVEsT0FBTyxPQUFPO0FBQ3JELGFBQU8sRUFBRSxVQUFVO0FBQUEsUUFDakIsT0FBTywwQkFBMEIsTUFBTyxXQUFVLFNBQVMsU0FBVSxVQUFXO0FBQUEsUUFDaEYsT0FBTyxZQUFZO0FBQUEsUUFDbkIsTUFBTTtBQUFBLFFBQ04sUUFBUTtBQUFBLFFBQ1IsZ0JBQWdCO0FBQUEsUUFDaEIsb0JBQW9CO0FBQUEsUUFDcEIscUJBQXFCO0FBQUEsUUFDckIsSUFBSSxRQUFRO0FBQUEsUUFDWixJQUFJLFFBQVE7QUFBQSxRQUNaLEdBQUc7QUFBQSxNQUNYLENBQU87QUFBQSxJQUNGO0FBRUQsV0FBTyxNQUFNO0FBQ1gsWUFBTSxXQUFXLENBQUU7QUFFbkIsWUFBTSxnQkFBZ0IsVUFBVSxNQUFNLGdCQUFnQixpQkFBaUIsU0FBUyxLQUM5RSxFQUFFLFVBQVU7QUFBQSxRQUNWLE9BQU8sb0NBQXFDLE1BQU07QUFBQSxRQUNsRCxNQUFNO0FBQUEsUUFDTixHQUFHLFNBQVMsWUFBWSxRQUFRO0FBQUEsUUFDaEMsSUFBSSxRQUFRO0FBQUEsUUFDWixJQUFJLFFBQVE7QUFBQSxNQUN0QixDQUFTLENBQ0Y7QUFFRCxZQUFNLGVBQWUsVUFBVSxNQUFNLGVBQWUsaUJBQWlCLFNBQVMsS0FDNUUsVUFBVTtBQUFBLFFBQ1IsS0FBSztBQUFBLFFBQ0wsV0FBVyxZQUFZO0FBQUEsUUFDdkIsUUFBUTtBQUFBLFFBQ1IsT0FBTyxNQUFNO0FBQUEsTUFDdkIsQ0FBUyxDQUNGO0FBRUQsZUFBUyxLQUNQLFVBQVU7QUFBQSxRQUNSLEtBQUs7QUFBQSxRQUNMLFdBQVcsWUFBWTtBQUFBLFFBQ3ZCLFFBQVEsaUJBQWlCO0FBQUEsUUFDekIsT0FBTyxNQUFNO0FBQUEsTUFDdkIsQ0FBUyxDQUNGO0FBRUQsWUFBTSxRQUFRO0FBQUEsUUFDWixFQUFFLE9BQU87QUFBQSxVQUNQLE9BQU87QUFBQSxVQUNQLE9BQU8sU0FBUztBQUFBLFVBQ2hCLFNBQVMsWUFBWTtBQUFBLFVBQ3JCLGVBQWU7QUFBQSxRQUNoQixHQUFFLFFBQVE7QUFBQSxNQUNaO0FBRUQsWUFBTSxjQUFjLFFBQVEsTUFBTSxLQUNoQyxFQUFFLE9BQU87QUFBQSxRQUNQLE9BQU87QUFBQSxRQUNQLE9BQU8sRUFBRSxVQUFVLE1BQU0sU0FBVTtBQUFBLE1BQ3BDLEdBQUUsTUFBTSxZQUFZLFNBQVMsTUFBTSxZQUFZLENBQUUsRUFBRSxPQUFPLFdBQVcsS0FBSyxDQUFDLENBQUUsQ0FDL0U7QUFFRCxhQUFPLEVBQUUsT0FBTztBQUFBLFFBQ2QsT0FBTyw0Q0FBNkMsTUFBTSxrQkFBa0IsT0FBTyxPQUFPO0FBQUEsUUFDMUYsT0FBTyxVQUFVO0FBQUEsUUFDakIsTUFBTTtBQUFBLFFBQ04saUJBQWlCLE1BQU07QUFBQSxRQUN2QixpQkFBaUIsTUFBTTtBQUFBLFFBQ3ZCLGlCQUFpQixNQUFNLGtCQUFrQixPQUFPLFNBQVMsV0FBVztBQUFBLE1BQ3JFLEdBQUUsaUJBQWlCLE1BQU0sVUFBVSxLQUFLLENBQUM7QUFBQSxJQUMzQztBQUFBLEVBQ0Y7QUFDSCxDQUFDO0FDMUdNLE1BQU0sZUFBZSxDQUFFLFVBQVU7QUNLakMsTUFBTSxZQUFZO0FBQUEsRUFDdkIsR0FBRztBQUFBLEVBQ0g7QUFBQSxFQUFTO0FBQUEsRUFBVTtBQUFBLEVBQVM7QUFDOUI7QUMxQ0EsTUFBTSxTQUFTLE1BQU07QUFFTix3QkFBVSxZQUFZO0FBQ25DLFFBQU0sY0FBYyxDQUFFO0FBRXRCLGFBQVcsUUFBUSxTQUFPO0FBQ3hCLGdCQUFhLE9BQVE7QUFBQSxFQUN6QixDQUFHO0FBRUQsU0FBTztBQUNUO0FDSndCLGVBQWUsU0FBUztBQ0ZoRCxJQUNFLEtBQ0EsU0FBUztBQUNYLE1BQU0sV0FBVyxJQUFJLE1BQU0sR0FBRztBQUc5QixTQUFTLElBQUksR0FBRyxJQUFJLEtBQUssS0FBSztBQUM1QixXQUFVLEtBQU8sS0FBSSxLQUFPLFNBQVMsRUFBRSxFQUFFLFVBQVUsQ0FBQztBQUN0RDtBQUdBLE1BQU0sY0FBZSxPQUFNO0FBRXpCLFFBQU0sTUFBTSxPQUFPLFdBQVcsY0FDMUIsU0FFRSxPQUFPLFdBQVcsY0FDZCxPQUFPLFVBQVUsT0FBTyxXQUN4QjtBQUdWLE1BQUksUUFBUSxRQUFRO0FBQ2xCLFFBQUksSUFBSSxnQkFBZ0IsUUFBUTtBQUM5QixhQUFPLElBQUk7QUFBQSxJQUNaO0FBQ0QsUUFBSSxJQUFJLG9CQUFvQixRQUFRO0FBQ2xDLGFBQU8sT0FBSztBQUNWLGNBQU0sUUFBUSxJQUFJLFdBQVcsQ0FBQztBQUM5QixZQUFJLGdCQUFnQixLQUFLO0FBQ3pCLGVBQU87QUFBQSxNQUNSO0FBQUEsSUFDRjtBQUFBLEVBQ0Y7QUFFRCxTQUFPLE9BQUs7QUFDVixVQUFNLElBQUksQ0FBRTtBQUNaLGFBQVMsSUFBSSxHQUFHLElBQUksR0FBRyxLQUFLO0FBQzFCLFFBQUUsS0FBSyxLQUFLLE1BQU0sS0FBSyxPQUFNLElBQUssR0FBRyxDQUFDO0FBQUEsSUFDdkM7QUFDRCxXQUFPO0FBQUEsRUFDUjtBQUNILEdBQUk7QUFLSixNQUFNLGNBQWM7QUFFTCxlQUFZO0FBRXpCLE1BQUksUUFBUSxVQUFXLFNBQVMsS0FBSyxhQUFjO0FBQ2pELGFBQVM7QUFDVCxVQUFNLFlBQVksV0FBVztBQUFBLEVBQzlCO0FBRUQsUUFBTSxJQUFJLE1BQU0sVUFBVSxNQUFNLEtBQUssS0FBSyxRQUFTLFVBQVUsRUFBSTtBQUNqRSxJQUFHLEtBQU8sRUFBRyxLQUFNLEtBQVE7QUFDM0IsSUFBRyxLQUFPLEVBQUcsS0FBTSxLQUFRO0FBRTNCLFNBQU8sU0FBVSxFQUFHLE1BQVEsU0FBVSxFQUFHLE1BQ3JDLFNBQVUsRUFBRyxNQUFRLFNBQVUsRUFBRyxNQUFRLE1BQzFDLFNBQVUsRUFBRyxNQUFRLFNBQVUsRUFBRyxNQUFRLE1BQzFDLFNBQVUsRUFBRyxNQUFRLFNBQVUsRUFBRyxNQUFRLE1BQzFDLFNBQVUsRUFBRyxNQUFRLFNBQVUsRUFBRyxNQUFRLE1BQzFDLFNBQVUsRUFBRyxPQUFTLFNBQVUsRUFBRyxPQUNuQyxTQUFVLEVBQUcsT0FBUyxTQUFVLEVBQUcsT0FDbkMsU0FBVSxFQUFHLE9BQVMsU0FBVSxFQUFHO0FBQ3pDO0FDaEVBLE1BQU0sa0JBQWtCLENBQUUsTUFBTSxPQUFPLFVBQVk7QUFFNUMsTUFBTSxtQkFBbUI7QUFBQSxFQUM5QixZQUFZLENBQUU7QUFBQSxFQUVkLE9BQU87QUFBQSxJQUNMLE1BQU07QUFBQSxJQUNOLFNBQVM7QUFBQSxFQUNWO0FBQUEsRUFDRCxjQUFjO0FBQUEsRUFDZCxhQUFhO0FBQUEsRUFFYixPQUFPO0FBQUEsRUFDUCxlQUFlO0FBQUEsRUFDZixXQUFXO0FBQUEsSUFDVCxNQUFNLENBQUUsU0FBUyxNQUFRO0FBQUEsSUFDekIsV0FBVyxPQUFLLGdCQUFnQixTQUFTLENBQUM7QUFBQSxFQUMzQztBQUNIO0FBRWUscUJBQVUsU0FBUyxjQUFjO0FBQzlDLFFBQU0sRUFBRSxPQUFPLFVBQVUsbUJBQW9CO0FBRTdDLFFBQU0sYUFBYSxJQUFJLEtBQUs7QUFDNUIsUUFBTSxvQkFBb0IsSUFBSSxJQUFJO0FBQ2xDLFFBQU0sZUFBZSxJQUFJLElBQUk7QUFFN0IsZUFBYSxFQUFFLFVBQVUsaUJBQWlCO0FBRTFDLE1BQUksZ0JBQWdCLEdBQUc7QUFFdkIsUUFBTSxXQUFXLFNBQVMsTUFDeEIsTUFBTSxVQUFVLFVBQ2IsTUFBTSxVQUFVLFFBQ2hCLE1BQU0sTUFBTSxTQUFTLENBQ3pCO0FBRUQsUUFBTSxpQkFBaUIsU0FBUyxNQUM5QixNQUFNLFlBQVksUUFDZixTQUFTLFVBQVUsSUFDdkI7QUFFRCxRQUFNLFdBQVcsU0FBUyxNQUN4QixNQUFNLFVBQVUsUUFBUSxXQUFXLFVBQVUsSUFDOUM7QUFFRCxRQUFNLGVBQWUsU0FBUyxNQUM1QixPQUFPLE1BQU0saUJBQWlCLFlBQVksTUFBTSxhQUFhLFNBQVMsSUFDbEUsTUFBTSxlQUNOLGtCQUFrQixLQUN2QjtBQUVELFFBQU0sTUFBTSxNQUFNLFlBQVksTUFBTTtBQUNsQyxxQkFBa0I7QUFBQSxFQUN0QixDQUFHO0FBRUQsUUFBTSxNQUFNLE1BQU0sZUFBZSxTQUFPO0FBQ3RDLFFBQUksUUFBUSxNQUFNO0FBQ2hCLFVBQUksaUJBQWlCLFFBQVE7QUFDM0IsdUJBQWUsTUFBTSxNQUFNLE1BQU0sT0FBTyxNQUFNO0FBQzVDLDJCQUFpQixJQUFJO0FBQUEsUUFDL0IsQ0FBUztBQUFBLE1BQ0Y7QUFBQSxJQUNGLFdBQ1EsaUJBQWlCLFFBQVE7QUFDaEMsbUJBQWM7QUFDZCxxQkFBZTtBQUFBLElBQ2hCO0FBQUEsRUFDTCxHQUFLLEVBQUUsV0FBVyxNQUFNO0FBRXRCLFFBQU0sU0FBUyxTQUFPO0FBQ3BCLFFBQUksUUFBUSxNQUFNO0FBQ2hCLFVBQUksYUFBYSxVQUFVLE1BQU07QUFDL0IscUJBQWEsUUFBUTtBQUFBLE1BQ3RCO0FBQUEsSUFDRixXQUNRLGFBQWEsVUFBVSxPQUFPO0FBQ3JDLG1CQUFhLFFBQVE7QUFFckIsVUFDRSxlQUFlLFVBQVUsUUFDdEIsTUFBTSxjQUFjLGNBSXBCLGFBQWEsVUFBVSxPQUMxQjtBQUNBLDBCQUFtQjtBQUFBLE1BQ3BCO0FBQUEsSUFDRjtBQUFBLEVBQ0wsQ0FBRztBQUVELDZCQUE0QjtBQUMxQjtBQUNBLGlCQUFhLFFBQVE7QUFDckIsaUJBQWEsUUFBUTtBQUNyQixlQUFXLFFBQVE7QUFDbkIsc0JBQWtCLFFBQVE7QUFDMUIsc0JBQWtCLE9BQVE7QUFBQSxFQUMzQjtBQVFELG9CQUFtQixNQUFNLE1BQU0sWUFBWTtBQUN6QyxRQUFJLGVBQWUsVUFBVSxNQUFNO0FBQ2pDLGFBQU87QUFBQSxJQUNSO0FBRUQsVUFBTSxRQUFRLEVBQUU7QUFFaEIsUUFBSSxhQUFhLFVBQVUsUUFBUSxNQUFNLGNBQWMsTUFBTTtBQUMzRCxtQkFBYSxRQUFRO0FBQUEsSUFDdEI7QUFFRCxVQUFNLFNBQVMsQ0FBQyxLQUFLLFFBQVE7QUFDM0IsVUFBSSxXQUFXLFVBQVUsS0FBSztBQUM1QixtQkFBVyxRQUFRO0FBQUEsTUFDcEI7QUFFRCxZQUFNLElBQUksT0FBTztBQUVqQixVQUFJLGtCQUFrQixVQUFVLEdBQUc7QUFDakMsMEJBQWtCLFFBQVE7QUFBQSxNQUMzQjtBQUVELG1CQUFhLFFBQVE7QUFBQSxJQUN0QjtBQUVELFVBQU0sV0FBVyxDQUFFO0FBRW5CLGFBQVMsSUFBSSxHQUFHLElBQUksTUFBTSxNQUFNLFFBQVEsS0FBSztBQUMzQyxZQUFNLE9BQU8sTUFBTSxNQUFPO0FBQzFCLFVBQUk7QUFFSixVQUFJLE9BQU8sU0FBUyxZQUFZO0FBQzlCLGNBQU0sS0FBSyxHQUFHO0FBQUEsTUFDZixXQUNRLE9BQU8sU0FBUyxZQUFZLFlBQWEsVUFBVyxRQUFRO0FBQ25FLGNBQU0sWUFBYSxNQUFPLEdBQUc7QUFBQSxNQUM5QjtBQUVELFVBQUksUUFBUSxTQUFTLE9BQU8sUUFBUSxVQUFVO0FBQzVDLGVBQU8sTUFBTSxHQUFHO0FBQ2hCLGVBQU87QUFBQSxNQUNSLFdBQ1EsUUFBUSxRQUFRLFFBQVEsUUFBUTtBQUN2QyxpQkFBUyxLQUFLLEdBQUc7QUFBQSxNQUNsQjtBQUFBLElBQ0Y7QUFFRCxRQUFJLFNBQVMsV0FBVyxHQUFHO0FBQ3pCLGFBQU8sS0FBSztBQUNaLGFBQU87QUFBQSxJQUNSO0FBRUQsaUJBQWEsUUFBUTtBQUVyQixXQUFPLFFBQVEsSUFBSSxRQUFRLEVBQUUsS0FDM0IsU0FBTztBQUNMLFVBQUksUUFBUSxVQUFVLE1BQU0sUUFBUSxHQUFHLE1BQU0sU0FBUyxJQUFJLFdBQVcsR0FBRztBQUN0RSxrQkFBVSxpQkFBaUIsT0FBTyxLQUFLO0FBQ3ZDLGVBQU87QUFBQSxNQUNSO0FBRUQsWUFBTSxNQUFNLElBQUksS0FBSyxPQUFLLE1BQU0sU0FBUyxPQUFPLE1BQU0sUUFBUTtBQUM5RCxnQkFBVSxpQkFBaUIsT0FBTyxRQUFRLFFBQVEsR0FBRztBQUNyRCxhQUFPLFFBQVE7QUFBQSxJQUNoQixHQUNELE9BQUs7QUFDSCxVQUFJLFVBQVUsZUFBZTtBQUMzQixnQkFBUSxNQUFNLENBQUM7QUFDZixlQUFPLElBQUk7QUFBQSxNQUNaO0FBRUQsYUFBTztBQUFBLElBQ1IsQ0FDRjtBQUFBLEVBQ0Y7QUFFRCw0QkFBMkIsY0FBYztBQUN2QyxRQUNFLGVBQWUsVUFBVSxRQUN0QixNQUFNLGNBQWMsY0FDbkIsY0FBYSxVQUFVLFFBQVMsTUFBTSxjQUFjLFFBQVEsaUJBQWlCLE9BQ2pGO0FBQ0Esd0JBQW1CO0FBQUEsSUFDcEI7QUFBQSxFQUNGO0FBRUQsUUFBTSxvQkFBb0IsU0FBUyxVQUFVLENBQUM7QUFFOUMsa0JBQWdCLE1BQU07QUFDcEIscUJBQWlCLFVBQVUsYUFBYztBQUN6QyxzQkFBa0IsT0FBUTtBQUFBLEVBQzlCLENBQUc7QUFHRCxTQUFPLE9BQU8sT0FBTyxFQUFFLGlCQUFpQixTQUFRLENBQUU7QUFDbEQsYUFBVyxPQUFPLFlBQVksTUFBTSxTQUFTLEtBQUs7QUFFbEQsU0FBTztBQUFBLElBQ0w7QUFBQSxJQUNBO0FBQUEsSUFDQTtBQUFBLElBQ0E7QUFBQSxJQUVBO0FBQUEsSUFDQTtBQUFBLEVBQ0Q7QUFDSDtBQzFOQSxNQUFNLGFBQWE7QUFFSix1QkFBVSxPQUFPLE9BQU87QUFDckMsUUFBTSxNQUFNO0FBQUEsSUFDVixXQUFXLElBQUksRUFBRTtBQUFBLElBQ2pCLFlBQVksSUFBSSxFQUFFO0FBQUEsRUFDbkI7QUFFRCxvQkFBbUI7QUFDakIsVUFBTSxhQUFhLENBQUU7QUFDckIsVUFBTSxZQUFZLENBQUU7QUFFcEIsZUFBVyxPQUFPLE9BQU87QUFDdkIsVUFBSSxRQUFRLFdBQVcsUUFBUSxXQUFXLFdBQVcsS0FBSyxHQUFHLE1BQU0sT0FBTztBQUN4RSxtQkFBWSxPQUFRLE1BQU87QUFBQSxNQUM1QjtBQUFBLElBQ0Y7QUFFRCxlQUFXLE9BQU8sTUFBTSxPQUFPO0FBQzdCLFVBQUksV0FBVyxLQUFLLEdBQUcsTUFBTSxNQUFNO0FBQ2pDLGtCQUFXLE9BQVEsTUFBTSxNQUFPO0FBQUEsTUFDakM7QUFBQSxJQUNGO0FBRUQsUUFBSSxXQUFXLFFBQVE7QUFDdkIsUUFBSSxVQUFVLFFBQVE7QUFBQSxFQUN2QjtBQUVELGlCQUFlLE1BQU07QUFFckIsU0FBUTtBQUVSLFNBQU87QUFDVDtBQ25DQSxJQUFJLFFBQVEsQ0FBRTtBQUNkLElBQUksWUFBWSxDQUFFO0FBcUJYLG9CQUFxQixJQUFJO0FBQzlCLE1BQUksVUFBVSxXQUFXLEdBQUc7QUFDMUIsT0FBSTtBQUFBLEVBQ0wsT0FDSTtBQUNILFVBQU0sS0FBSyxFQUFFO0FBQUEsRUFDZDtBQUNIO0FBRU8sdUJBQXdCLElBQUk7QUFDakMsVUFBUSxNQUFNLE9BQU8sV0FBUyxVQUFVLEVBQUU7QUFDNUM7QUNqQkEsc0JBQXVCLEtBQUs7QUFDMUIsU0FBTyxRQUFRLFNBQVMsS0FBTSxJQUFHLE1BQVE7QUFDM0M7QUFFTyw0QkFBNkIsS0FBSztBQUN2QyxTQUFPLFFBQVEsVUFDVixRQUFRLFFBQ1AsTUFBSyxLQUFLLFNBQVM7QUFDM0I7QUFFTyxNQUFNLGdCQUFnQixnREFDeEIsZUFDQSxtQkFGd0I7QUFBQSxFQUkzQixPQUFPO0FBQUEsRUFDUCxZQUFZO0FBQUEsRUFDWixNQUFNO0FBQUEsRUFDTixVQUFVO0FBQUEsRUFDVixRQUFRO0FBQUEsRUFDUixRQUFRO0FBQUEsRUFFUixZQUFZO0FBQUEsRUFDWixPQUFPO0FBQUEsRUFDUCxTQUFTO0FBQUEsRUFFVCxRQUFRO0FBQUEsRUFDUixVQUFVO0FBQUEsRUFDVixZQUFZO0FBQUEsRUFDWixVQUFVLENBQUUsU0FBUyxNQUFRO0FBQUEsRUFFN0IsUUFBUTtBQUFBLEVBRVIsU0FBUztBQUFBLEVBRVQsV0FBVztBQUFBLEVBRVgsYUFBYTtBQUFBLEVBQ2IsaUJBQWlCO0FBQUEsRUFFakIsU0FBUztBQUFBLEVBQ1QsT0FBTztBQUFBLEVBQ1AsYUFBYTtBQUFBLEVBRWIsU0FBUztBQUFBLEVBRVQsV0FBVztBQUFBLEVBQ1gsV0FBVztBQUFBLEVBRVgsU0FBUztBQUFBLEVBQ1QsVUFBVTtBQUFBLEVBRVYsV0FBVztBQUFBLEVBRVgsS0FBSztBQUFBLEVBRUwsV0FBVyxDQUFFLFFBQVEsTUFBUTtBQUMvQjtBQUVPLE1BQU0sZ0JBQWdCLENBQUUscUJBQXFCLFNBQVMsU0FBUyxRQUFRLGNBQWMsWUFBYztBQUVuRyx5QkFBMEI7QUFDL0IsUUFBTSxFQUFFLE9BQU8sT0FBTyxPQUFPLFVBQVUsbUJBQW9CO0FBRTNELFFBQU0sU0FBUyxRQUFRLE9BQU8sTUFBTSxFQUFFO0FBRXRDLFNBQU87QUFBQSxJQUNMO0FBQUEsSUFFQSxVQUFVLFNBQVMsTUFDakIsTUFBTSxZQUFZLFFBQVEsTUFBTSxhQUFhLElBQzlDO0FBQUEsSUFFRCxjQUFjLElBQUksS0FBSztBQUFBLElBQ3ZCLFNBQVMsSUFBSSxLQUFLO0FBQUEsSUFDbEIsY0FBYztBQUFBLElBRWQsWUFBWSxjQUFjLE9BQU8sS0FBSztBQUFBLElBQ3RDLFdBQVcsSUFBSSxhQUFhLE1BQU0sR0FBRyxDQUFDO0FBQUEsSUFFdEMsU0FBUyxJQUFJLElBQUk7QUFBQSxJQUNqQixXQUFXLElBQUksSUFBSTtBQUFBLElBQ25CLFlBQVksSUFBSSxJQUFJO0FBQUEsRUFvQnJCO0FBQ0g7QUFFZSxrQkFBVSxPQUFPO0FBQzlCLFFBQU0sRUFBRSxPQUFPLE1BQU0sT0FBTyxPQUFPLFVBQVUsbUJBQW9CO0FBQ2pFLFFBQU0sRUFBRSxPQUFPO0FBRWYsTUFBSTtBQUVKLE1BQUksTUFBTSxhQUFhLFFBQVE7QUFDN0IsVUFBTSxXQUFXLFNBQVMsTUFBTSxtQkFBbUIsTUFBTSxVQUFVLENBQUM7QUFBQSxFQUNyRTtBQUVELE1BQUksTUFBTSxjQUFjLFFBQVE7QUFDOUIsVUFBTSxZQUFZLFdBQVM7QUFDekIsV0FBSyxxQkFBcUIsS0FBSztBQUFBLElBQ2hDO0FBQUEsRUFDRjtBQUVELE1BQUksTUFBTSxrQkFBa0IsUUFBUTtBQUNsQyxVQUFNLGdCQUFnQjtBQUFBLE1BQ3BCLFdBQVc7QUFBQSxNQUNYLFlBQVk7QUFBQSxJQUNiO0FBQUEsRUFDRjtBQUVELFNBQU8sT0FBTyxPQUFPO0FBQUEsSUFDbkI7QUFBQSxJQUNBO0FBQUEsSUFDQTtBQUFBLElBQ0E7QUFBQSxFQUNKLENBQUc7QUFFRCxNQUFJLE1BQU0sb0JBQW9CLFFBQVE7QUFDcEMsVUFBTSxrQkFBa0IsU0FBUyxNQUFNO0FBQ3JDLFVBQUksTUFBTSxZQUFZLE9BQU87QUFDM0IsY0FBTSxNQUFNLE9BQU8sTUFBTSxlQUFlLFlBQVksT0FBTyxNQUFNLGVBQWUsV0FDM0UsTUFBSyxNQUFNLFlBQVksU0FDdkIsTUFBTSxRQUFRLE1BQU0sVUFBVSxNQUFNLE9BQU8sTUFBTSxXQUFXLFNBQVM7QUFFMUUsY0FBTSxNQUFNLE1BQU0sY0FBYyxTQUM1QixNQUFNLFlBQ04sTUFBTTtBQUVWLGVBQU8sTUFBTyxTQUFRLFNBQVMsUUFBUSxNQUFNO0FBQUEsTUFDOUM7QUFBQSxJQUNQLENBQUs7QUFBQSxFQUNGO0FBRUQsUUFBTTtBQUFBLElBQ0o7QUFBQSxJQUNBO0FBQUEsSUFDQTtBQUFBLElBQ0E7QUFBQSxJQUNBO0FBQUEsTUFDRSxZQUFZLE1BQU0sU0FBUyxNQUFNLFlBQVk7QUFFakQsUUFBTSxnQkFBZ0IsTUFBTSxrQkFBa0IsU0FDMUMsU0FBUyxNQUFNLE1BQU0sZUFBZSxRQUFRLE1BQU0sUUFBUSxVQUFVLFFBQVEsTUFBTSxjQUFjLFVBQVUsSUFBSSxJQUM5RyxTQUFTLE1BQU0sTUFBTSxlQUFlLFFBQVEsTUFBTSxRQUFRLFVBQVUsUUFBUSxNQUFNLFNBQVMsVUFBVSxJQUFJO0FBRTdHLFFBQU0scUJBQXFCLFNBQVMsTUFDbEMsTUFBTSxnQkFBZ0IsUUFDbkIsTUFBTSxTQUFTLFVBQ2YsU0FBUyxVQUFVLFFBQ25CLE1BQU0sWUFBWSxRQUNsQixNQUFNLFVBQVUsSUFDcEI7QUFFRCxRQUFNLFlBQVksU0FBUyxNQUFNO0FBQy9CLFFBQUksTUFBTSxXQUFXLE1BQU07QUFBRSxhQUFPO0FBQUEsSUFBVTtBQUM5QyxRQUFJLE1BQU0sYUFBYSxNQUFNO0FBQUUsYUFBTztBQUFBLElBQVk7QUFDbEQsUUFBSSxNQUFNLGVBQWUsTUFBTTtBQUFFLGFBQU87QUFBQSxJQUFjO0FBQ3RELFFBQUksTUFBTSxVQUFVO0FBQUUsYUFBTztBQUFBLElBQVk7QUFDekMsV0FBTztBQUFBLEVBQ1gsQ0FBRztBQUVELFFBQU0sVUFBVSxTQUFTLE1BQ3ZCLDRDQUE2QyxVQUFVLFVBQ3BELE9BQU0sZUFBZSxTQUFTLElBQUssTUFBTSxXQUFXLFVBQVcsTUFDL0QsT0FBTSxZQUFZLE9BQU8sc0JBQXNCLE1BQy9DLE9BQU0sV0FBVyxPQUFPLHFCQUFxQixNQUM3QyxlQUFjLFVBQVUsT0FBTyxvQkFBb0IsTUFDbkQsVUFBUyxVQUFVLE9BQU8sc0JBQXNCLE1BQ2hELE9BQU0sVUFBVSxPQUFPLG9CQUFvQixNQUMzQyxPQUFNLGdCQUFnQixPQUFPLHVDQUF1QyxNQUNwRSxPQUFNLE9BQU8sVUFBVSxPQUFPLG1CQUFtQixNQUNqRCxPQUFNLGVBQWUsU0FBUywwQkFBMEIsTUFDeEQsT0FBTSxRQUFRLFVBQVUsT0FBTyxzQkFBc0IsTUFDckQsVUFBUyxVQUFVLE9BQU8sb0JBQW9CLE1BQzlDLFVBQVMsVUFBVSxRQUFRLE1BQU0sUUFBUSxVQUFVLE9BQU8sMEJBQTBCLE1BQ3BGLE9BQU0sb0JBQW9CLFFBQVEsbUJBQW1CLFVBQVUsT0FBTywwQkFBMEIsTUFDaEcsT0FBTSxZQUFZLE9BQU8sdUJBQXdCLE1BQU0sYUFBYSxPQUFPLHVCQUF1QixHQUN0RztBQUVELFFBQU0sZUFBZSxTQUFTLE1BQzVCLG1EQUNHLE9BQU0sWUFBWSxTQUFTLE9BQVEsTUFBTSxZQUFhLE1BRXZELFVBQVMsVUFBVSxPQUNmLG1CQUVFLE9BQU8sTUFBTSxhQUFhLFlBQVksTUFBTSxTQUFTLFNBQVMsS0FBSyxNQUFNLFFBQVEsVUFBVSxPQUN2RixJQUFLLE1BQU0sYUFDVixNQUFNLFVBQVUsU0FBUyxTQUFVLE1BQU0sVUFBVyxHQUdsRTtBQUVELFFBQU0sV0FBVyxTQUFTLE1BQ3hCLE1BQU0sY0FBYyxRQUFRLE1BQU0sVUFBVSxNQUM3QztBQUVELFFBQU0sYUFBYSxTQUFTLE1BQzFCLHVEQUNHLE9BQU0sZUFBZSxVQUFVLFNBQVMsVUFBVSxPQUFPLFNBQVUsTUFBTSxlQUFnQixHQUM3RjtBQUVELFFBQU0sbUJBQW1CLFNBQVMsTUFBTztBQUFBLElBQ3ZDLElBQUksTUFBTSxVQUFVO0FBQUEsSUFDcEIsVUFBVSxNQUFNLFNBQVM7QUFBQSxJQUN6QixTQUFTLE1BQU0sUUFBUTtBQUFBLElBQ3ZCLGVBQWUsY0FBYztBQUFBLElBQzdCLFlBQVksTUFBTTtBQUFBLElBQ2xCLFdBQVcsTUFBTTtBQUFBLEVBQ3JCLEVBQUk7QUFFRixRQUFNLGFBQWEsU0FBUyxNQUFNO0FBQ2hDLFVBQU0sTUFBTTtBQUFBLE1BQ1YsS0FBSyxNQUFNLFVBQVU7QUFBQSxJQUN0QjtBQUVELFFBQUksTUFBTSxZQUFZLE1BQU07QUFDMUIsVUFBSyxtQkFBb0I7QUFBQSxJQUMxQixXQUNRLE1BQU0sYUFBYSxNQUFNO0FBQ2hDLFVBQUssbUJBQW9CO0FBQUEsSUFDMUI7QUFFRCxXQUFPO0FBQUEsRUFDWCxDQUFHO0FBRUQsUUFBTSxNQUFNLE1BQU0sS0FBSyxTQUFPO0FBRzVCLFVBQU0sVUFBVSxRQUFRLGFBQWEsR0FBRztBQUFBLEVBQzVDLENBQUc7QUFFRCwwQkFBeUI7QUFDdkIsVUFBTSxLQUFLLFNBQVM7QUFDcEIsUUFBSSxTQUFTLE1BQU0sY0FBYyxVQUFVLE1BQU0sVUFBVTtBQUUzRCxRQUFJLFVBQVcsUUFBTyxRQUFRLEdBQUcsT0FBTyxNQUFNLFVBQVUsUUFBUTtBQUM5RCxhQUFPLGFBQWEsVUFBVSxNQUFNLFFBQVMsVUFBUyxPQUFPLGNBQWMsWUFBWTtBQUN2RixVQUFJLFVBQVUsV0FBVyxJQUFJO0FBQzNCLGVBQU8sTUFBTSxFQUFFLGVBQWUsS0FBSSxDQUFFO0FBQUEsTUFDckM7QUFBQSxJQUNGO0FBQUEsRUFDRjtBQUVELG1CQUFrQjtBQUNoQixlQUFXLFlBQVk7QUFBQSxFQUN4QjtBQUVELGtCQUFpQjtBQUNmLGtCQUFjLFlBQVk7QUFDMUIsVUFBTSxLQUFLLFNBQVM7QUFDcEIsUUFBSSxPQUFPLFFBQVEsTUFBTSxRQUFRLE1BQU0sU0FBUyxFQUFFLEdBQUc7QUFDbkQsU0FBRyxLQUFNO0FBQUEsSUFDVjtBQUFBLEVBQ0Y7QUFFRCw0QkFBMkIsR0FBRztBQUM1QixpQkFBYSxhQUFhO0FBQzFCLFFBQUksTUFBTSxTQUFTLFVBQVUsUUFBUSxNQUFNLFFBQVEsVUFBVSxPQUFPO0FBQ2xFLFlBQU0sUUFBUSxRQUFRO0FBQ3RCLFdBQUssU0FBUyxDQUFDO0FBQUEsSUFDaEI7QUFBQSxFQUNGO0FBRUQsNkJBQTRCLEdBQUcsTUFBTTtBQUNuQyxpQkFBYSxhQUFhO0FBQzFCLG9CQUFnQixXQUFXLE1BQU07QUFDL0IsVUFDRSxTQUFTLFNBQVEsTUFBTyxRQUN0QixPQUFNLGlCQUFpQixRQUNwQixNQUFNLGVBQWUsVUFDckIsTUFBTSxXQUFXLFVBQVUsUUFDM0IsTUFBTSxXQUFXLE1BQU0sU0FBUyxTQUFTLGFBQWEsTUFBTSxRQUVqRTtBQUNBO0FBQUEsTUFDRDtBQUVELFVBQUksTUFBTSxRQUFRLFVBQVUsTUFBTTtBQUNoQyxjQUFNLFFBQVEsUUFBUTtBQUN0QixhQUFLLFFBQVEsQ0FBQztBQUFBLE1BQ2Y7QUFFRCxlQUFTLFVBQVUsS0FBTTtBQUFBLElBQy9CLENBQUs7QUFBQSxFQUNGO0FBRUQsc0JBQXFCLEdBQUc7QUFFdEIsbUJBQWUsQ0FBQztBQUVoQixRQUFJLEdBQUcsU0FBUyxHQUFHLFdBQVcsTUFBTTtBQUNsQyxZQUFNLEtBQU0sTUFBTSxjQUFjLFVBQVUsTUFBTSxVQUFVLFNBQVUsTUFBTSxRQUFRO0FBQ2xGLFNBQUcsTUFBTztBQUFBLElBQ1gsV0FDUSxNQUFNLFFBQVEsTUFBTSxTQUFTLFNBQVMsYUFBYSxNQUFNLE1BQU07QUFDdEUsZUFBUyxjQUFjLEtBQU07QUFBQSxJQUM5QjtBQUVELFFBQUksTUFBTSxTQUFTLFFBQVE7QUFJekIsWUFBTSxTQUFTLE1BQU0sUUFBUTtBQUFBLElBQzlCO0FBRUQsU0FBSyxxQkFBcUIsSUFBSTtBQUM5QixTQUFLLFNBQVMsTUFBTSxVQUFVO0FBRTlCLGFBQVMsTUFBTTtBQUNiLHNCQUFpQjtBQUVqQixVQUFJLEdBQUcsU0FBUyxHQUFHLFdBQVcsTUFBTTtBQUNsQyxxQkFBYSxRQUFRO0FBQUEsTUFDdEI7QUFBQSxJQUNQLENBQUs7QUFBQSxFQUNGO0FBRUQsd0JBQXVCO0FBQ3JCLFVBQU0sT0FBTyxDQUFFO0FBRWYsVUFBTSxZQUFZLFVBQVUsS0FBSyxLQUMvQixFQUFFLE9BQU87QUFBQSxNQUNQLE9BQU87QUFBQSxNQUNQLEtBQUs7QUFBQSxNQUNMLFNBQVM7QUFBQSxJQUNqQixHQUFTLE1BQU0sU0FBUyxDQUNuQjtBQUVELFNBQUssS0FDSCxFQUFFLE9BQU87QUFBQSxNQUNQLE9BQU87QUFBQSxJQUNSLEdBQUUsb0JBQW1CLENBQUUsQ0FDekI7QUFFRCxhQUFTLFVBQVUsUUFBUSxNQUFNLGdCQUFnQixTQUFTLEtBQUssS0FDN0QsbUJBQW1CLFNBQVM7QUFBQSxNQUMxQixFQUFFLE9BQU8sRUFBRSxNQUFNLEdBQUcsUUFBUSxNQUFNLE9BQU8sT0FBTyxZQUFZO0FBQUEsSUFDcEUsQ0FBTyxDQUNGO0FBRUQsUUFBSSxNQUFNLFlBQVksUUFBUSxNQUFNLGFBQWEsVUFBVSxNQUFNO0FBQy9ELFdBQUssS0FDSCxtQkFDRSx3QkFDQSxNQUFNLFlBQVksU0FDZCxNQUFNLFFBQVMsSUFDZixDQUFFLEVBQUUsVUFBVSxFQUFFLE9BQU8sTUFBTSxNQUFLLENBQUUsQ0FBRyxDQUM1QyxDQUNGO0FBQUEsSUFDRixXQUNRLE1BQU0sY0FBYyxRQUFRLE1BQU0sU0FBUyxVQUFVLFFBQVEsTUFBTSxTQUFTLFVBQVUsTUFBTTtBQUNuRyxXQUFLLEtBQ0gsbUJBQW1CLDBCQUEwQjtBQUFBLFFBQzNDLEVBQUUsT0FBTztBQUFBLFVBQ1AsT0FBTztBQUFBLFVBQ1AsS0FBSztBQUFBLFVBQ0wsTUFBTSxNQUFNLGFBQWEsR0FBRyxRQUFRLE1BQU07QUFBQSxVQUMxQyxVQUFVO0FBQUEsVUFDVixNQUFNO0FBQUEsVUFDTixlQUFlO0FBQUEsVUFDZixNQUFNO0FBQUEsVUFDTixTQUFTO0FBQUEsUUFDckIsQ0FBVztBQUFBLE1BQ1gsQ0FBUyxDQUNGO0FBQUEsSUFDRjtBQUVELFVBQU0sV0FBVyxVQUFVLEtBQUssS0FDOUIsRUFBRSxPQUFPO0FBQUEsTUFDUCxPQUFPO0FBQUEsTUFDUCxLQUFLO0FBQUEsTUFDTCxTQUFTO0FBQUEsSUFDakIsR0FBUyxNQUFNLFFBQVEsQ0FDbEI7QUFFRCxVQUFNLG1CQUFtQixVQUFVLEtBQUssS0FDdEMsbUJBQW1CLGdCQUFnQixNQUFNLGdCQUFnQixDQUMxRDtBQUVELFVBQU0sb0JBQW9CLFVBQVUsS0FBSyxLQUN2QyxNQUFNLGdCQUFpQixDQUN4QjtBQUVELFdBQU87QUFBQSxFQUNSO0FBRUQsaUNBQWdDO0FBQzlCLFVBQU0sT0FBTyxDQUFFO0FBRWYsVUFBTSxXQUFXLFVBQVUsTUFBTSxXQUFXLFFBQVEsS0FBSyxLQUN2RCxFQUFFLE9BQU87QUFBQSxNQUNQLE9BQU87QUFBQSxJQUNmLEdBQVMsTUFBTSxNQUFNLENBQ2hCO0FBRUQsUUFBSSxNQUFNLHFCQUFxQixVQUFVLE1BQU0sVUFBVSxVQUFVLE1BQU07QUFDdkUsV0FBSyxLQUNILE1BQU0saUJBQWtCLENBQ3pCO0FBQUEsSUFDRjtBQUVELFFBQUksTUFBTSxlQUFlLFFBQVE7QUFDL0IsV0FBSyxLQUFLLE1BQU0sWUFBWTtBQUFBLElBQzdCLFdBRVEsTUFBTSxlQUFlLFFBQVE7QUFDcEMsV0FBSyxLQUFLLE1BQU0sWUFBWTtBQUFBLElBQzdCLFdBQ1EsTUFBTSxZQUFZLFFBQVE7QUFDakMsV0FBSyxLQUNILEVBQUUsT0FBTztBQUFBLFFBQ1AsS0FBSyxNQUFNO0FBQUEsUUFDWCxPQUFPO0FBQUEsU0FDSixNQUFNLFdBQVcsV0FBVyxRQUh4QjtBQUFBLFFBSVAsa0JBQWtCLE1BQU0sY0FBYyxRQUFRO0FBQUEsTUFDL0MsSUFBRSxNQUFNLFFBQVEsaUJBQWlCLEtBQUssQ0FBQyxDQUN6QztBQUFBLElBQ0Y7QUFFRCxhQUFTLFVBQVUsUUFBUSxLQUFLLEtBQzlCLEVBQUUsT0FBTztBQUFBLE1BQ1AsT0FBTyxXQUFXO0FBQUEsSUFDbkIsR0FBRSxNQUFNLE1BQU0sT0FBTyxNQUFNLEtBQUssQ0FBQyxDQUNuQztBQUVELFVBQU0sV0FBVyxVQUFVLE1BQU0sV0FBVyxRQUFRLEtBQUssS0FDdkQsRUFBRSxPQUFPO0FBQUEsTUFDUCxPQUFPO0FBQUEsSUFDZixHQUFTLE1BQU0sTUFBTSxDQUNoQjtBQUVELFdBQU8sS0FBSyxPQUFPLE1BQU0sTUFBTSxPQUFPLENBQUM7QUFBQSxFQUN4QztBQUVELHVCQUFzQjtBQUNwQixRQUFJLEtBQUs7QUFFVCxRQUFJLFNBQVMsVUFBVSxNQUFNO0FBQzNCLFVBQUksYUFBYSxVQUFVLE1BQU07QUFDL0IsY0FBTSxDQUFFLEVBQUUsT0FBTyxFQUFFLE1BQU0sUUFBUyxHQUFFLGFBQWEsS0FBSyxDQUFHO0FBQ3pELGNBQU0saUJBQWtCLGFBQWE7QUFBQSxNQUN0QyxPQUNJO0FBQ0gsY0FBTSxNQUFNLE1BQU0sS0FBSztBQUN2QixjQUFNO0FBQUEsTUFDUDtBQUFBLElBQ0YsV0FDUSxNQUFNLGFBQWEsUUFBUSxNQUFNLFFBQVEsVUFBVSxNQUFNO0FBQ2hFLFVBQUksTUFBTSxTQUFTLFFBQVE7QUFDekIsY0FBTSxDQUFFLEVBQUUsT0FBTyxNQUFNLElBQUksQ0FBRztBQUM5QixjQUFNLGdCQUFpQixNQUFNO0FBQUEsTUFDOUIsT0FDSTtBQUNILGNBQU0sTUFBTSxNQUFNLElBQUk7QUFDdEIsY0FBTTtBQUFBLE1BQ1A7QUFBQSxJQUNGO0FBRUQsVUFBTSxhQUFhLE1BQU0sWUFBWSxRQUFRLE1BQU0sWUFBWTtBQUUvRCxRQUFJLE1BQU0sb0JBQW9CLFFBQVEsZUFBZSxTQUFTLFFBQVEsUUFBUTtBQUM1RTtBQUFBLElBQ0Q7QUFFRCxVQUFNLE9BQU8sRUFBRSxPQUFPO0FBQUEsTUFDcEI7QUFBQSxNQUNBLE9BQU87QUFBQSxJQUNSLEdBQUUsR0FBRztBQUVOLFdBQU8sRUFBRSxPQUFPO0FBQUEsTUFDZCxPQUFPLHNEQUNGLE9BQU0sb0JBQW9CLE9BQU8sYUFBYTtBQUFBLElBQ3pELEdBQU87QUFBQSxNQUNELE1BQU0sb0JBQW9CLE9BQ3RCLE9BQ0EsRUFBRSxZQUFZLEVBQUUsTUFBTSw4QkFBK0IsR0FBRSxNQUFNLElBQUk7QUFBQSxNQUVyRSxlQUFlLE9BQ1gsRUFBRSxPQUFPO0FBQUEsUUFDVCxPQUFPO0FBQUEsTUFDakIsR0FBVyxNQUFNLFlBQVksU0FBUyxNQUFNLFlBQVksTUFBTSxnQkFBZ0IsS0FBSyxJQUN6RTtBQUFBLElBQ1YsQ0FBSztBQUFBLEVBQ0Y7QUFFRCw4QkFBNkIsS0FBSyxTQUFTO0FBQ3pDLFdBQU8sWUFBWSxPQUNmLE9BQ0EsRUFBRSxPQUFPO0FBQUEsTUFDVDtBQUFBLE1BQ0EsT0FBTztBQUFBLElBQ1IsR0FBRSxPQUFPO0FBQUEsRUFDYjtBQUdELFNBQU8sT0FBTyxPQUFPLEVBQUUsT0FBTyxLQUFJLENBQUU7QUFFcEMsTUFBSSxpQkFBaUI7QUFFckIsZ0JBQWMsTUFBTTtBQUNsQixxQkFBaUI7QUFBQSxFQUNyQixDQUFHO0FBRUQsY0FBWSxNQUFNO0FBQ2hCLHVCQUFtQixRQUFRLE1BQU0sY0FBYyxRQUFRLE1BQU0sTUFBTztBQUFBLEVBQ3hFLENBQUc7QUFFRCxZQUFVLE1BQU07QUFDZCxRQUFJLHlCQUF5QixVQUFVLFFBQVEsTUFBTSxRQUFRLFFBQVE7QUFDbkUsWUFBTSxVQUFVLFFBQVEsYUFBYztBQUFBLElBQ3ZDO0FBRUQsVUFBTSxjQUFjLFFBQVEsTUFBTSxNQUFPO0FBQUEsRUFDN0MsQ0FBRztBQUVELGtCQUFnQixNQUFNO0FBQ3BCLGlCQUFhLGFBQWE7QUFBQSxFQUM5QixDQUFHO0FBRUQsU0FBTyx1QkFBd0I7QUFDN0IsVUFBTSxhQUFhLE1BQU0sZUFBZSxVQUFVLE1BQU0sWUFBWSxTQUNoRSxnREFDSyxNQUFNLFdBQVcsV0FBVyxRQURqQztBQUFBLE1BRUUsa0JBQWtCLE1BQU07QUFBQSxRQUNyQixXQUFXLFNBRWhCLFdBQVc7QUFFZixXQUFPLEVBQUUsU0FBUztBQUFBLE1BQ2hCLEtBQUssTUFBTTtBQUFBLE1BQ1gsT0FBTztBQUFBLFFBQ0wsUUFBUTtBQUFBLFFBQ1IsTUFBTTtBQUFBLE1BQ1A7QUFBQSxNQUNELE9BQU8sTUFBTTtBQUFBLE9BQ1YsYUFDRjtBQUFBLE1BQ0QsTUFBTSxXQUFXLFNBQ2IsRUFBRSxPQUFPO0FBQUEsUUFDVCxPQUFPO0FBQUEsUUFDUCxTQUFTO0FBQUEsTUFDbkIsR0FBVyxNQUFNLFFBQVEsSUFDZjtBQUFBLE1BRUosRUFBRSxPQUFPO0FBQUEsUUFDUCxPQUFPO0FBQUEsTUFDZixHQUFTO0FBQUEsUUFDRCxFQUFFLE9BQU87QUFBQSxVQUNQLEtBQUssTUFBTTtBQUFBLFVBQ1gsT0FBTyxhQUFhO0FBQUEsVUFDcEIsVUFBVTtBQUFBLFdBQ1AsTUFBTSxnQkFDUixXQUFVLENBQUU7QUFBQSxRQUVmLG1CQUFtQixVQUFVLE9BQ3pCLFVBQVcsSUFDWDtBQUFBLE1BQ1osQ0FBTztBQUFBLE1BRUQsTUFBTSxVQUFVLFNBQ1osRUFBRSxPQUFPO0FBQUEsUUFDVCxPQUFPO0FBQUEsUUFDUCxTQUFTO0FBQUEsTUFDbkIsR0FBVyxNQUFNLE9BQU8sSUFDZDtBQUFBLElBQ1YsQ0FBSztBQUFBLEVBQ0Y7QUFDSDtBQ3BsQkEsTUFBTSxjQUFjO0FBQUEsRUFDbEIsTUFBTTtBQUFBLEVBQ04sVUFBVTtBQUFBLEVBQ1YsTUFBTTtBQUFBLEVBQ04sVUFBVTtBQUFBLEVBQ1YsT0FBTztBQUFBLEVBQ1AsTUFBTTtBQUNSO0FBRUEsTUFBTSxTQUFTO0FBQUEsRUFDYixLQUFLLEVBQUUsU0FBUyxTQUFTLFFBQVEsU0FBVTtBQUFBLEVBRTNDLEdBQUcsRUFBRSxTQUFTLFlBQVksUUFBUSxZQUFhO0FBQUEsRUFDL0MsR0FBRyxFQUFFLFNBQVMsZUFBZSxRQUFRLGVBQWdCO0FBQUEsRUFFckQsR0FBRyxFQUFFLFNBQVMsWUFBWSxRQUFRLGFBQWEsV0FBVyxPQUFLLEVBQUUsb0JBQXFCO0FBQUEsRUFDdEYsR0FBRyxFQUFFLFNBQVMsWUFBWSxRQUFRLGFBQWEsV0FBVyxPQUFLLEVBQUUsb0JBQXFCO0FBQUEsRUFFdEYsR0FBRyxFQUFFLFNBQVMsZUFBZSxRQUFRLGdCQUFnQixXQUFXLE9BQUssRUFBRSxvQkFBcUI7QUFBQSxFQUM1RixHQUFHLEVBQUUsU0FBUyxlQUFlLFFBQVEsZ0JBQWdCLFdBQVcsT0FBSyxFQUFFLG9CQUFxQjtBQUM5RjtBQUVBLE1BQU0sT0FBTyxPQUFPLEtBQUssTUFBTTtBQUMvQixLQUFLLFFBQVEsU0FBTztBQUNsQixTQUFRLEtBQU0sUUFBUSxJQUFJLE9BQU8sT0FBUSxLQUFNLE9BQU87QUFDeEQsQ0FBQztBQUVELE1BQ0UsaUJBQWlCLElBQUksT0FBTyxxREFBcUQsS0FBSyxLQUFLLEVBQUUsSUFBSSxVQUFVLEdBQUcsR0FDOUcsV0FBVztBQUViLE1BQU0sU0FBUyxPQUFPLGFBQWEsQ0FBQztBQUU3QixNQUFNLGVBQWU7QUFBQSxFQUMxQixNQUFNO0FBQUEsRUFDTixpQkFBaUI7QUFBQSxFQUNqQixVQUFVLENBQUUsU0FBUyxNQUFRO0FBQUEsRUFDN0IsZUFBZTtBQUNqQjtBQUVlLGlCQUFVLE9BQU8sTUFBTSxXQUFXLFVBQVU7QUFDekQsTUFBSSxZQUFZLGNBQWMsY0FBYztBQUU1QyxRQUFNLFVBQVUsSUFBSSxJQUFJO0FBQ3hCLFFBQU0sYUFBYSxJQUFJLHVCQUF1QjtBQUU5QywyQkFBMEI7QUFDeEIsV0FBTyxNQUFNLGFBQWEsUUFDckIsQ0FBRSxZQUFZLFFBQVEsVUFBVSxPQUFPLE9BQU8sWUFBYSxTQUFTLE1BQU0sSUFBSTtBQUFBLEVBQ3BGO0FBRUQsUUFBTSxNQUFNLE1BQU0sT0FBTyxNQUFNLFVBQVUsbUJBQW1CO0FBRTVELFFBQU0sTUFBTSxNQUFNLE1BQU0sT0FBSztBQUMzQixRQUFJLE1BQU0sUUFBUTtBQUNoQixzQkFBZ0IsV0FBVyxPQUFPLElBQUk7QUFBQSxJQUN2QyxPQUNJO0FBQ0gsWUFBTSxNQUFNLFlBQVksV0FBVyxLQUFLO0FBQ3hDLDBCQUFxQjtBQUNyQixZQUFNLGVBQWUsT0FBTyxLQUFLLHFCQUFxQixHQUFHO0FBQUEsSUFDMUQ7QUFBQSxFQUNMLENBQUc7QUFFRCxRQUFNLE1BQU0sTUFBTSxXQUFXLE1BQU0saUJBQWlCLE1BQU07QUFDeEQsWUFBUSxVQUFVLFFBQVEsZ0JBQWdCLFdBQVcsT0FBTyxJQUFJO0FBQUEsRUFDcEUsQ0FBRztBQUVELFFBQU0sTUFBTSxNQUFNLGVBQWUsTUFBTTtBQUNyQyxZQUFRLFVBQVUsUUFBUSxnQkFBZ0IsV0FBVyxLQUFLO0FBQUEsRUFDOUQsQ0FBRztBQUVELG1DQUFrQztBQUNoQyx3QkFBcUI7QUFFckIsUUFBSSxRQUFRLFVBQVUsTUFBTTtBQUMxQixZQUFNLFNBQVMsVUFBVSxZQUFZLE1BQU0sVUFBVSxDQUFDO0FBRXRELGFBQU8sTUFBTSxhQUFhLFFBQ3RCLGFBQWEsTUFBTSxJQUNuQjtBQUFBLElBQ0w7QUFFRCxXQUFPLE1BQU07QUFBQSxFQUNkO0FBRUQsK0JBQThCLE1BQU07QUFDbEMsUUFBSSxPQUFPLFdBQVcsUUFBUTtBQUM1QixhQUFPLFdBQVcsTUFBTSxDQUFDLElBQUk7QUFBQSxJQUM5QjtBQUVELFFBQUksTUFBTSxJQUFJLGtCQUFrQjtBQUNoQyxVQUFNLFNBQVMsZ0JBQWdCLFFBQVEsTUFBTTtBQUU3QyxRQUFJLFNBQVMsSUFBSTtBQUNmLGVBQVMsSUFBSSxPQUFPLGdCQUFnQixRQUFRLElBQUksR0FBRyxLQUFLO0FBQ3RELGVBQU87QUFBQSxNQUNSO0FBRUQsd0JBQWtCLGdCQUFnQixNQUFNLEdBQUcsTUFBTSxJQUFJLE1BQU0sZ0JBQWdCLE1BQU0sTUFBTTtBQUFBLElBQ3hGO0FBRUQsV0FBTztBQUFBLEVBQ1I7QUFFRCxpQ0FBZ0M7QUFDOUIsWUFBUSxRQUFRLE1BQU0sU0FBUyxVQUMxQixNQUFNLEtBQUssU0FBUyxLQUNwQixjQUFlO0FBRXBCLFFBQUksUUFBUSxVQUFVLE9BQU87QUFDM0IsdUJBQWlCO0FBQ2pCLG1CQUFhO0FBQ2IscUJBQWU7QUFDZjtBQUFBLElBQ0Q7QUFFRCxVQUNFLG9CQUFvQixZQUFhLE1BQU0sVUFBVyxTQUM5QyxNQUFNLE9BQ04sWUFBYSxNQUFNLE9BQ3ZCLFdBQVcsT0FBTyxNQUFNLGFBQWEsWUFBWSxNQUFNLFNBQVMsU0FBUyxJQUNyRSxNQUFNLFNBQVMsTUFBTSxHQUFHLENBQUMsSUFDekIsS0FDSixrQkFBa0IsU0FBUyxRQUFRLFVBQVUsTUFBTSxHQUNuRCxTQUFTLENBQUUsR0FDWCxVQUFVLENBQUUsR0FDWixPQUFPLENBQUU7QUFFWCxRQUNFLGFBQWEsTUFBTSxvQkFBb0IsTUFDdkMsYUFBYSxJQUNiLGFBQWE7QUFFZixzQkFBa0IsUUFBUSxnQkFBZ0IsQ0FBQyxHQUFHLE9BQU8sS0FBSyxPQUFPLFVBQVU7QUFDekUsVUFBSSxVQUFVLFFBQVE7QUFDcEIsY0FBTSxJQUFJLE9BQVE7QUFDbEIsYUFBSyxLQUFLLENBQUM7QUFDWCxxQkFBYSxFQUFFO0FBQ2YsWUFBSSxlQUFlLE1BQU07QUFDdkIsa0JBQVEsS0FBSyxRQUFRLGFBQWEsU0FBUyxFQUFFLFVBQVUsV0FBVyxhQUFhLFNBQVMsRUFBRSxVQUFVLEtBQUs7QUFDekcsdUJBQWE7QUFBQSxRQUNkO0FBQ0QsZ0JBQVEsS0FBSyxRQUFRLGFBQWEsU0FBUyxFQUFFLFVBQVUsSUFBSTtBQUFBLE1BQzVELFdBQ1EsUUFBUSxRQUFRO0FBQ3ZCLHFCQUFhLE9BQVEsU0FBUSxPQUFPLEtBQUs7QUFDekMsYUFBSyxLQUFLLEdBQUc7QUFDYixlQUFPLEtBQUssUUFBUSxhQUFhLFNBQVMsYUFBYSxHQUFHO0FBQUEsTUFDM0QsT0FDSTtBQUNILGNBQU0sSUFBSSxVQUFVLFNBQVMsUUFBUTtBQUNyQyxxQkFBYSxNQUFNLE9BQU8sYUFBYSxFQUFFLFFBQVEsVUFBVSxRQUFRO0FBQ25FLGFBQUssS0FBSyxDQUFDO0FBQ1gsZUFBTyxLQUFLLFFBQVEsYUFBYSxTQUFTLGFBQWEsR0FBRztBQUFBLE1BQzNEO0FBQUEsSUFDUCxDQUFLO0FBRUQsVUFDRSxnQkFBZ0IsSUFBSSxPQUNsQixNQUNFLE9BQU8sS0FBSyxFQUFFLElBQ2QsTUFBTyxnQkFBZSxLQUFLLE1BQU0sT0FBTyxhQUFhLE9BQU8sTUFFL0QsR0FDRCxjQUFjLFFBQVEsU0FBUyxHQUMvQixpQkFBaUIsUUFBUSxJQUFJLENBQUMsSUFBSSxVQUFVO0FBQzFDLFVBQUksVUFBVSxLQUFLLE1BQU0sb0JBQW9CLE1BQU07QUFDakQsZUFBTyxJQUFJLE9BQU8sTUFBTSxrQkFBa0IsTUFBTSxFQUFFO0FBQUEsTUFDbkQsV0FDUSxVQUFVLGFBQWE7QUFDOUIsZUFBTyxJQUFJLE9BQ1QsTUFBTSxLQUNKLE1BQU8sZ0JBQWUsS0FBSyxNQUFNLGNBQWMsUUFDOUMsT0FBTSxvQkFBb0IsT0FBTyxNQUFNLGtCQUFrQixJQUM3RDtBQUFBLE1BQ0Y7QUFFRCxhQUFPLElBQUksT0FBTyxNQUFNLEVBQUU7QUFBQSxJQUNsQyxDQUFPO0FBRUgsbUJBQWU7QUFDZixxQkFBaUIsU0FBTztBQUN0QixZQUFNLGNBQWMsY0FBYyxLQUFLLEdBQUc7QUFDMUMsVUFBSSxnQkFBZ0IsTUFBTTtBQUN4QixjQUFNLFlBQVksTUFBTSxDQUFDLEVBQUUsS0FBSyxFQUFFO0FBQUEsTUFDbkM7QUFFRCxZQUNFLGVBQWUsQ0FBRSxHQUNqQix1QkFBdUIsZUFBZTtBQUV4QyxlQUFTLElBQUksR0FBRyxNQUFNLEtBQUssSUFBSSxzQkFBc0IsS0FBSztBQUN4RCxjQUFNLElBQUksZUFBZ0IsR0FBSSxLQUFLLEdBQUc7QUFFdEMsWUFBSSxNQUFNLE1BQU07QUFDZDtBQUFBLFFBQ0Q7QUFFRCxjQUFNLElBQUksTUFBTSxFQUFFLE1BQUssRUFBRyxNQUFNO0FBQ2hDLHFCQUFhLEtBQUssR0FBRyxDQUFDO0FBQUEsTUFDdkI7QUFDRCxVQUFJLGFBQWEsU0FBUyxHQUFHO0FBQzNCLGVBQU8sYUFBYSxLQUFLLEVBQUU7QUFBQSxNQUM1QjtBQUVELGFBQU87QUFBQSxJQUNSO0FBQ0QsaUJBQWEsS0FBSyxJQUFJLE9BQU0sT0FBTyxNQUFNLFdBQVcsSUFBSSxNQUFPLEVBQUUsS0FBSyxFQUFFO0FBQ3hFLG1CQUFlLFdBQVcsTUFBTSxNQUFNLEVBQUUsS0FBSyxRQUFRO0FBQUEsRUFDdEQ7QUFFRCwyQkFBMEIsUUFBUSx5QkFBeUIsV0FBVztBQUNwRSxVQUNFLE1BQU0sU0FBUyxPQUNmLE1BQU0sSUFBSSxjQUNWLGFBQWEsSUFBSSxNQUFNLFNBQVMsS0FDaEMsV0FBVyxZQUFZLE1BQU07QUFHL0IsZ0NBQTRCLFFBQVEsb0JBQXFCO0FBRXpELFVBQ0UsWUFBWSxVQUFVLFFBQVEsR0FDOUIsU0FBUyxNQUFNLGFBQWEsUUFDeEIsYUFBYSxTQUFTLElBQ3RCLFdBQ0osVUFBVSxXQUFXLFVBQVU7QUFHakMsUUFBSSxVQUFVLFVBQVcsS0FBSSxRQUFRO0FBRXJDLGdCQUFZLFFBQVMsWUFBVyxRQUFRO0FBRXhDLGFBQVMsa0JBQWtCLE9BQU8sU0FBUyxNQUFNO0FBQy9DLFVBQUksV0FBVyxjQUFjO0FBQzNCLGNBQU0sU0FBUyxNQUFNLG9CQUFvQixPQUFPLGFBQWEsU0FBUztBQUN0RSxZQUFJLGtCQUFrQixRQUFRLFFBQVEsU0FBUztBQUUvQztBQUFBLE1BQ0Q7QUFFRCxVQUFJLGNBQWMscUJBQXFCLE1BQU0sb0JBQW9CLE1BQU07QUFDckUsY0FBTSxTQUFTLE1BQU07QUFDckIsbUJBQVcsTUFBTSxLQUFLLFFBQVEsTUFBTTtBQUVwQztBQUFBLE1BQ0Q7QUFFRCxVQUFJLENBQUUseUJBQXlCLHNCQUF3QixFQUFDLFFBQVEsU0FBUyxJQUFJLElBQUk7QUFDL0UsY0FBTSxTQUFTLE1BQU0sb0JBQW9CLE9BRW5DLFFBQVEsSUFDSCxPQUFPLFNBQVMsVUFBVSxTQUFTLElBQUksSUFDeEMsS0FBSyxJQUFJLEdBQUcsT0FBTyxTQUFVLFlBQVcsZUFBZSxJQUFJLEtBQUssSUFBSSxVQUFVLFFBQVEsVUFBVSxJQUFJLEVBQUUsSUFBSSxJQUVoSDtBQUVKLFlBQUksa0JBQWtCLFFBQVEsUUFBUSxTQUFTO0FBQy9DO0FBQUEsTUFDRDtBQUVELFVBQUksTUFBTSxvQkFBb0IsTUFBTTtBQUNsQyxZQUFJLFlBQVksTUFBTTtBQUNwQixnQkFBTSxTQUFTLEtBQUssSUFBSSxHQUFHLE9BQU8sU0FBVSxZQUFXLGVBQWUsSUFBSSxLQUFLLElBQUksVUFBVSxRQUFRLGFBQWEsQ0FBQyxFQUFFO0FBRXJILGNBQUksV0FBVyxLQUFLLFFBQVEsR0FBRztBQUM3QixnQkFBSSxrQkFBa0IsUUFBUSxRQUFRLFNBQVM7QUFBQSxVQUNoRCxPQUNJO0FBQ0gsdUJBQVcsYUFBYSxLQUFLLFFBQVEsTUFBTTtBQUFBLFVBQzVDO0FBQUEsUUFDRixPQUNJO0FBQ0gsZ0JBQU0sU0FBUyxPQUFPLFNBQVM7QUFDL0IsY0FBSSxrQkFBa0IsUUFBUSxRQUFRLFVBQVU7QUFBQSxRQUNqRDtBQUFBLE1BQ0YsT0FDSTtBQUNILFlBQUksWUFBWSxNQUFNO0FBQ3BCLGdCQUFNLFNBQVMsS0FBSyxJQUFJLEdBQUcsV0FBVyxRQUFRLE1BQU0sR0FBRyxLQUFLLElBQUksVUFBVSxRQUFRLEdBQUcsSUFBSSxDQUFDO0FBQzFGLHFCQUFXLE1BQU0sS0FBSyxRQUFRLE1BQU07QUFBQSxRQUNyQyxPQUNJO0FBQ0gsZ0JBQU0sU0FBUyxNQUFNO0FBQ3JCLHFCQUFXLE1BQU0sS0FBSyxRQUFRLE1BQU07QUFBQSxRQUNyQztBQUFBLE1BQ0Y7QUFBQSxJQUNQLENBQUs7QUFFRCxVQUFNLE1BQU0sTUFBTSxrQkFBa0IsT0FDaEMsWUFBWSxNQUFNLElBQ2xCO0FBRUosV0FBTyxNQUFNLFVBQVUsTUFBTSxPQUFPLFVBQVUsS0FBSyxJQUFJO0FBQUEsRUFDeEQ7QUFFRCw4QkFBNkIsS0FBSyxPQUFPLEtBQUs7QUFDNUMsVUFBTSxZQUFZLFVBQVUsWUFBWSxJQUFJLEtBQUssQ0FBQztBQUVsRCxZQUFRLEtBQUssSUFBSSxHQUFHLFdBQVcsUUFBUSxNQUFNLEdBQUcsS0FBSyxJQUFJLFVBQVUsUUFBUSxLQUFLLENBQUM7QUFFakYsUUFBSSxrQkFBa0IsT0FBTyxLQUFLLFNBQVM7QUFBQSxFQUM1QztBQUVELFFBQU0sYUFBYTtBQUFBLElBQ2pCLEtBQU0sS0FBSyxPQUFPLEtBQUssV0FBVztBQUNoQyxZQUFNLGVBQWUsV0FBVyxNQUFNLFFBQVEsQ0FBQyxFQUFFLFFBQVEsTUFBTSxNQUFNO0FBQ3JFLFVBQUksSUFBSSxLQUFLLElBQUksR0FBRyxRQUFRLENBQUM7QUFFN0IsYUFBTyxLQUFLLEdBQUcsS0FBSztBQUNsQixZQUFJLFdBQVksT0FBUSxRQUFRO0FBQzlCLGtCQUFRO0FBQ1IsMkJBQWlCLFFBQVE7QUFDekI7QUFBQSxRQUNEO0FBQUEsTUFDRjtBQUVELFVBQ0UsSUFBSSxLQUNELFdBQVksV0FBWSxVQUN4QixXQUFZLFdBQVksUUFDM0I7QUFDQSxlQUFPLFdBQVcsTUFBTSxLQUFLLEdBQUcsQ0FBQztBQUFBLE1BQ2xDO0FBRUQsZUFBUyxLQUFLLElBQUksa0JBQ2hCLE9BQ0EsY0FBYyxPQUFPLE1BQU0sT0FBTyxVQUNuQztBQUFBLElBQ0Y7QUFBQSxJQUVELE1BQU8sS0FBSyxPQUFPLEtBQUssV0FBVztBQUNqQyxZQUFNLFFBQVEsSUFBSSxNQUFNO0FBQ3hCLFVBQUksSUFBSSxLQUFLLElBQUksT0FBTyxNQUFNLENBQUM7QUFFL0IsYUFBTyxLQUFLLE9BQU8sS0FBSztBQUN0QixZQUFJLFdBQVksT0FBUSxRQUFRO0FBQzlCLGdCQUFNO0FBQ047QUFBQSxRQUNELFdBQ1EsV0FBWSxJQUFJLE9BQVEsUUFBUTtBQUN2QyxnQkFBTTtBQUFBLFFBQ1A7QUFBQSxNQUNGO0FBRUQsVUFDRSxJQUFJLFNBQ0QsV0FBWSxNQUFNLE9BQVEsVUFDMUIsV0FBWSxNQUFNLE9BQVEsUUFDN0I7QUFDQSxlQUFPLFdBQVcsS0FBSyxLQUFLLE9BQU8sS0FBSztBQUFBLE1BQ3pDO0FBRUQsVUFBSSxrQkFBa0IsWUFBWSxRQUFRLEtBQUssS0FBSyxTQUFTO0FBQUEsSUFDOUQ7QUFBQSxJQUVELFlBQWEsS0FBSyxPQUFPLEtBQUssV0FBVztBQUN2QyxZQUNFLGtCQUFrQixvQkFBb0IsSUFBSSxNQUFNLE1BQU07QUFDeEQsVUFBSSxJQUFJLEtBQUssSUFBSSxHQUFHLFFBQVEsQ0FBQztBQUU3QixhQUFPLEtBQUssR0FBRyxLQUFLO0FBQ2xCLFlBQUksZ0JBQWlCLElBQUksT0FBUSxRQUFRO0FBQ3ZDLGtCQUFRO0FBQ1I7QUFBQSxRQUNELFdBQ1EsZ0JBQWlCLE9BQVEsUUFBUTtBQUN4QyxrQkFBUTtBQUNSLGNBQUksTUFBTSxHQUFHO0FBQ1g7QUFBQSxVQUNEO0FBQUEsUUFDRjtBQUFBLE1BQ0Y7QUFFRCxVQUNFLElBQUksS0FDRCxnQkFBaUIsV0FBWSxVQUM3QixnQkFBaUIsV0FBWSxRQUNoQztBQUNBLGVBQU8sV0FBVyxhQUFhLEtBQUssR0FBRyxDQUFDO0FBQUEsTUFDekM7QUFFRCxlQUFTLEtBQUssSUFBSSxrQkFDaEIsT0FDQSxjQUFjLE9BQU8sTUFBTSxPQUFPLFVBQ25DO0FBQUEsSUFDRjtBQUFBLElBRUQsYUFBYyxLQUFLLE9BQU8sS0FBSyxXQUFXO0FBQ3hDLFlBQ0UsUUFBUSxJQUFJLE1BQU0sUUFDbEIsa0JBQWtCLG9CQUFvQixLQUFLLEdBQzNDLGVBQWUsZ0JBQWdCLE1BQU0sR0FBRyxNQUFNLENBQUMsRUFBRSxRQUFRLE1BQU0sTUFBTTtBQUN2RSxVQUFJLElBQUksS0FBSyxJQUFJLE9BQU8sTUFBTSxDQUFDO0FBRS9CLGFBQU8sS0FBSyxPQUFPLEtBQUs7QUFDdEIsWUFBSSxnQkFBaUIsSUFBSSxPQUFRLFFBQVE7QUFDdkMsZ0JBQU07QUFDTixnQkFBTSxLQUFLLGlCQUFpQixRQUFRO0FBQ3BDO0FBQUEsUUFDRDtBQUFBLE1BQ0Y7QUFFRCxVQUNFLElBQUksU0FDRCxnQkFBaUIsTUFBTSxPQUFRLFVBQy9CLGdCQUFpQixNQUFNLE9BQVEsUUFDbEM7QUFDQSxlQUFPLFdBQVcsWUFBWSxLQUFLLE9BQU8sS0FBSztBQUFBLE1BQ2hEO0FBRUQsVUFBSSxrQkFBa0IsY0FBYyxPQUFPLFFBQVEsS0FBSyxLQUFLLFNBQVM7QUFBQSxJQUN2RTtBQUFBLEVBQ0Y7QUFFRCwyQkFBMEIsR0FBRztBQUMzQixRQUFJLGdCQUFnQixDQUFDLE1BQU0sTUFBTTtBQUMvQjtBQUFBLElBQ0Q7QUFFRCxVQUNFLE1BQU0sU0FBUyxPQUNmLFFBQVEsSUFBSSxnQkFDWixNQUFNLElBQUk7QUFFWixRQUFJLEVBQUUsWUFBWSxNQUFNLEVBQUUsWUFBWSxJQUFJO0FBQ3hDLFlBQU0sS0FBSyxXQUFhLEdBQUUsWUFBWSxLQUFLLFVBQVUsVUFBVyxPQUFNLG9CQUFvQixPQUFPLFlBQVk7QUFFN0csUUFBRSxlQUFnQjtBQUNsQixTQUFHLEtBQUssT0FBTyxLQUFLLEVBQUUsUUFBUTtBQUFBLElBQy9CLFdBRUMsRUFBRSxZQUFZLEtBQ1gsTUFBTSxvQkFBb0IsUUFDMUIsVUFBVSxLQUNiO0FBQ0EsaUJBQVcsS0FBSyxLQUFLLE9BQU8sS0FBSyxJQUFJO0FBQUEsSUFDdEMsV0FFQyxFQUFFLFlBQVksTUFDWCxNQUFNLG9CQUFvQixRQUMxQixVQUFVLEtBQ2I7QUFDQSxpQkFBVyxhQUFhLEtBQUssT0FBTyxLQUFLLElBQUk7QUFBQSxJQUM5QztBQUFBLEVBQ0Y7QUFFRCxxQkFBb0IsS0FBSztBQUN2QixRQUFJLFFBQVEsVUFBVSxRQUFRLFFBQVEsUUFBUSxJQUFJO0FBQUUsYUFBTztBQUFBLElBQUk7QUFFL0QsUUFBSSxNQUFNLG9CQUFvQixNQUFNO0FBQ2xDLGFBQU8saUJBQWlCLEdBQUc7QUFBQSxJQUM1QjtBQUVELFVBQU0sT0FBTztBQUViLFFBQUksV0FBVyxHQUFHLFNBQVM7QUFFM0IsYUFBUyxZQUFZLEdBQUcsWUFBWSxLQUFLLFFBQVEsYUFBYTtBQUM1RCxZQUNFLFVBQVUsSUFBSyxXQUNmLFVBQVUsS0FBTTtBQUVsQixVQUFJLE9BQU8sWUFBWSxVQUFVO0FBQy9CLGtCQUFVO0FBQ1Ysb0JBQVksV0FBVztBQUFBLE1BQ3hCLFdBQ1EsWUFBWSxVQUFVLFFBQVEsTUFBTSxLQUFLLE9BQU8sR0FBRztBQUMxRCxrQkFBVSxRQUFRLGNBQWMsU0FDNUIsUUFBUSxVQUFVLE9BQU8sSUFDekI7QUFDSjtBQUFBLE1BQ0QsT0FDSTtBQUNILGVBQU87QUFBQSxNQUNSO0FBQUEsSUFDRjtBQUVELFdBQU87QUFBQSxFQUNSO0FBRUQsNEJBQTJCLEtBQUs7QUFDOUIsVUFDRSxPQUFPLGNBQ1Asa0JBQWtCLFdBQVcsUUFBUSxNQUFNO0FBRTdDLFFBQUksV0FBVyxJQUFJLFNBQVMsR0FBRyxTQUFTO0FBRXhDLGFBQVMsWUFBWSxLQUFLLFNBQVMsR0FBRyxhQUFhLEtBQUssV0FBVyxJQUFJLGFBQWE7QUFDbEYsWUFBTSxVQUFVLEtBQU07QUFFdEIsVUFBSSxVQUFVLElBQUs7QUFFbkIsVUFBSSxPQUFPLFlBQVksVUFBVTtBQUMvQixpQkFBUyxVQUFVO0FBQ25CLG9CQUFZLFdBQVc7QUFBQSxNQUN4QixXQUNRLFlBQVksVUFBVSxRQUFRLE1BQU0sS0FBSyxPQUFPLEdBQUc7QUFDMUQsV0FBRztBQUNELG1CQUFVLFNBQVEsY0FBYyxTQUFTLFFBQVEsVUFBVSxPQUFPLElBQUksV0FBVztBQUNqRjtBQUNBLG9CQUFVLElBQUs7QUFBQSxRQUV6QixTQUFpQixvQkFBb0IsYUFBYSxZQUFZLFVBQVUsUUFBUSxNQUFNLEtBQUssT0FBTztBQUFBLE1BQzNGLE9BQ0k7QUFDSCxlQUFPO0FBQUEsTUFDUjtBQUFBLElBQ0Y7QUFFRCxXQUFPO0FBQUEsRUFDUjtBQUVELHVCQUFzQixLQUFLO0FBQ3pCLFdBQU8sT0FBTyxRQUFRLFlBQVksbUJBQW1CLFNBQ2hELE9BQU8sUUFBUSxXQUFXLGVBQWUsS0FBSyxHQUFHLElBQUksTUFDdEQsZUFBZSxHQUFHO0FBQUEsRUFDdkI7QUFFRCx3QkFBdUIsS0FBSztBQUMxQixRQUFJLGFBQWEsU0FBUyxJQUFJLFVBQVUsR0FBRztBQUN6QyxhQUFPO0FBQUEsSUFDUjtBQUVELFdBQU8sTUFBTSxvQkFBb0IsUUFBUSxJQUFJLFNBQVMsSUFDbEQsYUFBYSxNQUFNLEdBQUcsQ0FBQyxJQUFJLE1BQU0sSUFBSSxNQUNyQyxNQUFNLGFBQWEsTUFBTSxJQUFJLE1BQU07QUFBQSxFQUN4QztBQUVELFNBQU87QUFBQSxJQUNMO0FBQUEsSUFDQTtBQUFBLElBQ0E7QUFBQSxJQUNBO0FBQUEsSUFDQTtBQUFBLEVBQ0Q7QUFDSDtBQzVoQmUsNkJBQVUsT0FBTyxXQUFXO0FBQ3pDLDZCQUE0QjtBQUMxQixVQUFNLFFBQVEsTUFBTTtBQUVwQixRQUFJO0FBQ0YsWUFBTSxLQUFLLGtCQUFrQixTQUN6QixJQUFJLGFBQWMsSUFDakIsb0JBQW9CLFNBQ2pCLElBQUksZUFBZSxFQUFFLEVBQUUsZ0JBQ3ZCO0FBR1IsVUFBSSxPQUFPLEtBQUssTUFBTSxPQUFPO0FBQzNCLFFBQUMsYUFBWSxRQUNULE1BQU0sS0FBSyxLQUFLLElBQ2hCLENBQUUsS0FBTyxHQUNYLFFBQVEsVUFBUTtBQUNoQixhQUFHLE1BQU0sSUFBSSxJQUFJO0FBQUEsUUFDM0IsQ0FBUztBQUFBLE1BQ0Y7QUFFRCxhQUFPO0FBQUEsUUFDTCxPQUFPLEdBQUc7QUFBQSxNQUNYO0FBQUEsSUFDRixTQUNNLEdBQVA7QUFDRSxhQUFPO0FBQUEsUUFDTCxPQUFPO0FBQUEsTUFDUjtBQUFBLElBQ0Y7QUFBQSxFQUNGO0FBRUQsU0FBTyxjQUFjLE9BQ2pCLFNBQVMsTUFBTTtBQUNmLFFBQUksTUFBTSxTQUFTLFFBQVE7QUFDekI7QUFBQSxJQUNEO0FBRUQsV0FBTyxnQkFBaUI7QUFBQSxFQUM5QixDQUFLLElBQ0MsU0FBUyxlQUFlO0FBQzlCO0FDM0NBLE1BQU0sYUFBYTtBQUNuQixNQUFNLFlBQVk7QUFDbEIsTUFBTSxXQUFXO0FBRUYsMkJBQVUsU0FBUztBQUNoQyxTQUFPLHVCQUF3QixHQUFHO0FBQ2hDLFFBQUksRUFBRSxTQUFTLG9CQUFvQixFQUFFLFNBQVMsVUFBVTtBQUN0RCxVQUFJLEVBQUUsT0FBTyxjQUFjLE1BQU07QUFBRTtBQUFBLE1BQVE7QUFDM0MsUUFBRSxPQUFPLFlBQVk7QUFDckIsY0FBUSxDQUFDO0FBQUEsSUFDVixXQUNRLEVBQUUsU0FBUyxxQkFBcUI7QUFDdkMsVUFDRSxPQUFPLEVBQUUsU0FBUyxZQUNmLFdBQVcsS0FBSyxFQUFFLElBQUksTUFBTSxTQUM1QixVQUFVLEtBQUssRUFBRSxJQUFJLE1BQU0sU0FDM0IsU0FBUyxLQUFLLEVBQUUsSUFBSSxNQUFNLE9BQzdCO0FBQ0EsVUFBRSxPQUFPLFlBQVk7QUFBQSxNQUN0QjtBQUFBLElBQ0YsT0FDSTtBQUNILFFBQUUsT0FBTyxZQUFZO0FBQUEsSUFDdEI7QUFBQSxFQUNGO0FBQ0g7QUNiQSxJQUFBLFNBQWUsZ0JBQWdCO0FBQUEsRUFDN0IsTUFBTTtBQUFBLEVBRU4sY0FBYztBQUFBLEVBRWQsT0FBTywrREFDRixnQkFDQSxlQUNBLGVBSEU7QUFBQSxJQUtMLFlBQVksRUFBRSxVQUFVLE1BQU87QUFBQSxJQUUvQixZQUFZO0FBQUEsSUFFWixNQUFNO0FBQUEsTUFDSixNQUFNO0FBQUEsTUFDTixTQUFTO0FBQUEsSUFDVjtBQUFBLElBRUQsVUFBVSxDQUFFLFFBQVEsTUFBUTtBQUFBLElBRTVCLFVBQVU7QUFBQSxJQUVWLFlBQVksQ0FBRSxPQUFPLFFBQVEsTUFBUTtBQUFBLElBQ3JDLFlBQVksQ0FBRSxPQUFPLFFBQVEsTUFBUTtBQUFBLEVBQ3RDO0FBQUEsRUFFRCxPQUFPO0FBQUEsSUFDTCxHQUFHO0FBQUEsSUFDSDtBQUFBLElBQVM7QUFBQSxFQUNWO0FBQUEsRUFFRCxNQUFPLE9BQU8sRUFBRSxNQUFNLFNBQVM7QUFDN0IsVUFBTSxPQUFPLENBQUU7QUFDZixRQUFJLGtCQUFrQixLQUFLLGFBQWEsa0JBQWtCLFdBQVc7QUFFckUsVUFBTSxXQUFXLElBQUksSUFBSTtBQUN6QixVQUFNLFdBQVcscUJBQXFCLEtBQUs7QUFFM0MsVUFBTTtBQUFBLE1BQ0o7QUFBQSxNQUNBO0FBQUEsTUFDQTtBQUFBLE1BQ0E7QUFBQSxNQUNBO0FBQUEsUUFDRSxRQUFRLE9BQU8sTUFBTSxXQUFXLFFBQVE7QUFFNUMsVUFBTSxlQUFlLG9CQUFvQixPQUF3QixJQUFJO0FBQ3JFLFVBQU0sV0FBVyxTQUFTLE1BQU0sbUJBQW1CLFdBQVcsS0FBSyxDQUFDO0FBRXBFLFVBQU0sZ0JBQWdCLGtCQUFrQixPQUFPO0FBRS9DLFVBQU0sUUFBUSxjQUFlO0FBRTdCLFVBQU0sYUFBYSxTQUFTLE1BQzFCLE1BQU0sU0FBUyxjQUFjLE1BQU0sYUFBYSxJQUNqRDtBQUVELFVBQU0sYUFBYSxTQUFTLE1BQzFCLFdBQVcsVUFBVSxRQUNsQixDQUFFLFFBQVEsVUFBVSxPQUFPLE9BQU8sWUFBYSxTQUFTLE1BQU0sSUFBSSxDQUN0RTtBQUVELFVBQU0sV0FBVyxTQUFTLE1BQU07QUFDOUIsWUFBTSxNQUFNLGlDQUNQLE1BQU0sV0FBVyxVQUFVLFFBRHBCO0FBQUEsUUFFVjtBQUFBLFFBQ0E7QUFBQSxRQUtBO0FBQUEsUUFDQSxRQUFRO0FBQUEsUUFDUixTQUFTO0FBQUEsTUFDVjtBQUVELFVBQUkscUJBQXFCLElBQUksc0JBQXNCLElBQUksbUJBQW1CO0FBRTFFLFVBQUksUUFBUSxVQUFVLE1BQU07QUFDMUIsWUFBSSxZQUFZO0FBQUEsTUFDakI7QUFFRCxVQUFJLE1BQU0sYUFBYSxNQUFNO0FBQzNCLFlBQUksaUJBQWlCO0FBQUEsTUFDdEI7QUFFRCxhQUFPO0FBQUEsSUFDYixDQUFLO0FBRUQsVUFBTSxhQUFhLFNBQVMsTUFBTTtBQUNoQyxZQUFNLFNBQVE7QUFBQSxRQUNaLFVBQVU7QUFBQSxRQUNWLGtCQUFrQixNQUFNLGNBQWMsUUFBUTtBQUFBLFFBQzlDLE1BQU0sTUFBTSxTQUFTLGFBQWEsSUFBSTtBQUFBLFFBQ3RDLGNBQWMsTUFBTTtBQUFBLFFBQ3BCLE1BQU0sU0FBUztBQUFBLFNBQ1osTUFBTSxXQUFXLFdBQVcsUUFObkI7QUFBQSxRQU9aLElBQUksTUFBTSxVQUFVO0FBQUEsUUFDcEIsV0FBVyxNQUFNO0FBQUEsUUFDakIsVUFBVSxNQUFNLFlBQVk7QUFBQSxRQUM1QixVQUFVLE1BQU0sYUFBYTtBQUFBLE1BQzlCO0FBRUQsVUFBSSxXQUFXLFVBQVUsT0FBTztBQUM5QixlQUFNLE9BQU8sTUFBTTtBQUFBLE1BQ3BCO0FBRUQsVUFBSSxNQUFNLGFBQWEsTUFBTTtBQUMzQixlQUFNLE9BQU87QUFBQSxNQUNkO0FBRUQsYUFBTztBQUFBLElBQ2IsQ0FBSztBQUtELFVBQU0sTUFBTSxNQUFNLE1BQU0sTUFBTTtBQUM1QixVQUFJLFNBQVMsT0FBTztBQUNsQixpQkFBUyxNQUFNLFFBQVEsTUFBTTtBQUFBLE1BQzlCO0FBQUEsSUFDUCxDQUFLO0FBRUQsVUFBTSxNQUFNLE1BQU0sWUFBWSxPQUFLO0FBQ2pDLFVBQUksUUFBUSxVQUFVLE1BQU07QUFDMUIsWUFBSSxxQkFBcUIsTUFBTTtBQUM3Qiw2QkFBbUI7QUFFbkIsY0FBSSxPQUFPLENBQUMsTUFBTSxpQkFBaUI7QUFDakM7QUFBQSxVQUNEO0FBQUEsUUFDRjtBQUVELHdCQUFnQixDQUFDO0FBQUEsTUFDbEIsV0FDUSxXQUFXLFVBQVUsR0FBRztBQUMvQixtQkFBVyxRQUFRO0FBRW5CLFlBQ0UsTUFBTSxTQUFTLFlBQ1osS0FBSyxlQUFlLE9BQU8sTUFBTSxNQUNwQztBQUNBLGNBQUksZ0JBQWdCLE1BQU07QUFDeEIsMEJBQWM7QUFBQSxVQUNmLE9BQ0k7QUFDSCxtQkFBTyxLQUFLO0FBQUEsVUFDYjtBQUFBLFFBQ0Y7QUFBQSxNQUNGO0FBR0QsWUFBTSxhQUFhLFFBQVEsU0FBUyxZQUFZO0FBQUEsSUFDdEQsQ0FBSztBQUVELFVBQU0sTUFBTSxNQUFNLFVBQVUsU0FBTztBQUVqQyxVQUFJLFFBQVEsTUFBTTtBQUNoQixpQkFBUyxZQUFZO0FBQUEsTUFDdEIsV0FFUSxTQUFTLFVBQVUsUUFBUSxNQUFNLE9BQU8sR0FBRztBQUNsRCxpQkFBUyxNQUFNLE1BQU0sU0FBUztBQUFBLE1BQy9CO0FBQUEsSUFDUCxDQUFLO0FBRUQsVUFBTSxNQUFNLE1BQU0sT0FBTyxNQUFNO0FBQzdCLFlBQU0sYUFBYSxRQUFRLFNBQVMsWUFBWTtBQUFBLElBQ3RELENBQUs7QUFFRCxxQkFBa0I7QUFDaEIsaUJBQVcsTUFBTTtBQUNmLGNBQU0sS0FBSyxTQUFTO0FBQ3BCLFlBQ0UsU0FBUyxVQUFVLFFBQ2hCLFNBQVMsVUFBVSxNQUNsQixRQUFPLFFBQVEsR0FBRyxPQUFPLE1BQU0sVUFBVSxRQUM3QztBQUNBLG1CQUFTLE1BQU0sTUFBTSxFQUFFLGVBQWUsS0FBSSxDQUFFO0FBQUEsUUFDN0M7QUFBQSxNQUNULENBQU87QUFBQSxJQUNGO0FBRUQsc0JBQW1CO0FBQ2pCLGVBQVMsVUFBVSxRQUFRLFNBQVMsTUFBTSxPQUFRO0FBQUEsSUFDbkQ7QUFFRCxxQkFBa0IsR0FBRztBQUNuQixVQUFJLFFBQVEsVUFBVSxRQUFRLE1BQU0sb0JBQW9CLE1BQU07QUFDNUQsY0FBTSxNQUFNLEVBQUU7QUFDZCwyQkFBbUIsS0FBSyxJQUFJLGdCQUFnQixJQUFJLFlBQVk7QUFBQSxNQUM3RDtBQUVELFdBQUssU0FBUyxDQUFDO0FBQUEsSUFDaEI7QUFFRCxxQkFBa0IsR0FBRztBQUNuQixVQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsVUFBVSxFQUFFLE9BQU8sY0FBYyxNQUFNO0FBQ2xEO0FBQUEsTUFDRDtBQUVELFVBQUksTUFBTSxTQUFTLFFBQVE7QUFDekIsYUFBSyxxQkFBcUIsRUFBRSxPQUFPLEtBQUs7QUFDeEM7QUFBQSxNQUNEO0FBRUQsWUFBTSxNQUFNLEVBQUUsT0FBTztBQUVyQixVQUFJLFFBQVEsVUFBVSxNQUFNO0FBQzFCLHdCQUFnQixLQUFLLE9BQU8sRUFBRSxTQUFTO0FBQUEsTUFDeEMsT0FDSTtBQUNILGtCQUFVLEdBQUc7QUFFYixZQUFJLFdBQVcsVUFBVSxRQUFRLEVBQUUsV0FBVyxTQUFTLGVBQWU7QUFDcEUsZ0JBQU0sRUFBRSxnQkFBZ0IsaUJBQWlCLEVBQUU7QUFFM0MsY0FBSSxtQkFBbUIsVUFBVSxpQkFBaUIsUUFBUTtBQUN4RCxxQkFBUyxNQUFNO0FBQ2Isa0JBQUksRUFBRSxXQUFXLFNBQVMsaUJBQWlCLElBQUksUUFBUSxFQUFFLE9BQU8sS0FBSyxNQUFNLEdBQUc7QUFDNUUsa0JBQUUsT0FBTyxrQkFBa0IsZ0JBQWdCLFlBQVk7QUFBQSxjQUN4RDtBQUFBLFlBQ2YsQ0FBYTtBQUFBLFVBQ0Y7QUFBQSxRQUNGO0FBQUEsTUFDRjtBQUlELFlBQU0sYUFBYSxRQUFRLGFBQWM7QUFBQSxJQUMxQztBQUVELHVCQUFvQixLQUFLLGFBQWE7QUFDcEMsb0JBQWMsTUFBTTtBQUNsQixZQUNFLE1BQU0sU0FBUyxZQUNaLEtBQUssZUFBZSxPQUFPLE1BQU0sTUFDcEM7QUFDQSxpQkFBTyxLQUFLO0FBQUEsUUFDYjtBQUVELFlBQUksTUFBTSxlQUFlLE9BQU8sb0JBQW9CLEtBQUs7QUFDdkQsMEJBQWdCLFFBQVMsb0JBQW1CO0FBQzVDLGVBQUsscUJBQXFCLEdBQUc7QUFFN0IsbUJBQVMsTUFBTTtBQUNiLGdDQUFvQixPQUFRLG1CQUFrQjtBQUFBLFVBQzFELENBQVc7QUFBQSxRQUNGO0FBRUQsc0JBQWM7QUFBQSxNQUNmO0FBRUQsVUFBSSxNQUFNLFNBQVMsVUFBVTtBQUMzQixzQkFBYztBQUNkLGFBQUssUUFBUTtBQUFBLE1BQ2Q7QUFFRCxVQUFJLE1BQU0sYUFBYSxRQUFRO0FBQzdCLHFCQUFhLFNBQVM7QUFDdEIsYUFBSyxRQUFRO0FBQ2Isb0JBQVksV0FBVyxhQUFhLE1BQU0sUUFBUTtBQUFBLE1BQ25ELE9BQ0k7QUFDSCxvQkFBYTtBQUFBLE1BQ2Q7QUFBQSxJQUNGO0FBR0QsNEJBQXlCO0FBQ3ZCLFlBQU0sTUFBTSxTQUFTO0FBQ3JCLFVBQUksUUFBUSxNQUFNO0FBQ2hCLGNBQU0sY0FBYyxJQUFJLFdBQVc7QUFJbkMsb0JBQVksZUFBZ0IsSUFBSSxlQUFlLElBQUs7QUFDcEQsWUFBSSxNQUFNLFNBQVM7QUFFbkIsWUFBSSxNQUFNLFNBQVMsSUFBSSxlQUFlO0FBQ3RDLG9CQUFZLGVBQWU7QUFBQSxNQUM1QjtBQUFBLElBQ0Y7QUFFRCxzQkFBbUIsR0FBRztBQUNwQixvQkFBYyxDQUFDO0FBRWYsbUJBQWEsU0FBUztBQUN0QixzQkFBZ0IsVUFBVSxZQUFhO0FBRXZDLFdBQUssVUFBVSxFQUFFLE9BQU8sS0FBSztBQUFBLElBQzlCO0FBRUQsNkJBQTBCLEdBQUc7QUFDM0IsWUFBTSxVQUFVLEtBQUssQ0FBQztBQUV0QixtQkFBYSxTQUFTO0FBQ3RCLHNCQUFnQixVQUFVLFlBQWE7QUFFdkMsb0JBQWM7QUFDZCx5QkFBbUI7QUFDbkIsYUFBTyxLQUFLO0FBSVosWUFBTSxTQUFTLFVBQVUsV0FBVyxNQUFNO0FBQ3hDLFlBQUksU0FBUyxVQUFVLE1BQU07QUFDM0IsbUJBQVMsTUFBTSxRQUFRLFdBQVcsVUFBVSxTQUFTLFdBQVcsUUFBUTtBQUFBLFFBQ3pFO0FBQUEsTUFDVCxDQUFPO0FBQUEsSUFDRjtBQUVELDJCQUF3QjtBQUN0QixhQUFPLEtBQUssZUFBZSxPQUFPLE1BQU0sT0FDcEMsS0FBSyxRQUNKLFdBQVcsVUFBVSxTQUFTLFdBQVcsUUFBUTtBQUFBLElBQ3ZEO0FBRUQsb0JBQWdCLE1BQU07QUFDcEIsc0JBQWlCO0FBQUEsSUFDdkIsQ0FBSztBQUVELGNBQVUsTUFBTTtBQUVkLFlBQU0sYUFBYSxRQUFRLGFBQWM7QUFBQSxJQUMvQyxDQUFLO0FBRUQsV0FBTyxPQUFPLE9BQU87QUFBQSxNQUNuQjtBQUFBLE1BRUEsWUFBWSxTQUFTLE1BQ25CLEtBQU0sV0FBVyxVQUFVLE9BQU8sYUFBYSxZQUM1QyxPQUFNLGFBQWEsT0FBTywwQkFBMEIsR0FDeEQ7QUFBQSxNQUVELFdBQVcsU0FBUyxNQUNsQixNQUFNLFNBQVMsVUFDWixPQUFPLE1BQU0sZUFBZSxZQUM1QixNQUFNLFdBQVcsU0FBUyxDQUM5QjtBQUFBLE1BRUQ7QUFBQSxNQUVBO0FBQUEsTUFFQTtBQUFBLE1BRUEsZUFBZSxTQUFTLE1BQ3RCLFNBQVMsVUFBVSxRQUNoQixtQkFBbUIsTUFBTSxZQUFZLENBQ3pDO0FBQUEsTUFFRCxZQUFZLE1BQU07QUFDaEIsZUFBTyxFQUFFLFdBQVcsVUFBVSxPQUFPLGFBQWEsU0FBUztBQUFBLFVBQ3pELEtBQUs7QUFBQSxVQUNMLE9BQU87QUFBQSxZQUNMO0FBQUEsWUFDQSxNQUFNO0FBQUEsVUFDUDtBQUFBLFVBQ0QsT0FBTyxNQUFNO0FBQUEsV0FDVixXQUFXLFFBQ1gsU0FBUyxRQUVWLE1BQU0sU0FBUyxTQUNYLEVBQUUsT0FBTyxjQUFlLElBQ3hCLGFBQWEsTUFFcEI7QUFBQSxNQUNGO0FBQUEsTUFFRCxrQkFBa0IsTUFBTTtBQUN0QixlQUFPLEVBQUUsT0FBTztBQUFBLFVBQ2QsT0FBTyxzRUFDRixZQUFXLFVBQVUsT0FBTyxLQUFLO0FBQUEsUUFDaEQsR0FBVztBQUFBLFVBQ0QsRUFBRSxRQUFRLEVBQUUsT0FBTyxZQUFhLEdBQUUsWUFBVyxDQUFFO0FBQUEsVUFDL0MsRUFBRSxRQUFRLE1BQU0sVUFBVTtBQUFBLFFBQ3BDLENBQVM7QUFBQSxNQUNGO0FBQUEsSUFDUCxDQUFLO0FBRUQsVUFBTSxXQUFXLFNBQVMsS0FBSztBQUcvQixVQUFNLEtBQUssbUJBQW9CO0FBQy9CLFdBQU8sT0FBTyxHQUFHLE9BQU87QUFBQSxNQUN0QjtBQUFBLE1BQ0E7QUFBQSxNQUNBLGtCQUFrQixNQUFNLFNBQVM7QUFBQSxJQUN2QyxDQUFLO0FBRUQsV0FBTztBQUFBLEVBQ1I7QUFDSCxDQUFDO0FDdllELE1BQUssWUFBYSxnQkFBYTtBQUFBLEVBQzdCLE1BQU07QUFBQSxFQUNOLE9BQVE7QUFDQyxXQUFBO0FBQUEsTUFDTCxNQUFNLElBQUksZ0JBQWdCO0FBQUEsTUFDMUIsTUFBTSxNQUFpQjtBQUFBLElBQUE7QUFBQSxFQUUzQjtBQUFBLEVBQ0EsU0FBUztBQUFBLElBQ1AsY0FBYztBQUNDLG1CQUFBLE9BQU8sRUFBRSxNQUFNLEtBQUssS0FBTSxDQUFBLEVBQUUsS0FBSyxNQUFNO0FBQzdDLGFBQUEsT0FBTyxJQUFJO01BQWdCLENBQ2pDO0FBQUEsSUFDSDtBQUFBLElBQ0EsYUFBYTtBQUNFLG1CQUFBLFFBQUEsRUFBVSxLQUFLLENBQVUsV0FBQTtBQUNwQyxhQUFLLEtBQUssU0FBUztBQUNkLGFBQUEsS0FBSyxLQUFLLEdBQUcsTUFBTTtBQUFBLE1BQUEsQ0FDekI7QUFBQSxJQUNIO0FBQUEsSUFDQSxXQUFXO0FBQ0QsY0FBQSxJQUFJLFFBQVEsR0FBRztBQUFBLElBQ3pCO0FBQUEsRUFDRjtBQUNGLENBQUM7O3NCQXRDQ0EsWUFPUyxPQUFBLEVBQUEsT0FBQSxxQ0FQc0M7QUFBQSxJQUFBLFNBQUFDLFFBQzdDLE1BQThDO0FBQUEsTUFBOUNDLFlBQThDLFFBQUE7QUFBQSxRQUFBLFlBQTVCLEtBQUssS0FBQTtBQUFBLFFBQUEsdUJBQUEsT0FBQSxNQUFBLFFBQUEsS0FBQSxDQUFBLFdBQUwsVUFBSyxPQUFJO0FBQUEsUUFBRSxPQUFNO0FBQUEsTUFBQSxHQUFBLE1BQUEsR0FBQSxDQUFBLFlBQUEsQ0FBQTtBQUFBLE1BQ25DQSxZQUE2QyxNQUFBO0FBQUEsUUFBckMsU0FBTyxLQUFBO0FBQUEsUUFBYSxPQUFNO0FBQUEsTUFBQSxHQUFBLE1BQUEsR0FBQSxDQUFBLFNBQUEsQ0FBQTtBQUFBLE1BQ2xDQSxZQUEwQyxNQUFBO0FBQUEsUUFBbEMsU0FBTyxLQUFBO0FBQUEsUUFBWSxPQUFNO0FBQUEsTUFBQSxHQUFBLE1BQUEsR0FBQSxDQUFBLFNBQUEsQ0FBQTtBQUFBLE1BQ2pDQyxnQkFFSyxNQUFBLE1BQUE7QUFBQSxRQUFBQyxXQUFBLElBQUEsR0FESEMsbUJBQXlEQyxVQUFBLE1BQUFDLFdBQXZDLEtBQUksTUFBQSxDQUFYLFFBQUc7OEJBQWRGLG1CQUF5RCxNQUFBO0FBQUEsWUFBaEMsS0FBSyxJQUFJO0FBQUEsVUFBQSxHQUFBRyxnQkFBTyxJQUFJLElBQUksR0FBQSxDQUFBO0FBQUEsUUFBQSxDQUFBLEdBQUEsR0FBQTtBQUFBOzs7Ozs7OyJ9
