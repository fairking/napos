var __defProp = Object.defineProperty;
var __defProps = Object.defineProperties;
var __getOwnPropDescs = Object.getOwnPropertyDescriptors;
var __getOwnPropSymbols = Object.getOwnPropertySymbols;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __propIsEnum = Object.prototype.propertyIsEnumerable;
var __defNormalProp = (obj, key, value) => key in obj ? __defProp(obj, key, { enumerable: true, configurable: true, writable: true, value }) : obj[key] = value;
var __spreadValues = (a, b) => {
  for (var prop in b || (b = {}))
    if (__hasOwnProp.call(b, prop))
      __defNormalProp(a, prop, b[prop]);
  if (__getOwnPropSymbols)
    for (var prop of __getOwnPropSymbols(b)) {
      if (__propIsEnum.call(b, prop))
        __defNormalProp(a, prop, b[prop]);
    }
  return a;
};
var __spreadProps = (a, b) => __defProps(a, __getOwnPropDescs(b));
import { c as computed, h, i as isRef, G as isKeyCode, u as addEvt, A as cleanEvt, y as stop, z as position, x as prevent, g as getCurrentInstance, r as ref, j as onBeforeUnmount, V as Transition, B as withDirectives, s as stopAndPrevent, l as listenOpts } from "./index.b4b5ef26.js";
import { u as useSizeDefaults, a as useSize, b as useSizeProps, Q as QIcon } from "./QIcon.58674f5b.js";
import { c as createComponent, b as createDirective, e as hMergeSlot } from "./render.c97de2b6.js";
const useSpinnerProps = {
  size: {
    type: [Number, String],
    default: "1em"
  },
  color: String
};
function useSpinner(props) {
  return {
    cSize: computed(() => props.size in useSizeDefaults ? `${useSizeDefaults[props.size]}px` : props.size),
    classes: computed(() => "q-spinner" + (props.color ? ` text-${props.color}` : ""))
  };
}
var QSpinner = createComponent({
  name: "QSpinner",
  props: __spreadProps(__spreadValues({}, useSpinnerProps), {
    thickness: {
      type: Number,
      default: 5
    }
  }),
  setup(props) {
    const { cSize, classes } = useSpinner(props);
    return () => h("svg", {
      class: classes.value + " q-spinner-mat",
      width: cSize.value,
      height: cSize.value,
      viewBox: "25 25 50 50"
    }, [
      h("circle", {
        class: "path",
        cx: "50",
        cy: "50",
        r: "20",
        fill: "none",
        stroke: "currentColor",
        "stroke-width": props.thickness,
        "stroke-miterlimit": "10"
      })
    ]);
  }
});
function css(element, css2) {
  const style = element.style;
  for (const prop in css2) {
    style[prop] = css2[prop];
  }
}
function getElement(el) {
  if (el === void 0 || el === null) {
    return void 0;
  }
  if (typeof el === "string") {
    try {
      return document.querySelector(el) || void 0;
    } catch (err) {
      return void 0;
    }
  }
  const target = isRef(el) === true ? el.value : el;
  if (target) {
    return target.$el || target;
  }
}
function throttle(fn, limit = 250) {
  let wait = false, result;
  return function() {
    if (wait === false) {
      wait = true;
      setTimeout(() => {
        wait = false;
      }, limit);
      result = fn.apply(this, arguments);
    }
    return result;
  };
}
function showRipple(evt, el, ctx, forceCenter) {
  ctx.modifiers.stop === true && stop(evt);
  const color = ctx.modifiers.color;
  let center = ctx.modifiers.center;
  center = center === true || forceCenter === true;
  const node = document.createElement("span"), innerNode = document.createElement("span"), pos = position(evt), { left, top, width, height } = el.getBoundingClientRect(), diameter = Math.sqrt(width * width + height * height), radius = diameter / 2, centerX = `${(width - diameter) / 2}px`, x = center ? centerX : `${pos.left - left - radius}px`, centerY = `${(height - diameter) / 2}px`, y = center ? centerY : `${pos.top - top - radius}px`;
  innerNode.className = "q-ripple__inner";
  css(innerNode, {
    height: `${diameter}px`,
    width: `${diameter}px`,
    transform: `translate3d(${x},${y},0) scale3d(.2,.2,1)`,
    opacity: 0
  });
  node.className = `q-ripple${color ? " text-" + color : ""}`;
  node.setAttribute("dir", "ltr");
  node.appendChild(innerNode);
  el.appendChild(node);
  const abort = () => {
    node.remove();
    clearTimeout(timer);
  };
  ctx.abort.push(abort);
  let timer = setTimeout(() => {
    innerNode.classList.add("q-ripple__inner--enter");
    innerNode.style.transform = `translate3d(${centerX},${centerY},0) scale3d(1,1,1)`;
    innerNode.style.opacity = 0.2;
    timer = setTimeout(() => {
      innerNode.classList.remove("q-ripple__inner--enter");
      innerNode.classList.add("q-ripple__inner--leave");
      innerNode.style.opacity = 0;
      timer = setTimeout(() => {
        node.remove();
        ctx.abort.splice(ctx.abort.indexOf(abort), 1);
      }, 275);
    }, 250);
  }, 50);
}
function updateModifiers(ctx, { modifiers, value, arg, instance }) {
  const cfg = Object.assign({}, instance.$q.config.ripple, modifiers, value);
  ctx.modifiers = {
    early: cfg.early === true,
    stop: cfg.stop === true,
    center: cfg.center === true,
    color: cfg.color || arg,
    keyCodes: [].concat(cfg.keyCodes || 13)
  };
}
var Ripple = createDirective({
  name: "ripple",
  beforeMount(el, binding) {
    const ctx = {
      enabled: binding.value !== false,
      modifiers: {},
      abort: [],
      start(evt) {
        if (ctx.enabled === true && evt.qSkipRipple !== true && (ctx.modifiers.early === true ? ["mousedown", "touchstart"].includes(evt.type) === true : evt.type === "click")) {
          showRipple(evt, el, ctx, evt.qKeyEvent === true);
        }
      },
      keystart: throttle((evt) => {
        if (ctx.enabled === true && evt.qSkipRipple !== true && isKeyCode(evt, ctx.modifiers.keyCodes) === true && evt.type === `key${ctx.modifiers.early === true ? "down" : "up"}`) {
          showRipple(evt, el, ctx, true);
        }
      }, 300)
    };
    updateModifiers(ctx, binding);
    el.__qripple = ctx;
    addEvt(ctx, "main", [
      [el, "mousedown", "start", "passive"],
      [el, "touchstart", "start", "passive"],
      [el, "click", "start", "passive"],
      [el, "keydown", "keystart", "passive"],
      [el, "keyup", "keystart", "passive"]
    ]);
  },
  updated(el, binding) {
    if (binding.oldValue !== binding.value) {
      const ctx = el.__qripple;
      ctx.enabled = binding.value !== false;
      if (ctx.enabled === true && Object(binding.value) === binding.value) {
        updateModifiers(ctx, binding);
      }
    }
  },
  beforeUnmount(el) {
    const ctx = el.__qripple;
    ctx.abort.forEach((fn) => {
      fn();
    });
    cleanEvt(ctx, "main");
    delete el._qripple;
  }
});
const alignMap = {
  left: "start",
  center: "center",
  right: "end",
  between: "between",
  around: "around",
  evenly: "evenly",
  stretch: "stretch"
};
const alignValues = Object.keys(alignMap);
const useAlignProps = {
  align: {
    type: String,
    validator: (v) => alignValues.includes(v)
  }
};
function useAlign(props) {
  return computed(() => {
    const align = props.align === void 0 ? props.vertical === true ? "stretch" : "left" : props.align;
    return `${props.vertical === true ? "items" : "justify"}-${alignMap[align]}`;
  });
}
function vmHasRouter(vm) {
  return vm.appContext.config.globalProperties.$router !== void 0;
}
function getOriginalPath(record) {
  return record ? record.aliasOf ? record.aliasOf.path : record.path : "";
}
function isSameRouteRecord(a, b) {
  return (a.aliasOf || a) === (b.aliasOf || b);
}
function includesParams(outer, inner) {
  for (const key in inner) {
    const innerValue = inner[key], outerValue = outer[key];
    if (typeof innerValue === "string") {
      if (innerValue !== outerValue) {
        return false;
      }
    } else if (Array.isArray(outerValue) === false || outerValue.length !== innerValue.length || innerValue.some((value, i) => value !== outerValue[i])) {
      return false;
    }
  }
  return true;
}
function isEquivalentArray(a, b) {
  return Array.isArray(b) === true ? a.length === b.length && a.every((value, i) => value === b[i]) : a.length === 1 && a[0] === b;
}
function isSameRouteLocationParamsValue(a, b) {
  return Array.isArray(a) === true ? isEquivalentArray(a, b) : Array.isArray(b) === true ? isEquivalentArray(b, a) : a === b;
}
function isSameRouteLocationParams(a, b) {
  if (Object.keys(a).length !== Object.keys(b).length) {
    return false;
  }
  for (const key in a) {
    if (isSameRouteLocationParamsValue(a[key], b[key]) === false) {
      return false;
    }
  }
  return true;
}
const useRouterLinkProps = {
  to: [String, Object],
  replace: Boolean,
  exact: Boolean,
  activeClass: {
    type: String,
    default: "q-router-link--active"
  },
  exactActiveClass: {
    type: String,
    default: "q-router-link--exact-active"
  },
  href: String,
  target: String,
  disable: Boolean
};
function useRouterLink(fallbackTag) {
  const vm = getCurrentInstance();
  const { props, proxy } = vm;
  const hasRouter = vmHasRouter(vm);
  const hasHrefLink = computed(() => props.disable !== true && props.href !== void 0);
  const hasRouterLinkProps = computed(() => hasRouter === true && props.disable !== true && hasHrefLink.value !== true && props.to !== void 0 && props.to !== null && props.to !== "");
  const linkRoute = computed(() => {
    if (hasRouterLinkProps.value === true) {
      try {
        return proxy.$router.resolve(props.to);
      } catch (err) {
      }
    }
    return null;
  });
  const hasRouterLink = computed(() => linkRoute.value !== null);
  const hasLink = computed(() => hasHrefLink.value === true || hasRouterLink.value === true);
  const linkTag = computed(() => props.type === "a" || hasLink.value === true ? "a" : props.tag || fallbackTag || "div");
  const linkProps = computed(() => hasHrefLink.value === true ? {
    href: props.href,
    target: props.target
  } : hasRouterLink.value === true ? {
    href: linkRoute.value.href,
    target: props.target
  } : {});
  const linkActiveIndex = computed(() => {
    if (hasRouterLink.value === false) {
      return null;
    }
    const { matched } = linkRoute.value, { length } = matched, routeMatched = matched[length - 1];
    if (routeMatched === void 0) {
      return -1;
    }
    const currentMatched = proxy.$route.matched;
    if (currentMatched.length === 0) {
      return -1;
    }
    const index = currentMatched.findIndex(isSameRouteRecord.bind(null, routeMatched));
    if (index > -1) {
      return index;
    }
    const parentRecordPath = getOriginalPath(matched[length - 2]);
    return length > 1 && getOriginalPath(routeMatched) === parentRecordPath && currentMatched[currentMatched.length - 1].path !== parentRecordPath ? currentMatched.findIndex(isSameRouteRecord.bind(null, matched[length - 2])) : index;
  });
  const linkIsActive = computed(() => hasRouterLink.value === true && linkActiveIndex.value > -1 && includesParams(proxy.$route.params, linkRoute.value.params));
  const linkIsExactActive = computed(() => linkIsActive.value === true && linkActiveIndex.value === proxy.$route.matched.length - 1 && isSameRouteLocationParams(proxy.$route.params, linkRoute.value.params));
  const linkClass = computed(() => hasRouterLink.value === true ? linkIsExactActive.value === true ? ` ${props.exactActiveClass} ${props.activeClass}` : props.exact === true ? "" : linkIsActive.value === true ? ` ${props.activeClass}` : "" : "");
  function navigateToRouterLink(e) {
    if (props.disable === true || e.metaKey || e.altKey || e.ctrlKey || e.shiftKey || e.__qNavigate !== true && e.defaultPrevented === true || e.button !== void 0 && e.button !== 0 || props.target === "_blank") {
      return false;
    }
    prevent(e);
    return proxy.$router[props.replace === true ? "replace" : "push"](props.to).catch((err) => err);
  }
  return {
    hasRouterLink,
    hasHrefLink,
    hasLink,
    linkTag,
    linkRoute,
    linkIsActive,
    linkIsExactActive,
    linkClass,
    linkProps,
    navigateToRouterLink
  };
}
const padding = {
  none: 0,
  xs: 4,
  sm: 8,
  md: 16,
  lg: 24,
  xl: 32
};
const defaultSizes = {
  xs: 8,
  sm: 10,
  md: 14,
  lg: 20,
  xl: 24
};
const formTypes = ["button", "submit", "reset"];
const mediaTypeRE = /[^\s]\/[^\s]/;
const useBtnProps = __spreadProps(__spreadValues(__spreadValues({}, useSizeProps), useRouterLinkProps), {
  type: {
    type: String,
    default: "button"
  },
  label: [Number, String],
  icon: String,
  iconRight: String,
  round: Boolean,
  outline: Boolean,
  flat: Boolean,
  unelevated: Boolean,
  rounded: Boolean,
  push: Boolean,
  glossy: Boolean,
  size: String,
  fab: Boolean,
  fabMini: Boolean,
  padding: String,
  color: String,
  textColor: String,
  noCaps: Boolean,
  noWrap: Boolean,
  dense: Boolean,
  tabindex: [Number, String],
  ripple: {
    type: [Boolean, Object],
    default: true
  },
  align: __spreadProps(__spreadValues({}, useAlignProps.align), {
    default: "center"
  }),
  stack: Boolean,
  stretch: Boolean,
  loading: {
    type: Boolean,
    default: null
  },
  disable: Boolean
});
function useBtn(props) {
  const sizeStyle = useSize(props, defaultSizes);
  const alignClass = useAlign(props);
  const { hasRouterLink, hasLink, linkTag, linkProps, navigateToRouterLink } = useRouterLink("button");
  const style = computed(() => {
    const obj = props.fab === false && props.fabMini === false ? sizeStyle.value : {};
    return props.padding !== void 0 ? Object.assign({}, obj, {
      padding: props.padding.split(/\s+/).map((v) => v in padding ? padding[v] + "px" : v).join(" "),
      minWidth: "0",
      minHeight: "0"
    }) : obj;
  });
  const isRounded = computed(() => props.rounded === true || props.fab === true || props.fabMini === true);
  const isActionable = computed(() => props.disable !== true && props.loading !== true);
  const tabIndex = computed(() => isActionable.value === true ? props.tabindex || 0 : -1);
  const design = computed(() => {
    if (props.flat === true)
      return "flat";
    if (props.outline === true)
      return "outline";
    if (props.push === true)
      return "push";
    if (props.unelevated === true)
      return "unelevated";
    return "standard";
  });
  const attributes = computed(() => {
    const acc = { tabindex: tabIndex.value };
    if (hasLink.value === true) {
      Object.assign(acc, linkProps.value);
    } else if (formTypes.includes(props.type) === true) {
      acc.type = props.type;
    }
    if (linkTag.value === "a") {
      if (props.disable === true) {
        acc["aria-disabled"] = "true";
      } else if (acc.href === void 0) {
        acc.role = "button";
      }
      if (hasRouterLink.value !== true && mediaTypeRE.test(props.type) === true) {
        acc.type = props.type;
      }
    } else if (props.disable === true) {
      acc.disabled = "";
      acc["aria-disabled"] = "true";
    }
    if (props.loading === true && props.percentage !== void 0) {
      Object.assign(acc, {
        role: "progressbar",
        "aria-valuemin": 0,
        "aria-valuemax": 100,
        "aria-valuenow": props.percentage
      });
    }
    return acc;
  });
  const classes = computed(() => {
    let colors;
    if (props.color !== void 0) {
      if (props.flat === true || props.outline === true) {
        colors = `text-${props.textColor || props.color}`;
      } else {
        colors = `bg-${props.color} text-${props.textColor || "white"}`;
      }
    } else if (props.textColor) {
      colors = `text-${props.textColor}`;
    }
    return `q-btn--${design.value} q-btn--${props.round === true ? "round" : `rectangle${isRounded.value === true ? " q-btn--rounded" : ""}`}` + (colors !== void 0 ? " " + colors : "") + (isActionable.value === true ? " q-btn--actionable q-focusable q-hoverable" : props.disable === true ? " disabled" : "") + (props.fab === true ? " q-btn--fab" : props.fabMini === true ? " q-btn--fab-mini" : "") + (props.noCaps === true ? " q-btn--no-uppercase" : "") + (props.dense === true ? " q-btn--dense" : "") + (props.stretch === true ? " no-border-radius self-stretch" : "") + (props.glossy === true ? " glossy" : "");
  });
  const innerClasses = computed(() => alignClass.value + (props.stack === true ? " column" : " row") + (props.noWrap === true ? " no-wrap text-no-wrap" : "") + (props.loading === true ? " q-btn__content--hidden" : ""));
  return {
    classes,
    style,
    innerClasses,
    attributes,
    hasRouterLink,
    hasLink,
    linkTag,
    navigateToRouterLink,
    isActionable
  };
}
const { passiveCapture } = listenOpts;
let touchTarget = null, keyboardTarget = null, mouseTarget = null;
var QBtn = createComponent({
  name: "QBtn",
  props: __spreadProps(__spreadValues({}, useBtnProps), {
    percentage: Number,
    darkPercentage: Boolean
  }),
  emits: ["click", "keydown", "touchstart", "mousedown", "keyup"],
  setup(props, { slots, emit }) {
    const { proxy } = getCurrentInstance();
    const {
      classes,
      style,
      innerClasses,
      attributes,
      hasRouterLink,
      hasLink,
      linkTag,
      navigateToRouterLink,
      isActionable
    } = useBtn(props);
    const rootRef = ref(null);
    const blurTargetRef = ref(null);
    let localTouchTargetEl = null, avoidMouseRipple, mouseTimer;
    const hasLabel = computed(() => props.label !== void 0 && props.label !== null && props.label !== "");
    const ripple = computed(() => props.disable === true || props.ripple === false ? false : __spreadValues({
      keyCodes: hasLink.value === true ? [13, 32] : [13]
    }, props.ripple === true ? {} : props.ripple));
    const rippleProps = computed(() => ({ center: props.round }));
    const percentageStyle = computed(() => {
      const val = Math.max(0, Math.min(100, props.percentage));
      return val > 0 ? { transition: "transform 0.6s", transform: `translateX(${val - 100}%)` } : {};
    });
    const onEvents = computed(() => {
      if (props.loading === true) {
        return {
          onMousedown: onLoadingEvt,
          onTouchstartPassive: onLoadingEvt,
          onClick: onLoadingEvt,
          onKeydown: onLoadingEvt,
          onKeyup: onLoadingEvt
        };
      }
      if (isActionable.value === true) {
        return {
          onClick,
          onKeydown,
          onMousedown,
          onTouchstart
        };
      }
      return {
        onClick: stopAndPrevent
      };
    });
    const nodeProps = computed(() => __spreadValues(__spreadValues({
      ref: rootRef,
      class: "q-btn q-btn-item non-selectable no-outline " + classes.value,
      style: style.value
    }, attributes.value), onEvents.value));
    function onClick(e) {
      if (rootRef.value === null) {
        return;
      }
      if (e !== void 0) {
        if (e.defaultPrevented === true) {
          return;
        }
        const el = document.activeElement;
        if (props.type === "submit" && el !== document.body && rootRef.value.contains(el) === false && el.contains(rootRef.value) === false) {
          rootRef.value.focus();
          const onClickCleanup = () => {
            document.removeEventListener("keydown", stopAndPrevent, true);
            document.removeEventListener("keyup", onClickCleanup, passiveCapture);
            rootRef.value !== null && rootRef.value.removeEventListener("blur", onClickCleanup, passiveCapture);
          };
          document.addEventListener("keydown", stopAndPrevent, true);
          document.addEventListener("keyup", onClickCleanup, passiveCapture);
          rootRef.value.addEventListener("blur", onClickCleanup, passiveCapture);
        }
      }
      if (hasRouterLink.value === true) {
        const go = () => {
          e.__qNavigate = true;
          navigateToRouterLink(e);
        };
        emit("click", e, go);
        e.defaultPrevented !== true && go();
      } else {
        emit("click", e);
      }
    }
    function onKeydown(e) {
      if (rootRef.value === null) {
        return;
      }
      emit("keydown", e);
      if (isKeyCode(e, [13, 32]) === true && keyboardTarget !== rootRef.value) {
        keyboardTarget !== null && cleanup();
        if (e.defaultPrevented !== true) {
          rootRef.value.focus();
          keyboardTarget = rootRef.value;
          rootRef.value.classList.add("q-btn--active");
          document.addEventListener("keyup", onPressEnd, true);
          rootRef.value.addEventListener("blur", onPressEnd, passiveCapture);
        }
        stopAndPrevent(e);
      }
    }
    function onTouchstart(e) {
      if (rootRef.value === null) {
        return;
      }
      emit("touchstart", e);
      if (e.defaultPrevented === true) {
        return;
      }
      if (touchTarget !== rootRef.value) {
        touchTarget !== null && cleanup();
        touchTarget = rootRef.value;
        localTouchTargetEl = e.target;
        localTouchTargetEl.addEventListener("touchcancel", onPressEnd, passiveCapture);
        localTouchTargetEl.addEventListener("touchend", onPressEnd, passiveCapture);
      }
      avoidMouseRipple = true;
      clearTimeout(mouseTimer);
      mouseTimer = setTimeout(() => {
        avoidMouseRipple = false;
      }, 200);
    }
    function onMousedown(e) {
      if (rootRef.value === null) {
        return;
      }
      e.qSkipRipple = avoidMouseRipple === true;
      emit("mousedown", e);
      if (e.defaultPrevented !== true && mouseTarget !== rootRef.value) {
        mouseTarget !== null && cleanup();
        mouseTarget = rootRef.value;
        rootRef.value.classList.add("q-btn--active");
        document.addEventListener("mouseup", onPressEnd, passiveCapture);
      }
    }
    function onPressEnd(e) {
      if (rootRef.value === null) {
        return;
      }
      if (e !== void 0 && e.type === "blur" && document.activeElement === rootRef.value) {
        return;
      }
      if (e !== void 0 && e.type === "keyup") {
        if (keyboardTarget === rootRef.value && isKeyCode(e, [13, 32]) === true) {
          const evt = new MouseEvent("click", e);
          evt.qKeyEvent = true;
          e.defaultPrevented === true && prevent(evt);
          e.cancelBubble === true && stop(evt);
          rootRef.value.dispatchEvent(evt);
          stopAndPrevent(e);
          e.qKeyEvent = true;
        }
        emit("keyup", e);
      }
      cleanup();
    }
    function cleanup(destroying) {
      const blurTarget = blurTargetRef.value;
      if (destroying !== true && (touchTarget === rootRef.value || mouseTarget === rootRef.value) && blurTarget !== null && blurTarget !== document.activeElement) {
        blurTarget.setAttribute("tabindex", -1);
        blurTarget.focus();
      }
      if (touchTarget === rootRef.value) {
        if (localTouchTargetEl !== null) {
          localTouchTargetEl.removeEventListener("touchcancel", onPressEnd, passiveCapture);
          localTouchTargetEl.removeEventListener("touchend", onPressEnd, passiveCapture);
        }
        touchTarget = localTouchTargetEl = null;
      }
      if (mouseTarget === rootRef.value) {
        document.removeEventListener("mouseup", onPressEnd, passiveCapture);
        mouseTarget = null;
      }
      if (keyboardTarget === rootRef.value) {
        document.removeEventListener("keyup", onPressEnd, true);
        rootRef.value !== null && rootRef.value.removeEventListener("blur", onPressEnd, passiveCapture);
        keyboardTarget = null;
      }
      rootRef.value !== null && rootRef.value.classList.remove("q-btn--active");
    }
    function onLoadingEvt(evt) {
      stopAndPrevent(evt);
      evt.qSkipRipple = true;
    }
    onBeforeUnmount(() => {
      cleanup(true);
    });
    Object.assign(proxy, { click: onClick });
    return () => {
      let inner = [];
      props.icon !== void 0 && inner.push(h(QIcon, {
        name: props.icon,
        left: props.stack === false && hasLabel.value === true,
        role: "img",
        "aria-hidden": "true"
      }));
      hasLabel.value === true && inner.push(h("span", { class: "block" }, [props.label]));
      inner = hMergeSlot(slots.default, inner);
      if (props.iconRight !== void 0 && props.round === false) {
        inner.push(h(QIcon, {
          name: props.iconRight,
          right: props.stack === false && hasLabel.value === true,
          role: "img",
          "aria-hidden": "true"
        }));
      }
      const child = [
        h("span", {
          class: "q-focus-helper",
          ref: blurTargetRef
        })
      ];
      if (props.loading === true && props.percentage !== void 0) {
        child.push(h("span", {
          class: "q-btn__progress absolute-full overflow-hidden"
        }, [
          h("span", {
            class: "q-btn__progress-indicator fit block" + (props.darkPercentage === true ? " q-btn__progress--dark" : ""),
            style: percentageStyle.value
          })
        ]));
      }
      child.push(h("span", {
        class: "q-btn__content text-center col items-center q-anchor--skip " + innerClasses.value
      }, inner));
      props.loading !== null && child.push(h(Transition, {
        name: "q-transition--fade"
      }, () => props.loading === true ? [
        h("span", {
          key: "loading",
          class: "absolute-full flex flex-center"
        }, slots.loading !== void 0 ? slots.loading() : [h(QSpinner)])
      ] : null));
      return withDirectives(h(linkTag.value, nodeProps.value, child), [[
        Ripple,
        ripple.value,
        void 0,
        rippleProps.value
      ]]);
    };
  }
});
export { QBtn as Q, useRouterLink as a, QSpinner as b, css as c, getElement as g, useRouterLinkProps as u, vmHasRouter as v };
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUUJ0bi5kMDM4ZjVjZi5qcyIsInNvdXJjZXMiOlsiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3F1YXNhci9zcmMvY29tcG9uZW50cy9zcGlubmVyL3VzZS1zcGlubmVyLmpzIiwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3F1YXNhci9zcmMvY29tcG9uZW50cy9zcGlubmVyL1FTcGlubmVyLmpzIiwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3F1YXNhci9zcmMvdXRpbHMvZG9tLmpzIiwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3F1YXNhci9zcmMvdXRpbHMvdGhyb3R0bGUuanMiLCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvcXVhc2FyL3NyYy9kaXJlY3RpdmVzL1JpcHBsZS5qcyIsIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9xdWFzYXIvc3JjL2NvbXBvc2FibGVzL3ByaXZhdGUvdXNlLWFsaWduLmpzIiwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3F1YXNhci9zcmMvdXRpbHMvcHJpdmF0ZS92bS5qcyIsIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9xdWFzYXIvc3JjL2NvbXBvc2FibGVzL3ByaXZhdGUvdXNlLXJvdXRlci1saW5rLmpzIiwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3F1YXNhci9zcmMvY29tcG9uZW50cy9idG4vdXNlLWJ0bi5qcyIsIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9xdWFzYXIvc3JjL2NvbXBvbmVudHMvYnRuL1FCdG4uanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgY29tcHV0ZWQgfSBmcm9tICd2dWUnXG5pbXBvcnQgeyB1c2VTaXplRGVmYXVsdHMgfSBmcm9tICcuLi8uLi9jb21wb3NhYmxlcy9wcml2YXRlL3VzZS1zaXplLmpzJ1xuXG5leHBvcnQgY29uc3QgdXNlU3Bpbm5lclByb3BzID0ge1xuICBzaXplOiB7XG4gICAgdHlwZTogWyBOdW1iZXIsIFN0cmluZyBdLFxuICAgIGRlZmF1bHQ6ICcxZW0nXG4gIH0sXG4gIGNvbG9yOiBTdHJpbmdcbn1cblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gdXNlU3Bpbm5lciAocHJvcHMpIHtcbiAgcmV0dXJuIHtcbiAgICBjU2l6ZTogY29tcHV0ZWQoKCkgPT4gKFxuICAgICAgcHJvcHMuc2l6ZSBpbiB1c2VTaXplRGVmYXVsdHNcbiAgICAgICAgPyBgJHsgdXNlU2l6ZURlZmF1bHRzWyBwcm9wcy5zaXplIF0gfXB4YFxuICAgICAgICA6IHByb3BzLnNpemVcbiAgICApKSxcblxuICAgIGNsYXNzZXM6IGNvbXB1dGVkKCgpID0+XG4gICAgICAncS1zcGlubmVyJyArIChwcm9wcy5jb2xvciA/IGAgdGV4dC0keyBwcm9wcy5jb2xvciB9YCA6ICcnKVxuICAgIClcbiAgfVxufVxuIiwiaW1wb3J0IHsgaCB9IGZyb20gJ3Z1ZSdcblxuaW1wb3J0IHVzZVNwaW5uZXIsIHsgdXNlU3Bpbm5lclByb3BzIH0gZnJvbSAnLi91c2Utc3Bpbm5lci5qcydcblxuaW1wb3J0IHsgY3JlYXRlQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vdXRpbHMvcHJpdmF0ZS9jcmVhdGUuanMnXG5cbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZUNvbXBvbmVudCh7XG4gIG5hbWU6ICdRU3Bpbm5lcicsXG5cbiAgcHJvcHM6IHtcbiAgICAuLi51c2VTcGlubmVyUHJvcHMsXG5cbiAgICB0aGlja25lc3M6IHtcbiAgICAgIHR5cGU6IE51bWJlcixcbiAgICAgIGRlZmF1bHQ6IDVcbiAgICB9XG4gIH0sXG5cbiAgc2V0dXAgKHByb3BzKSB7XG4gICAgY29uc3QgeyBjU2l6ZSwgY2xhc3NlcyB9ID0gdXNlU3Bpbm5lcihwcm9wcylcblxuICAgIHJldHVybiAoKSA9PiBoKCdzdmcnLCB7XG4gICAgICBjbGFzczogY2xhc3Nlcy52YWx1ZSArICcgcS1zcGlubmVyLW1hdCcsXG4gICAgICB3aWR0aDogY1NpemUudmFsdWUsXG4gICAgICBoZWlnaHQ6IGNTaXplLnZhbHVlLFxuICAgICAgdmlld0JveDogJzI1IDI1IDUwIDUwJ1xuICAgIH0sIFtcbiAgICAgIGgoJ2NpcmNsZScsIHtcbiAgICAgICAgY2xhc3M6ICdwYXRoJyxcbiAgICAgICAgY3g6ICc1MCcsXG4gICAgICAgIGN5OiAnNTAnLFxuICAgICAgICByOiAnMjAnLFxuICAgICAgICBmaWxsOiAnbm9uZScsXG4gICAgICAgIHN0cm9rZTogJ2N1cnJlbnRDb2xvcicsXG4gICAgICAgICdzdHJva2Utd2lkdGgnOiBwcm9wcy50aGlja25lc3MsXG4gICAgICAgICdzdHJva2UtbWl0ZXJsaW1pdCc6ICcxMCdcbiAgICAgIH0pXG4gICAgXSlcbiAgfVxufSlcbiIsImltcG9ydCB7IGlzUmVmIH0gZnJvbSAndnVlJ1xuXG5leHBvcnQgZnVuY3Rpb24gb2Zmc2V0IChlbCkge1xuICBpZiAoZWwgPT09IHdpbmRvdykge1xuICAgIHJldHVybiB7IHRvcDogMCwgbGVmdDogMCB9XG4gIH1cbiAgY29uc3QgeyB0b3AsIGxlZnQgfSA9IGVsLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpXG4gIHJldHVybiB7IHRvcCwgbGVmdCB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzdHlsZSAoZWwsIHByb3BlcnR5KSB7XG4gIHJldHVybiB3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShlbCkuZ2V0UHJvcGVydHlWYWx1ZShwcm9wZXJ0eSlcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGhlaWdodCAoZWwpIHtcbiAgcmV0dXJuIGVsID09PSB3aW5kb3dcbiAgICA/IHdpbmRvdy5pbm5lckhlaWdodFxuICAgIDogZWwuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkuaGVpZ2h0XG59XG5cbmV4cG9ydCBmdW5jdGlvbiB3aWR0aCAoZWwpIHtcbiAgcmV0dXJuIGVsID09PSB3aW5kb3dcbiAgICA/IHdpbmRvdy5pbm5lcldpZHRoXG4gICAgOiBlbC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS53aWR0aFxufVxuXG5leHBvcnQgZnVuY3Rpb24gY3NzIChlbGVtZW50LCBjc3MpIHtcbiAgY29uc3Qgc3R5bGUgPSBlbGVtZW50LnN0eWxlXG5cbiAgZm9yIChjb25zdCBwcm9wIGluIGNzcykge1xuICAgIHN0eWxlWyBwcm9wIF0gPSBjc3NbIHByb3AgXVxuICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBjc3NCYXRjaCAoZWxlbWVudHMsIHN0eWxlKSB7XG4gIGVsZW1lbnRzLmZvckVhY2goZWwgPT4gY3NzKGVsLCBzdHlsZSkpXG59XG5cbmV4cG9ydCBmdW5jdGlvbiByZWFkeSAoZm4pIHtcbiAgaWYgKHR5cGVvZiBmbiAhPT0gJ2Z1bmN0aW9uJykge1xuICAgIHJldHVyblxuICB9XG5cbiAgaWYgKGRvY3VtZW50LnJlYWR5U3RhdGUgIT09ICdsb2FkaW5nJykge1xuICAgIHJldHVybiBmbigpXG4gIH1cblxuICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgZm4sIGZhbHNlKVxufVxuXG4vLyBpbnRlcm5hbFxuZXhwb3J0IGZ1bmN0aW9uIGdldEVsZW1lbnQgKGVsKSB7XG4gIGlmIChlbCA9PT0gdm9pZCAwIHx8IGVsID09PSBudWxsKSB7XG4gICAgcmV0dXJuIHZvaWQgMFxuICB9XG5cbiAgaWYgKHR5cGVvZiBlbCA9PT0gJ3N0cmluZycpIHtcbiAgICB0cnkge1xuICAgICAgcmV0dXJuIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoZWwpIHx8IHZvaWQgMFxuICAgIH1cbiAgICBjYXRjaCAoZXJyKSB7XG4gICAgICByZXR1cm4gdm9pZCAwXG4gICAgfVxuICB9XG5cbiAgY29uc3QgdGFyZ2V0ID0gaXNSZWYoZWwpID09PSB0cnVlXG4gICAgPyBlbC52YWx1ZVxuICAgIDogZWxcblxuICBpZiAodGFyZ2V0KSB7XG4gICAgcmV0dXJuIHRhcmdldC4kZWwgfHwgdGFyZ2V0XG4gIH1cbn1cblxuLy8gaW50ZXJuYWxcbmV4cG9ydCBmdW5jdGlvbiBjaGlsZEhhc0ZvY3VzIChlbCwgZm9jdXNlZEVsKSB7XG4gIGlmIChlbCA9PT0gdm9pZCAwIHx8IGVsID09PSBudWxsIHx8IGVsLmNvbnRhaW5zKGZvY3VzZWRFbCkgPT09IHRydWUpIHtcbiAgICByZXR1cm4gdHJ1ZVxuICB9XG5cbiAgZm9yIChsZXQgbmV4dCA9IGVsLm5leHRFbGVtZW50U2libGluZzsgbmV4dCAhPT0gbnVsbDsgbmV4dCA9IG5leHQubmV4dEVsZW1lbnRTaWJsaW5nKSB7XG4gICAgaWYgKG5leHQuY29udGFpbnMoZm9jdXNlZEVsKSkge1xuICAgICAgcmV0dXJuIHRydWVcbiAgICB9XG4gIH1cblxuICByZXR1cm4gZmFsc2Vcbn1cblxuZXhwb3J0IGRlZmF1bHQge1xuICBvZmZzZXQsXG4gIHN0eWxlLFxuICBoZWlnaHQsXG4gIHdpZHRoLFxuICBjc3MsXG4gIGNzc0JhdGNoLFxuICByZWFkeVxufVxuIiwiZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gKGZuLCBsaW1pdCA9IDI1MCkge1xuICBsZXQgd2FpdCA9IGZhbHNlLCByZXN1bHRcblxuICByZXR1cm4gZnVuY3Rpb24gKC8qIC4uLmFyZ3MgKi8pIHtcbiAgICBpZiAod2FpdCA9PT0gZmFsc2UpIHtcbiAgICAgIHdhaXQgPSB0cnVlXG4gICAgICBzZXRUaW1lb3V0KCgpID0+IHsgd2FpdCA9IGZhbHNlIH0sIGxpbWl0KVxuICAgICAgcmVzdWx0ID0gZm4uYXBwbHkodGhpcywgYXJndW1lbnRzKVxuICAgIH1cblxuICAgIHJldHVybiByZXN1bHRcbiAgfVxufVxuIiwiaW1wb3J0IHsgY3JlYXRlRGlyZWN0aXZlIH0gZnJvbSAnLi4vdXRpbHMvcHJpdmF0ZS9jcmVhdGUuanMnXG5pbXBvcnQgeyBjc3MgfSBmcm9tICcuLi91dGlscy9kb20uanMnXG5pbXBvcnQgeyBwb3NpdGlvbiwgc3RvcCwgYWRkRXZ0LCBjbGVhbkV2dCB9IGZyb20gJy4uL3V0aWxzL2V2ZW50LmpzJ1xuaW1wb3J0IHsgaXNLZXlDb2RlIH0gZnJvbSAnLi4vdXRpbHMvcHJpdmF0ZS9rZXktY29tcG9zaXRpb24uanMnXG5pbXBvcnQgdGhyb3R0bGUgZnJvbSAnLi4vdXRpbHMvdGhyb3R0bGUuanMnXG5pbXBvcnQgZ2V0U1NSUHJvcHMgZnJvbSAnLi4vdXRpbHMvcHJpdmF0ZS9ub29wLXNzci1kaXJlY3RpdmUtdHJhbnNmb3JtLmpzJ1xuXG5mdW5jdGlvbiBzaG93UmlwcGxlIChldnQsIGVsLCBjdHgsIGZvcmNlQ2VudGVyKSB7XG4gIGN0eC5tb2RpZmllcnMuc3RvcCA9PT0gdHJ1ZSAmJiBzdG9wKGV2dClcblxuICBjb25zdCBjb2xvciA9IGN0eC5tb2RpZmllcnMuY29sb3JcbiAgbGV0IGNlbnRlciA9IGN0eC5tb2RpZmllcnMuY2VudGVyXG4gIGNlbnRlciA9IGNlbnRlciA9PT0gdHJ1ZSB8fCBmb3JjZUNlbnRlciA9PT0gdHJ1ZVxuXG4gIGNvbnN0XG4gICAgbm9kZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NwYW4nKSxcbiAgICBpbm5lck5vZGUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzcGFuJyksXG4gICAgcG9zID0gcG9zaXRpb24oZXZ0KSxcbiAgICB7IGxlZnQsIHRvcCwgd2lkdGgsIGhlaWdodCB9ID0gZWwuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCksXG4gICAgZGlhbWV0ZXIgPSBNYXRoLnNxcnQod2lkdGggKiB3aWR0aCArIGhlaWdodCAqIGhlaWdodCksXG4gICAgcmFkaXVzID0gZGlhbWV0ZXIgLyAyLFxuICAgIGNlbnRlclggPSBgJHsgKHdpZHRoIC0gZGlhbWV0ZXIpIC8gMiB9cHhgLFxuICAgIHggPSBjZW50ZXIgPyBjZW50ZXJYIDogYCR7IHBvcy5sZWZ0IC0gbGVmdCAtIHJhZGl1cyB9cHhgLFxuICAgIGNlbnRlclkgPSBgJHsgKGhlaWdodCAtIGRpYW1ldGVyKSAvIDIgfXB4YCxcbiAgICB5ID0gY2VudGVyID8gY2VudGVyWSA6IGAkeyBwb3MudG9wIC0gdG9wIC0gcmFkaXVzIH1weGBcblxuICBpbm5lck5vZGUuY2xhc3NOYW1lID0gJ3EtcmlwcGxlX19pbm5lcidcbiAgY3NzKGlubmVyTm9kZSwge1xuICAgIGhlaWdodDogYCR7IGRpYW1ldGVyIH1weGAsXG4gICAgd2lkdGg6IGAkeyBkaWFtZXRlciB9cHhgLFxuICAgIHRyYW5zZm9ybTogYHRyYW5zbGF0ZTNkKCR7IHggfSwkeyB5IH0sMCkgc2NhbGUzZCguMiwuMiwxKWAsXG4gICAgb3BhY2l0eTogMFxuICB9KVxuXG4gIG5vZGUuY2xhc3NOYW1lID0gYHEtcmlwcGxlJHsgY29sb3IgPyAnIHRleHQtJyArIGNvbG9yIDogJycgfWBcbiAgbm9kZS5zZXRBdHRyaWJ1dGUoJ2RpcicsICdsdHInKVxuICBub2RlLmFwcGVuZENoaWxkKGlubmVyTm9kZSlcbiAgZWwuYXBwZW5kQ2hpbGQobm9kZSlcblxuICBjb25zdCBhYm9ydCA9ICgpID0+IHtcbiAgICBub2RlLnJlbW92ZSgpXG4gICAgY2xlYXJUaW1lb3V0KHRpbWVyKVxuICB9XG4gIGN0eC5hYm9ydC5wdXNoKGFib3J0KVxuXG4gIGxldCB0aW1lciA9IHNldFRpbWVvdXQoKCkgPT4ge1xuICAgIGlubmVyTm9kZS5jbGFzc0xpc3QuYWRkKCdxLXJpcHBsZV9faW5uZXItLWVudGVyJylcbiAgICBpbm5lck5vZGUuc3R5bGUudHJhbnNmb3JtID0gYHRyYW5zbGF0ZTNkKCR7IGNlbnRlclggfSwkeyBjZW50ZXJZIH0sMCkgc2NhbGUzZCgxLDEsMSlgXG4gICAgaW5uZXJOb2RlLnN0eWxlLm9wYWNpdHkgPSAwLjJcblxuICAgIHRpbWVyID0gc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICBpbm5lck5vZGUuY2xhc3NMaXN0LnJlbW92ZSgncS1yaXBwbGVfX2lubmVyLS1lbnRlcicpXG4gICAgICBpbm5lck5vZGUuY2xhc3NMaXN0LmFkZCgncS1yaXBwbGVfX2lubmVyLS1sZWF2ZScpXG4gICAgICBpbm5lck5vZGUuc3R5bGUub3BhY2l0eSA9IDBcblxuICAgICAgdGltZXIgPSBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgbm9kZS5yZW1vdmUoKVxuICAgICAgICBjdHguYWJvcnQuc3BsaWNlKGN0eC5hYm9ydC5pbmRleE9mKGFib3J0KSwgMSlcbiAgICAgIH0sIDI3NSlcbiAgICB9LCAyNTApXG4gIH0sIDUwKVxufVxuXG5mdW5jdGlvbiB1cGRhdGVNb2RpZmllcnMgKGN0eCwgeyBtb2RpZmllcnMsIHZhbHVlLCBhcmcsIGluc3RhbmNlIH0pIHtcbiAgY29uc3QgY2ZnID0gT2JqZWN0LmFzc2lnbih7fSwgaW5zdGFuY2UuJHEuY29uZmlnLnJpcHBsZSwgbW9kaWZpZXJzLCB2YWx1ZSlcbiAgY3R4Lm1vZGlmaWVycyA9IHtcbiAgICBlYXJseTogY2ZnLmVhcmx5ID09PSB0cnVlLFxuICAgIHN0b3A6IGNmZy5zdG9wID09PSB0cnVlLFxuICAgIGNlbnRlcjogY2ZnLmNlbnRlciA9PT0gdHJ1ZSxcbiAgICBjb2xvcjogY2ZnLmNvbG9yIHx8IGFyZyxcbiAgICBrZXlDb2RlczogW10uY29uY2F0KGNmZy5rZXlDb2RlcyB8fCAxMylcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVEaXJlY3RpdmUoX19RVUFTQVJfU1NSX1NFUlZFUl9fXG4gID8geyBuYW1lOiAncmlwcGxlJywgZ2V0U1NSUHJvcHMgfVxuICA6IHtcbiAgICAgIG5hbWU6ICdyaXBwbGUnLFxuXG4gICAgICBiZWZvcmVNb3VudCAoZWwsIGJpbmRpbmcpIHtcbiAgICAgICAgY29uc3QgY3R4ID0ge1xuICAgICAgICAgIGVuYWJsZWQ6IGJpbmRpbmcudmFsdWUgIT09IGZhbHNlLFxuICAgICAgICAgIG1vZGlmaWVyczoge30sXG4gICAgICAgICAgYWJvcnQ6IFtdLFxuXG4gICAgICAgICAgc3RhcnQgKGV2dCkge1xuICAgICAgICAgICAgaWYgKFxuICAgICAgICAgICAgICBjdHguZW5hYmxlZCA9PT0gdHJ1ZVxuICAgICAgICAgICAgICAmJiBldnQucVNraXBSaXBwbGUgIT09IHRydWVcbiAgICAgICAgICAgICAgJiYgKFxuICAgICAgICAgICAgICAgIGN0eC5tb2RpZmllcnMuZWFybHkgPT09IHRydWVcbiAgICAgICAgICAgICAgICAgID8gWyAnbW91c2Vkb3duJywgJ3RvdWNoc3RhcnQnIF0uaW5jbHVkZXMoZXZ0LnR5cGUpID09PSB0cnVlXG4gICAgICAgICAgICAgICAgICA6IGV2dC50eXBlID09PSAnY2xpY2snXG4gICAgICAgICAgICAgIClcbiAgICAgICAgICAgICkge1xuICAgICAgICAgICAgICBzaG93UmlwcGxlKGV2dCwgZWwsIGN0eCwgZXZ0LnFLZXlFdmVudCA9PT0gdHJ1ZSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuXG4gICAgICAgICAga2V5c3RhcnQ6IHRocm90dGxlKGV2dCA9PiB7XG4gICAgICAgICAgICBpZiAoXG4gICAgICAgICAgICAgIGN0eC5lbmFibGVkID09PSB0cnVlXG4gICAgICAgICAgICAgICYmIGV2dC5xU2tpcFJpcHBsZSAhPT0gdHJ1ZVxuICAgICAgICAgICAgICAmJiBpc0tleUNvZGUoZXZ0LCBjdHgubW9kaWZpZXJzLmtleUNvZGVzKSA9PT0gdHJ1ZVxuICAgICAgICAgICAgICAmJiBldnQudHlwZSA9PT0gYGtleSR7IGN0eC5tb2RpZmllcnMuZWFybHkgPT09IHRydWUgPyAnZG93bicgOiAndXAnIH1gXG4gICAgICAgICAgICApIHtcbiAgICAgICAgICAgICAgc2hvd1JpcHBsZShldnQsIGVsLCBjdHgsIHRydWUpXG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSwgMzAwKVxuICAgICAgICB9XG5cbiAgICAgICAgdXBkYXRlTW9kaWZpZXJzKGN0eCwgYmluZGluZylcblxuICAgICAgICBlbC5fX3FyaXBwbGUgPSBjdHhcblxuICAgICAgICBhZGRFdnQoY3R4LCAnbWFpbicsIFtcbiAgICAgICAgICBbIGVsLCAnbW91c2Vkb3duJywgJ3N0YXJ0JywgJ3Bhc3NpdmUnIF0sXG4gICAgICAgICAgWyBlbCwgJ3RvdWNoc3RhcnQnLCAnc3RhcnQnLCAncGFzc2l2ZScgXSxcbiAgICAgICAgICBbIGVsLCAnY2xpY2snLCAnc3RhcnQnLCAncGFzc2l2ZScgXSxcbiAgICAgICAgICBbIGVsLCAna2V5ZG93bicsICdrZXlzdGFydCcsICdwYXNzaXZlJyBdLFxuICAgICAgICAgIFsgZWwsICdrZXl1cCcsICdrZXlzdGFydCcsICdwYXNzaXZlJyBdXG4gICAgICAgIF0pXG4gICAgICB9LFxuXG4gICAgICB1cGRhdGVkIChlbCwgYmluZGluZykge1xuICAgICAgICBpZiAoYmluZGluZy5vbGRWYWx1ZSAhPT0gYmluZGluZy52YWx1ZSkge1xuICAgICAgICAgIGNvbnN0IGN0eCA9IGVsLl9fcXJpcHBsZVxuICAgICAgICAgIGN0eC5lbmFibGVkID0gYmluZGluZy52YWx1ZSAhPT0gZmFsc2VcblxuICAgICAgICAgIGlmIChjdHguZW5hYmxlZCA9PT0gdHJ1ZSAmJiBPYmplY3QoYmluZGluZy52YWx1ZSkgPT09IGJpbmRpbmcudmFsdWUpIHtcbiAgICAgICAgICAgIHVwZGF0ZU1vZGlmaWVycyhjdHgsIGJpbmRpbmcpXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9LFxuXG4gICAgICBiZWZvcmVVbm1vdW50IChlbCkge1xuICAgICAgICBjb25zdCBjdHggPSBlbC5fX3FyaXBwbGVcbiAgICAgICAgY3R4LmFib3J0LmZvckVhY2goZm4gPT4geyBmbigpIH0pXG4gICAgICAgIGNsZWFuRXZ0KGN0eCwgJ21haW4nKVxuICAgICAgICBkZWxldGUgZWwuX3FyaXBwbGVcbiAgICAgIH1cbiAgICB9XG4pXG4iLCJpbXBvcnQgeyBjb21wdXRlZCB9IGZyb20gJ3Z1ZSdcblxuZXhwb3J0IGNvbnN0IGFsaWduTWFwID0ge1xuICBsZWZ0OiAnc3RhcnQnLFxuICBjZW50ZXI6ICdjZW50ZXInLFxuICByaWdodDogJ2VuZCcsXG4gIGJldHdlZW46ICdiZXR3ZWVuJyxcbiAgYXJvdW5kOiAnYXJvdW5kJyxcbiAgZXZlbmx5OiAnZXZlbmx5JyxcbiAgc3RyZXRjaDogJ3N0cmV0Y2gnXG59XG5cbmV4cG9ydCBjb25zdCBhbGlnblZhbHVlcyA9IE9iamVjdC5rZXlzKGFsaWduTWFwKVxuXG5leHBvcnQgY29uc3QgdXNlQWxpZ25Qcm9wcyA9IHtcbiAgYWxpZ246IHtcbiAgICB0eXBlOiBTdHJpbmcsXG4gICAgdmFsaWRhdG9yOiB2ID0+IGFsaWduVmFsdWVzLmluY2x1ZGVzKHYpXG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gKHByb3BzKSB7XG4gIC8vIHJldHVybiBhbGlnbkNsYXNzXG4gIHJldHVybiBjb21wdXRlZCgoKSA9PiB7XG4gICAgY29uc3QgYWxpZ24gPSBwcm9wcy5hbGlnbiA9PT0gdm9pZCAwXG4gICAgICA/IHByb3BzLnZlcnRpY2FsID09PSB0cnVlID8gJ3N0cmV0Y2gnIDogJ2xlZnQnXG4gICAgICA6IHByb3BzLmFsaWduXG5cbiAgICByZXR1cm4gYCR7IHByb3BzLnZlcnRpY2FsID09PSB0cnVlID8gJ2l0ZW1zJyA6ICdqdXN0aWZ5JyB9LSR7IGFsaWduTWFwWyBhbGlnbiBdIH1gXG4gIH0pXG59XG4iLCIvLyB1c2VkIGRpcmVjdGx5IGJ5IGRvY3MgdG9vXG5leHBvcnQgZnVuY3Rpb24gZ2V0UGFyZW50Vm0gKHZtKSB7XG4gIGlmIChPYmplY3Qodm0uJHBhcmVudCkgPT09IHZtLiRwYXJlbnQpIHtcbiAgICByZXR1cm4gdm0uJHBhcmVudFxuICB9XG5cbiAgdm0gPSB2bS4kLnBhcmVudFxuXG4gIHdoaWxlIChPYmplY3Qodm0pID09PSB2bSkge1xuICAgIGlmIChPYmplY3Qodm0ucHJveHkpID09PSB2bS5wcm94eSkge1xuICAgICAgcmV0dXJuIHZtLnByb3h5XG4gICAgfVxuXG4gICAgdm0gPSB2bS5wYXJlbnRcbiAgfVxufVxuXG5mdW5jdGlvbiBmaWxsTm9ybWFsaXplZFZOb2RlcyAoY2hpbGRyZW4sIHZub2RlKSB7XG4gIGlmICh0eXBlb2Ygdm5vZGUudHlwZSA9PT0gJ3N5bWJvbCcpIHtcbiAgICBpZiAoQXJyYXkuaXNBcnJheSh2bm9kZS5jaGlsZHJlbikgPT09IHRydWUpIHtcbiAgICAgIHZub2RlLmNoaWxkcmVuLmZvckVhY2goY2hpbGQgPT4ge1xuICAgICAgICBmaWxsTm9ybWFsaXplZFZOb2RlcyhjaGlsZHJlbiwgY2hpbGQpXG4gICAgICB9KVxuICAgIH1cbiAgfVxuICBlbHNlIHtcbiAgICBjaGlsZHJlbi5hZGQodm5vZGUpXG4gIH1cbn1cblxuLy8gdm5vZGVzIGZyb20gcmVuZGVyZWQgaW4gYWR2YW5jZWQgc2xvdHNcbmV4cG9ydCBmdW5jdGlvbiBnZXROb3JtYWxpemVkVk5vZGVzICh2bm9kZXMpIHtcbiAgY29uc3QgY2hpbGRyZW4gPSBuZXcgU2V0KClcblxuICB2bm9kZXMuZm9yRWFjaCh2bm9kZSA9PiB7XG4gICAgZmlsbE5vcm1hbGl6ZWRWTm9kZXMoY2hpbGRyZW4sIHZub2RlKVxuICB9KVxuXG4gIHJldHVybiBBcnJheS5mcm9tKGNoaWxkcmVuKVxufVxuXG5leHBvcnQgZnVuY3Rpb24gdm1IYXNSb3V0ZXIgKHZtKSB7XG4gIHJldHVybiB2bS5hcHBDb250ZXh0LmNvbmZpZy5nbG9iYWxQcm9wZXJ0aWVzLiRyb3V0ZXIgIT09IHZvaWQgMFxufVxuIiwiLypcbiAqIEluc3BpcmVkIGJ5IFJvdXRlckxpbmsgZnJvbSBWdWUgUm91dGVyXG4gKiAgLS0+IEFQSSBzaG91bGQgbWF0Y2ghXG4gKi9cblxuaW1wb3J0IHsgY29tcHV0ZWQsIGdldEN1cnJlbnRJbnN0YW5jZSB9IGZyb20gJ3Z1ZSdcblxuaW1wb3J0IHsgcHJldmVudCB9IGZyb20gJy4uLy4uL3V0aWxzL2V2ZW50LmpzJ1xuaW1wb3J0IHsgdm1IYXNSb3V0ZXIgfSBmcm9tICcuLi8uLi91dGlscy9wcml2YXRlL3ZtLmpzJ1xuXG4vLyBHZXQgdGhlIG9yaWdpbmFsIHBhdGggdmFsdWUgb2YgYSByZWNvcmQgYnkgZm9sbG93aW5nIGl0cyBhbGlhc09mXG5mdW5jdGlvbiBnZXRPcmlnaW5hbFBhdGggKHJlY29yZCkge1xuICByZXR1cm4gcmVjb3JkXG4gICAgPyAoXG4gICAgICAgIHJlY29yZC5hbGlhc09mXG4gICAgICAgICAgPyByZWNvcmQuYWxpYXNPZi5wYXRoXG4gICAgICAgICAgOiByZWNvcmQucGF0aFxuICAgICAgKSA6ICcnXG59XG5cbmZ1bmN0aW9uIGlzU2FtZVJvdXRlUmVjb3JkIChhLCBiKSB7XG4gIC8vIHNpbmNlIHRoZSBvcmlnaW5hbCByZWNvcmQgaGFzIGFuIHVuZGVmaW5lZCB2YWx1ZSBmb3IgYWxpYXNPZlxuICAvLyBidXQgYWxsIGFsaWFzZXMgcG9pbnQgdG8gdGhlIG9yaWdpbmFsIHJlY29yZCwgdGhpcyB3aWxsIGFsd2F5cyBjb21wYXJlXG4gIC8vIHRoZSBvcmlnaW5hbCByZWNvcmRcbiAgcmV0dXJuIChhLmFsaWFzT2YgfHwgYSkgPT09IChiLmFsaWFzT2YgfHwgYilcbn1cblxuZnVuY3Rpb24gaW5jbHVkZXNQYXJhbXMgKG91dGVyLCBpbm5lcikge1xuICBmb3IgKGNvbnN0IGtleSBpbiBpbm5lcikge1xuICAgIGNvbnN0XG4gICAgICBpbm5lclZhbHVlID0gaW5uZXJbIGtleSBdLFxuICAgICAgb3V0ZXJWYWx1ZSA9IG91dGVyWyBrZXkgXVxuXG4gICAgaWYgKHR5cGVvZiBpbm5lclZhbHVlID09PSAnc3RyaW5nJykge1xuICAgICAgaWYgKGlubmVyVmFsdWUgIT09IG91dGVyVmFsdWUpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlXG4gICAgICB9XG4gICAgfVxuICAgIGVsc2UgaWYgKFxuICAgICAgQXJyYXkuaXNBcnJheShvdXRlclZhbHVlKSA9PT0gZmFsc2VcbiAgICAgIHx8IG91dGVyVmFsdWUubGVuZ3RoICE9PSBpbm5lclZhbHVlLmxlbmd0aFxuICAgICAgfHwgaW5uZXJWYWx1ZS5zb21lKCh2YWx1ZSwgaSkgPT4gdmFsdWUgIT09IG91dGVyVmFsdWVbIGkgXSlcbiAgICApIHtcbiAgICAgIHJldHVybiBmYWxzZVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiB0cnVlXG59XG5cbmZ1bmN0aW9uIGlzRXF1aXZhbGVudEFycmF5IChhLCBiKSB7XG4gIHJldHVybiBBcnJheS5pc0FycmF5KGIpID09PSB0cnVlXG4gICAgPyBhLmxlbmd0aCA9PT0gYi5sZW5ndGggJiYgYS5ldmVyeSgodmFsdWUsIGkpID0+IHZhbHVlID09PSBiWyBpIF0pXG4gICAgOiBhLmxlbmd0aCA9PT0gMSAmJiBhWyAwIF0gPT09IGJcbn1cblxuZnVuY3Rpb24gaXNTYW1lUm91dGVMb2NhdGlvblBhcmFtc1ZhbHVlIChhLCBiKSB7XG4gIHJldHVybiBBcnJheS5pc0FycmF5KGEpID09PSB0cnVlXG4gICAgPyBpc0VxdWl2YWxlbnRBcnJheShhLCBiKVxuICAgIDogKFxuICAgICAgICBBcnJheS5pc0FycmF5KGIpID09PSB0cnVlXG4gICAgICAgICAgPyBpc0VxdWl2YWxlbnRBcnJheShiLCBhKVxuICAgICAgICAgIDogYSA9PT0gYlxuICAgICAgKVxufVxuXG5mdW5jdGlvbiBpc1NhbWVSb3V0ZUxvY2F0aW9uUGFyYW1zIChhLCBiKSB7XG4gIGlmIChPYmplY3Qua2V5cyhhKS5sZW5ndGggIT09IE9iamVjdC5rZXlzKGIpLmxlbmd0aCkge1xuICAgIHJldHVybiBmYWxzZVxuICB9XG5cbiAgZm9yIChjb25zdCBrZXkgaW4gYSkge1xuICAgIGlmIChpc1NhbWVSb3V0ZUxvY2F0aW9uUGFyYW1zVmFsdWUoYVsga2V5IF0sIGJbIGtleSBdKSA9PT0gZmFsc2UpIHtcbiAgICAgIHJldHVybiBmYWxzZVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiB0cnVlXG59XG5cbmV4cG9ydCBjb25zdCB1c2VSb3V0ZXJMaW5rUHJvcHMgPSB7XG4gIC8vIHJvdXRlci1saW5rXG4gIHRvOiBbIFN0cmluZywgT2JqZWN0IF0sXG4gIHJlcGxhY2U6IEJvb2xlYW4sXG4gIGV4YWN0OiBCb29sZWFuLFxuICBhY3RpdmVDbGFzczoge1xuICAgIHR5cGU6IFN0cmluZyxcbiAgICBkZWZhdWx0OiAncS1yb3V0ZXItbGluay0tYWN0aXZlJ1xuICB9LFxuICBleGFjdEFjdGl2ZUNsYXNzOiB7XG4gICAgdHlwZTogU3RyaW5nLFxuICAgIGRlZmF1bHQ6ICdxLXJvdXRlci1saW5rLS1leGFjdC1hY3RpdmUnXG4gIH0sXG5cbiAgLy8gcmVndWxhciA8YT4gbGlua1xuICBocmVmOiBTdHJpbmcsXG4gIHRhcmdldDogU3RyaW5nLFxuXG4gIC8vIHN0YXRlXG4gIGRpc2FibGU6IEJvb2xlYW5cbn1cblxuLy8gZXh0ZXJuYWwgcHJvcHM6IHR5cGUsIHRhZ1xuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiAoZmFsbGJhY2tUYWcpIHtcbiAgY29uc3Qgdm0gPSBnZXRDdXJyZW50SW5zdGFuY2UoKVxuICBjb25zdCB7IHByb3BzLCBwcm94eSB9ID0gdm1cblxuICBjb25zdCBoYXNSb3V0ZXIgPSB2bUhhc1JvdXRlcih2bSlcbiAgY29uc3QgaGFzSHJlZkxpbmsgPSBjb21wdXRlZCgoKSA9PiBwcm9wcy5kaXNhYmxlICE9PSB0cnVlICYmIHByb3BzLmhyZWYgIT09IHZvaWQgMClcblxuICBjb25zdCBoYXNSb3V0ZXJMaW5rUHJvcHMgPSBjb21wdXRlZCgoKSA9PlxuICAgIGhhc1JvdXRlciA9PT0gdHJ1ZVxuICAgICYmIHByb3BzLmRpc2FibGUgIT09IHRydWVcbiAgICAmJiBoYXNIcmVmTGluay52YWx1ZSAhPT0gdHJ1ZVxuICAgICYmIHByb3BzLnRvICE9PSB2b2lkIDAgJiYgcHJvcHMudG8gIT09IG51bGwgJiYgcHJvcHMudG8gIT09ICcnXG4gIClcblxuICBjb25zdCBsaW5rUm91dGUgPSBjb21wdXRlZCgoKSA9PiB7XG4gICAgaWYgKGhhc1JvdXRlckxpbmtQcm9wcy52YWx1ZSA9PT0gdHJ1ZSkge1xuICAgICAgdHJ5IHsgcmV0dXJuIHByb3h5LiRyb3V0ZXIucmVzb2x2ZShwcm9wcy50bykgfVxuICAgICAgY2F0Y2ggKGVycikge31cbiAgICB9XG5cbiAgICByZXR1cm4gbnVsbFxuICB9KVxuXG4gIGNvbnN0IGhhc1JvdXRlckxpbmsgPSBjb21wdXRlZCgoKSA9PiBsaW5rUm91dGUudmFsdWUgIT09IG51bGwpXG4gIGNvbnN0IGhhc0xpbmsgPSBjb21wdXRlZCgoKSA9PiBoYXNIcmVmTGluay52YWx1ZSA9PT0gdHJ1ZSB8fCBoYXNSb3V0ZXJMaW5rLnZhbHVlID09PSB0cnVlKVxuXG4gIGNvbnN0IGxpbmtUYWcgPSBjb21wdXRlZCgoKSA9PiAoXG4gICAgcHJvcHMudHlwZSA9PT0gJ2EnIHx8IGhhc0xpbmsudmFsdWUgPT09IHRydWVcbiAgICAgID8gJ2EnXG4gICAgICA6IChwcm9wcy50YWcgfHwgZmFsbGJhY2tUYWcgfHwgJ2RpdicpXG4gICkpXG5cbiAgY29uc3QgbGlua1Byb3BzID0gY29tcHV0ZWQoKCkgPT4gKFxuICAgIGhhc0hyZWZMaW5rLnZhbHVlID09PSB0cnVlXG4gICAgICA/IHtcbiAgICAgICAgICBocmVmOiBwcm9wcy5ocmVmLFxuICAgICAgICAgIHRhcmdldDogcHJvcHMudGFyZ2V0XG4gICAgICAgIH1cbiAgICAgIDogKFxuICAgICAgICAgIGhhc1JvdXRlckxpbmsudmFsdWUgPT09IHRydWVcbiAgICAgICAgICAgID8ge1xuICAgICAgICAgICAgICAgIGhyZWY6IGxpbmtSb3V0ZS52YWx1ZS5ocmVmLFxuICAgICAgICAgICAgICAgIHRhcmdldDogcHJvcHMudGFyZ2V0XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIDoge31cbiAgICAgICAgKVxuICApKVxuXG4gIGNvbnN0IGxpbmtBY3RpdmVJbmRleCA9IGNvbXB1dGVkKCgpID0+IHtcbiAgICBpZiAoaGFzUm91dGVyTGluay52YWx1ZSA9PT0gZmFsc2UpIHtcbiAgICAgIHJldHVybiBudWxsXG4gICAgfVxuXG4gICAgY29uc3RcbiAgICAgIHsgbWF0Y2hlZCB9ID0gbGlua1JvdXRlLnZhbHVlLFxuICAgICAgeyBsZW5ndGggfSA9IG1hdGNoZWQsXG4gICAgICByb3V0ZU1hdGNoZWQgPSBtYXRjaGVkWyBsZW5ndGggLSAxIF1cblxuICAgIGlmIChyb3V0ZU1hdGNoZWQgPT09IHZvaWQgMCkge1xuICAgICAgcmV0dXJuIC0xXG4gICAgfVxuXG4gICAgY29uc3QgY3VycmVudE1hdGNoZWQgPSBwcm94eS4kcm91dGUubWF0Y2hlZFxuXG4gICAgaWYgKGN1cnJlbnRNYXRjaGVkLmxlbmd0aCA9PT0gMCkge1xuICAgICAgcmV0dXJuIC0xXG4gICAgfVxuXG4gICAgY29uc3QgaW5kZXggPSBjdXJyZW50TWF0Y2hlZC5maW5kSW5kZXgoXG4gICAgICBpc1NhbWVSb3V0ZVJlY29yZC5iaW5kKG51bGwsIHJvdXRlTWF0Y2hlZClcbiAgICApXG5cbiAgICBpZiAoaW5kZXggPiAtMSkge1xuICAgICAgcmV0dXJuIGluZGV4XG4gICAgfVxuXG4gICAgLy8gcG9zc2libGUgcGFyZW50IHJlY29yZFxuICAgIGNvbnN0IHBhcmVudFJlY29yZFBhdGggPSBnZXRPcmlnaW5hbFBhdGgobWF0Y2hlZFsgbGVuZ3RoIC0gMiBdKVxuXG4gICAgcmV0dXJuIChcbiAgICAgIC8vIHdlIGFyZSBkZWFsaW5nIHdpdGggbmVzdGVkIHJvdXRlc1xuICAgICAgbGVuZ3RoID4gMVxuICAgICAgLy8gaWYgdGhlIHBhcmVudCBhbmQgbWF0Y2hlZCByb3V0ZSBoYXZlIHRoZSBzYW1lIHBhdGgsIHRoaXMgbGluayBpc1xuICAgICAgLy8gcmVmZXJyaW5nIHRvIHRoZSBlbXB0eSBjaGlsZC4gT3Igd2UgY3VycmVudGx5IGFyZSBvbiBhIGRpZmZlcmVudFxuICAgICAgLy8gY2hpbGQgb2YgdGhlIHNhbWUgcGFyZW50XG4gICAgICAmJiBnZXRPcmlnaW5hbFBhdGgocm91dGVNYXRjaGVkKSA9PT0gcGFyZW50UmVjb3JkUGF0aFxuICAgICAgLy8gYXZvaWQgY29tcGFyaW5nIHRoZSBjaGlsZCB3aXRoIGl0cyBwYXJlbnRcbiAgICAgICYmIGN1cnJlbnRNYXRjaGVkWyBjdXJyZW50TWF0Y2hlZC5sZW5ndGggLSAxIF0ucGF0aCAhPT0gcGFyZW50UmVjb3JkUGF0aFxuICAgICAgICA/IGN1cnJlbnRNYXRjaGVkLmZpbmRJbmRleChcbiAgICAgICAgICBpc1NhbWVSb3V0ZVJlY29yZC5iaW5kKG51bGwsIG1hdGNoZWRbIGxlbmd0aCAtIDIgXSlcbiAgICAgICAgKVxuICAgICAgICA6IGluZGV4XG4gICAgKVxuICB9KVxuXG4gIGNvbnN0IGxpbmtJc0FjdGl2ZSA9IGNvbXB1dGVkKCgpID0+XG4gICAgaGFzUm91dGVyTGluay52YWx1ZSA9PT0gdHJ1ZVxuICAgICYmIGxpbmtBY3RpdmVJbmRleC52YWx1ZSA+IC0xXG4gICAgJiYgaW5jbHVkZXNQYXJhbXMocHJveHkuJHJvdXRlLnBhcmFtcywgbGlua1JvdXRlLnZhbHVlLnBhcmFtcylcbiAgKVxuXG4gIGNvbnN0IGxpbmtJc0V4YWN0QWN0aXZlID0gY29tcHV0ZWQoKCkgPT5cbiAgICBsaW5rSXNBY3RpdmUudmFsdWUgPT09IHRydWVcbiAgICAgICYmIGxpbmtBY3RpdmVJbmRleC52YWx1ZSA9PT0gcHJveHkuJHJvdXRlLm1hdGNoZWQubGVuZ3RoIC0gMVxuICAgICAgJiYgaXNTYW1lUm91dGVMb2NhdGlvblBhcmFtcyhwcm94eS4kcm91dGUucGFyYW1zLCBsaW5rUm91dGUudmFsdWUucGFyYW1zKVxuICApXG5cbiAgY29uc3QgbGlua0NsYXNzID0gY29tcHV0ZWQoKCkgPT4gKFxuICAgIGhhc1JvdXRlckxpbmsudmFsdWUgPT09IHRydWVcbiAgICAgID8gKFxuICAgICAgICAgIGxpbmtJc0V4YWN0QWN0aXZlLnZhbHVlID09PSB0cnVlXG4gICAgICAgICAgICA/IGAgJHsgcHJvcHMuZXhhY3RBY3RpdmVDbGFzcyB9ICR7IHByb3BzLmFjdGl2ZUNsYXNzIH1gXG4gICAgICAgICAgICA6IChcbiAgICAgICAgICAgICAgICBwcm9wcy5leGFjdCA9PT0gdHJ1ZVxuICAgICAgICAgICAgICAgICAgPyAnJ1xuICAgICAgICAgICAgICAgICAgOiAobGlua0lzQWN0aXZlLnZhbHVlID09PSB0cnVlID8gYCAkeyBwcm9wcy5hY3RpdmVDbGFzcyB9YCA6ICcnKVxuICAgICAgICAgICAgICApXG4gICAgICAgIClcbiAgICAgIDogJydcbiAgKSlcblxuICAvLyBzaG91bGQgbWF0Y2ggUm91dGVyTGluayBmcm9tIFZ1ZSBSb3V0ZXJcbiAgZnVuY3Rpb24gbmF2aWdhdGVUb1JvdXRlckxpbmsgKGUpIHtcbiAgICBpZiAoXG4gICAgICAvLyBjb21wb25lbnQgaXMgbm90IGRpc2FibGVkXG4gICAgICBwcm9wcy5kaXNhYmxlID09PSB0cnVlXG5cbiAgICAgIC8vIGRvbid0IHJlZGlyZWN0IHdpdGggY29udHJvbCBrZXlzXG4gICAgICB8fCBlLm1ldGFLZXkgfHwgZS5hbHRLZXkgfHwgZS5jdHJsS2V5IHx8IGUuc2hpZnRLZXlcblxuICAgICAgLy8gZG9uJ3QgcmVkaXJlY3Qgd2hlbiBwcmV2ZW50RGVmYXVsdCBjYWxsZWRcbiAgICAgIC8vIC4uLnVubGVzcyBjYWxsaW5nIGdvKCkgZnJvbSBAY2xpY2soZSwgZ28pXG4gICAgICB8fCAoZS5fX3FOYXZpZ2F0ZSAhPT0gdHJ1ZSAmJiBlLmRlZmF1bHRQcmV2ZW50ZWQgPT09IHRydWUpXG5cbiAgICAgIC8vIGRvbid0IHJlZGlyZWN0IG9uIHJpZ2h0IGNsaWNrXG4gICAgICB8fCAoZS5idXR0b24gIT09IHVuZGVmaW5lZCAmJiBlLmJ1dHRvbiAhPT0gMClcblxuICAgICAgLy8gZG9uJ3QgcmVkaXJlY3QgaWYgaXQgc2hvdWxkIG9wZW4gaW4gYSBuZXcgd2luZG93XG4gICAgICB8fCBwcm9wcy50YXJnZXQgPT09ICdfYmxhbmsnXG4gICAgKSB7XG4gICAgICByZXR1cm4gZmFsc2VcbiAgICB9XG5cbiAgICBwcmV2ZW50KGUpXG5cbiAgICByZXR1cm4gcHJveHkuJHJvdXRlclsgcHJvcHMucmVwbGFjZSA9PT0gdHJ1ZSA/ICdyZXBsYWNlJyA6ICdwdXNoJyBdKHByb3BzLnRvKVxuICAgICAgLmNhdGNoKGVyciA9PiBlcnIpXG4gIH1cblxuICByZXR1cm4ge1xuICAgIGhhc1JvdXRlckxpbmssXG4gICAgaGFzSHJlZkxpbmssXG4gICAgaGFzTGluayxcblxuICAgIGxpbmtUYWcsXG4gICAgbGlua1JvdXRlLFxuICAgIGxpbmtJc0FjdGl2ZSxcbiAgICBsaW5rSXNFeGFjdEFjdGl2ZSxcbiAgICBsaW5rQ2xhc3MsXG4gICAgbGlua1Byb3BzLFxuXG4gICAgbmF2aWdhdGVUb1JvdXRlckxpbmtcbiAgfVxufVxuIiwiaW1wb3J0IHsgY29tcHV0ZWQgfSBmcm9tICd2dWUnXG5cbmltcG9ydCB1c2VBbGlnbiwgeyB1c2VBbGlnblByb3BzIH0gZnJvbSAnLi4vLi4vY29tcG9zYWJsZXMvcHJpdmF0ZS91c2UtYWxpZ24uanMnXG5pbXBvcnQgdXNlU2l6ZSwgeyB1c2VTaXplUHJvcHMgfSBmcm9tICcuLi8uLi9jb21wb3NhYmxlcy9wcml2YXRlL3VzZS1zaXplLmpzJ1xuaW1wb3J0IHVzZVJvdXRlckxpbmssIHsgdXNlUm91dGVyTGlua1Byb3BzIH0gZnJvbSAnLi4vLi4vY29tcG9zYWJsZXMvcHJpdmF0ZS91c2Utcm91dGVyLWxpbmsuanMnXG5cbmNvbnN0IHBhZGRpbmcgPSB7XG4gIG5vbmU6IDAsXG4gIHhzOiA0LFxuICBzbTogOCxcbiAgbWQ6IDE2LFxuICBsZzogMjQsXG4gIHhsOiAzMlxufVxuXG5jb25zdCBkZWZhdWx0U2l6ZXMgPSB7XG4gIHhzOiA4LFxuICBzbTogMTAsXG4gIG1kOiAxNCxcbiAgbGc6IDIwLFxuICB4bDogMjRcbn1cblxuY29uc3QgZm9ybVR5cGVzID0gWyAnYnV0dG9uJywgJ3N1Ym1pdCcsICdyZXNldCcgXVxuY29uc3QgbWVkaWFUeXBlUkUgPSAvW15cXHNdXFwvW15cXHNdL1xuXG5leHBvcnQgY29uc3QgdXNlQnRuUHJvcHMgPSB7XG4gIC4uLnVzZVNpemVQcm9wcyxcbiAgLi4udXNlUm91dGVyTGlua1Byb3BzLFxuXG4gIHR5cGU6IHtcbiAgICB0eXBlOiBTdHJpbmcsXG4gICAgZGVmYXVsdDogJ2J1dHRvbidcbiAgfSxcblxuICBsYWJlbDogWyBOdW1iZXIsIFN0cmluZyBdLFxuICBpY29uOiBTdHJpbmcsXG4gIGljb25SaWdodDogU3RyaW5nLFxuXG4gIHJvdW5kOiBCb29sZWFuLFxuICBvdXRsaW5lOiBCb29sZWFuLFxuICBmbGF0OiBCb29sZWFuLFxuICB1bmVsZXZhdGVkOiBCb29sZWFuLFxuICByb3VuZGVkOiBCb29sZWFuLFxuICBwdXNoOiBCb29sZWFuLFxuICBnbG9zc3k6IEJvb2xlYW4sXG5cbiAgc2l6ZTogU3RyaW5nLFxuICBmYWI6IEJvb2xlYW4sXG4gIGZhYk1pbmk6IEJvb2xlYW4sXG4gIHBhZGRpbmc6IFN0cmluZyxcblxuICBjb2xvcjogU3RyaW5nLFxuICB0ZXh0Q29sb3I6IFN0cmluZyxcbiAgbm9DYXBzOiBCb29sZWFuLFxuICBub1dyYXA6IEJvb2xlYW4sXG4gIGRlbnNlOiBCb29sZWFuLFxuXG4gIHRhYmluZGV4OiBbIE51bWJlciwgU3RyaW5nIF0sXG5cbiAgcmlwcGxlOiB7XG4gICAgdHlwZTogWyBCb29sZWFuLCBPYmplY3QgXSxcbiAgICBkZWZhdWx0OiB0cnVlXG4gIH0sXG5cbiAgYWxpZ246IHtcbiAgICAuLi51c2VBbGlnblByb3BzLmFsaWduLFxuICAgIGRlZmF1bHQ6ICdjZW50ZXInXG4gIH0sXG4gIHN0YWNrOiBCb29sZWFuLFxuICBzdHJldGNoOiBCb29sZWFuLFxuICBsb2FkaW5nOiB7XG4gICAgdHlwZTogQm9vbGVhbixcbiAgICBkZWZhdWx0OiBudWxsXG4gIH0sXG4gIGRpc2FibGU6IEJvb2xlYW5cbn1cblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gKHByb3BzKSB7XG4gIGNvbnN0IHNpemVTdHlsZSA9IHVzZVNpemUocHJvcHMsIGRlZmF1bHRTaXplcylcbiAgY29uc3QgYWxpZ25DbGFzcyA9IHVzZUFsaWduKHByb3BzKVxuICBjb25zdCB7IGhhc1JvdXRlckxpbmssIGhhc0xpbmssIGxpbmtUYWcsIGxpbmtQcm9wcywgbmF2aWdhdGVUb1JvdXRlckxpbmsgfSA9IHVzZVJvdXRlckxpbmsoJ2J1dHRvbicpXG5cbiAgY29uc3Qgc3R5bGUgPSBjb21wdXRlZCgoKSA9PiB7XG4gICAgY29uc3Qgb2JqID0gcHJvcHMuZmFiID09PSBmYWxzZSAmJiBwcm9wcy5mYWJNaW5pID09PSBmYWxzZVxuICAgICAgPyBzaXplU3R5bGUudmFsdWVcbiAgICAgIDoge31cblxuICAgIHJldHVybiBwcm9wcy5wYWRkaW5nICE9PSB2b2lkIDBcbiAgICAgID8gT2JqZWN0LmFzc2lnbih7fSwgb2JqLCB7XG4gICAgICAgIHBhZGRpbmc6IHByb3BzLnBhZGRpbmdcbiAgICAgICAgICAuc3BsaXQoL1xccysvKVxuICAgICAgICAgIC5tYXAodiA9PiAodiBpbiBwYWRkaW5nID8gcGFkZGluZ1sgdiBdICsgJ3B4JyA6IHYpKVxuICAgICAgICAgIC5qb2luKCcgJyksXG4gICAgICAgIG1pbldpZHRoOiAnMCcsXG4gICAgICAgIG1pbkhlaWdodDogJzAnXG4gICAgICB9KVxuICAgICAgOiBvYmpcbiAgfSlcblxuICBjb25zdCBpc1JvdW5kZWQgPSBjb21wdXRlZCgoKSA9PlxuICAgIHByb3BzLnJvdW5kZWQgPT09IHRydWUgfHwgcHJvcHMuZmFiID09PSB0cnVlIHx8IHByb3BzLmZhYk1pbmkgPT09IHRydWVcbiAgKVxuXG4gIGNvbnN0IGlzQWN0aW9uYWJsZSA9IGNvbXB1dGVkKCgpID0+XG4gICAgcHJvcHMuZGlzYWJsZSAhPT0gdHJ1ZSAmJiBwcm9wcy5sb2FkaW5nICE9PSB0cnVlXG4gIClcblxuICBjb25zdCB0YWJJbmRleCA9IGNvbXB1dGVkKCgpID0+IChcbiAgICBpc0FjdGlvbmFibGUudmFsdWUgPT09IHRydWUgPyBwcm9wcy50YWJpbmRleCB8fCAwIDogLTFcbiAgKSlcblxuICBjb25zdCBkZXNpZ24gPSBjb21wdXRlZCgoKSA9PiB7XG4gICAgaWYgKHByb3BzLmZsYXQgPT09IHRydWUpIHJldHVybiAnZmxhdCdcbiAgICBpZiAocHJvcHMub3V0bGluZSA9PT0gdHJ1ZSkgcmV0dXJuICdvdXRsaW5lJ1xuICAgIGlmIChwcm9wcy5wdXNoID09PSB0cnVlKSByZXR1cm4gJ3B1c2gnXG4gICAgaWYgKHByb3BzLnVuZWxldmF0ZWQgPT09IHRydWUpIHJldHVybiAndW5lbGV2YXRlZCdcbiAgICByZXR1cm4gJ3N0YW5kYXJkJ1xuICB9KVxuXG4gIGNvbnN0IGF0dHJpYnV0ZXMgPSBjb21wdXRlZCgoKSA9PiB7XG4gICAgY29uc3QgYWNjID0geyB0YWJpbmRleDogdGFiSW5kZXgudmFsdWUgfVxuXG4gICAgaWYgKGhhc0xpbmsudmFsdWUgPT09IHRydWUpIHtcbiAgICAgIE9iamVjdC5hc3NpZ24oYWNjLCBsaW5rUHJvcHMudmFsdWUpXG4gICAgfVxuICAgIGVsc2UgaWYgKGZvcm1UeXBlcy5pbmNsdWRlcyhwcm9wcy50eXBlKSA9PT0gdHJ1ZSkge1xuICAgICAgYWNjLnR5cGUgPSBwcm9wcy50eXBlXG4gICAgfVxuXG4gICAgaWYgKGxpbmtUYWcudmFsdWUgPT09ICdhJykge1xuICAgICAgaWYgKHByb3BzLmRpc2FibGUgPT09IHRydWUpIHtcbiAgICAgICAgYWNjWyAnYXJpYS1kaXNhYmxlZCcgXSA9ICd0cnVlJ1xuICAgICAgfVxuICAgICAgZWxzZSBpZiAoYWNjLmhyZWYgPT09IHZvaWQgMCkge1xuICAgICAgICBhY2Mucm9sZSA9ICdidXR0b24nXG4gICAgICB9XG4gICAgICBpZiAoaGFzUm91dGVyTGluay52YWx1ZSAhPT0gdHJ1ZSAmJiBtZWRpYVR5cGVSRS50ZXN0KHByb3BzLnR5cGUpID09PSB0cnVlKSB7XG4gICAgICAgIGFjYy50eXBlID0gcHJvcHMudHlwZVxuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIGlmIChwcm9wcy5kaXNhYmxlID09PSB0cnVlKSB7XG4gICAgICBhY2MuZGlzYWJsZWQgPSAnJ1xuICAgICAgYWNjWyAnYXJpYS1kaXNhYmxlZCcgXSA9ICd0cnVlJ1xuICAgIH1cblxuICAgIGlmIChwcm9wcy5sb2FkaW5nID09PSB0cnVlICYmIHByb3BzLnBlcmNlbnRhZ2UgIT09IHZvaWQgMCkge1xuICAgICAgT2JqZWN0LmFzc2lnbihhY2MsIHtcbiAgICAgICAgcm9sZTogJ3Byb2dyZXNzYmFyJyxcbiAgICAgICAgJ2FyaWEtdmFsdWVtaW4nOiAwLFxuICAgICAgICAnYXJpYS12YWx1ZW1heCc6IDEwMCxcbiAgICAgICAgJ2FyaWEtdmFsdWVub3cnOiBwcm9wcy5wZXJjZW50YWdlXG4gICAgICB9KVxuICAgIH1cblxuICAgIHJldHVybiBhY2NcbiAgfSlcblxuICBjb25zdCBjbGFzc2VzID0gY29tcHV0ZWQoKCkgPT4ge1xuICAgIGxldCBjb2xvcnNcblxuICAgIGlmIChwcm9wcy5jb2xvciAhPT0gdm9pZCAwKSB7XG4gICAgICBpZiAocHJvcHMuZmxhdCA9PT0gdHJ1ZSB8fCBwcm9wcy5vdXRsaW5lID09PSB0cnVlKSB7XG4gICAgICAgIGNvbG9ycyA9IGB0ZXh0LSR7IHByb3BzLnRleHRDb2xvciB8fCBwcm9wcy5jb2xvciB9YFxuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIGNvbG9ycyA9IGBiZy0keyBwcm9wcy5jb2xvciB9IHRleHQtJHsgcHJvcHMudGV4dENvbG9yIHx8ICd3aGl0ZScgfWBcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSBpZiAocHJvcHMudGV4dENvbG9yKSB7XG4gICAgICBjb2xvcnMgPSBgdGV4dC0keyBwcm9wcy50ZXh0Q29sb3IgfWBcbiAgICB9XG5cbiAgICByZXR1cm4gYHEtYnRuLS0keyBkZXNpZ24udmFsdWUgfSBgXG4gICAgICArIGBxLWJ0bi0tJHsgcHJvcHMucm91bmQgPT09IHRydWUgPyAncm91bmQnIDogYHJlY3RhbmdsZSR7IGlzUm91bmRlZC52YWx1ZSA9PT0gdHJ1ZSA/ICcgcS1idG4tLXJvdW5kZWQnIDogJycgfWAgfWBcbiAgICAgICsgKGNvbG9ycyAhPT0gdm9pZCAwID8gJyAnICsgY29sb3JzIDogJycpXG4gICAgICArIChpc0FjdGlvbmFibGUudmFsdWUgPT09IHRydWUgPyAnIHEtYnRuLS1hY3Rpb25hYmxlIHEtZm9jdXNhYmxlIHEtaG92ZXJhYmxlJyA6IChwcm9wcy5kaXNhYmxlID09PSB0cnVlID8gJyBkaXNhYmxlZCcgOiAnJykpXG4gICAgICArIChwcm9wcy5mYWIgPT09IHRydWUgPyAnIHEtYnRuLS1mYWInIDogKHByb3BzLmZhYk1pbmkgPT09IHRydWUgPyAnIHEtYnRuLS1mYWItbWluaScgOiAnJykpXG4gICAgICArIChwcm9wcy5ub0NhcHMgPT09IHRydWUgPyAnIHEtYnRuLS1uby11cHBlcmNhc2UnIDogJycpXG4gICAgICArIChwcm9wcy5kZW5zZSA9PT0gdHJ1ZSA/ICcgcS1idG4tLWRlbnNlJyA6ICcnKVxuICAgICAgKyAocHJvcHMuc3RyZXRjaCA9PT0gdHJ1ZSA/ICcgbm8tYm9yZGVyLXJhZGl1cyBzZWxmLXN0cmV0Y2gnIDogJycpXG4gICAgICArIChwcm9wcy5nbG9zc3kgPT09IHRydWUgPyAnIGdsb3NzeScgOiAnJylcbiAgfSlcblxuICBjb25zdCBpbm5lckNsYXNzZXMgPSBjb21wdXRlZCgoKSA9PlxuICAgIGFsaWduQ2xhc3MudmFsdWUgKyAocHJvcHMuc3RhY2sgPT09IHRydWUgPyAnIGNvbHVtbicgOiAnIHJvdycpXG4gICAgKyAocHJvcHMubm9XcmFwID09PSB0cnVlID8gJyBuby13cmFwIHRleHQtbm8td3JhcCcgOiAnJylcbiAgICArIChwcm9wcy5sb2FkaW5nID09PSB0cnVlID8gJyBxLWJ0bl9fY29udGVudC0taGlkZGVuJyA6ICcnKVxuICApXG5cbiAgcmV0dXJuIHtcbiAgICBjbGFzc2VzLFxuICAgIHN0eWxlLFxuICAgIGlubmVyQ2xhc3NlcyxcbiAgICBhdHRyaWJ1dGVzLFxuICAgIGhhc1JvdXRlckxpbmssXG4gICAgaGFzTGluayxcbiAgICBsaW5rVGFnLFxuICAgIG5hdmlnYXRlVG9Sb3V0ZXJMaW5rLFxuICAgIGlzQWN0aW9uYWJsZVxuICB9XG59XG4iLCJpbXBvcnQgeyBoLCByZWYsIGNvbXB1dGVkLCBUcmFuc2l0aW9uLCBvbkJlZm9yZVVubW91bnQsIHdpdGhEaXJlY3RpdmVzLCBnZXRDdXJyZW50SW5zdGFuY2UgfSBmcm9tICd2dWUnXG5cbmltcG9ydCBRSWNvbiBmcm9tICcuLi9pY29uL1FJY29uLmpzJ1xuaW1wb3J0IFFTcGlubmVyIGZyb20gJy4uL3NwaW5uZXIvUVNwaW5uZXIuanMnXG5cbmltcG9ydCBSaXBwbGUgZnJvbSAnLi4vLi4vZGlyZWN0aXZlcy9SaXBwbGUuanMnXG5cbmltcG9ydCB1c2VCdG4sIHsgdXNlQnRuUHJvcHMgfSBmcm9tICcuL3VzZS1idG4uanMnXG5cbmltcG9ydCB7IGNyZWF0ZUNvbXBvbmVudCB9IGZyb20gJy4uLy4uL3V0aWxzL3ByaXZhdGUvY3JlYXRlLmpzJ1xuaW1wb3J0IHsgaE1lcmdlU2xvdCB9IGZyb20gJy4uLy4uL3V0aWxzL3ByaXZhdGUvcmVuZGVyLmpzJ1xuaW1wb3J0IHsgc3RvcCwgcHJldmVudCwgc3RvcEFuZFByZXZlbnQsIGxpc3Rlbk9wdHMgfSBmcm9tICcuLi8uLi91dGlscy9ldmVudC5qcydcbmltcG9ydCB7IGlzS2V5Q29kZSB9IGZyb20gJy4uLy4uL3V0aWxzL3ByaXZhdGUva2V5LWNvbXBvc2l0aW9uLmpzJ1xuXG5jb25zdCB7IHBhc3NpdmVDYXB0dXJlIH0gPSBsaXN0ZW5PcHRzXG5cbmxldFxuICB0b3VjaFRhcmdldCA9IG51bGwsXG4gIGtleWJvYXJkVGFyZ2V0ID0gbnVsbCxcbiAgbW91c2VUYXJnZXQgPSBudWxsXG5cbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZUNvbXBvbmVudCh7XG4gIG5hbWU6ICdRQnRuJyxcblxuICBwcm9wczoge1xuICAgIC4uLnVzZUJ0blByb3BzLFxuXG4gICAgcGVyY2VudGFnZTogTnVtYmVyLFxuICAgIGRhcmtQZXJjZW50YWdlOiBCb29sZWFuXG4gIH0sXG5cbiAgZW1pdHM6IFsgJ2NsaWNrJywgJ2tleWRvd24nLCAndG91Y2hzdGFydCcsICdtb3VzZWRvd24nLCAna2V5dXAnIF0sXG5cbiAgc2V0dXAgKHByb3BzLCB7IHNsb3RzLCBlbWl0IH0pIHtcbiAgICBjb25zdCB7IHByb3h5IH0gPSBnZXRDdXJyZW50SW5zdGFuY2UoKVxuXG4gICAgY29uc3Qge1xuICAgICAgY2xhc3Nlcywgc3R5bGUsIGlubmVyQ2xhc3NlcyxcbiAgICAgIGF0dHJpYnV0ZXMsXG4gICAgICBoYXNSb3V0ZXJMaW5rLCBoYXNMaW5rLCBsaW5rVGFnLCBuYXZpZ2F0ZVRvUm91dGVyTGluayxcbiAgICAgIGlzQWN0aW9uYWJsZVxuICAgIH0gPSB1c2VCdG4ocHJvcHMpXG5cbiAgICBjb25zdCByb290UmVmID0gcmVmKG51bGwpXG4gICAgY29uc3QgYmx1clRhcmdldFJlZiA9IHJlZihudWxsKVxuXG4gICAgbGV0IGxvY2FsVG91Y2hUYXJnZXRFbCA9IG51bGwsIGF2b2lkTW91c2VSaXBwbGUsIG1vdXNlVGltZXJcblxuICAgIGNvbnN0IGhhc0xhYmVsID0gY29tcHV0ZWQoKCkgPT5cbiAgICAgIHByb3BzLmxhYmVsICE9PSB2b2lkIDAgJiYgcHJvcHMubGFiZWwgIT09IG51bGwgJiYgcHJvcHMubGFiZWwgIT09ICcnXG4gICAgKVxuXG4gICAgY29uc3QgcmlwcGxlID0gY29tcHV0ZWQoKCkgPT4gKFxuICAgICAgcHJvcHMuZGlzYWJsZSA9PT0gdHJ1ZSB8fCBwcm9wcy5yaXBwbGUgPT09IGZhbHNlXG4gICAgICAgID8gZmFsc2VcbiAgICAgICAgOiB7XG4gICAgICAgICAgICBrZXlDb2RlczogaGFzTGluay52YWx1ZSA9PT0gdHJ1ZSA/IFsgMTMsIDMyIF0gOiBbIDEzIF0sXG4gICAgICAgICAgICAuLi4ocHJvcHMucmlwcGxlID09PSB0cnVlID8ge30gOiBwcm9wcy5yaXBwbGUpXG4gICAgICAgICAgfVxuICAgICkpXG5cbiAgICBjb25zdCByaXBwbGVQcm9wcyA9IGNvbXB1dGVkKCgpID0+ICh7IGNlbnRlcjogcHJvcHMucm91bmQgfSkpXG5cbiAgICBjb25zdCBwZXJjZW50YWdlU3R5bGUgPSBjb21wdXRlZCgoKSA9PiB7XG4gICAgICBjb25zdCB2YWwgPSBNYXRoLm1heCgwLCBNYXRoLm1pbigxMDAsIHByb3BzLnBlcmNlbnRhZ2UpKVxuICAgICAgcmV0dXJuIHZhbCA+IDBcbiAgICAgICAgPyB7IHRyYW5zaXRpb246ICd0cmFuc2Zvcm0gMC42cycsIHRyYW5zZm9ybTogYHRyYW5zbGF0ZVgoJHsgdmFsIC0gMTAwIH0lKWAgfVxuICAgICAgICA6IHt9XG4gICAgfSlcblxuICAgIGNvbnN0IG9uRXZlbnRzID0gY29tcHV0ZWQoKCkgPT4ge1xuICAgICAgaWYgKHByb3BzLmxvYWRpbmcgPT09IHRydWUpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICBvbk1vdXNlZG93bjogb25Mb2FkaW5nRXZ0LFxuICAgICAgICAgIG9uVG91Y2hzdGFydFBhc3NpdmU6IG9uTG9hZGluZ0V2dCxcbiAgICAgICAgICBvbkNsaWNrOiBvbkxvYWRpbmdFdnQsXG4gICAgICAgICAgb25LZXlkb3duOiBvbkxvYWRpbmdFdnQsXG4gICAgICAgICAgb25LZXl1cDogb25Mb2FkaW5nRXZ0XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKGlzQWN0aW9uYWJsZS52YWx1ZSA9PT0gdHJ1ZSkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIG9uQ2xpY2ssXG4gICAgICAgICAgb25LZXlkb3duLFxuICAgICAgICAgIG9uTW91c2Vkb3duLFxuICAgICAgICAgIG9uVG91Y2hzdGFydFxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB7XG4gICAgICAgIC8vIG5lZWRlZDsgZXNwZWNpYWxseSBmb3IgZGlzYWJsZWQgPGE+IHRhZ3NcbiAgICAgICAgb25DbGljazogc3RvcEFuZFByZXZlbnRcbiAgICAgIH1cbiAgICB9KVxuXG4gICAgY29uc3Qgbm9kZVByb3BzID0gY29tcHV0ZWQoKCkgPT4gKHtcbiAgICAgIHJlZjogcm9vdFJlZixcbiAgICAgIGNsYXNzOiAncS1idG4gcS1idG4taXRlbSBub24tc2VsZWN0YWJsZSBuby1vdXRsaW5lICcgKyBjbGFzc2VzLnZhbHVlLFxuICAgICAgc3R5bGU6IHN0eWxlLnZhbHVlLFxuICAgICAgLi4uYXR0cmlidXRlcy52YWx1ZSxcbiAgICAgIC4uLm9uRXZlbnRzLnZhbHVlXG4gICAgfSkpXG5cbiAgICBmdW5jdGlvbiBvbkNsaWNrIChlKSB7XG4gICAgICAvLyBpcyBpdCBhbHJlYWR5IGRlc3Ryb3llZD9cbiAgICAgIGlmIChyb290UmVmLnZhbHVlID09PSBudWxsKSB7IHJldHVybiB9XG5cbiAgICAgIGlmIChlICE9PSB2b2lkIDApIHtcbiAgICAgICAgaWYgKGUuZGVmYXVsdFByZXZlbnRlZCA9PT0gdHJ1ZSkge1xuICAgICAgICAgIHJldHVyblxuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgZWwgPSBkb2N1bWVudC5hY3RpdmVFbGVtZW50XG4gICAgICAgIC8vIGZvY3VzIGJ1dHRvbiBpZiBpdCBjYW1lIGZyb20gRU5URVIgb24gZm9ybVxuICAgICAgICAvLyBwcmV2ZW50IHRoZSBuZXcgc3VibWl0IChhbHJlYWR5IGRvbmUpXG4gICAgICAgIGlmIChcbiAgICAgICAgICBwcm9wcy50eXBlID09PSAnc3VibWl0J1xuICAgICAgICAgICYmIGVsICE9PSBkb2N1bWVudC5ib2R5XG4gICAgICAgICAgJiYgcm9vdFJlZi52YWx1ZS5jb250YWlucyhlbCkgPT09IGZhbHNlXG4gICAgICAgICAgLy8gcmVxdWlyZWQgZm9yIGlPUyBhbmQgZGVza3RvcCBTYWZhcmlcbiAgICAgICAgICAmJiBlbC5jb250YWlucyhyb290UmVmLnZhbHVlKSA9PT0gZmFsc2VcbiAgICAgICAgKSB7XG4gICAgICAgICAgcm9vdFJlZi52YWx1ZS5mb2N1cygpXG5cbiAgICAgICAgICBjb25zdCBvbkNsaWNrQ2xlYW51cCA9ICgpID0+IHtcbiAgICAgICAgICAgIGRvY3VtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2tleWRvd24nLCBzdG9wQW5kUHJldmVudCwgdHJ1ZSlcbiAgICAgICAgICAgIGRvY3VtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2tleXVwJywgb25DbGlja0NsZWFudXAsIHBhc3NpdmVDYXB0dXJlKVxuICAgICAgICAgICAgcm9vdFJlZi52YWx1ZSAhPT0gbnVsbCAmJiByb290UmVmLnZhbHVlLnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2JsdXInLCBvbkNsaWNrQ2xlYW51cCwgcGFzc2l2ZUNhcHR1cmUpXG4gICAgICAgICAgfVxuXG4gICAgICAgICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcigna2V5ZG93bicsIHN0b3BBbmRQcmV2ZW50LCB0cnVlKVxuICAgICAgICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2tleXVwJywgb25DbGlja0NsZWFudXAsIHBhc3NpdmVDYXB0dXJlKVxuICAgICAgICAgIHJvb3RSZWYudmFsdWUuYWRkRXZlbnRMaXN0ZW5lcignYmx1cicsIG9uQ2xpY2tDbGVhbnVwLCBwYXNzaXZlQ2FwdHVyZSlcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpZiAoaGFzUm91dGVyTGluay52YWx1ZSA9PT0gdHJ1ZSkge1xuICAgICAgICBjb25zdCBnbyA9ICgpID0+IHtcbiAgICAgICAgICBlLl9fcU5hdmlnYXRlID0gdHJ1ZVxuICAgICAgICAgIG5hdmlnYXRlVG9Sb3V0ZXJMaW5rKGUpXG4gICAgICAgIH1cblxuICAgICAgICBlbWl0KCdjbGljaycsIGUsIGdvKVxuICAgICAgICBlLmRlZmF1bHRQcmV2ZW50ZWQgIT09IHRydWUgJiYgZ28oKVxuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIGVtaXQoJ2NsaWNrJywgZSlcbiAgICAgIH1cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBvbktleWRvd24gKGUpIHtcbiAgICAgIC8vIGlzIGl0IGFscmVhZHkgZGVzdHJveWVkP1xuICAgICAgaWYgKHJvb3RSZWYudmFsdWUgPT09IG51bGwpIHsgcmV0dXJuIH1cblxuICAgICAgZW1pdCgna2V5ZG93bicsIGUpXG5cbiAgICAgIGlmIChpc0tleUNvZGUoZSwgWyAxMywgMzIgXSkgPT09IHRydWUgJiYga2V5Ym9hcmRUYXJnZXQgIT09IHJvb3RSZWYudmFsdWUpIHtcbiAgICAgICAga2V5Ym9hcmRUYXJnZXQgIT09IG51bGwgJiYgY2xlYW51cCgpXG5cbiAgICAgICAgaWYgKGUuZGVmYXVsdFByZXZlbnRlZCAhPT0gdHJ1ZSkge1xuICAgICAgICAgIC8vIGZvY3VzIGV4dGVybmFsIGJ1dHRvbiBpZiB0aGUgZm9jdXMgaGVscGVyIHdhcyBmb2N1c2VkIGJlZm9yZVxuICAgICAgICAgIHJvb3RSZWYudmFsdWUuZm9jdXMoKVxuXG4gICAgICAgICAga2V5Ym9hcmRUYXJnZXQgPSByb290UmVmLnZhbHVlXG4gICAgICAgICAgcm9vdFJlZi52YWx1ZS5jbGFzc0xpc3QuYWRkKCdxLWJ0bi0tYWN0aXZlJylcbiAgICAgICAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdrZXl1cCcsIG9uUHJlc3NFbmQsIHRydWUpXG4gICAgICAgICAgcm9vdFJlZi52YWx1ZS5hZGRFdmVudExpc3RlbmVyKCdibHVyJywgb25QcmVzc0VuZCwgcGFzc2l2ZUNhcHR1cmUpXG4gICAgICAgIH1cblxuICAgICAgICBzdG9wQW5kUHJldmVudChlKVxuICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIG9uVG91Y2hzdGFydCAoZSkge1xuICAgICAgLy8gaXMgaXQgYWxyZWFkeSBkZXN0cm95ZWQ/XG4gICAgICBpZiAocm9vdFJlZi52YWx1ZSA9PT0gbnVsbCkgeyByZXR1cm4gfVxuXG4gICAgICBlbWl0KCd0b3VjaHN0YXJ0JywgZSlcblxuICAgICAgaWYgKGUuZGVmYXVsdFByZXZlbnRlZCA9PT0gdHJ1ZSkgeyByZXR1cm4gfVxuXG4gICAgICBpZiAodG91Y2hUYXJnZXQgIT09IHJvb3RSZWYudmFsdWUpIHtcbiAgICAgICAgdG91Y2hUYXJnZXQgIT09IG51bGwgJiYgY2xlYW51cCgpXG4gICAgICAgIHRvdWNoVGFyZ2V0ID0gcm9vdFJlZi52YWx1ZVxuXG4gICAgICAgIGxvY2FsVG91Y2hUYXJnZXRFbCA9IGUudGFyZ2V0XG4gICAgICAgIGxvY2FsVG91Y2hUYXJnZXRFbC5hZGRFdmVudExpc3RlbmVyKCd0b3VjaGNhbmNlbCcsIG9uUHJlc3NFbmQsIHBhc3NpdmVDYXB0dXJlKVxuICAgICAgICBsb2NhbFRvdWNoVGFyZ2V0RWwuYWRkRXZlbnRMaXN0ZW5lcigndG91Y2hlbmQnLCBvblByZXNzRW5kLCBwYXNzaXZlQ2FwdHVyZSlcbiAgICAgIH1cblxuICAgICAgLy8gYXZvaWQgZHVwbGljYXRlZCBtb3VzZWRvd24gZXZlbnRcbiAgICAgIC8vIHRyaWdnZXJpbmcgYW5vdGhlciBlYXJseSByaXBwbGVcbiAgICAgIGF2b2lkTW91c2VSaXBwbGUgPSB0cnVlXG4gICAgICBjbGVhclRpbWVvdXQobW91c2VUaW1lcilcbiAgICAgIG1vdXNlVGltZXIgPSBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgYXZvaWRNb3VzZVJpcHBsZSA9IGZhbHNlXG4gICAgICB9LCAyMDApXG4gICAgfVxuXG4gICAgZnVuY3Rpb24gb25Nb3VzZWRvd24gKGUpIHtcbiAgICAgIC8vIGlzIGl0IGFscmVhZHkgZGVzdHJveWVkP1xuICAgICAgaWYgKHJvb3RSZWYudmFsdWUgPT09IG51bGwpIHsgcmV0dXJuIH1cblxuICAgICAgZS5xU2tpcFJpcHBsZSA9IGF2b2lkTW91c2VSaXBwbGUgPT09IHRydWVcbiAgICAgIGVtaXQoJ21vdXNlZG93bicsIGUpXG5cbiAgICAgIGlmIChlLmRlZmF1bHRQcmV2ZW50ZWQgIT09IHRydWUgJiYgbW91c2VUYXJnZXQgIT09IHJvb3RSZWYudmFsdWUpIHtcbiAgICAgICAgbW91c2VUYXJnZXQgIT09IG51bGwgJiYgY2xlYW51cCgpXG4gICAgICAgIG1vdXNlVGFyZ2V0ID0gcm9vdFJlZi52YWx1ZVxuICAgICAgICByb290UmVmLnZhbHVlLmNsYXNzTGlzdC5hZGQoJ3EtYnRuLS1hY3RpdmUnKVxuICAgICAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdtb3VzZXVwJywgb25QcmVzc0VuZCwgcGFzc2l2ZUNhcHR1cmUpXG4gICAgICB9XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gb25QcmVzc0VuZCAoZSkge1xuICAgICAgLy8gaXMgaXQgYWxyZWFkeSBkZXN0cm95ZWQ/XG4gICAgICBpZiAocm9vdFJlZi52YWx1ZSA9PT0gbnVsbCkgeyByZXR1cm4gfVxuXG4gICAgICAvLyBuZWVkZWQgZm9yIElFIChiZWNhdXNlIGl0IGVtaXRzIGJsdXIgd2hlbiBmb2N1c2luZyBidXR0b24gZnJvbSBmb2N1cyBoZWxwZXIpXG4gICAgICBpZiAoZSAhPT0gdm9pZCAwICYmIGUudHlwZSA9PT0gJ2JsdXInICYmIGRvY3VtZW50LmFjdGl2ZUVsZW1lbnQgPT09IHJvb3RSZWYudmFsdWUpIHtcbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG5cbiAgICAgIGlmIChlICE9PSB2b2lkIDAgJiYgZS50eXBlID09PSAna2V5dXAnKSB7XG4gICAgICAgIGlmIChrZXlib2FyZFRhcmdldCA9PT0gcm9vdFJlZi52YWx1ZSAmJiBpc0tleUNvZGUoZSwgWyAxMywgMzIgXSkgPT09IHRydWUpIHtcbiAgICAgICAgICAvLyBmb3IgY2xpY2sgdHJpZ2dlclxuICAgICAgICAgIGNvbnN0IGV2dCA9IG5ldyBNb3VzZUV2ZW50KCdjbGljaycsIGUpXG4gICAgICAgICAgZXZ0LnFLZXlFdmVudCA9IHRydWVcbiAgICAgICAgICBlLmRlZmF1bHRQcmV2ZW50ZWQgPT09IHRydWUgJiYgcHJldmVudChldnQpXG4gICAgICAgICAgZS5jYW5jZWxCdWJibGUgPT09IHRydWUgJiYgc3RvcChldnQpXG4gICAgICAgICAgcm9vdFJlZi52YWx1ZS5kaXNwYXRjaEV2ZW50KGV2dClcblxuICAgICAgICAgIHN0b3BBbmRQcmV2ZW50KGUpXG5cbiAgICAgICAgICAvLyBmb3IgcmlwcGxlXG4gICAgICAgICAgZS5xS2V5RXZlbnQgPSB0cnVlXG4gICAgICAgIH1cblxuICAgICAgICBlbWl0KCdrZXl1cCcsIGUpXG4gICAgICB9XG5cbiAgICAgIGNsZWFudXAoKVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNsZWFudXAgKGRlc3Ryb3lpbmcpIHtcbiAgICAgIGNvbnN0IGJsdXJUYXJnZXQgPSBibHVyVGFyZ2V0UmVmLnZhbHVlXG5cbiAgICAgIGlmIChcbiAgICAgICAgZGVzdHJveWluZyAhPT0gdHJ1ZVxuICAgICAgICAmJiAodG91Y2hUYXJnZXQgPT09IHJvb3RSZWYudmFsdWUgfHwgbW91c2VUYXJnZXQgPT09IHJvb3RSZWYudmFsdWUpXG4gICAgICAgICYmIGJsdXJUYXJnZXQgIT09IG51bGxcbiAgICAgICAgJiYgYmx1clRhcmdldCAhPT0gZG9jdW1lbnQuYWN0aXZlRWxlbWVudFxuICAgICAgKSB7XG4gICAgICAgIGJsdXJUYXJnZXQuc2V0QXR0cmlidXRlKCd0YWJpbmRleCcsIC0xKVxuICAgICAgICBibHVyVGFyZ2V0LmZvY3VzKClcbiAgICAgIH1cblxuICAgICAgaWYgKHRvdWNoVGFyZ2V0ID09PSByb290UmVmLnZhbHVlKSB7XG4gICAgICAgIGlmIChsb2NhbFRvdWNoVGFyZ2V0RWwgIT09IG51bGwpIHtcbiAgICAgICAgICBsb2NhbFRvdWNoVGFyZ2V0RWwucmVtb3ZlRXZlbnRMaXN0ZW5lcigndG91Y2hjYW5jZWwnLCBvblByZXNzRW5kLCBwYXNzaXZlQ2FwdHVyZSlcbiAgICAgICAgICBsb2NhbFRvdWNoVGFyZ2V0RWwucmVtb3ZlRXZlbnRMaXN0ZW5lcigndG91Y2hlbmQnLCBvblByZXNzRW5kLCBwYXNzaXZlQ2FwdHVyZSlcbiAgICAgICAgfVxuICAgICAgICB0b3VjaFRhcmdldCA9IGxvY2FsVG91Y2hUYXJnZXRFbCA9IG51bGxcbiAgICAgIH1cblxuICAgICAgaWYgKG1vdXNlVGFyZ2V0ID09PSByb290UmVmLnZhbHVlKSB7XG4gICAgICAgIGRvY3VtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ21vdXNldXAnLCBvblByZXNzRW5kLCBwYXNzaXZlQ2FwdHVyZSlcbiAgICAgICAgbW91c2VUYXJnZXQgPSBudWxsXG4gICAgICB9XG5cbiAgICAgIGlmIChrZXlib2FyZFRhcmdldCA9PT0gcm9vdFJlZi52YWx1ZSkge1xuICAgICAgICBkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdrZXl1cCcsIG9uUHJlc3NFbmQsIHRydWUpXG4gICAgICAgIHJvb3RSZWYudmFsdWUgIT09IG51bGwgJiYgcm9vdFJlZi52YWx1ZS5yZW1vdmVFdmVudExpc3RlbmVyKCdibHVyJywgb25QcmVzc0VuZCwgcGFzc2l2ZUNhcHR1cmUpXG4gICAgICAgIGtleWJvYXJkVGFyZ2V0ID0gbnVsbFxuICAgICAgfVxuXG4gICAgICByb290UmVmLnZhbHVlICE9PSBudWxsICYmIHJvb3RSZWYudmFsdWUuY2xhc3NMaXN0LnJlbW92ZSgncS1idG4tLWFjdGl2ZScpXG4gICAgfVxuXG4gICAgZnVuY3Rpb24gb25Mb2FkaW5nRXZ0IChldnQpIHtcbiAgICAgIHN0b3BBbmRQcmV2ZW50KGV2dClcbiAgICAgIGV2dC5xU2tpcFJpcHBsZSA9IHRydWVcbiAgICB9XG5cbiAgICBvbkJlZm9yZVVubW91bnQoKCkgPT4ge1xuICAgICAgY2xlYW51cCh0cnVlKVxuICAgIH0pXG5cbiAgICAvLyBleHBvc2UgcHVibGljIG1ldGhvZHNcbiAgICBPYmplY3QuYXNzaWduKHByb3h5LCB7IGNsaWNrOiBvbkNsaWNrIH0pXG5cbiAgICByZXR1cm4gKCkgPT4ge1xuICAgICAgbGV0IGlubmVyID0gW11cblxuICAgICAgcHJvcHMuaWNvbiAhPT0gdm9pZCAwICYmIGlubmVyLnB1c2goXG4gICAgICAgIGgoUUljb24sIHtcbiAgICAgICAgICBuYW1lOiBwcm9wcy5pY29uLFxuICAgICAgICAgIGxlZnQ6IHByb3BzLnN0YWNrID09PSBmYWxzZSAmJiBoYXNMYWJlbC52YWx1ZSA9PT0gdHJ1ZSxcbiAgICAgICAgICByb2xlOiAnaW1nJyxcbiAgICAgICAgICAnYXJpYS1oaWRkZW4nOiAndHJ1ZSdcbiAgICAgICAgfSlcbiAgICAgIClcblxuICAgICAgaGFzTGFiZWwudmFsdWUgPT09IHRydWUgJiYgaW5uZXIucHVzaChcbiAgICAgICAgaCgnc3BhbicsIHsgY2xhc3M6ICdibG9jaycgfSwgWyBwcm9wcy5sYWJlbCBdKVxuICAgICAgKVxuXG4gICAgICBpbm5lciA9IGhNZXJnZVNsb3Qoc2xvdHMuZGVmYXVsdCwgaW5uZXIpXG5cbiAgICAgIGlmIChwcm9wcy5pY29uUmlnaHQgIT09IHZvaWQgMCAmJiBwcm9wcy5yb3VuZCA9PT0gZmFsc2UpIHtcbiAgICAgICAgaW5uZXIucHVzaChcbiAgICAgICAgICBoKFFJY29uLCB7XG4gICAgICAgICAgICBuYW1lOiBwcm9wcy5pY29uUmlnaHQsXG4gICAgICAgICAgICByaWdodDogcHJvcHMuc3RhY2sgPT09IGZhbHNlICYmIGhhc0xhYmVsLnZhbHVlID09PSB0cnVlLFxuICAgICAgICAgICAgcm9sZTogJ2ltZycsXG4gICAgICAgICAgICAnYXJpYS1oaWRkZW4nOiAndHJ1ZSdcbiAgICAgICAgICB9KVxuICAgICAgICApXG4gICAgICB9XG5cbiAgICAgIGNvbnN0IGNoaWxkID0gW1xuICAgICAgICBoKCdzcGFuJywge1xuICAgICAgICAgIGNsYXNzOiAncS1mb2N1cy1oZWxwZXInLFxuICAgICAgICAgIHJlZjogYmx1clRhcmdldFJlZlxuICAgICAgICB9KVxuICAgICAgXVxuXG4gICAgICBpZiAocHJvcHMubG9hZGluZyA9PT0gdHJ1ZSAmJiBwcm9wcy5wZXJjZW50YWdlICE9PSB2b2lkIDApIHtcbiAgICAgICAgY2hpbGQucHVzaChcbiAgICAgICAgICBoKCdzcGFuJywge1xuICAgICAgICAgICAgY2xhc3M6ICdxLWJ0bl9fcHJvZ3Jlc3MgYWJzb2x1dGUtZnVsbCBvdmVyZmxvdy1oaWRkZW4nXG4gICAgICAgICAgfSwgW1xuICAgICAgICAgICAgaCgnc3BhbicsIHtcbiAgICAgICAgICAgICAgY2xhc3M6ICdxLWJ0bl9fcHJvZ3Jlc3MtaW5kaWNhdG9yIGZpdCBibG9jaycgKyAocHJvcHMuZGFya1BlcmNlbnRhZ2UgPT09IHRydWUgPyAnIHEtYnRuX19wcm9ncmVzcy0tZGFyaycgOiAnJyksXG4gICAgICAgICAgICAgIHN0eWxlOiBwZXJjZW50YWdlU3R5bGUudmFsdWVcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgXSlcbiAgICAgICAgKVxuICAgICAgfVxuXG4gICAgICBjaGlsZC5wdXNoKFxuICAgICAgICBoKCdzcGFuJywge1xuICAgICAgICAgIGNsYXNzOiAncS1idG5fX2NvbnRlbnQgdGV4dC1jZW50ZXIgY29sIGl0ZW1zLWNlbnRlciBxLWFuY2hvci0tc2tpcCAnICsgaW5uZXJDbGFzc2VzLnZhbHVlXG4gICAgICAgIH0sIGlubmVyKVxuICAgICAgKVxuXG4gICAgICBwcm9wcy5sb2FkaW5nICE9PSBudWxsICYmIGNoaWxkLnB1c2goXG4gICAgICAgIGgoVHJhbnNpdGlvbiwge1xuICAgICAgICAgIG5hbWU6ICdxLXRyYW5zaXRpb24tLWZhZGUnXG4gICAgICAgIH0sICgpID0+IChcbiAgICAgICAgICBwcm9wcy5sb2FkaW5nID09PSB0cnVlXG4gICAgICAgICAgICA/IFtcbiAgICAgICAgICAgICAgICBoKCdzcGFuJywge1xuICAgICAgICAgICAgICAgICAga2V5OiAnbG9hZGluZycsXG4gICAgICAgICAgICAgICAgICBjbGFzczogJ2Fic29sdXRlLWZ1bGwgZmxleCBmbGV4LWNlbnRlcidcbiAgICAgICAgICAgICAgICB9LCBzbG90cy5sb2FkaW5nICE9PSB2b2lkIDAgPyBzbG90cy5sb2FkaW5nKCkgOiBbIGgoUVNwaW5uZXIpIF0pXG4gICAgICAgICAgICAgIF1cbiAgICAgICAgICAgIDogbnVsbFxuICAgICAgICApKVxuICAgICAgKVxuXG4gICAgICByZXR1cm4gd2l0aERpcmVjdGl2ZXMoXG4gICAgICAgIGgoXG4gICAgICAgICAgbGlua1RhZy52YWx1ZSxcbiAgICAgICAgICBub2RlUHJvcHMudmFsdWUsXG4gICAgICAgICAgY2hpbGRcbiAgICAgICAgKSxcbiAgICAgICAgWyBbXG4gICAgICAgICAgUmlwcGxlLFxuICAgICAgICAgIHJpcHBsZS52YWx1ZSxcbiAgICAgICAgICB2b2lkIDAsXG4gICAgICAgICAgcmlwcGxlUHJvcHMudmFsdWVcbiAgICAgICAgXSBdXG4gICAgICApXG4gICAgfVxuICB9XG59KVxuIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFHTyxNQUFNLGtCQUFrQjtBQUFBLEVBQzdCLE1BQU07QUFBQSxJQUNKLE1BQU0sQ0FBRSxRQUFRLE1BQVE7QUFBQSxJQUN4QixTQUFTO0FBQUEsRUFDVjtBQUFBLEVBQ0QsT0FBTztBQUNUO0FBRWUsb0JBQXFCLE9BQU87QUFDekMsU0FBTztBQUFBLElBQ0wsT0FBTyxTQUFTLE1BQ2QsTUFBTSxRQUFRLGtCQUNWLEdBQUksZ0JBQWlCLE1BQU0sWUFDM0IsTUFBTSxJQUNYO0FBQUEsSUFFRCxTQUFTLFNBQVMsTUFDaEIsY0FBZSxPQUFNLFFBQVEsU0FBVSxNQUFNLFVBQVcsR0FDekQ7QUFBQSxFQUNGO0FBQ0g7QUNqQkEsSUFBQSxXQUFlLGdCQUFnQjtBQUFBLEVBQzdCLE1BQU07QUFBQSxFQUVOLE9BQU8saUNBQ0Ysa0JBREU7QUFBQSxJQUdMLFdBQVc7QUFBQSxNQUNULE1BQU07QUFBQSxNQUNOLFNBQVM7QUFBQSxJQUNWO0FBQUEsRUFDRjtBQUFBLEVBRUQsTUFBTyxPQUFPO0FBQ1osVUFBTSxFQUFFLE9BQU8sWUFBWSxXQUFXLEtBQUs7QUFFM0MsV0FBTyxNQUFNLEVBQUUsT0FBTztBQUFBLE1BQ3BCLE9BQU8sUUFBUSxRQUFRO0FBQUEsTUFDdkIsT0FBTyxNQUFNO0FBQUEsTUFDYixRQUFRLE1BQU07QUFBQSxNQUNkLFNBQVM7QUFBQSxJQUNmLEdBQU87QUFBQSxNQUNELEVBQUUsVUFBVTtBQUFBLFFBQ1YsT0FBTztBQUFBLFFBQ1AsSUFBSTtBQUFBLFFBQ0osSUFBSTtBQUFBLFFBQ0osR0FBRztBQUFBLFFBQ0gsTUFBTTtBQUFBLFFBQ04sUUFBUTtBQUFBLFFBQ1IsZ0JBQWdCLE1BQU07QUFBQSxRQUN0QixxQkFBcUI7QUFBQSxNQUM3QixDQUFPO0FBQUEsSUFDUCxDQUFLO0FBQUEsRUFDRjtBQUNILENBQUM7QUNiTSxhQUFjLFNBQVMsTUFBSztBQUNqQyxRQUFNLFFBQVEsUUFBUTtBQUV0QixhQUFXLFFBQVEsTUFBSztBQUN0QixVQUFPLFFBQVMsS0FBSztBQUFBLEVBQ3RCO0FBQ0g7QUFtQk8sb0JBQXFCLElBQUk7QUFDOUIsTUFBSSxPQUFPLFVBQVUsT0FBTyxNQUFNO0FBQ2hDLFdBQU87QUFBQSxFQUNSO0FBRUQsTUFBSSxPQUFPLE9BQU8sVUFBVTtBQUMxQixRQUFJO0FBQ0YsYUFBTyxTQUFTLGNBQWMsRUFBRSxLQUFLO0FBQUEsSUFDdEMsU0FDTSxLQUFQO0FBQ0UsYUFBTztBQUFBLElBQ1I7QUFBQSxFQUNGO0FBRUQsUUFBTSxTQUFTLE1BQU0sRUFBRSxNQUFNLE9BQ3pCLEdBQUcsUUFDSDtBQUVKLE1BQUksUUFBUTtBQUNWLFdBQU8sT0FBTyxPQUFPO0FBQUEsRUFDdEI7QUFDSDtBQ3hFZSxrQkFBVSxJQUFJLFFBQVEsS0FBSztBQUN4QyxNQUFJLE9BQU8sT0FBTztBQUVsQixTQUFPLFdBQXlCO0FBQzlCLFFBQUksU0FBUyxPQUFPO0FBQ2xCLGFBQU87QUFDUCxpQkFBVyxNQUFNO0FBQUUsZUFBTztBQUFBLE1BQUssR0FBSSxLQUFLO0FBQ3hDLGVBQVMsR0FBRyxNQUFNLE1BQU0sU0FBUztBQUFBLElBQ2xDO0FBRUQsV0FBTztBQUFBLEVBQ1I7QUFDSDtBQ0xBLG9CQUFxQixLQUFLLElBQUksS0FBSyxhQUFhO0FBQzlDLE1BQUksVUFBVSxTQUFTLFFBQVEsS0FBSyxHQUFHO0FBRXZDLFFBQU0sUUFBUSxJQUFJLFVBQVU7QUFDNUIsTUFBSSxTQUFTLElBQUksVUFBVTtBQUMzQixXQUFTLFdBQVcsUUFBUSxnQkFBZ0I7QUFFNUMsUUFDRSxPQUFPLFNBQVMsY0FBYyxNQUFNLEdBQ3BDLFlBQVksU0FBUyxjQUFjLE1BQU0sR0FDekMsTUFBTSxTQUFTLEdBQUcsR0FDbEIsRUFBRSxNQUFNLEtBQUssT0FBTyxXQUFXLEdBQUcsc0JBQXVCLEdBQ3pELFdBQVcsS0FBSyxLQUFLLFFBQVEsUUFBUSxTQUFTLE1BQU0sR0FDcEQsU0FBUyxXQUFXLEdBQ3BCLFVBQVUsR0FBSyxTQUFRLFlBQVksT0FDbkMsSUFBSSxTQUFTLFVBQVUsR0FBSSxJQUFJLE9BQU8sT0FBTyxZQUM3QyxVQUFVLEdBQUssVUFBUyxZQUFZLE9BQ3BDLElBQUksU0FBUyxVQUFVLEdBQUksSUFBSSxNQUFNLE1BQU07QUFFN0MsWUFBVSxZQUFZO0FBQ3RCLE1BQUksV0FBVztBQUFBLElBQ2IsUUFBUSxHQUFJO0FBQUEsSUFDWixPQUFPLEdBQUk7QUFBQSxJQUNYLFdBQVcsZUFBZ0IsS0FBTztBQUFBLElBQ2xDLFNBQVM7QUFBQSxFQUNiLENBQUc7QUFFRCxPQUFLLFlBQVksV0FBWSxRQUFRLFdBQVcsUUFBUTtBQUN4RCxPQUFLLGFBQWEsT0FBTyxLQUFLO0FBQzlCLE9BQUssWUFBWSxTQUFTO0FBQzFCLEtBQUcsWUFBWSxJQUFJO0FBRW5CLFFBQU0sUUFBUSxNQUFNO0FBQ2xCLFNBQUssT0FBUTtBQUNiLGlCQUFhLEtBQUs7QUFBQSxFQUNuQjtBQUNELE1BQUksTUFBTSxLQUFLLEtBQUs7QUFFcEIsTUFBSSxRQUFRLFdBQVcsTUFBTTtBQUMzQixjQUFVLFVBQVUsSUFBSSx3QkFBd0I7QUFDaEQsY0FBVSxNQUFNLFlBQVksZUFBZ0IsV0FBYTtBQUN6RCxjQUFVLE1BQU0sVUFBVTtBQUUxQixZQUFRLFdBQVcsTUFBTTtBQUN2QixnQkFBVSxVQUFVLE9BQU8sd0JBQXdCO0FBQ25ELGdCQUFVLFVBQVUsSUFBSSx3QkFBd0I7QUFDaEQsZ0JBQVUsTUFBTSxVQUFVO0FBRTFCLGNBQVEsV0FBVyxNQUFNO0FBQ3ZCLGFBQUssT0FBUTtBQUNiLFlBQUksTUFBTSxPQUFPLElBQUksTUFBTSxRQUFRLEtBQUssR0FBRyxDQUFDO0FBQUEsTUFDN0MsR0FBRSxHQUFHO0FBQUEsSUFDUCxHQUFFLEdBQUc7QUFBQSxFQUNQLEdBQUUsRUFBRTtBQUNQO0FBRUEseUJBQTBCLEtBQUssRUFBRSxXQUFXLE9BQU8sS0FBSyxZQUFZO0FBQ2xFLFFBQU0sTUFBTSxPQUFPLE9BQU8sSUFBSSxTQUFTLEdBQUcsT0FBTyxRQUFRLFdBQVcsS0FBSztBQUN6RSxNQUFJLFlBQVk7QUFBQSxJQUNkLE9BQU8sSUFBSSxVQUFVO0FBQUEsSUFDckIsTUFBTSxJQUFJLFNBQVM7QUFBQSxJQUNuQixRQUFRLElBQUksV0FBVztBQUFBLElBQ3ZCLE9BQU8sSUFBSSxTQUFTO0FBQUEsSUFDcEIsVUFBVSxDQUFBLEVBQUcsT0FBTyxJQUFJLFlBQVksRUFBRTtBQUFBLEVBQ3ZDO0FBQ0g7QUFFQSxJQUFBLFNBQWUsZ0JBRVg7QUFBQSxFQUNFLE1BQU07QUFBQSxFQUVOLFlBQWEsSUFBSSxTQUFTO0FBQ3hCLFVBQU0sTUFBTTtBQUFBLE1BQ1YsU0FBUyxRQUFRLFVBQVU7QUFBQSxNQUMzQixXQUFXLENBQUU7QUFBQSxNQUNiLE9BQU8sQ0FBRTtBQUFBLE1BRVQsTUFBTyxLQUFLO0FBQ1YsWUFDRSxJQUFJLFlBQVksUUFDYixJQUFJLGdCQUFnQixRQUVyQixLQUFJLFVBQVUsVUFBVSxPQUNwQixDQUFFLGFBQWEsWUFBYyxFQUFDLFNBQVMsSUFBSSxJQUFJLE1BQU0sT0FDckQsSUFBSSxTQUFTLFVBRW5CO0FBQ0EscUJBQVcsS0FBSyxJQUFJLEtBQUssSUFBSSxjQUFjLElBQUk7QUFBQSxRQUNoRDtBQUFBLE1BQ0Y7QUFBQSxNQUVELFVBQVUsU0FBUyxTQUFPO0FBQ3hCLFlBQ0UsSUFBSSxZQUFZLFFBQ2IsSUFBSSxnQkFBZ0IsUUFDcEIsVUFBVSxLQUFLLElBQUksVUFBVSxRQUFRLE1BQU0sUUFDM0MsSUFBSSxTQUFTLE1BQU8sSUFBSSxVQUFVLFVBQVUsT0FBTyxTQUFTLFFBQy9EO0FBQ0EscUJBQVcsS0FBSyxJQUFJLEtBQUssSUFBSTtBQUFBLFFBQzlCO0FBQUEsTUFDRixHQUFFLEdBQUc7QUFBQSxJQUNQO0FBRUQsb0JBQWdCLEtBQUssT0FBTztBQUU1QixPQUFHLFlBQVk7QUFFZixXQUFPLEtBQUssUUFBUTtBQUFBLE1BQ2xCLENBQUUsSUFBSSxhQUFhLFNBQVMsU0FBVztBQUFBLE1BQ3ZDLENBQUUsSUFBSSxjQUFjLFNBQVMsU0FBVztBQUFBLE1BQ3hDLENBQUUsSUFBSSxTQUFTLFNBQVMsU0FBVztBQUFBLE1BQ25DLENBQUUsSUFBSSxXQUFXLFlBQVksU0FBVztBQUFBLE1BQ3hDLENBQUUsSUFBSSxTQUFTLFlBQVksU0FBVztBQUFBLElBQ2hELENBQVM7QUFBQSxFQUNGO0FBQUEsRUFFRCxRQUFTLElBQUksU0FBUztBQUNwQixRQUFJLFFBQVEsYUFBYSxRQUFRLE9BQU87QUFDdEMsWUFBTSxNQUFNLEdBQUc7QUFDZixVQUFJLFVBQVUsUUFBUSxVQUFVO0FBRWhDLFVBQUksSUFBSSxZQUFZLFFBQVEsT0FBTyxRQUFRLEtBQUssTUFBTSxRQUFRLE9BQU87QUFDbkUsd0JBQWdCLEtBQUssT0FBTztBQUFBLE1BQzdCO0FBQUEsSUFDRjtBQUFBLEVBQ0Y7QUFBQSxFQUVELGNBQWUsSUFBSTtBQUNqQixVQUFNLE1BQU0sR0FBRztBQUNmLFFBQUksTUFBTSxRQUFRLFFBQU07QUFBRSxTQUFJO0FBQUEsSUFBQSxDQUFFO0FBQ2hDLGFBQVMsS0FBSyxNQUFNO0FBQ3BCLFdBQU8sR0FBRztBQUFBLEVBQ1g7QUFDRixDQUNMO0FDNUlPLE1BQU0sV0FBVztBQUFBLEVBQ3RCLE1BQU07QUFBQSxFQUNOLFFBQVE7QUFBQSxFQUNSLE9BQU87QUFBQSxFQUNQLFNBQVM7QUFBQSxFQUNULFFBQVE7QUFBQSxFQUNSLFFBQVE7QUFBQSxFQUNSLFNBQVM7QUFDWDtBQUVPLE1BQU0sY0FBYyxPQUFPLEtBQUssUUFBUTtBQUV4QyxNQUFNLGdCQUFnQjtBQUFBLEVBQzNCLE9BQU87QUFBQSxJQUNMLE1BQU07QUFBQSxJQUNOLFdBQVcsT0FBSyxZQUFZLFNBQVMsQ0FBQztBQUFBLEVBQ3ZDO0FBQ0g7QUFFZSxrQkFBVSxPQUFPO0FBRTlCLFNBQU8sU0FBUyxNQUFNO0FBQ3BCLFVBQU0sUUFBUSxNQUFNLFVBQVUsU0FDMUIsTUFBTSxhQUFhLE9BQU8sWUFBWSxTQUN0QyxNQUFNO0FBRVYsV0FBTyxHQUFJLE1BQU0sYUFBYSxPQUFPLFVBQVUsYUFBZSxTQUFVO0FBQUEsRUFDNUUsQ0FBRztBQUNIO0FDV08scUJBQXNCLElBQUk7QUFDL0IsU0FBTyxHQUFHLFdBQVcsT0FBTyxpQkFBaUIsWUFBWTtBQUMzRDtBQ2hDQSx5QkFBMEIsUUFBUTtBQUNoQyxTQUFPLFNBRUQsT0FBTyxVQUNILE9BQU8sUUFBUSxPQUNmLE9BQU8sT0FDVDtBQUNWO0FBRUEsMkJBQTRCLEdBQUcsR0FBRztBQUloQyxTQUFRLEdBQUUsV0FBVyxPQUFRLEdBQUUsV0FBVztBQUM1QztBQUVBLHdCQUF5QixPQUFPLE9BQU87QUFDckMsYUFBVyxPQUFPLE9BQU87QUFDdkIsVUFDRSxhQUFhLE1BQU8sTUFDcEIsYUFBYSxNQUFPO0FBRXRCLFFBQUksT0FBTyxlQUFlLFVBQVU7QUFDbEMsVUFBSSxlQUFlLFlBQVk7QUFDN0IsZUFBTztBQUFBLE1BQ1I7QUFBQSxJQUNGLFdBRUMsTUFBTSxRQUFRLFVBQVUsTUFBTSxTQUMzQixXQUFXLFdBQVcsV0FBVyxVQUNqQyxXQUFXLEtBQUssQ0FBQyxPQUFPLE1BQU0sVUFBVSxXQUFZLEVBQUcsR0FDMUQ7QUFDQSxhQUFPO0FBQUEsSUFDUjtBQUFBLEVBQ0Y7QUFFRCxTQUFPO0FBQ1Q7QUFFQSwyQkFBNEIsR0FBRyxHQUFHO0FBQ2hDLFNBQU8sTUFBTSxRQUFRLENBQUMsTUFBTSxPQUN4QixFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQUUsTUFBTSxDQUFDLE9BQU8sTUFBTSxVQUFVLEVBQUcsRUFBRyxJQUMvRCxFQUFFLFdBQVcsS0FBSyxFQUFHLE9BQVE7QUFDbkM7QUFFQSx3Q0FBeUMsR0FBRyxHQUFHO0FBQzdDLFNBQU8sTUFBTSxRQUFRLENBQUMsTUFBTSxPQUN4QixrQkFBa0IsR0FBRyxDQUFDLElBRXBCLE1BQU0sUUFBUSxDQUFDLE1BQU0sT0FDakIsa0JBQWtCLEdBQUcsQ0FBQyxJQUN0QixNQUFNO0FBRWxCO0FBRUEsbUNBQW9DLEdBQUcsR0FBRztBQUN4QyxNQUFJLE9BQU8sS0FBSyxDQUFDLEVBQUUsV0FBVyxPQUFPLEtBQUssQ0FBQyxFQUFFLFFBQVE7QUFDbkQsV0FBTztBQUFBLEVBQ1I7QUFFRCxhQUFXLE9BQU8sR0FBRztBQUNuQixRQUFJLCtCQUErQixFQUFHLE1BQU8sRUFBRyxJQUFLLE1BQU0sT0FBTztBQUNoRSxhQUFPO0FBQUEsSUFDUjtBQUFBLEVBQ0Y7QUFFRCxTQUFPO0FBQ1Q7QUFFWSxNQUFDLHFCQUFxQjtBQUFBLEVBRWhDLElBQUksQ0FBRSxRQUFRLE1BQVE7QUFBQSxFQUN0QixTQUFTO0FBQUEsRUFDVCxPQUFPO0FBQUEsRUFDUCxhQUFhO0FBQUEsSUFDWCxNQUFNO0FBQUEsSUFDTixTQUFTO0FBQUEsRUFDVjtBQUFBLEVBQ0Qsa0JBQWtCO0FBQUEsSUFDaEIsTUFBTTtBQUFBLElBQ04sU0FBUztBQUFBLEVBQ1Y7QUFBQSxFQUdELE1BQU07QUFBQSxFQUNOLFFBQVE7QUFBQSxFQUdSLFNBQVM7QUFDWDtBQUllLHVCQUFVLGFBQWE7QUFDcEMsUUFBTSxLQUFLLG1CQUFvQjtBQUMvQixRQUFNLEVBQUUsT0FBTyxVQUFVO0FBRXpCLFFBQU0sWUFBWSxZQUFZLEVBQUU7QUFDaEMsUUFBTSxjQUFjLFNBQVMsTUFBTSxNQUFNLFlBQVksUUFBUSxNQUFNLFNBQVMsTUFBTTtBQUVsRixRQUFNLHFCQUFxQixTQUFTLE1BQ2xDLGNBQWMsUUFDWCxNQUFNLFlBQVksUUFDbEIsWUFBWSxVQUFVLFFBQ3RCLE1BQU0sT0FBTyxVQUFVLE1BQU0sT0FBTyxRQUFRLE1BQU0sT0FBTyxFQUM3RDtBQUVELFFBQU0sWUFBWSxTQUFTLE1BQU07QUFDL0IsUUFBSSxtQkFBbUIsVUFBVSxNQUFNO0FBQ3JDLFVBQUk7QUFBRSxlQUFPLE1BQU0sUUFBUSxRQUFRLE1BQU0sRUFBRTtBQUFBLE1BQUcsU0FDdkMsS0FBUDtBQUFBLE1BQWM7QUFBQSxJQUNmO0FBRUQsV0FBTztBQUFBLEVBQ1gsQ0FBRztBQUVELFFBQU0sZ0JBQWdCLFNBQVMsTUFBTSxVQUFVLFVBQVUsSUFBSTtBQUM3RCxRQUFNLFVBQVUsU0FBUyxNQUFNLFlBQVksVUFBVSxRQUFRLGNBQWMsVUFBVSxJQUFJO0FBRXpGLFFBQU0sVUFBVSxTQUFTLE1BQ3ZCLE1BQU0sU0FBUyxPQUFPLFFBQVEsVUFBVSxPQUNwQyxNQUNDLE1BQU0sT0FBTyxlQUFlLEtBQ2xDO0FBRUQsUUFBTSxZQUFZLFNBQVMsTUFDekIsWUFBWSxVQUFVLE9BQ2xCO0FBQUEsSUFDRSxNQUFNLE1BQU07QUFBQSxJQUNaLFFBQVEsTUFBTTtBQUFBLEVBQ2YsSUFFQyxjQUFjLFVBQVUsT0FDcEI7QUFBQSxJQUNFLE1BQU0sVUFBVSxNQUFNO0FBQUEsSUFDdEIsUUFBUSxNQUFNO0FBQUEsRUFDZixJQUNELENBQUUsQ0FFYjtBQUVELFFBQU0sa0JBQWtCLFNBQVMsTUFBTTtBQUNyQyxRQUFJLGNBQWMsVUFBVSxPQUFPO0FBQ2pDLGFBQU87QUFBQSxJQUNSO0FBRUQsVUFDRSxFQUFFLFlBQVksVUFBVSxPQUN4QixFQUFFLFdBQVcsU0FDYixlQUFlLFFBQVMsU0FBUztBQUVuQyxRQUFJLGlCQUFpQixRQUFRO0FBQzNCLGFBQU87QUFBQSxJQUNSO0FBRUQsVUFBTSxpQkFBaUIsTUFBTSxPQUFPO0FBRXBDLFFBQUksZUFBZSxXQUFXLEdBQUc7QUFDL0IsYUFBTztBQUFBLElBQ1I7QUFFRCxVQUFNLFFBQVEsZUFBZSxVQUMzQixrQkFBa0IsS0FBSyxNQUFNLFlBQVksQ0FDMUM7QUFFRCxRQUFJLFFBQVEsSUFBSTtBQUNkLGFBQU87QUFBQSxJQUNSO0FBR0QsVUFBTSxtQkFBbUIsZ0JBQWdCLFFBQVMsU0FBUyxFQUFHO0FBRTlELFdBRUUsU0FBUyxLQUlOLGdCQUFnQixZQUFZLE1BQU0sb0JBRWxDLGVBQWdCLGVBQWUsU0FBUyxHQUFJLFNBQVMsbUJBQ3BELGVBQWUsVUFDZixrQkFBa0IsS0FBSyxNQUFNLFFBQVMsU0FBUyxFQUFHLENBQ25ELElBQ0M7QUFBQSxFQUVWLENBQUc7QUFFRCxRQUFNLGVBQWUsU0FBUyxNQUM1QixjQUFjLFVBQVUsUUFDckIsZ0JBQWdCLFFBQVEsTUFDeEIsZUFBZSxNQUFNLE9BQU8sUUFBUSxVQUFVLE1BQU0sTUFBTSxDQUM5RDtBQUVELFFBQU0sb0JBQW9CLFNBQVMsTUFDakMsYUFBYSxVQUFVLFFBQ2xCLGdCQUFnQixVQUFVLE1BQU0sT0FBTyxRQUFRLFNBQVMsS0FDeEQsMEJBQTBCLE1BQU0sT0FBTyxRQUFRLFVBQVUsTUFBTSxNQUFNLENBQzNFO0FBRUQsUUFBTSxZQUFZLFNBQVMsTUFDekIsY0FBYyxVQUFVLE9BRWxCLGtCQUFrQixVQUFVLE9BQ3hCLElBQUssTUFBTSxvQkFBc0IsTUFBTSxnQkFFckMsTUFBTSxVQUFVLE9BQ1osS0FDQyxhQUFhLFVBQVUsT0FBTyxJQUFLLE1BQU0sZ0JBQWlCLEtBR3ZFLEVBQ0w7QUFHRCxnQ0FBK0IsR0FBRztBQUNoQyxRQUVFLE1BQU0sWUFBWSxRQUdmLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsWUFJdkMsRUFBRSxnQkFBZ0IsUUFBUSxFQUFFLHFCQUFxQixRQUdqRCxFQUFFLFdBQVcsVUFBYSxFQUFFLFdBQVcsS0FHeEMsTUFBTSxXQUFXLFVBQ3BCO0FBQ0EsYUFBTztBQUFBLElBQ1I7QUFFRCxZQUFRLENBQUM7QUFFVCxXQUFPLE1BQU0sUUFBUyxNQUFNLFlBQVksT0FBTyxZQUFZLFFBQVMsTUFBTSxFQUFFLEVBQ3pFLE1BQU0sU0FBTyxHQUFHO0FBQUEsRUFDcEI7QUFFRCxTQUFPO0FBQUEsSUFDTDtBQUFBLElBQ0E7QUFBQSxJQUNBO0FBQUEsSUFFQTtBQUFBLElBQ0E7QUFBQSxJQUNBO0FBQUEsSUFDQTtBQUFBLElBQ0E7QUFBQSxJQUNBO0FBQUEsSUFFQTtBQUFBLEVBQ0Q7QUFDSDtBQ3JRQSxNQUFNLFVBQVU7QUFBQSxFQUNkLE1BQU07QUFBQSxFQUNOLElBQUk7QUFBQSxFQUNKLElBQUk7QUFBQSxFQUNKLElBQUk7QUFBQSxFQUNKLElBQUk7QUFBQSxFQUNKLElBQUk7QUFDTjtBQUVBLE1BQU0sZUFBZTtBQUFBLEVBQ25CLElBQUk7QUFBQSxFQUNKLElBQUk7QUFBQSxFQUNKLElBQUk7QUFBQSxFQUNKLElBQUk7QUFBQSxFQUNKLElBQUk7QUFDTjtBQUVBLE1BQU0sWUFBWSxDQUFFLFVBQVUsVUFBVSxPQUFTO0FBQ2pELE1BQU0sY0FBYztBQUViLE1BQU0sY0FBYyxnREFDdEIsZUFDQSxxQkFGc0I7QUFBQSxFQUl6QixNQUFNO0FBQUEsSUFDSixNQUFNO0FBQUEsSUFDTixTQUFTO0FBQUEsRUFDVjtBQUFBLEVBRUQsT0FBTyxDQUFFLFFBQVEsTUFBUTtBQUFBLEVBQ3pCLE1BQU07QUFBQSxFQUNOLFdBQVc7QUFBQSxFQUVYLE9BQU87QUFBQSxFQUNQLFNBQVM7QUFBQSxFQUNULE1BQU07QUFBQSxFQUNOLFlBQVk7QUFBQSxFQUNaLFNBQVM7QUFBQSxFQUNULE1BQU07QUFBQSxFQUNOLFFBQVE7QUFBQSxFQUVSLE1BQU07QUFBQSxFQUNOLEtBQUs7QUFBQSxFQUNMLFNBQVM7QUFBQSxFQUNULFNBQVM7QUFBQSxFQUVULE9BQU87QUFBQSxFQUNQLFdBQVc7QUFBQSxFQUNYLFFBQVE7QUFBQSxFQUNSLFFBQVE7QUFBQSxFQUNSLE9BQU87QUFBQSxFQUVQLFVBQVUsQ0FBRSxRQUFRLE1BQVE7QUFBQSxFQUU1QixRQUFRO0FBQUEsSUFDTixNQUFNLENBQUUsU0FBUyxNQUFRO0FBQUEsSUFDekIsU0FBUztBQUFBLEVBQ1Y7QUFBQSxFQUVELE9BQU8saUNBQ0YsY0FBYyxRQURaO0FBQUEsSUFFTCxTQUFTO0FBQUEsRUFDVjtBQUFBLEVBQ0QsT0FBTztBQUFBLEVBQ1AsU0FBUztBQUFBLEVBQ1QsU0FBUztBQUFBLElBQ1AsTUFBTTtBQUFBLElBQ04sU0FBUztBQUFBLEVBQ1Y7QUFBQSxFQUNELFNBQVM7QUFDWDtBQUVlLGdCQUFVLE9BQU87QUFDOUIsUUFBTSxZQUFZLFFBQVEsT0FBTyxZQUFZO0FBQzdDLFFBQU0sYUFBYSxTQUFTLEtBQUs7QUFDakMsUUFBTSxFQUFFLGVBQWUsU0FBUyxTQUFTLFdBQVcseUJBQXlCLGNBQWMsUUFBUTtBQUVuRyxRQUFNLFFBQVEsU0FBUyxNQUFNO0FBQzNCLFVBQU0sTUFBTSxNQUFNLFFBQVEsU0FBUyxNQUFNLFlBQVksUUFDakQsVUFBVSxRQUNWLENBQUU7QUFFTixXQUFPLE1BQU0sWUFBWSxTQUNyQixPQUFPLE9BQU8sQ0FBRSxHQUFFLEtBQUs7QUFBQSxNQUN2QixTQUFTLE1BQU0sUUFDWixNQUFNLEtBQUssRUFDWCxJQUFJLE9BQU0sS0FBSyxVQUFVLFFBQVMsS0FBTSxPQUFPLENBQUUsRUFDakQsS0FBSyxHQUFHO0FBQUEsTUFDWCxVQUFVO0FBQUEsTUFDVixXQUFXO0FBQUEsSUFDbkIsQ0FBTyxJQUNDO0FBQUEsRUFDUixDQUFHO0FBRUQsUUFBTSxZQUFZLFNBQVMsTUFDekIsTUFBTSxZQUFZLFFBQVEsTUFBTSxRQUFRLFFBQVEsTUFBTSxZQUFZLElBQ25FO0FBRUQsUUFBTSxlQUFlLFNBQVMsTUFDNUIsTUFBTSxZQUFZLFFBQVEsTUFBTSxZQUFZLElBQzdDO0FBRUQsUUFBTSxXQUFXLFNBQVMsTUFDeEIsYUFBYSxVQUFVLE9BQU8sTUFBTSxZQUFZLElBQUksRUFDckQ7QUFFRCxRQUFNLFNBQVMsU0FBUyxNQUFNO0FBQzVCLFFBQUksTUFBTSxTQUFTO0FBQU0sYUFBTztBQUNoQyxRQUFJLE1BQU0sWUFBWTtBQUFNLGFBQU87QUFDbkMsUUFBSSxNQUFNLFNBQVM7QUFBTSxhQUFPO0FBQ2hDLFFBQUksTUFBTSxlQUFlO0FBQU0sYUFBTztBQUN0QyxXQUFPO0FBQUEsRUFDWCxDQUFHO0FBRUQsUUFBTSxhQUFhLFNBQVMsTUFBTTtBQUNoQyxVQUFNLE1BQU0sRUFBRSxVQUFVLFNBQVMsTUFBTztBQUV4QyxRQUFJLFFBQVEsVUFBVSxNQUFNO0FBQzFCLGFBQU8sT0FBTyxLQUFLLFVBQVUsS0FBSztBQUFBLElBQ25DLFdBQ1EsVUFBVSxTQUFTLE1BQU0sSUFBSSxNQUFNLE1BQU07QUFDaEQsVUFBSSxPQUFPLE1BQU07QUFBQSxJQUNsQjtBQUVELFFBQUksUUFBUSxVQUFVLEtBQUs7QUFDekIsVUFBSSxNQUFNLFlBQVksTUFBTTtBQUMxQixZQUFLLG1CQUFvQjtBQUFBLE1BQzFCLFdBQ1EsSUFBSSxTQUFTLFFBQVE7QUFDNUIsWUFBSSxPQUFPO0FBQUEsTUFDWjtBQUNELFVBQUksY0FBYyxVQUFVLFFBQVEsWUFBWSxLQUFLLE1BQU0sSUFBSSxNQUFNLE1BQU07QUFDekUsWUFBSSxPQUFPLE1BQU07QUFBQSxNQUNsQjtBQUFBLElBQ0YsV0FDUSxNQUFNLFlBQVksTUFBTTtBQUMvQixVQUFJLFdBQVc7QUFDZixVQUFLLG1CQUFvQjtBQUFBLElBQzFCO0FBRUQsUUFBSSxNQUFNLFlBQVksUUFBUSxNQUFNLGVBQWUsUUFBUTtBQUN6RCxhQUFPLE9BQU8sS0FBSztBQUFBLFFBQ2pCLE1BQU07QUFBQSxRQUNOLGlCQUFpQjtBQUFBLFFBQ2pCLGlCQUFpQjtBQUFBLFFBQ2pCLGlCQUFpQixNQUFNO0FBQUEsTUFDL0IsQ0FBTztBQUFBLElBQ0Y7QUFFRCxXQUFPO0FBQUEsRUFDWCxDQUFHO0FBRUQsUUFBTSxVQUFVLFNBQVMsTUFBTTtBQUM3QixRQUFJO0FBRUosUUFBSSxNQUFNLFVBQVUsUUFBUTtBQUMxQixVQUFJLE1BQU0sU0FBUyxRQUFRLE1BQU0sWUFBWSxNQUFNO0FBQ2pELGlCQUFTLFFBQVMsTUFBTSxhQUFhLE1BQU07QUFBQSxNQUM1QyxPQUNJO0FBQ0gsaUJBQVMsTUFBTyxNQUFNLGNBQWdCLE1BQU0sYUFBYTtBQUFBLE1BQzFEO0FBQUEsSUFDRixXQUNRLE1BQU0sV0FBVztBQUN4QixlQUFTLFFBQVMsTUFBTTtBQUFBLElBQ3pCO0FBRUQsV0FBTyxVQUFXLE9BQU8sZ0JBQ1YsTUFBTSxVQUFVLE9BQU8sVUFBVSxZQUFhLFVBQVUsVUFBVSxPQUFPLG9CQUFvQixTQUN2RyxZQUFXLFNBQVMsTUFBTSxTQUFTLE1BQ25DLGNBQWEsVUFBVSxPQUFPLCtDQUFnRCxNQUFNLFlBQVksT0FBTyxjQUFjLE1BQ3JILE9BQU0sUUFBUSxPQUFPLGdCQUFpQixNQUFNLFlBQVksT0FBTyxxQkFBcUIsTUFDcEYsT0FBTSxXQUFXLE9BQU8seUJBQXlCLE1BQ2pELE9BQU0sVUFBVSxPQUFPLGtCQUFrQixNQUN6QyxPQUFNLFlBQVksT0FBTyxtQ0FBbUMsTUFDNUQsT0FBTSxXQUFXLE9BQU8sWUFBWTtBQUFBLEVBQzdDLENBQUc7QUFFRCxRQUFNLGVBQWUsU0FBUyxNQUM1QixXQUFXLFFBQVMsT0FBTSxVQUFVLE9BQU8sWUFBWSxVQUNwRCxPQUFNLFdBQVcsT0FBTywwQkFBMEIsTUFDbEQsT0FBTSxZQUFZLE9BQU8sNEJBQTRCLEdBQ3pEO0FBRUQsU0FBTztBQUFBLElBQ0w7QUFBQSxJQUNBO0FBQUEsSUFDQTtBQUFBLElBQ0E7QUFBQSxJQUNBO0FBQUEsSUFDQTtBQUFBLElBQ0E7QUFBQSxJQUNBO0FBQUEsSUFDQTtBQUFBLEVBQ0Q7QUFDSDtBQzNMQSxNQUFNLEVBQUUsbUJBQW1CO0FBRTNCLElBQ0UsY0FBYyxNQUNkLGlCQUFpQixNQUNqQixjQUFjO0FBRWhCLElBQUEsT0FBZSxnQkFBZ0I7QUFBQSxFQUM3QixNQUFNO0FBQUEsRUFFTixPQUFPLGlDQUNGLGNBREU7QUFBQSxJQUdMLFlBQVk7QUFBQSxJQUNaLGdCQUFnQjtBQUFBLEVBQ2pCO0FBQUEsRUFFRCxPQUFPLENBQUUsU0FBUyxXQUFXLGNBQWMsYUFBYSxPQUFTO0FBQUEsRUFFakUsTUFBTyxPQUFPLEVBQUUsT0FBTyxRQUFRO0FBQzdCLFVBQU0sRUFBRSxVQUFVLG1CQUFvQjtBQUV0QyxVQUFNO0FBQUEsTUFDSjtBQUFBLE1BQVM7QUFBQSxNQUFPO0FBQUEsTUFDaEI7QUFBQSxNQUNBO0FBQUEsTUFBZTtBQUFBLE1BQVM7QUFBQSxNQUFTO0FBQUEsTUFDakM7QUFBQSxRQUNFLE9BQU8sS0FBSztBQUVoQixVQUFNLFVBQVUsSUFBSSxJQUFJO0FBQ3hCLFVBQU0sZ0JBQWdCLElBQUksSUFBSTtBQUU5QixRQUFJLHFCQUFxQixNQUFNLGtCQUFrQjtBQUVqRCxVQUFNLFdBQVcsU0FBUyxNQUN4QixNQUFNLFVBQVUsVUFBVSxNQUFNLFVBQVUsUUFBUSxNQUFNLFVBQVUsRUFDbkU7QUFFRCxVQUFNLFNBQVMsU0FBUyxNQUN0QixNQUFNLFlBQVksUUFBUSxNQUFNLFdBQVcsUUFDdkMsUUFDQTtBQUFBLE1BQ0UsVUFBVSxRQUFRLFVBQVUsT0FBTyxDQUFFLElBQUksRUFBRSxJQUFLLENBQUUsRUFBSTtBQUFBLE9BQ2xELE1BQU0sV0FBVyxPQUFPLENBQUEsSUFBSyxNQUFNLE9BRTlDO0FBRUQsVUFBTSxjQUFjLFNBQVMsTUFBTyxHQUFFLFFBQVEsTUFBTSxNQUFLLEVBQUc7QUFFNUQsVUFBTSxrQkFBa0IsU0FBUyxNQUFNO0FBQ3JDLFlBQU0sTUFBTSxLQUFLLElBQUksR0FBRyxLQUFLLElBQUksS0FBSyxNQUFNLFVBQVUsQ0FBQztBQUN2RCxhQUFPLE1BQU0sSUFDVCxFQUFFLFlBQVksa0JBQWtCLFdBQVcsY0FBZSxNQUFNLFFBQVUsSUFDMUUsQ0FBRTtBQUFBLElBQ1osQ0FBSztBQUVELFVBQU0sV0FBVyxTQUFTLE1BQU07QUFDOUIsVUFBSSxNQUFNLFlBQVksTUFBTTtBQUMxQixlQUFPO0FBQUEsVUFDTCxhQUFhO0FBQUEsVUFDYixxQkFBcUI7QUFBQSxVQUNyQixTQUFTO0FBQUEsVUFDVCxXQUFXO0FBQUEsVUFDWCxTQUFTO0FBQUEsUUFDVjtBQUFBLE1BQ0Y7QUFFRCxVQUFJLGFBQWEsVUFBVSxNQUFNO0FBQy9CLGVBQU87QUFBQSxVQUNMO0FBQUEsVUFDQTtBQUFBLFVBQ0E7QUFBQSxVQUNBO0FBQUEsUUFDRDtBQUFBLE1BQ0Y7QUFFRCxhQUFPO0FBQUEsUUFFTCxTQUFTO0FBQUEsTUFDVjtBQUFBLElBQ1AsQ0FBSztBQUVELFVBQU0sWUFBWSxTQUFTLE1BQU87QUFBQSxNQUNoQyxLQUFLO0FBQUEsTUFDTCxPQUFPLGdEQUFnRCxRQUFRO0FBQUEsTUFDL0QsT0FBTyxNQUFNO0FBQUEsT0FDVixXQUFXLFFBQ1gsU0FBUyxNQUNaO0FBRUYscUJBQWtCLEdBQUc7QUFFbkIsVUFBSSxRQUFRLFVBQVUsTUFBTTtBQUFFO0FBQUEsTUFBUTtBQUV0QyxVQUFJLE1BQU0sUUFBUTtBQUNoQixZQUFJLEVBQUUscUJBQXFCLE1BQU07QUFDL0I7QUFBQSxRQUNEO0FBRUQsY0FBTSxLQUFLLFNBQVM7QUFHcEIsWUFDRSxNQUFNLFNBQVMsWUFDWixPQUFPLFNBQVMsUUFDaEIsUUFBUSxNQUFNLFNBQVMsRUFBRSxNQUFNLFNBRS9CLEdBQUcsU0FBUyxRQUFRLEtBQUssTUFBTSxPQUNsQztBQUNBLGtCQUFRLE1BQU0sTUFBTztBQUVyQixnQkFBTSxpQkFBaUIsTUFBTTtBQUMzQixxQkFBUyxvQkFBb0IsV0FBVyxnQkFBZ0IsSUFBSTtBQUM1RCxxQkFBUyxvQkFBb0IsU0FBUyxnQkFBZ0IsY0FBYztBQUNwRSxvQkFBUSxVQUFVLFFBQVEsUUFBUSxNQUFNLG9CQUFvQixRQUFRLGdCQUFnQixjQUFjO0FBQUEsVUFDbkc7QUFFRCxtQkFBUyxpQkFBaUIsV0FBVyxnQkFBZ0IsSUFBSTtBQUN6RCxtQkFBUyxpQkFBaUIsU0FBUyxnQkFBZ0IsY0FBYztBQUNqRSxrQkFBUSxNQUFNLGlCQUFpQixRQUFRLGdCQUFnQixjQUFjO0FBQUEsUUFDdEU7QUFBQSxNQUNGO0FBRUQsVUFBSSxjQUFjLFVBQVUsTUFBTTtBQUNoQyxjQUFNLEtBQUssTUFBTTtBQUNmLFlBQUUsY0FBYztBQUNoQiwrQkFBcUIsQ0FBQztBQUFBLFFBQ3ZCO0FBRUQsYUFBSyxTQUFTLEdBQUcsRUFBRTtBQUNuQixVQUFFLHFCQUFxQixRQUFRLEdBQUk7QUFBQSxNQUNwQyxPQUNJO0FBQ0gsYUFBSyxTQUFTLENBQUM7QUFBQSxNQUNoQjtBQUFBLElBQ0Y7QUFFRCx1QkFBb0IsR0FBRztBQUVyQixVQUFJLFFBQVEsVUFBVSxNQUFNO0FBQUU7QUFBQSxNQUFRO0FBRXRDLFdBQUssV0FBVyxDQUFDO0FBRWpCLFVBQUksVUFBVSxHQUFHLENBQUUsSUFBSSxHQUFJLE1BQU0sUUFBUSxtQkFBbUIsUUFBUSxPQUFPO0FBQ3pFLDJCQUFtQixRQUFRLFFBQVM7QUFFcEMsWUFBSSxFQUFFLHFCQUFxQixNQUFNO0FBRS9CLGtCQUFRLE1BQU0sTUFBTztBQUVyQiwyQkFBaUIsUUFBUTtBQUN6QixrQkFBUSxNQUFNLFVBQVUsSUFBSSxlQUFlO0FBQzNDLG1CQUFTLGlCQUFpQixTQUFTLFlBQVksSUFBSTtBQUNuRCxrQkFBUSxNQUFNLGlCQUFpQixRQUFRLFlBQVksY0FBYztBQUFBLFFBQ2xFO0FBRUQsdUJBQWUsQ0FBQztBQUFBLE1BQ2pCO0FBQUEsSUFDRjtBQUVELDBCQUF1QixHQUFHO0FBRXhCLFVBQUksUUFBUSxVQUFVLE1BQU07QUFBRTtBQUFBLE1BQVE7QUFFdEMsV0FBSyxjQUFjLENBQUM7QUFFcEIsVUFBSSxFQUFFLHFCQUFxQixNQUFNO0FBQUU7QUFBQSxNQUFRO0FBRTNDLFVBQUksZ0JBQWdCLFFBQVEsT0FBTztBQUNqQyx3QkFBZ0IsUUFBUSxRQUFTO0FBQ2pDLHNCQUFjLFFBQVE7QUFFdEIsNkJBQXFCLEVBQUU7QUFDdkIsMkJBQW1CLGlCQUFpQixlQUFlLFlBQVksY0FBYztBQUM3RSwyQkFBbUIsaUJBQWlCLFlBQVksWUFBWSxjQUFjO0FBQUEsTUFDM0U7QUFJRCx5QkFBbUI7QUFDbkIsbUJBQWEsVUFBVTtBQUN2QixtQkFBYSxXQUFXLE1BQU07QUFDNUIsMkJBQW1CO0FBQUEsTUFDcEIsR0FBRSxHQUFHO0FBQUEsSUFDUDtBQUVELHlCQUFzQixHQUFHO0FBRXZCLFVBQUksUUFBUSxVQUFVLE1BQU07QUFBRTtBQUFBLE1BQVE7QUFFdEMsUUFBRSxjQUFjLHFCQUFxQjtBQUNyQyxXQUFLLGFBQWEsQ0FBQztBQUVuQixVQUFJLEVBQUUscUJBQXFCLFFBQVEsZ0JBQWdCLFFBQVEsT0FBTztBQUNoRSx3QkFBZ0IsUUFBUSxRQUFTO0FBQ2pDLHNCQUFjLFFBQVE7QUFDdEIsZ0JBQVEsTUFBTSxVQUFVLElBQUksZUFBZTtBQUMzQyxpQkFBUyxpQkFBaUIsV0FBVyxZQUFZLGNBQWM7QUFBQSxNQUNoRTtBQUFBLElBQ0Y7QUFFRCx3QkFBcUIsR0FBRztBQUV0QixVQUFJLFFBQVEsVUFBVSxNQUFNO0FBQUU7QUFBQSxNQUFRO0FBR3RDLFVBQUksTUFBTSxVQUFVLEVBQUUsU0FBUyxVQUFVLFNBQVMsa0JBQWtCLFFBQVEsT0FBTztBQUNqRjtBQUFBLE1BQ0Q7QUFFRCxVQUFJLE1BQU0sVUFBVSxFQUFFLFNBQVMsU0FBUztBQUN0QyxZQUFJLG1CQUFtQixRQUFRLFNBQVMsVUFBVSxHQUFHLENBQUUsSUFBSSxHQUFJLE1BQU0sTUFBTTtBQUV6RSxnQkFBTSxNQUFNLElBQUksV0FBVyxTQUFTLENBQUM7QUFDckMsY0FBSSxZQUFZO0FBQ2hCLFlBQUUscUJBQXFCLFFBQVEsUUFBUSxHQUFHO0FBQzFDLFlBQUUsaUJBQWlCLFFBQVEsS0FBSyxHQUFHO0FBQ25DLGtCQUFRLE1BQU0sY0FBYyxHQUFHO0FBRS9CLHlCQUFlLENBQUM7QUFHaEIsWUFBRSxZQUFZO0FBQUEsUUFDZjtBQUVELGFBQUssU0FBUyxDQUFDO0FBQUEsTUFDaEI7QUFFRCxjQUFTO0FBQUEsSUFDVjtBQUVELHFCQUFrQixZQUFZO0FBQzVCLFlBQU0sYUFBYSxjQUFjO0FBRWpDLFVBQ0UsZUFBZSxRQUNYLGlCQUFnQixRQUFRLFNBQVMsZ0JBQWdCLFFBQVEsVUFDMUQsZUFBZSxRQUNmLGVBQWUsU0FBUyxlQUMzQjtBQUNBLG1CQUFXLGFBQWEsWUFBWSxFQUFFO0FBQ3RDLG1CQUFXLE1BQU87QUFBQSxNQUNuQjtBQUVELFVBQUksZ0JBQWdCLFFBQVEsT0FBTztBQUNqQyxZQUFJLHVCQUF1QixNQUFNO0FBQy9CLDZCQUFtQixvQkFBb0IsZUFBZSxZQUFZLGNBQWM7QUFDaEYsNkJBQW1CLG9CQUFvQixZQUFZLFlBQVksY0FBYztBQUFBLFFBQzlFO0FBQ0Qsc0JBQWMscUJBQXFCO0FBQUEsTUFDcEM7QUFFRCxVQUFJLGdCQUFnQixRQUFRLE9BQU87QUFDakMsaUJBQVMsb0JBQW9CLFdBQVcsWUFBWSxjQUFjO0FBQ2xFLHNCQUFjO0FBQUEsTUFDZjtBQUVELFVBQUksbUJBQW1CLFFBQVEsT0FBTztBQUNwQyxpQkFBUyxvQkFBb0IsU0FBUyxZQUFZLElBQUk7QUFDdEQsZ0JBQVEsVUFBVSxRQUFRLFFBQVEsTUFBTSxvQkFBb0IsUUFBUSxZQUFZLGNBQWM7QUFDOUYseUJBQWlCO0FBQUEsTUFDbEI7QUFFRCxjQUFRLFVBQVUsUUFBUSxRQUFRLE1BQU0sVUFBVSxPQUFPLGVBQWU7QUFBQSxJQUN6RTtBQUVELDBCQUF1QixLQUFLO0FBQzFCLHFCQUFlLEdBQUc7QUFDbEIsVUFBSSxjQUFjO0FBQUEsSUFDbkI7QUFFRCxvQkFBZ0IsTUFBTTtBQUNwQixjQUFRLElBQUk7QUFBQSxJQUNsQixDQUFLO0FBR0QsV0FBTyxPQUFPLE9BQU8sRUFBRSxPQUFPLFFBQU8sQ0FBRTtBQUV2QyxXQUFPLE1BQU07QUFDWCxVQUFJLFFBQVEsQ0FBRTtBQUVkLFlBQU0sU0FBUyxVQUFVLE1BQU0sS0FDN0IsRUFBRSxPQUFPO0FBQUEsUUFDUCxNQUFNLE1BQU07QUFBQSxRQUNaLE1BQU0sTUFBTSxVQUFVLFNBQVMsU0FBUyxVQUFVO0FBQUEsUUFDbEQsTUFBTTtBQUFBLFFBQ04sZUFBZTtBQUFBLE1BQ3pCLENBQVMsQ0FDRjtBQUVELGVBQVMsVUFBVSxRQUFRLE1BQU0sS0FDL0IsRUFBRSxRQUFRLEVBQUUsT0FBTyxRQUFPLEdBQUksQ0FBRSxNQUFNLE1BQU8sQ0FDOUM7QUFFRCxjQUFRLFdBQVcsTUFBTSxTQUFTLEtBQUs7QUFFdkMsVUFBSSxNQUFNLGNBQWMsVUFBVSxNQUFNLFVBQVUsT0FBTztBQUN2RCxjQUFNLEtBQ0osRUFBRSxPQUFPO0FBQUEsVUFDUCxNQUFNLE1BQU07QUFBQSxVQUNaLE9BQU8sTUFBTSxVQUFVLFNBQVMsU0FBUyxVQUFVO0FBQUEsVUFDbkQsTUFBTTtBQUFBLFVBQ04sZUFBZTtBQUFBLFFBQzNCLENBQVcsQ0FDRjtBQUFBLE1BQ0Y7QUFFRCxZQUFNLFFBQVE7QUFBQSxRQUNaLEVBQUUsUUFBUTtBQUFBLFVBQ1IsT0FBTztBQUFBLFVBQ1AsS0FBSztBQUFBLFFBQ2YsQ0FBUztBQUFBLE1BQ0Y7QUFFRCxVQUFJLE1BQU0sWUFBWSxRQUFRLE1BQU0sZUFBZSxRQUFRO0FBQ3pELGNBQU0sS0FDSixFQUFFLFFBQVE7QUFBQSxVQUNSLE9BQU87QUFBQSxRQUNuQixHQUFhO0FBQUEsVUFDRCxFQUFFLFFBQVE7QUFBQSxZQUNSLE9BQU8sd0NBQXlDLE9BQU0sbUJBQW1CLE9BQU8sMkJBQTJCO0FBQUEsWUFDM0csT0FBTyxnQkFBZ0I7QUFBQSxVQUNyQyxDQUFhO0FBQUEsUUFDYixDQUFXLENBQ0Y7QUFBQSxNQUNGO0FBRUQsWUFBTSxLQUNKLEVBQUUsUUFBUTtBQUFBLFFBQ1IsT0FBTyxnRUFBZ0UsYUFBYTtBQUFBLE1BQ3JGLEdBQUUsS0FBSyxDQUNUO0FBRUQsWUFBTSxZQUFZLFFBQVEsTUFBTSxLQUM5QixFQUFFLFlBQVk7QUFBQSxRQUNaLE1BQU07QUFBQSxNQUNoQixHQUFXLE1BQ0QsTUFBTSxZQUFZLE9BQ2Q7QUFBQSxRQUNFLEVBQUUsUUFBUTtBQUFBLFVBQ1IsS0FBSztBQUFBLFVBQ0wsT0FBTztBQUFBLFFBQ3pCLEdBQW1CLE1BQU0sWUFBWSxTQUFTLE1BQU0sUUFBTyxJQUFLLENBQUUsRUFBRSxRQUFRLEVBQUc7QUFBQSxNQUNoRSxJQUNELElBQ0wsQ0FDRjtBQUVELGFBQU8sZUFDTCxFQUNFLFFBQVEsT0FDUixVQUFVLE9BQ1YsS0FDRCxHQUNELENBQUU7QUFBQSxRQUNBO0FBQUEsUUFDQSxPQUFPO0FBQUEsUUFDUDtBQUFBLFFBQ0EsWUFBWTtBQUFBLE1BQ3RCLENBQVcsQ0FDSjtBQUFBLElBQ0Y7QUFBQSxFQUNGO0FBQ0gsQ0FBQzs7In0=
