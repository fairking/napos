import { Q as QPage } from "./QPage.2bcd042a.js";
import { _ as _export_sfc, I as defineComponent, J as openBlock, K as createBlock, L as withCtx, U as createBaseVNode } from "./index.b4b5ef26.js";
import "./render.c97de2b6.js";
const _sfc_main = defineComponent({
  name: "IndexPage",
  data() {
    return {
      themeOptions: [
        {
          label: "System",
          value: null
        },
        {
          label: "Light",
          value: false
        },
        {
          label: "Dark",
          value: true
        }
      ],
      settings: {
        theme: null
      }
    };
  },
  beforeMount() {
    this.setTheme();
  },
  methods: {
    setTheme() {
      var _a;
      this.$q.dark.set((_a = this.settings.theme) != null ? _a : "auto");
    },
    onThemeChange() {
      this.setTheme();
      this.onChange();
    },
    onChange() {
    }
  }
});
const _hoisted_1 = /* @__PURE__ */ createBaseVNode("h4", null, "About", -1);
const _hoisted_2 = /* @__PURE__ */ createBaseVNode("p", null, "Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit nihil praesentium molestias a adipisci, dolore vitae odit, quidem consequatur optio voluptates asperiores pariatur eos numquam rerum delectus commodi perferendis voluptate?", -1);
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createBlock(QPage, { padding: "" }, {
    default: withCtx(() => [
      _hoisted_1,
      _hoisted_2
    ]),
    _: 1
  });
}
var AboutPage = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "AboutPage.vue"]]);
export { AboutPage as default };
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQWJvdXRQYWdlLjU0M2MxMWJlLmpzIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvcGFnZXMvQWJvdXRQYWdlLnZ1ZSJdLCJzb3VyY2VzQ29udGVudCI6WyI8dGVtcGxhdGU+XG4gICAgPHEtcGFnZSBwYWRkaW5nPlxuICAgICAgICA8aDQ+QWJvdXQ8L2g0PlxuICAgICAgICA8cD5Mb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCBjb25zZWN0ZXR1ciBhZGlwaXNpY2luZyBlbGl0LiBGdWdpdCBuaWhpbCBwcmFlc2VudGl1bSBtb2xlc3RpYXMgYSBhZGlwaXNjaSwgZG9sb3JlIHZpdGFlIG9kaXQsIHF1aWRlbSBjb25zZXF1YXR1ciBvcHRpbyB2b2x1cHRhdGVzIGFzcGVyaW9yZXMgcGFyaWF0dXIgZW9zIG51bXF1YW0gcmVydW0gZGVsZWN0dXMgY29tbW9kaSBwZXJmZXJlbmRpcyB2b2x1cHRhdGU/PC9wPlxuICAgIDwvcS1wYWdlPlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdCBsYW5nPVwidHNcIj5cbiAgICBpbXBvcnQgeyBkZWZpbmVDb21wb25lbnQgfSBmcm9tICd2dWUnO1xuICAgIC8vaW1wb3J0IHsgQ3JlYXRlU3RvcmVGb3JtLCBTdG9yZUl0ZW0sIFN0b3JlU2VydmljZSB9IGZyb20gJ3NyYy9zZXJ2aWNlcy9kb21haW4nO1xuXG4gICAgZXhwb3J0IGRlZmF1bHQgZGVmaW5lQ29tcG9uZW50KHtcbiAgICAgICAgbmFtZTogJ0luZGV4UGFnZScsXG4gICAgICAgIGRhdGEoKSB7XG4gICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgIHRoZW1lT3B0aW9uczogW1xuICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICBsYWJlbDogJ1N5c3RlbScsXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogbnVsbFxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICBsYWJlbDogJ0xpZ2h0JyxcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBmYWxzZVxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICBsYWJlbDogJ0RhcmsnLFxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRydWVcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgc2V0dGluZ3M6IHtcbiAgICAgICAgICAgICAgICAgICAgdGhlbWU6IG51bGxcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgfTtcbiAgICAgICAgfSxcbiAgICAgICAgYmVmb3JlTW91bnQoKSB7XG4gICAgICAgICAgICB0aGlzLnNldFRoZW1lKCk7XG4gICAgICAgIH0sXG4gICAgICAgIG1ldGhvZHM6IHtcbiAgICAgICAgICAgIHNldFRoZW1lKCkge1xuICAgICAgICAgICAgICAgIHRoaXMuJHEuZGFyay5zZXQodGhpcy5zZXR0aW5ncy50aGVtZSA/PyAnYXV0bycpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIG9uVGhlbWVDaGFuZ2UoKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRUaGVtZSgpO1xuICAgICAgICAgICAgICAgIHRoaXMub25DaGFuZ2UoKTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBvbkNoYW5nZSgpIHtcbiAgICAgICAgICAgICAgICAvLyBTYXZlIHNldHRpbmdzXG4gICAgICAgICAgICB9LFxuICAgICAgICB9XG4gICAgfSk7XG48L3NjcmlwdD5cbiJdLCJuYW1lcyI6WyJfY3JlYXRlRWxlbWVudFZOb2RlIiwiX29wZW5CbG9jayIsIl9jcmVhdGVCbG9jayIsIl93aXRoQ3R4Il0sIm1hcHBpbmdzIjoiOzs7QUFXSSxNQUFLLFlBQWEsZ0JBQWE7QUFBQSxFQUMzQixNQUFNO0FBQUEsRUFDTixPQUFPO0FBQ0ksV0FBQTtBQUFBLE1BQ0gsY0FBYztBQUFBLFFBQ1Y7QUFBQSxVQUNJLE9BQU87QUFBQSxVQUNQLE9BQU87QUFBQSxRQUNYO0FBQUEsUUFDQTtBQUFBLFVBQ0ksT0FBTztBQUFBLFVBQ1AsT0FBTztBQUFBLFFBQ1g7QUFBQSxRQUNBO0FBQUEsVUFDSSxPQUFPO0FBQUEsVUFDUCxPQUFPO0FBQUEsUUFDWDtBQUFBLE1BQ0o7QUFBQSxNQUNBLFVBQVU7QUFBQSxRQUNOLE9BQU87QUFBQSxNQUNYO0FBQUEsSUFBQTtBQUFBLEVBRVI7QUFBQSxFQUNBLGNBQWM7QUFDVixTQUFLLFNBQVM7QUFBQSxFQUNsQjtBQUFBLEVBQ0EsU0FBUztBQUFBLElBQ0wsV0FBVzs7QUFDUCxXQUFLLEdBQUcsS0FBSyxJQUFJLFdBQUssU0FBUyxVQUFkLFlBQXVCLE1BQU07QUFBQSxJQUNsRDtBQUFBLElBQ0EsZ0JBQWdCO0FBQ1osV0FBSyxTQUFTO0FBQ2QsV0FBSyxTQUFTO0FBQUEsSUFDbEI7QUFBQSxJQUNBLFdBQVc7QUFBQSxJQUVYO0FBQUEsRUFDSjtBQUNKLENBQUM7QUEvQ0csTUFBQSxhQUFBQSxnQ0FBYyxZQUFWLFNBQUssRUFBQTtBQUNULE1BQUEsYUFBQUEsZ0NBQWtQLFdBQS9PLCtPQUEyTyxFQUFBOztBQUZsUCxTQUFBQyxVQUFBLEdBQUFDLFlBR1Msd0JBSE07QUFBQSxJQUFBLFNBQUFDLFFBQ1gsTUFBYztBQUFBLE1BQWQ7QUFBQSxNQUNBO0FBQUEsSUFBQSxDQUFBO0FBQUE7Ozs7OyJ9
