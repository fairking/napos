import { e as boot } from "./index.b4b5ef26.js";
import { S as SettingService } from "./domain.0d7d0906.js";
const version = "0.0.1-beta";
var settings = boot(async ({ app }) => {
  const settings2 = await SettingService.get();
  app.config.globalProperties.$settings = settings2;
  app.config.globalProperties.$version = version;
});
export { settings as default };
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0dGluZ3MuOTVkMWZkMDMuanMiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9ib290L3NldHRpbmdzLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGJvb3QgfSBmcm9tICdxdWFzYXIvd3JhcHBlcnMnO1xuaW1wb3J0IHsgU2V0dGluZ1NlcnZpY2UsIFNldHRpbmdzRm9ybSB9IGZyb20gJ3NyYy9zZXJ2aWNlcy9kb21haW4nO1xuaW1wb3J0IHsgdmVyc2lvbiB9IGZyb20gJy9wYWNrYWdlLmpzb24nO1xuXG5kZWNsYXJlIG1vZHVsZSAnQHZ1ZS9ydW50aW1lLWNvcmUnIHtcbiAgICBpbnRlcmZhY2UgQ29tcG9uZW50Q3VzdG9tUHJvcGVydGllcyB7XG4gICAgICAgICRzZXR0aW5nczogU2V0dGluZ3NGb3JtO1xuICAgIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgYm9vdChhc3luYyAoeyBhcHAgfSkgPT4ge1xuICAgIGNvbnN0IHNldHRpbmdzID0gYXdhaXQgU2V0dGluZ1NlcnZpY2UuZ2V0KCk7XG4gICAgYXBwLmNvbmZpZy5nbG9iYWxQcm9wZXJ0aWVzLiRzZXR0aW5ncyA9IHNldHRpbmdzO1xuICAgIGFwcC5jb25maWcuZ2xvYmFsUHJvcGVydGllcy4kdmVyc2lvbiA9IHZlcnNpb247XG59KTtcbiJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFVQSxJQUFBLFdBQWUsS0FBSyxPQUFPLEVBQUUsVUFBVTtBQUM3QixRQUFBLFlBQVcsTUFBTSxlQUFlO0FBQ2xDLE1BQUEsT0FBTyxpQkFBaUIsWUFBWTtBQUNwQyxNQUFBLE9BQU8saUJBQWlCLFdBQVc7QUFDM0MsQ0FBQzs7In0=
