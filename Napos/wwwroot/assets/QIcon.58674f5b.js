var __defProp = Object.defineProperty;
var __defProps = Object.defineProperties;
var __getOwnPropDescs = Object.getOwnPropertyDescriptors;
var __getOwnPropSymbols = Object.getOwnPropertySymbols;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __propIsEnum = Object.prototype.propertyIsEnumerable;
var __defNormalProp = (obj, key, value) => key in obj ? __defProp(obj, key, { enumerable: true, configurable: true, writable: true, value }) : obj[key] = value;
var __spreadValues = (a, b) => {
  for (var prop in b || (b = {}))
    if (__hasOwnProp.call(b, prop))
      __defNormalProp(a, prop, b[prop]);
  if (__getOwnPropSymbols)
    for (var prop of __getOwnPropSymbols(b)) {
      if (__propIsEnum.call(b, prop))
        __defNormalProp(a, prop, b[prop]);
    }
  return a;
};
var __spreadProps = (a, b) => __defProps(a, __getOwnPropDescs(b));
import { c as computed, h, g as getCurrentInstance } from "./index.b4b5ef26.js";
import { c as createComponent, h as hSlot, e as hMergeSlot } from "./render.c97de2b6.js";
const useSizeDefaults = {
  xs: 18,
  sm: 24,
  md: 32,
  lg: 38,
  xl: 46
};
const useSizeProps = {
  size: String
};
function useSize(props, sizes = useSizeDefaults) {
  return computed(() => props.size !== void 0 ? { fontSize: props.size in sizes ? `${sizes[props.size]}px` : props.size } : null);
}
const defaultViewBox = "0 0 24 24";
const sameFn = (i) => i;
const ionFn = (i) => `ionicons ${i}`;
const libMap = {
  "mdi-": (i) => `mdi ${i}`,
  "icon-": sameFn,
  "bt-": (i) => `bt ${i}`,
  "eva-": (i) => `eva ${i}`,
  "ion-md": ionFn,
  "ion-ios": ionFn,
  "ion-logo": ionFn,
  "iconfont ": sameFn,
  "ti-": (i) => `themify-icon ${i}`,
  "bi-": (i) => `bootstrap-icons ${i}`
};
const matMap = {
  o_: "-outlined",
  r_: "-round",
  s_: "-sharp"
};
const libRE = new RegExp("^(" + Object.keys(libMap).join("|") + ")");
const matRE = new RegExp("^(" + Object.keys(matMap).join("|") + ")");
const mRE = /^[Mm]\s?[-+]?\.?\d/;
const imgRE = /^img:/;
const svgUseRE = /^svguse:/;
const ionRE = /^ion-/;
const faRE = /^(fa-(solid|regular|light|brands|duotone|thin)|[lf]a[srlbdk]?) /;
var QIcon = createComponent({
  name: "QIcon",
  props: __spreadProps(__spreadValues({}, useSizeProps), {
    tag: {
      type: String,
      default: "i"
    },
    name: String,
    color: String,
    left: Boolean,
    right: Boolean
  }),
  setup(props, { slots }) {
    const { proxy: { $q } } = getCurrentInstance();
    const sizeStyle = useSize(props);
    const classes = computed(() => "q-icon" + (props.left === true ? " on-left" : "") + (props.right === true ? " on-right" : "") + (props.color !== void 0 ? ` text-${props.color}` : ""));
    const type = computed(() => {
      let cls;
      let icon = props.name;
      if (icon === "none" || !icon) {
        return { none: true };
      }
      if ($q.iconMapFn !== null) {
        const res = $q.iconMapFn(icon);
        if (res !== void 0) {
          if (res.icon !== void 0) {
            icon = res.icon;
            if (icon === "none" || !icon) {
              return { none: true };
            }
          } else {
            return {
              cls: res.cls,
              content: res.content !== void 0 ? res.content : " "
            };
          }
        }
      }
      if (mRE.test(icon) === true) {
        const [def, viewBox = defaultViewBox] = icon.split("|");
        return {
          svg: true,
          viewBox,
          nodes: def.split("&&").map((path) => {
            const [d, style, transform] = path.split("@@");
            return h("path", { style, d, transform });
          })
        };
      }
      if (imgRE.test(icon) === true) {
        return {
          img: true,
          src: icon.substring(4)
        };
      }
      if (svgUseRE.test(icon) === true) {
        const [def, viewBox = defaultViewBox] = icon.split("|");
        return {
          svguse: true,
          src: def.substring(7),
          viewBox
        };
      }
      let content = " ";
      const matches = icon.match(libRE);
      if (matches !== null) {
        cls = libMap[matches[1]](icon);
      } else if (faRE.test(icon) === true) {
        cls = icon;
      } else if (ionRE.test(icon) === true) {
        cls = `ionicons ion-${$q.platform.is.ios === true ? "ios" : "md"}${icon.substring(3)}`;
      } else {
        cls = "notranslate material-icons";
        const matches2 = icon.match(matRE);
        if (matches2 !== null) {
          icon = icon.substring(2);
          cls += matMap[matches2[1]];
        }
        content = icon;
      }
      return {
        cls,
        content
      };
    });
    return () => {
      const data = {
        class: classes.value,
        style: sizeStyle.value,
        "aria-hidden": "true",
        role: "presentation"
      };
      if (type.value.none === true) {
        return h(props.tag, data, hSlot(slots.default));
      }
      if (type.value.img === true) {
        return h("span", data, hMergeSlot(slots.default, [
          h("img", { src: type.value.src })
        ]));
      }
      if (type.value.svg === true) {
        return h("span", data, hMergeSlot(slots.default, [
          h("svg", {
            viewBox: type.value.viewBox
          }, type.value.nodes)
        ]));
      }
      if (type.value.svguse === true) {
        return h("span", data, hMergeSlot(slots.default, [
          h("svg", {
            viewBox: type.value.viewBox
          }, [
            h("use", { "xlink:href": type.value.src })
          ])
        ]));
      }
      if (type.value.cls !== void 0) {
        data.class += " " + type.value.cls;
      }
      return h(props.tag, data, hMergeSlot(slots.default, [
        type.value.content
      ]));
    };
  }
});
export { QIcon as Q, useSize as a, useSizeProps as b, useSizeDefaults as u };
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUUljb24uNTg2NzRmNWIuanMiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9xdWFzYXIvc3JjL2NvbXBvc2FibGVzL3ByaXZhdGUvdXNlLXNpemUuanMiLCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvcXVhc2FyL3NyYy9jb21wb25lbnRzL2ljb24vUUljb24uanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgY29tcHV0ZWQgfSBmcm9tICd2dWUnXG5cbmV4cG9ydCBjb25zdCB1c2VTaXplRGVmYXVsdHMgPSB7XG4gIHhzOiAxOCxcbiAgc206IDI0LFxuICBtZDogMzIsXG4gIGxnOiAzOCxcbiAgeGw6IDQ2XG59XG5cbmV4cG9ydCBjb25zdCB1c2VTaXplUHJvcHMgPSB7XG4gIHNpemU6IFN0cmluZ1xufVxuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiAocHJvcHMsIHNpemVzID0gdXNlU2l6ZURlZmF1bHRzKSB7XG4gIC8vIHJldHVybiBzaXplU3R5bGVcbiAgcmV0dXJuIGNvbXB1dGVkKCgpID0+IChcbiAgICBwcm9wcy5zaXplICE9PSB2b2lkIDBcbiAgICAgID8geyBmb250U2l6ZTogcHJvcHMuc2l6ZSBpbiBzaXplcyA/IGAkeyBzaXplc1sgcHJvcHMuc2l6ZSBdIH1weGAgOiBwcm9wcy5zaXplIH1cbiAgICAgIDogbnVsbFxuICApKVxufVxuIiwiaW1wb3J0IHsgaCwgY29tcHV0ZWQsIGdldEN1cnJlbnRJbnN0YW5jZSB9IGZyb20gJ3Z1ZSdcblxuaW1wb3J0IHVzZVNpemUsIHsgdXNlU2l6ZVByb3BzIH0gZnJvbSAnLi4vLi4vY29tcG9zYWJsZXMvcHJpdmF0ZS91c2Utc2l6ZS5qcydcblxuaW1wb3J0IHsgY3JlYXRlQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vdXRpbHMvcHJpdmF0ZS9jcmVhdGUuanMnXG5pbXBvcnQgeyBoU2xvdCwgaE1lcmdlU2xvdCB9IGZyb20gJy4uLy4uL3V0aWxzL3ByaXZhdGUvcmVuZGVyLmpzJ1xuXG5jb25zdCBkZWZhdWx0Vmlld0JveCA9ICcwIDAgMjQgMjQnXG5cbmNvbnN0IHNhbWVGbiA9IGkgPT4gaVxuY29uc3QgaW9uRm4gPSBpID0+IGBpb25pY29ucyAkeyBpIH1gXG5cbmNvbnN0IGxpYk1hcCA9IHtcbiAgJ21kaS0nOiBpID0+IGBtZGkgJHsgaSB9YCxcbiAgJ2ljb24tJzogc2FtZUZuLCAvLyBmb250YXdlc29tZSBlcXVpdlxuICAnYnQtJzogaSA9PiBgYnQgJHsgaSB9YCxcbiAgJ2V2YS0nOiBpID0+IGBldmEgJHsgaSB9YCxcbiAgJ2lvbi1tZCc6IGlvbkZuLFxuICAnaW9uLWlvcyc6IGlvbkZuLFxuICAnaW9uLWxvZ28nOiBpb25GbixcbiAgJ2ljb25mb250ICc6IHNhbWVGbixcbiAgJ3RpLSc6IGkgPT4gYHRoZW1pZnktaWNvbiAkeyBpIH1gLFxuICAnYmktJzogaSA9PiBgYm9vdHN0cmFwLWljb25zICR7IGkgfWBcbn1cblxuY29uc3QgbWF0TWFwID0ge1xuICBvXzogJy1vdXRsaW5lZCcsXG4gIHJfOiAnLXJvdW5kJyxcbiAgc186ICctc2hhcnAnXG59XG5cbmNvbnN0IGxpYlJFID0gbmV3IFJlZ0V4cCgnXignICsgT2JqZWN0LmtleXMobGliTWFwKS5qb2luKCd8JykgKyAnKScpXG5jb25zdCBtYXRSRSA9IG5ldyBSZWdFeHAoJ14oJyArIE9iamVjdC5rZXlzKG1hdE1hcCkuam9pbignfCcpICsgJyknKVxuY29uc3QgbVJFID0gL15bTW1dXFxzP1stK10/XFwuP1xcZC9cbmNvbnN0IGltZ1JFID0gL15pbWc6L1xuY29uc3Qgc3ZnVXNlUkUgPSAvXnN2Z3VzZTovXG5jb25zdCBpb25SRSA9IC9eaW9uLS9cbmNvbnN0IGZhUkUgPSAvXihmYS0oc29saWR8cmVndWxhcnxsaWdodHxicmFuZHN8ZHVvdG9uZXx0aGluKXxbbGZdYVtzcmxiZGtdPykgL1xuXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVDb21wb25lbnQoe1xuICBuYW1lOiAnUUljb24nLFxuXG4gIHByb3BzOiB7XG4gICAgLi4udXNlU2l6ZVByb3BzLFxuXG4gICAgdGFnOiB7XG4gICAgICB0eXBlOiBTdHJpbmcsXG4gICAgICBkZWZhdWx0OiAnaSdcbiAgICB9LFxuXG4gICAgbmFtZTogU3RyaW5nLFxuICAgIGNvbG9yOiBTdHJpbmcsXG4gICAgbGVmdDogQm9vbGVhbixcbiAgICByaWdodDogQm9vbGVhblxuICB9LFxuXG4gIHNldHVwIChwcm9wcywgeyBzbG90cyB9KSB7XG4gICAgY29uc3QgeyBwcm94eTogeyAkcSB9IH0gPSBnZXRDdXJyZW50SW5zdGFuY2UoKVxuICAgIGNvbnN0IHNpemVTdHlsZSA9IHVzZVNpemUocHJvcHMpXG5cbiAgICBjb25zdCBjbGFzc2VzID0gY29tcHV0ZWQoKCkgPT5cbiAgICAgICdxLWljb24nXG4gICAgICArIChwcm9wcy5sZWZ0ID09PSB0cnVlID8gJyBvbi1sZWZ0JyA6ICcnKSAvLyBUT0RPIFF2MzogZHJvcCB0aGlzXG4gICAgICArIChwcm9wcy5yaWdodCA9PT0gdHJ1ZSA/ICcgb24tcmlnaHQnIDogJycpXG4gICAgICArIChwcm9wcy5jb2xvciAhPT0gdm9pZCAwID8gYCB0ZXh0LSR7IHByb3BzLmNvbG9yIH1gIDogJycpXG4gICAgKVxuXG4gICAgY29uc3QgdHlwZSA9IGNvbXB1dGVkKCgpID0+IHtcbiAgICAgIGxldCBjbHNcbiAgICAgIGxldCBpY29uID0gcHJvcHMubmFtZVxuXG4gICAgICBpZiAoaWNvbiA9PT0gJ25vbmUnIHx8ICFpY29uKSB7XG4gICAgICAgIHJldHVybiB7IG5vbmU6IHRydWUgfVxuICAgICAgfVxuXG4gICAgICBpZiAoJHEuaWNvbk1hcEZuICE9PSBudWxsKSB7XG4gICAgICAgIGNvbnN0IHJlcyA9ICRxLmljb25NYXBGbihpY29uKVxuICAgICAgICBpZiAocmVzICE9PSB2b2lkIDApIHtcbiAgICAgICAgICBpZiAocmVzLmljb24gIT09IHZvaWQgMCkge1xuICAgICAgICAgICAgaWNvbiA9IHJlcy5pY29uXG4gICAgICAgICAgICBpZiAoaWNvbiA9PT0gJ25vbmUnIHx8ICFpY29uKSB7XG4gICAgICAgICAgICAgIHJldHVybiB7IG5vbmU6IHRydWUgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgIGNsczogcmVzLmNscyxcbiAgICAgICAgICAgICAgY29udGVudDogcmVzLmNvbnRlbnQgIT09IHZvaWQgMFxuICAgICAgICAgICAgICAgID8gcmVzLmNvbnRlbnRcbiAgICAgICAgICAgICAgICA6ICcgJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpZiAobVJFLnRlc3QoaWNvbikgPT09IHRydWUpIHtcbiAgICAgICAgY29uc3QgWyBkZWYsIHZpZXdCb3ggPSBkZWZhdWx0Vmlld0JveCBdID0gaWNvbi5zcGxpdCgnfCcpXG5cbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICBzdmc6IHRydWUsXG4gICAgICAgICAgdmlld0JveCxcbiAgICAgICAgICBub2RlczogZGVmLnNwbGl0KCcmJicpLm1hcChwYXRoID0+IHtcbiAgICAgICAgICAgIGNvbnN0IFsgZCwgc3R5bGUsIHRyYW5zZm9ybSBdID0gcGF0aC5zcGxpdCgnQEAnKVxuICAgICAgICAgICAgcmV0dXJuIGgoJ3BhdGgnLCB7IHN0eWxlLCBkLCB0cmFuc2Zvcm0gfSlcbiAgICAgICAgICB9KVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmIChpbWdSRS50ZXN0KGljb24pID09PSB0cnVlKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgaW1nOiB0cnVlLFxuICAgICAgICAgIHNyYzogaWNvbi5zdWJzdHJpbmcoNClcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpZiAoc3ZnVXNlUkUudGVzdChpY29uKSA9PT0gdHJ1ZSkge1xuICAgICAgICBjb25zdCBbIGRlZiwgdmlld0JveCA9IGRlZmF1bHRWaWV3Qm94IF0gPSBpY29uLnNwbGl0KCd8JylcblxuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIHN2Z3VzZTogdHJ1ZSxcbiAgICAgICAgICBzcmM6IGRlZi5zdWJzdHJpbmcoNyksXG4gICAgICAgICAgdmlld0JveFxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGxldCBjb250ZW50ID0gJyAnXG4gICAgICBjb25zdCBtYXRjaGVzID0gaWNvbi5tYXRjaChsaWJSRSlcblxuICAgICAgaWYgKG1hdGNoZXMgIT09IG51bGwpIHtcbiAgICAgICAgY2xzID0gbGliTWFwWyBtYXRjaGVzWyAxIF0gXShpY29uKVxuICAgICAgfVxuICAgICAgZWxzZSBpZiAoZmFSRS50ZXN0KGljb24pID09PSB0cnVlKSB7XG4gICAgICAgIGNscyA9IGljb25cbiAgICAgIH1cbiAgICAgIGVsc2UgaWYgKGlvblJFLnRlc3QoaWNvbikgPT09IHRydWUpIHtcbiAgICAgICAgY2xzID0gYGlvbmljb25zIGlvbi0keyAkcS5wbGF0Zm9ybS5pcy5pb3MgPT09IHRydWUgPyAnaW9zJyA6ICdtZCcgfSR7IGljb24uc3Vic3RyaW5nKDMpIH1gXG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgLy8gXCJub3RyYW5zbGF0ZVwiIGNsYXNzIGlzIGZvciBHb29nbGUgVHJhbnNsYXRlXG4gICAgICAgIC8vIHRvIGF2b2lkIHRhbXBlcmluZyB3aXRoIE1hdGVyaWFsIEljb25zIGxpZ2F0dXJlIGZvbnRcbiAgICAgICAgLy9cbiAgICAgICAgLy8gQ2F1dGlvbjogVG8gYmUgYWJsZSB0byBhZGQgc3VmZml4IHRvIHRoZSBjbGFzcyBuYW1lLFxuICAgICAgICAvLyBrZWVwIHRoZSAnbWF0ZXJpYWwtaWNvbnMnIGF0IHRoZSBlbmQgb2YgdGhlIHN0cmluZy5cbiAgICAgICAgY2xzID0gJ25vdHJhbnNsYXRlIG1hdGVyaWFsLWljb25zJ1xuXG4gICAgICAgIGNvbnN0IG1hdGNoZXMgPSBpY29uLm1hdGNoKG1hdFJFKVxuICAgICAgICBpZiAobWF0Y2hlcyAhPT0gbnVsbCkge1xuICAgICAgICAgIGljb24gPSBpY29uLnN1YnN0cmluZygyKVxuICAgICAgICAgIGNscyArPSBtYXRNYXBbIG1hdGNoZXNbIDEgXSBdXG4gICAgICAgIH1cblxuICAgICAgICBjb250ZW50ID0gaWNvblxuICAgICAgfVxuXG4gICAgICByZXR1cm4ge1xuICAgICAgICBjbHMsXG4gICAgICAgIGNvbnRlbnRcbiAgICAgIH1cbiAgICB9KVxuXG4gICAgcmV0dXJuICgpID0+IHtcbiAgICAgIGNvbnN0IGRhdGEgPSB7XG4gICAgICAgIGNsYXNzOiBjbGFzc2VzLnZhbHVlLFxuICAgICAgICBzdHlsZTogc2l6ZVN0eWxlLnZhbHVlLFxuICAgICAgICAnYXJpYS1oaWRkZW4nOiAndHJ1ZScsXG4gICAgICAgIHJvbGU6ICdwcmVzZW50YXRpb24nXG4gICAgICB9XG5cbiAgICAgIGlmICh0eXBlLnZhbHVlLm5vbmUgPT09IHRydWUpIHtcbiAgICAgICAgcmV0dXJuIGgocHJvcHMudGFnLCBkYXRhLCBoU2xvdChzbG90cy5kZWZhdWx0KSlcbiAgICAgIH1cblxuICAgICAgaWYgKHR5cGUudmFsdWUuaW1nID09PSB0cnVlKSB7XG4gICAgICAgIHJldHVybiBoKCdzcGFuJywgZGF0YSwgaE1lcmdlU2xvdChzbG90cy5kZWZhdWx0LCBbXG4gICAgICAgICAgaCgnaW1nJywgeyBzcmM6IHR5cGUudmFsdWUuc3JjIH0pXG4gICAgICAgIF0pKVxuICAgICAgfVxuXG4gICAgICBpZiAodHlwZS52YWx1ZS5zdmcgPT09IHRydWUpIHtcbiAgICAgICAgcmV0dXJuIGgoJ3NwYW4nLCBkYXRhLCBoTWVyZ2VTbG90KHNsb3RzLmRlZmF1bHQsIFtcbiAgICAgICAgICBoKCdzdmcnLCB7XG4gICAgICAgICAgICB2aWV3Qm94OiB0eXBlLnZhbHVlLnZpZXdCb3hcbiAgICAgICAgICB9LCB0eXBlLnZhbHVlLm5vZGVzKVxuICAgICAgICBdKSlcbiAgICAgIH1cblxuICAgICAgaWYgKHR5cGUudmFsdWUuc3ZndXNlID09PSB0cnVlKSB7XG4gICAgICAgIHJldHVybiBoKCdzcGFuJywgZGF0YSwgaE1lcmdlU2xvdChzbG90cy5kZWZhdWx0LCBbXG4gICAgICAgICAgaCgnc3ZnJywge1xuICAgICAgICAgICAgdmlld0JveDogdHlwZS52YWx1ZS52aWV3Qm94XG4gICAgICAgICAgfSwgW1xuICAgICAgICAgICAgaCgndXNlJywgeyAneGxpbms6aHJlZic6IHR5cGUudmFsdWUuc3JjIH0pXG4gICAgICAgICAgXSlcbiAgICAgICAgXSkpXG4gICAgICB9XG5cbiAgICAgIGlmICh0eXBlLnZhbHVlLmNscyAhPT0gdm9pZCAwKSB7XG4gICAgICAgIGRhdGEuY2xhc3MgKz0gJyAnICsgdHlwZS52YWx1ZS5jbHNcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGgocHJvcHMudGFnLCBkYXRhLCBoTWVyZ2VTbG90KHNsb3RzLmRlZmF1bHQsIFtcbiAgICAgICAgdHlwZS52YWx1ZS5jb250ZW50XG4gICAgICBdKSlcbiAgICB9XG4gIH1cbn0pXG4iXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRVksTUFBQyxrQkFBa0I7QUFBQSxFQUM3QixJQUFJO0FBQUEsRUFDSixJQUFJO0FBQUEsRUFDSixJQUFJO0FBQUEsRUFDSixJQUFJO0FBQUEsRUFDSixJQUFJO0FBQ047QUFFWSxNQUFDLGVBQWU7QUFBQSxFQUMxQixNQUFNO0FBQ1I7QUFFZSxpQkFBVSxPQUFPLFFBQVEsaUJBQWlCO0FBRXZELFNBQU8sU0FBUyxNQUNkLE1BQU0sU0FBUyxTQUNYLEVBQUUsVUFBVSxNQUFNLFFBQVEsUUFBUSxHQUFJLE1BQU8sTUFBTSxZQUFjLE1BQU0sS0FBTSxJQUM3RSxJQUNMO0FBQ0g7QUNkQSxNQUFNLGlCQUFpQjtBQUV2QixNQUFNLFNBQVMsT0FBSztBQUNwQixNQUFNLFFBQVEsT0FBSyxZQUFhO0FBRWhDLE1BQU0sU0FBUztBQUFBLEVBQ2IsUUFBUSxPQUFLLE9BQVE7QUFBQSxFQUNyQixTQUFTO0FBQUEsRUFDVCxPQUFPLE9BQUssTUFBTztBQUFBLEVBQ25CLFFBQVEsT0FBSyxPQUFRO0FBQUEsRUFDckIsVUFBVTtBQUFBLEVBQ1YsV0FBVztBQUFBLEVBQ1gsWUFBWTtBQUFBLEVBQ1osYUFBYTtBQUFBLEVBQ2IsT0FBTyxPQUFLLGdCQUFpQjtBQUFBLEVBQzdCLE9BQU8sT0FBSyxtQkFBb0I7QUFDbEM7QUFFQSxNQUFNLFNBQVM7QUFBQSxFQUNiLElBQUk7QUFBQSxFQUNKLElBQUk7QUFBQSxFQUNKLElBQUk7QUFDTjtBQUVBLE1BQU0sUUFBUSxJQUFJLE9BQU8sT0FBTyxPQUFPLEtBQUssTUFBTSxFQUFFLEtBQUssR0FBRyxJQUFJLEdBQUc7QUFDbkUsTUFBTSxRQUFRLElBQUksT0FBTyxPQUFPLE9BQU8sS0FBSyxNQUFNLEVBQUUsS0FBSyxHQUFHLElBQUksR0FBRztBQUNuRSxNQUFNLE1BQU07QUFDWixNQUFNLFFBQVE7QUFDZCxNQUFNLFdBQVc7QUFDakIsTUFBTSxRQUFRO0FBQ2QsTUFBTSxPQUFPO0FBRWIsSUFBQSxRQUFlLGdCQUFnQjtBQUFBLEVBQzdCLE1BQU07QUFBQSxFQUVOLE9BQU8saUNBQ0YsZUFERTtBQUFBLElBR0wsS0FBSztBQUFBLE1BQ0gsTUFBTTtBQUFBLE1BQ04sU0FBUztBQUFBLElBQ1Y7QUFBQSxJQUVELE1BQU07QUFBQSxJQUNOLE9BQU87QUFBQSxJQUNQLE1BQU07QUFBQSxJQUNOLE9BQU87QUFBQSxFQUNSO0FBQUEsRUFFRCxNQUFPLE9BQU8sRUFBRSxTQUFTO0FBQ3ZCLFVBQU0sRUFBRSxPQUFPLEVBQUUsU0FBUyxtQkFBb0I7QUFDOUMsVUFBTSxZQUFZLFFBQVEsS0FBSztBQUUvQixVQUFNLFVBQVUsU0FBUyxNQUN2QixXQUNHLE9BQU0sU0FBUyxPQUFPLGFBQWEsTUFDbkMsT0FBTSxVQUFVLE9BQU8sY0FBYyxNQUNyQyxPQUFNLFVBQVUsU0FBUyxTQUFVLE1BQU0sVUFBVyxHQUN4RDtBQUVELFVBQU0sT0FBTyxTQUFTLE1BQU07QUFDMUIsVUFBSTtBQUNKLFVBQUksT0FBTyxNQUFNO0FBRWpCLFVBQUksU0FBUyxVQUFVLENBQUMsTUFBTTtBQUM1QixlQUFPLEVBQUUsTUFBTSxLQUFNO0FBQUEsTUFDdEI7QUFFRCxVQUFJLEdBQUcsY0FBYyxNQUFNO0FBQ3pCLGNBQU0sTUFBTSxHQUFHLFVBQVUsSUFBSTtBQUM3QixZQUFJLFFBQVEsUUFBUTtBQUNsQixjQUFJLElBQUksU0FBUyxRQUFRO0FBQ3ZCLG1CQUFPLElBQUk7QUFDWCxnQkFBSSxTQUFTLFVBQVUsQ0FBQyxNQUFNO0FBQzVCLHFCQUFPLEVBQUUsTUFBTSxLQUFNO0FBQUEsWUFDdEI7QUFBQSxVQUNGLE9BQ0k7QUFDSCxtQkFBTztBQUFBLGNBQ0wsS0FBSyxJQUFJO0FBQUEsY0FDVCxTQUFTLElBQUksWUFBWSxTQUNyQixJQUFJLFVBQ0o7QUFBQSxZQUNMO0FBQUEsVUFDRjtBQUFBLFFBQ0Y7QUFBQSxNQUNGO0FBRUQsVUFBSSxJQUFJLEtBQUssSUFBSSxNQUFNLE1BQU07QUFDM0IsY0FBTSxDQUFFLEtBQUssVUFBVSxrQkFBbUIsS0FBSyxNQUFNLEdBQUc7QUFFeEQsZUFBTztBQUFBLFVBQ0wsS0FBSztBQUFBLFVBQ0w7QUFBQSxVQUNBLE9BQU8sSUFBSSxNQUFNLElBQUksRUFBRSxJQUFJLFVBQVE7QUFDakMsa0JBQU0sQ0FBRSxHQUFHLE9BQU8sYUFBYyxLQUFLLE1BQU0sSUFBSTtBQUMvQyxtQkFBTyxFQUFFLFFBQVEsRUFBRSxPQUFPLEdBQUcsVUFBUyxDQUFFO0FBQUEsVUFDcEQsQ0FBVztBQUFBLFFBQ0Y7QUFBQSxNQUNGO0FBRUQsVUFBSSxNQUFNLEtBQUssSUFBSSxNQUFNLE1BQU07QUFDN0IsZUFBTztBQUFBLFVBQ0wsS0FBSztBQUFBLFVBQ0wsS0FBSyxLQUFLLFVBQVUsQ0FBQztBQUFBLFFBQ3RCO0FBQUEsTUFDRjtBQUVELFVBQUksU0FBUyxLQUFLLElBQUksTUFBTSxNQUFNO0FBQ2hDLGNBQU0sQ0FBRSxLQUFLLFVBQVUsa0JBQW1CLEtBQUssTUFBTSxHQUFHO0FBRXhELGVBQU87QUFBQSxVQUNMLFFBQVE7QUFBQSxVQUNSLEtBQUssSUFBSSxVQUFVLENBQUM7QUFBQSxVQUNwQjtBQUFBLFFBQ0Q7QUFBQSxNQUNGO0FBRUQsVUFBSSxVQUFVO0FBQ2QsWUFBTSxVQUFVLEtBQUssTUFBTSxLQUFLO0FBRWhDLFVBQUksWUFBWSxNQUFNO0FBQ3BCLGNBQU0sT0FBUSxRQUFTLElBQU0sSUFBSTtBQUFBLE1BQ2xDLFdBQ1EsS0FBSyxLQUFLLElBQUksTUFBTSxNQUFNO0FBQ2pDLGNBQU07QUFBQSxNQUNQLFdBQ1EsTUFBTSxLQUFLLElBQUksTUFBTSxNQUFNO0FBQ2xDLGNBQU0sZ0JBQWlCLEdBQUcsU0FBUyxHQUFHLFFBQVEsT0FBTyxRQUFRLE9BQVMsS0FBSyxVQUFVLENBQUM7QUFBQSxNQUN2RixPQUNJO0FBTUgsY0FBTTtBQUVOLGNBQU0sV0FBVSxLQUFLLE1BQU0sS0FBSztBQUNoQyxZQUFJLGFBQVksTUFBTTtBQUNwQixpQkFBTyxLQUFLLFVBQVUsQ0FBQztBQUN2QixpQkFBTyxPQUFRLFNBQVM7QUFBQSxRQUN6QjtBQUVELGtCQUFVO0FBQUEsTUFDWDtBQUVELGFBQU87QUFBQSxRQUNMO0FBQUEsUUFDQTtBQUFBLE1BQ0Q7QUFBQSxJQUNQLENBQUs7QUFFRCxXQUFPLE1BQU07QUFDWCxZQUFNLE9BQU87QUFBQSxRQUNYLE9BQU8sUUFBUTtBQUFBLFFBQ2YsT0FBTyxVQUFVO0FBQUEsUUFDakIsZUFBZTtBQUFBLFFBQ2YsTUFBTTtBQUFBLE1BQ1A7QUFFRCxVQUFJLEtBQUssTUFBTSxTQUFTLE1BQU07QUFDNUIsZUFBTyxFQUFFLE1BQU0sS0FBSyxNQUFNLE1BQU0sTUFBTSxPQUFPLENBQUM7QUFBQSxNQUMvQztBQUVELFVBQUksS0FBSyxNQUFNLFFBQVEsTUFBTTtBQUMzQixlQUFPLEVBQUUsUUFBUSxNQUFNLFdBQVcsTUFBTSxTQUFTO0FBQUEsVUFDL0MsRUFBRSxPQUFPLEVBQUUsS0FBSyxLQUFLLE1BQU0sS0FBSztBQUFBLFFBQzFDLENBQVMsQ0FBQztBQUFBLE1BQ0g7QUFFRCxVQUFJLEtBQUssTUFBTSxRQUFRLE1BQU07QUFDM0IsZUFBTyxFQUFFLFFBQVEsTUFBTSxXQUFXLE1BQU0sU0FBUztBQUFBLFVBQy9DLEVBQUUsT0FBTztBQUFBLFlBQ1AsU0FBUyxLQUFLLE1BQU07QUFBQSxVQUNoQyxHQUFhLEtBQUssTUFBTSxLQUFLO0FBQUEsUUFDN0IsQ0FBUyxDQUFDO0FBQUEsTUFDSDtBQUVELFVBQUksS0FBSyxNQUFNLFdBQVcsTUFBTTtBQUM5QixlQUFPLEVBQUUsUUFBUSxNQUFNLFdBQVcsTUFBTSxTQUFTO0FBQUEsVUFDL0MsRUFBRSxPQUFPO0FBQUEsWUFDUCxTQUFTLEtBQUssTUFBTTtBQUFBLFVBQ2hDLEdBQWE7QUFBQSxZQUNELEVBQUUsT0FBTyxFQUFFLGNBQWMsS0FBSyxNQUFNLEtBQUs7QUFBQSxVQUNyRCxDQUFXO0FBQUEsUUFDWCxDQUFTLENBQUM7QUFBQSxNQUNIO0FBRUQsVUFBSSxLQUFLLE1BQU0sUUFBUSxRQUFRO0FBQzdCLGFBQUssU0FBUyxNQUFNLEtBQUssTUFBTTtBQUFBLE1BQ2hDO0FBRUQsYUFBTyxFQUFFLE1BQU0sS0FBSyxNQUFNLFdBQVcsTUFBTSxTQUFTO0FBQUEsUUFDbEQsS0FBSyxNQUFNO0FBQUEsTUFDbkIsQ0FBTyxDQUFDO0FBQUEsSUFDSDtBQUFBLEVBQ0Y7QUFDSCxDQUFDOzsifQ==
