import { e as boot } from "./index.b4b5ef26.js";
import { L as LogService, C as ClientErrorVm } from "./domain.0d7d0906.js";
var errorlog = boot(async ({ app }) => {
  app.config.errorHandler = function(error, _, info) {
    LogService.logClientError({ body: new ClientErrorVm({ Message: "boot error" }) });
  };
});
export { errorlog as default };
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXJyb3Jsb2cuZTQ0ZDVjNTEuanMiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9ib290L2Vycm9ybG9nLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGJvb3QgfSBmcm9tICdxdWFzYXIvd3JhcHBlcnMnO1xuaW1wb3J0IHsgTG9nU2VydmljZSwgQ2xpZW50RXJyb3JWbSB9IGZyb20gJ3NyYy9zZXJ2aWNlcy9kb21haW4nO1xuXG5leHBvcnQgZGVmYXVsdCBib290KGFzeW5jICh7IGFwcCB9KSA9PiB7XG4gICAgYXBwLmNvbmZpZy5lcnJvckhhbmRsZXIgPSBmdW5jdGlvbiAoZXJyb3IsIF8sIGluZm8pIHtcbiAgICAgICAgTG9nU2VydmljZS5sb2dDbGllbnRFcnJvcih7IGJvZHk6IG5ldyBDbGllbnRFcnJvclZtKHsgTWVzc2FnZTogJ2Jvb3QgZXJyb3InIH0pIH0pO1xuICAgIH07XG59KTtcbiJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUdBLElBQUEsV0FBZSxLQUFLLE9BQU8sRUFBRSxVQUFVO0FBQ25DLE1BQUksT0FBTyxlQUFlLFNBQVUsT0FBTyxHQUFHLE1BQU07QUFDckMsZUFBQSxlQUFlLEVBQUUsTUFBTSxJQUFJLGNBQWMsRUFBRSxTQUFTLGNBQWMsRUFBQSxDQUFHO0FBQUEsRUFBQTtBQUV4RixDQUFDOzsifQ==
