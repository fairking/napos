var __defProp = Object.defineProperty;
var __defProps = Object.defineProperties;
var __getOwnPropDescs = Object.getOwnPropertyDescriptors;
var __getOwnPropSymbols = Object.getOwnPropertySymbols;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __propIsEnum = Object.prototype.propertyIsEnumerable;
var __defNormalProp = (obj, key, value) => key in obj ? __defProp(obj, key, { enumerable: true, configurable: true, writable: true, value }) : obj[key] = value;
var __spreadValues = (a, b) => {
  for (var prop in b || (b = {}))
    if (__hasOwnProp.call(b, prop))
      __defNormalProp(a, prop, b[prop]);
  if (__getOwnPropSymbols)
    for (var prop of __getOwnPropSymbols(b)) {
      if (__propIsEnum.call(b, prop))
        __defNormalProp(a, prop, b[prop]);
    }
  return a;
};
var __spreadProps = (a, b) => __defProps(a, __getOwnPropDescs(b));
import { b as useSizeProps, a as useSize, Q as QIcon } from "./QIcon.58674f5b.js";
import { u as useDarkProps, a as useDark } from "./use-dark.a3ae9942.js";
import { r as ref, c as computed, h, g as getCurrentInstance, s as stopAndPrevent, _ as _export_sfc, I as defineComponent, J as openBlock, K as createBlock, L as withCtx, d as createVNode, U as createBaseVNode } from "./index.b4b5ef26.js";
import { u as useFormProps, b as useFormInject } from "./use-form.2420a798.js";
import { c as createComponent, e as hMergeSlot, h as hSlot } from "./render.c97de2b6.js";
import { Q as QPage } from "./QPage.2bcd042a.js";
import { S as SettingService } from "./domain.0d7d0906.js";
function useRefocusTarget(props, rootRef) {
  const refocusRef = ref(null);
  const refocusTargetEl = computed(() => {
    if (props.disable !== true) {
      return null;
    }
    return h("span", {
      ref: refocusRef,
      class: "no-outline",
      tabindex: -1
    });
  });
  function refocusTarget(e) {
    const root = rootRef.value;
    if (e !== void 0 && e.type.indexOf("key") === 0) {
      if (root !== null && document.activeElement !== root && root.contains(document.activeElement) === true) {
        root.focus();
      }
    } else if (refocusRef.value !== null && (e === void 0 || root !== null && root.contains(e.target) === true)) {
      refocusRef.value.focus();
    }
  }
  return {
    refocusTargetEl,
    refocusTarget
  };
}
var optionSizes = {
  xs: 30,
  sm: 35,
  md: 40,
  lg: 50,
  xl: 60
};
const svg = h("svg", {
  key: "svg",
  class: "q-radio__bg absolute non-selectable",
  viewBox: "0 0 24 24",
  "aria-hidden": "true"
}, [
  h("path", {
    d: "M12,22a10,10 0 0 1 -10,-10a10,10 0 0 1 10,-10a10,10 0 0 1 10,10a10,10 0 0 1 -10,10m0,-22a12,12 0 0 0 -12,12a12,12 0 0 0 12,12a12,12 0 0 0 12,-12a12,12 0 0 0 -12,-12"
  }),
  h("path", {
    class: "q-radio__check",
    d: "M12,6a6,6 0 0 0 -6,6a6,6 0 0 0 6,6a6,6 0 0 0 6,-6a6,6 0 0 0 -6,-6"
  })
]);
var QRadio = createComponent({
  name: "QRadio",
  props: __spreadProps(__spreadValues(__spreadValues(__spreadValues({}, useDarkProps), useSizeProps), useFormProps), {
    modelValue: { required: true },
    val: { required: true },
    label: String,
    leftLabel: Boolean,
    checkedIcon: String,
    uncheckedIcon: String,
    color: String,
    keepColor: Boolean,
    dense: Boolean,
    disable: Boolean,
    tabindex: [String, Number]
  }),
  emits: ["update:modelValue"],
  setup(props, { slots, emit }) {
    const { proxy } = getCurrentInstance();
    const isDark = useDark(props, proxy.$q);
    const sizeStyle = useSize(props, optionSizes);
    const rootRef = ref(null);
    const { refocusTargetEl, refocusTarget } = useRefocusTarget(props, rootRef);
    const isTrue = computed(() => props.modelValue === props.val);
    const classes = computed(() => "q-radio cursor-pointer no-outline row inline no-wrap items-center" + (props.disable === true ? " disabled" : "") + (isDark.value === true ? " q-radio--dark" : "") + (props.dense === true ? " q-radio--dense" : "") + (props.leftLabel === true ? " reverse" : ""));
    const innerClass = computed(() => {
      const color = props.color !== void 0 && (props.keepColor === true || isTrue.value === true) ? ` text-${props.color}` : "";
      return `q-radio__inner relative-position q-radio__inner--${isTrue.value === true ? "truthy" : "falsy"}${color}`;
    });
    const icon = computed(() => (isTrue.value === true ? props.checkedIcon : props.uncheckedIcon) || null);
    const tabindex = computed(() => props.disable === true ? -1 : props.tabindex || 0);
    const formAttrs = computed(() => {
      const prop = { type: "radio" };
      props.name !== void 0 && Object.assign(prop, {
        "^checked": isTrue.value === true ? "checked" : void 0,
        name: props.name,
        value: props.val
      });
      return prop;
    });
    const injectFormInput = useFormInject(formAttrs);
    function onClick(e) {
      if (e !== void 0) {
        stopAndPrevent(e);
        refocusTarget(e);
      }
      if (props.disable !== true && isTrue.value !== true) {
        emit("update:modelValue", props.val, e);
      }
    }
    function onKeydown(e) {
      if (e.keyCode === 13 || e.keyCode === 32) {
        stopAndPrevent(e);
      }
    }
    function onKeyup(e) {
      if (e.keyCode === 13 || e.keyCode === 32) {
        onClick(e);
      }
    }
    Object.assign(proxy, { set: onClick });
    return () => {
      const content = icon.value !== null ? [
        h("div", {
          key: "icon",
          class: "q-radio__icon-container absolute-full flex flex-center no-wrap"
        }, [
          h(QIcon, {
            class: "q-radio__icon",
            name: icon.value
          })
        ])
      ] : [svg];
      props.disable !== true && injectFormInput(content, "unshift", " q-radio__native q-ma-none q-pa-none");
      const child = [
        h("div", {
          class: innerClass.value,
          style: sizeStyle.value
        }, content)
      ];
      if (refocusTargetEl.value !== null) {
        child.push(refocusTargetEl.value);
      }
      const label = props.label !== void 0 ? hMergeSlot(slots.default, [props.label]) : hSlot(slots.default);
      label !== void 0 && child.push(h("div", {
        class: "q-radio__label q-anchor--skip"
      }, label));
      return h("div", {
        ref: rootRef,
        class: classes.value,
        tabindex: tabindex.value,
        role: "radio",
        "aria-label": props.label,
        "aria-checked": isTrue.value === true ? "true" : "false",
        "aria-disabled": props.disable === true ? "true" : void 0,
        onClick,
        onKeydown,
        onKeyup
      }, child);
    };
  }
});
const useCheckboxProps = __spreadProps(__spreadValues(__spreadValues(__spreadValues({}, useDarkProps), useSizeProps), useFormProps), {
  modelValue: {
    required: true,
    default: null
  },
  val: {},
  trueValue: { default: true },
  falseValue: { default: false },
  indeterminateValue: { default: null },
  checkedIcon: String,
  uncheckedIcon: String,
  indeterminateIcon: String,
  toggleOrder: {
    type: String,
    validator: (v) => v === "tf" || v === "ft"
  },
  toggleIndeterminate: Boolean,
  label: String,
  leftLabel: Boolean,
  color: String,
  keepColor: Boolean,
  dense: Boolean,
  disable: Boolean,
  tabindex: [String, Number]
});
const useCheckboxEmits = ["update:modelValue"];
function useCheckbox(type, getInner) {
  const { props, slots, emit, proxy } = getCurrentInstance();
  const { $q } = proxy;
  const isDark = useDark(props, $q);
  const rootRef = ref(null);
  const { refocusTargetEl, refocusTarget } = useRefocusTarget(props, rootRef);
  const sizeStyle = useSize(props, optionSizes);
  const modelIsArray = computed(() => props.val !== void 0 && Array.isArray(props.modelValue));
  const index = computed(() => modelIsArray.value === true ? props.modelValue.indexOf(props.val) : -1);
  const isTrue = computed(() => modelIsArray.value === true ? index.value > -1 : props.modelValue === props.trueValue);
  const isFalse = computed(() => modelIsArray.value === true ? index.value === -1 : props.modelValue === props.falseValue);
  const isIndeterminate = computed(() => isTrue.value === false && isFalse.value === false);
  const tabindex = computed(() => props.disable === true ? -1 : props.tabindex || 0);
  const classes = computed(() => `q-${type} cursor-pointer no-outline row inline no-wrap items-center` + (props.disable === true ? " disabled" : "") + (isDark.value === true ? ` q-${type}--dark` : "") + (props.dense === true ? ` q-${type}--dense` : "") + (props.leftLabel === true ? " reverse" : ""));
  const innerClass = computed(() => {
    const state = isTrue.value === true ? "truthy" : isFalse.value === true ? "falsy" : "indet";
    const color = props.color !== void 0 && (props.keepColor === true || (type === "toggle" ? isTrue.value === true : isFalse.value !== true)) ? ` text-${props.color}` : "";
    return `q-${type}__inner relative-position non-selectable q-${type}__inner--${state}${color}`;
  });
  const formAttrs = computed(() => {
    const prop = { type: "checkbox" };
    props.name !== void 0 && Object.assign(prop, {
      "^checked": isTrue.value === true ? "checked" : void 0,
      name: props.name,
      value: modelIsArray.value === true ? props.val : props.trueValue
    });
    return prop;
  });
  const injectFormInput = useFormInject(formAttrs);
  const attributes = computed(() => {
    const attrs = {
      tabindex: tabindex.value,
      role: "checkbox",
      "aria-label": props.label,
      "aria-checked": isIndeterminate.value === true ? "mixed" : isTrue.value === true ? "true" : "false"
    };
    if (props.disable === true) {
      attrs["aria-disabled"] = "true";
    }
    return attrs;
  });
  function onClick(e) {
    if (e !== void 0) {
      stopAndPrevent(e);
      refocusTarget(e);
    }
    if (props.disable !== true) {
      emit("update:modelValue", getNextValue(), e);
    }
  }
  function getNextValue() {
    if (modelIsArray.value === true) {
      if (isTrue.value === true) {
        const val = props.modelValue.slice();
        val.splice(index.value, 1);
        return val;
      }
      return props.modelValue.concat([props.val]);
    }
    if (isTrue.value === true) {
      if (props.toggleOrder !== "ft" || props.toggleIndeterminate === false) {
        return props.falseValue;
      }
    } else if (isFalse.value === true) {
      if (props.toggleOrder === "ft" || props.toggleIndeterminate === false) {
        return props.trueValue;
      }
    } else {
      return props.toggleOrder !== "ft" ? props.trueValue : props.falseValue;
    }
    return props.indeterminateValue;
  }
  function onKeydown(e) {
    if (e.keyCode === 13 || e.keyCode === 32) {
      stopAndPrevent(e);
    }
  }
  function onKeyup(e) {
    if (e.keyCode === 13 || e.keyCode === 32) {
      onClick(e);
    }
  }
  const getInnerContent = getInner(isTrue, isIndeterminate);
  Object.assign(proxy, { toggle: onClick });
  return () => {
    const inner = getInnerContent();
    props.disable !== true && injectFormInput(inner, "unshift", ` q-${type}__native absolute q-ma-none q-pa-none`);
    const child = [
      h("div", {
        class: innerClass.value,
        style: sizeStyle.value
      }, inner)
    ];
    if (refocusTargetEl.value !== null) {
      child.push(refocusTargetEl.value);
    }
    const label = props.label !== void 0 ? hMergeSlot(slots.default, [props.label]) : hSlot(slots.default);
    label !== void 0 && child.push(h("div", {
      class: `q-${type}__label q-anchor--skip`
    }, label));
    return h("div", __spreadProps(__spreadValues({
      ref: rootRef,
      class: classes.value
    }, attributes.value), {
      onClick,
      onKeydown,
      onKeyup
    }), child);
  };
}
const bgNode = h("div", {
  key: "svg",
  class: "q-checkbox__bg absolute"
}, [
  h("svg", {
    class: "q-checkbox__svg fit absolute-full",
    viewBox: "0 0 24 24",
    "aria-hidden": "true"
  }, [
    h("path", {
      class: "q-checkbox__truthy",
      fill: "none",
      d: "M1.73,12.91 8.1,19.28 22.79,4.59"
    }),
    h("path", {
      class: "q-checkbox__indet",
      d: "M4,14H20V10H4"
    })
  ])
]);
var QCheckbox = createComponent({
  name: "QCheckbox",
  props: useCheckboxProps,
  emits: useCheckboxEmits,
  setup(props) {
    function getInner(isTrue, isIndeterminate) {
      const icon = computed(() => (isTrue.value === true ? props.checkedIcon : isIndeterminate.value === true ? props.indeterminateIcon : props.uncheckedIcon) || null);
      return () => icon.value !== null ? [
        h("div", {
          key: "icon",
          class: "q-checkbox__icon-container absolute-full flex flex-center no-wrap"
        }, [
          h(QIcon, {
            class: "q-checkbox__icon",
            name: icon.value
          })
        ])
      ] : [bgNode];
    }
    return useCheckbox("checkbox", getInner);
  }
});
var QToggle = createComponent({
  name: "QToggle",
  props: __spreadProps(__spreadValues({}, useCheckboxProps), {
    icon: String,
    iconColor: String
  }),
  emits: useCheckboxEmits,
  setup(props) {
    function getInner(isTrue, isIndeterminate) {
      const icon = computed(() => (isTrue.value === true ? props.checkedIcon : isIndeterminate.value === true ? props.indeterminateIcon : props.uncheckedIcon) || props.icon);
      const color = computed(() => isTrue.value === true ? props.iconColor : null);
      return () => [
        h("div", { class: "q-toggle__track" }),
        h("div", {
          class: "q-toggle__thumb absolute flex flex-center no-wrap"
        }, icon.value !== void 0 ? [
          h(QIcon, {
            name: icon.value,
            color: color.value
          })
        ] : void 0)
      ];
    }
    return useCheckbox("toggle", getInner);
  }
});
const components = {
  radio: QRadio,
  checkbox: QCheckbox,
  toggle: QToggle
};
const typeValues = Object.keys(components);
var QOptionGroup = createComponent({
  name: "QOptionGroup",
  props: __spreadProps(__spreadValues({}, useDarkProps), {
    modelValue: {
      required: true
    },
    options: {
      type: Array,
      validator: (opts) => opts.every((opt) => "value" in opt && "label" in opt)
    },
    name: String,
    type: {
      default: "radio",
      validator: (v) => typeValues.includes(v)
    },
    color: String,
    keepColor: Boolean,
    dense: Boolean,
    size: String,
    leftLabel: Boolean,
    inline: Boolean,
    disable: Boolean
  }),
  emits: ["update:modelValue"],
  setup(props, { emit, slots }) {
    const { proxy: { $q } } = getCurrentInstance();
    const arrayModel = Array.isArray(props.modelValue);
    if (props.type === "radio") {
      if (arrayModel === true) {
        console.error("q-option-group: model should not be array");
      }
    } else if (arrayModel === false) {
      console.error("q-option-group: model should be array in your case");
    }
    const isDark = useDark(props, $q);
    const component = computed(() => components[props.type]);
    const classes = computed(() => "q-option-group q-gutter-x-sm" + (props.inline === true ? " q-option-group--inline" : ""));
    const attrs = computed(() => {
      const attrs2 = {};
      if (props.type === "radio") {
        attrs2.role = "radiogroup";
        if (props.disable === true) {
          attrs2["aria-disabled"] = "true";
        }
      }
      return attrs2;
    });
    function onUpdateModelValue(value) {
      emit("update:modelValue", value);
    }
    return () => h("div", __spreadValues({
      class: classes.value
    }, attrs.value), props.options.map((opt, i) => {
      const child = slots["label-" + i] !== void 0 ? () => slots["label-" + i](opt) : slots.label !== void 0 ? () => slots.label(opt) : void 0;
      return h("div", [
        h(component.value, {
          modelValue: props.modelValue,
          val: opt.value,
          name: opt.name === void 0 ? props.name : opt.name,
          disable: props.disable || opt.disable,
          label: child === void 0 ? opt.label : null,
          leftLabel: opt.leftLabel === void 0 ? props.leftLabel : opt.leftLabel,
          color: opt.color === void 0 ? props.color : opt.color,
          checkedIcon: opt.checkedIcon,
          uncheckedIcon: opt.uncheckedIcon,
          dark: opt.dark || isDark.value,
          size: opt.size === void 0 ? props.size : opt.size,
          dense: props.dense,
          keepColor: opt.keepColor === void 0 ? props.keepColor : opt.keepColor,
          "onUpdate:modelValue": onUpdateModelValue
        }, child)
      ]);
    }));
  }
});
const _sfc_main = defineComponent({
  name: "IndexPage",
  data() {
    return {
      themeOptions: [
        {
          label: "System",
          value: null
        },
        {
          label: "Light",
          value: false
        },
        {
          label: "Dark",
          value: true
        }
      ],
      settings: {}
    };
  },
  async beforeMount() {
    await this.getSettings();
  },
  methods: {
    setTheme() {
      var _a;
      this.$q.dark.set((_a = this.settings.theme) != null ? _a : "auto");
    },
    onThemeChange() {
      this.setTheme();
      this.onChange();
    },
    onChange() {
      this.setSettings();
    },
    async getSettings() {
      this.settings = await SettingService.get();
    },
    setSettings() {
      SettingService.save({ body: this.settings });
    }
  }
});
const _hoisted_1 = /* @__PURE__ */ createBaseVNode("h4", null, "Theme", -1);
const _hoisted_2 = /* @__PURE__ */ createBaseVNode("h4", null, "Stores", -1);
const _hoisted_3 = /* @__PURE__ */ createBaseVNode("p", null, "List of stores", -1);
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createBlock(QPage, { padding: "" }, {
    default: withCtx(() => [
      _hoisted_1,
      createVNode(QOptionGroup, {
        modelValue: _ctx.settings.theme,
        "onUpdate:modelValue": [
          _cache[0] || (_cache[0] = ($event) => _ctx.settings.theme = $event),
          _ctx.onThemeChange
        ],
        options: _ctx.themeOptions,
        color: "primary",
        inline: ""
      }, null, 8, ["modelValue", "options", "onUpdate:modelValue"]),
      _hoisted_2,
      _hoisted_3
    ]),
    _: 1
  });
}
var SettingsPage = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "SettingsPage.vue"]]);
export { SettingsPage as default };
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2V0dGluZ3NQYWdlLjk1YjE5ODgzLmpzIiwic291cmNlcyI6WyIuLi8uLi8uLi9ub2RlX21vZHVsZXMvcXVhc2FyL3NyYy9jb21wb3NhYmxlcy9wcml2YXRlL3VzZS1yZWZvY3VzLXRhcmdldC5qcyIsIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9xdWFzYXIvc3JjL3V0aWxzL3ByaXZhdGUvb3B0aW9uLXNpemVzLmpzIiwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3F1YXNhci9zcmMvY29tcG9uZW50cy9yYWRpby9RUmFkaW8uanMiLCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvcXVhc2FyL3NyYy9jb21wb25lbnRzL2NoZWNrYm94L3VzZS1jaGVja2JveC5qcyIsIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9xdWFzYXIvc3JjL2NvbXBvbmVudHMvY2hlY2tib3gvUUNoZWNrYm94LmpzIiwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3F1YXNhci9zcmMvY29tcG9uZW50cy90b2dnbGUvUVRvZ2dsZS5qcyIsIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9xdWFzYXIvc3JjL2NvbXBvbmVudHMvb3B0aW9uLWdyb3VwL1FPcHRpb25Hcm91cC5qcyIsIi4uLy4uLy4uL3NyYy9wYWdlcy9TZXR0aW5nc1BhZ2UudnVlIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGgsIGNvbXB1dGVkLCByZWYgfSBmcm9tICd2dWUnXG5cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIChwcm9wcywgcm9vdFJlZikge1xuICBjb25zdCByZWZvY3VzUmVmID0gcmVmKG51bGwpXG5cbiAgY29uc3QgcmVmb2N1c1RhcmdldEVsID0gY29tcHV0ZWQoKCkgPT4ge1xuICAgIGlmIChwcm9wcy5kaXNhYmxlICE9PSB0cnVlKSB7XG4gICAgICByZXR1cm4gbnVsbFxuICAgIH1cblxuICAgIHJldHVybiBoKCdzcGFuJywge1xuICAgICAgcmVmOiByZWZvY3VzUmVmLFxuICAgICAgY2xhc3M6ICduby1vdXRsaW5lJyxcbiAgICAgIHRhYmluZGV4OiAtMVxuICAgIH0pXG4gIH0pXG5cbiAgZnVuY3Rpb24gcmVmb2N1c1RhcmdldCAoZSkge1xuICAgIGNvbnN0IHJvb3QgPSByb290UmVmLnZhbHVlXG5cbiAgICBpZiAoZSAhPT0gdm9pZCAwICYmIGUudHlwZS5pbmRleE9mKCdrZXknKSA9PT0gMCkge1xuICAgICAgaWYgKFxuICAgICAgICByb290ICE9PSBudWxsXG4gICAgICAgICYmIGRvY3VtZW50LmFjdGl2ZUVsZW1lbnQgIT09IHJvb3RcbiAgICAgICAgJiYgcm9vdC5jb250YWlucyhkb2N1bWVudC5hY3RpdmVFbGVtZW50KSA9PT0gdHJ1ZVxuICAgICAgKSB7XG4gICAgICAgIHJvb3QuZm9jdXMoKVxuICAgICAgfVxuICAgIH1cbiAgICBlbHNlIGlmIChcbiAgICAgIHJlZm9jdXNSZWYudmFsdWUgIT09IG51bGxcbiAgICAgICYmIChlID09PSB2b2lkIDAgfHwgKHJvb3QgIT09IG51bGwgJiYgcm9vdC5jb250YWlucyhlLnRhcmdldCkgPT09IHRydWUpKVxuICAgICkge1xuICAgICAgcmVmb2N1c1JlZi52YWx1ZS5mb2N1cygpXG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHtcbiAgICByZWZvY3VzVGFyZ2V0RWwsXG4gICAgcmVmb2N1c1RhcmdldFxuICB9XG59XG4iLCJleHBvcnQgZGVmYXVsdCB7XG4gIHhzOiAzMCxcbiAgc206IDM1LFxuICBtZDogNDAsXG4gIGxnOiA1MCxcbiAgeGw6IDYwXG59XG4iLCJpbXBvcnQgeyBoLCByZWYsIGNvbXB1dGVkLCBnZXRDdXJyZW50SW5zdGFuY2UgfSBmcm9tICd2dWUnXG5cbmltcG9ydCBRSWNvbiBmcm9tICcuLi9pY29uL1FJY29uLmpzJ1xuXG5pbXBvcnQgdXNlRGFyaywgeyB1c2VEYXJrUHJvcHMgfSBmcm9tICcuLi8uLi9jb21wb3NhYmxlcy9wcml2YXRlL3VzZS1kYXJrLmpzJ1xuaW1wb3J0IHVzZVNpemUsIHsgdXNlU2l6ZVByb3BzIH0gZnJvbSAnLi4vLi4vY29tcG9zYWJsZXMvcHJpdmF0ZS91c2Utc2l6ZS5qcydcbmltcG9ydCB1c2VSZWZvY3VzVGFyZ2V0IGZyb20gJy4uLy4uL2NvbXBvc2FibGVzL3ByaXZhdGUvdXNlLXJlZm9jdXMtdGFyZ2V0LmpzJ1xuaW1wb3J0IHsgdXNlRm9ybVByb3BzLCB1c2VGb3JtSW5qZWN0IH0gZnJvbSAnLi4vLi4vY29tcG9zYWJsZXMvcHJpdmF0ZS91c2UtZm9ybS5qcydcblxuaW1wb3J0IHsgY3JlYXRlQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vdXRpbHMvcHJpdmF0ZS9jcmVhdGUuanMnXG5pbXBvcnQgb3B0aW9uU2l6ZXMgZnJvbSAnLi4vLi4vdXRpbHMvcHJpdmF0ZS9vcHRpb24tc2l6ZXMuanMnXG5pbXBvcnQgeyBzdG9wQW5kUHJldmVudCB9IGZyb20gJy4uLy4uL3V0aWxzL2V2ZW50LmpzJ1xuaW1wb3J0IHsgaFNsb3QsIGhNZXJnZVNsb3QgfSBmcm9tICcuLi8uLi91dGlscy9wcml2YXRlL3JlbmRlci5qcydcblxuY29uc3Qgc3ZnID0gaCgnc3ZnJywge1xuICBrZXk6ICdzdmcnLFxuICBjbGFzczogJ3EtcmFkaW9fX2JnIGFic29sdXRlIG5vbi1zZWxlY3RhYmxlJyxcbiAgdmlld0JveDogJzAgMCAyNCAyNCcsXG4gICdhcmlhLWhpZGRlbic6ICd0cnVlJ1xufSwgW1xuICBoKCdwYXRoJywge1xuICAgIGQ6ICdNMTIsMjJhMTAsMTAgMCAwIDEgLTEwLC0xMGExMCwxMCAwIDAgMSAxMCwtMTBhMTAsMTAgMCAwIDEgMTAsMTBhMTAsMTAgMCAwIDEgLTEwLDEwbTAsLTIyYTEyLDEyIDAgMCAwIC0xMiwxMmExMiwxMiAwIDAgMCAxMiwxMmExMiwxMiAwIDAgMCAxMiwtMTJhMTIsMTIgMCAwIDAgLTEyLC0xMidcbiAgfSksXG5cbiAgaCgncGF0aCcsIHtcbiAgICBjbGFzczogJ3EtcmFkaW9fX2NoZWNrJyxcbiAgICBkOiAnTTEyLDZhNiw2IDAgMCAwIC02LDZhNiw2IDAgMCAwIDYsNmE2LDYgMCAwIDAgNiwtNmE2LDYgMCAwIDAgLTYsLTYnXG4gIH0pXG5dKVxuXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVDb21wb25lbnQoe1xuICBuYW1lOiAnUVJhZGlvJyxcblxuICBwcm9wczoge1xuICAgIC4uLnVzZURhcmtQcm9wcyxcbiAgICAuLi51c2VTaXplUHJvcHMsXG4gICAgLi4udXNlRm9ybVByb3BzLFxuXG4gICAgbW9kZWxWYWx1ZTogeyByZXF1aXJlZDogdHJ1ZSB9LFxuICAgIHZhbDogeyByZXF1aXJlZDogdHJ1ZSB9LFxuXG4gICAgbGFiZWw6IFN0cmluZyxcbiAgICBsZWZ0TGFiZWw6IEJvb2xlYW4sXG5cbiAgICBjaGVja2VkSWNvbjogU3RyaW5nLFxuICAgIHVuY2hlY2tlZEljb246IFN0cmluZyxcblxuICAgIGNvbG9yOiBTdHJpbmcsXG4gICAga2VlcENvbG9yOiBCb29sZWFuLFxuICAgIGRlbnNlOiBCb29sZWFuLFxuXG4gICAgZGlzYWJsZTogQm9vbGVhbixcbiAgICB0YWJpbmRleDogWyBTdHJpbmcsIE51bWJlciBdXG4gIH0sXG5cbiAgZW1pdHM6IFsgJ3VwZGF0ZTptb2RlbFZhbHVlJyBdLFxuXG4gIHNldHVwIChwcm9wcywgeyBzbG90cywgZW1pdCB9KSB7XG4gICAgY29uc3QgeyBwcm94eSB9ID0gZ2V0Q3VycmVudEluc3RhbmNlKClcblxuICAgIGNvbnN0IGlzRGFyayA9IHVzZURhcmsocHJvcHMsIHByb3h5LiRxKVxuICAgIGNvbnN0IHNpemVTdHlsZSA9IHVzZVNpemUocHJvcHMsIG9wdGlvblNpemVzKVxuXG4gICAgY29uc3Qgcm9vdFJlZiA9IHJlZihudWxsKVxuICAgIGNvbnN0IHsgcmVmb2N1c1RhcmdldEVsLCByZWZvY3VzVGFyZ2V0IH0gPSB1c2VSZWZvY3VzVGFyZ2V0KHByb3BzLCByb290UmVmKVxuXG4gICAgY29uc3QgaXNUcnVlID0gY29tcHV0ZWQoKCkgPT4gcHJvcHMubW9kZWxWYWx1ZSA9PT0gcHJvcHMudmFsKVxuXG4gICAgY29uc3QgY2xhc3NlcyA9IGNvbXB1dGVkKCgpID0+XG4gICAgICAncS1yYWRpbyBjdXJzb3ItcG9pbnRlciBuby1vdXRsaW5lIHJvdyBpbmxpbmUgbm8td3JhcCBpdGVtcy1jZW50ZXInXG4gICAgICArIChwcm9wcy5kaXNhYmxlID09PSB0cnVlID8gJyBkaXNhYmxlZCcgOiAnJylcbiAgICAgICsgKGlzRGFyay52YWx1ZSA9PT0gdHJ1ZSA/ICcgcS1yYWRpby0tZGFyaycgOiAnJylcbiAgICAgICsgKHByb3BzLmRlbnNlID09PSB0cnVlID8gJyBxLXJhZGlvLS1kZW5zZScgOiAnJylcbiAgICAgICsgKHByb3BzLmxlZnRMYWJlbCA9PT0gdHJ1ZSA/ICcgcmV2ZXJzZScgOiAnJylcbiAgICApXG5cbiAgICBjb25zdCBpbm5lckNsYXNzID0gY29tcHV0ZWQoKCkgPT4ge1xuICAgICAgY29uc3QgY29sb3IgPSBwcm9wcy5jb2xvciAhPT0gdm9pZCAwICYmIChcbiAgICAgICAgcHJvcHMua2VlcENvbG9yID09PSB0cnVlXG4gICAgICAgIHx8IGlzVHJ1ZS52YWx1ZSA9PT0gdHJ1ZVxuICAgICAgKVxuICAgICAgICA/IGAgdGV4dC0keyBwcm9wcy5jb2xvciB9YFxuICAgICAgICA6ICcnXG5cbiAgICAgIHJldHVybiAncS1yYWRpb19faW5uZXIgcmVsYXRpdmUtcG9zaXRpb24gJ1xuICAgICAgICArIGBxLXJhZGlvX19pbm5lci0tJHsgaXNUcnVlLnZhbHVlID09PSB0cnVlID8gJ3RydXRoeScgOiAnZmFsc3knIH0keyBjb2xvciB9YFxuICAgIH0pXG5cbiAgICBjb25zdCBpY29uID0gY29tcHV0ZWQoKCkgPT5cbiAgICAgIChpc1RydWUudmFsdWUgPT09IHRydWVcbiAgICAgICAgPyBwcm9wcy5jaGVja2VkSWNvblxuICAgICAgICA6IHByb3BzLnVuY2hlY2tlZEljb25cbiAgICAgICkgfHwgbnVsbFxuICAgIClcblxuICAgIGNvbnN0IHRhYmluZGV4ID0gY29tcHV0ZWQoKCkgPT4gKFxuICAgICAgcHJvcHMuZGlzYWJsZSA9PT0gdHJ1ZSA/IC0xIDogcHJvcHMudGFiaW5kZXggfHwgMFxuICAgICkpXG5cbiAgICBjb25zdCBmb3JtQXR0cnMgPSBjb21wdXRlZCgoKSA9PiB7XG4gICAgICBjb25zdCBwcm9wID0geyB0eXBlOiAncmFkaW8nIH1cblxuICAgICAgcHJvcHMubmFtZSAhPT0gdm9pZCAwICYmIE9iamVjdC5hc3NpZ24ocHJvcCwge1xuICAgICAgICAnXmNoZWNrZWQnOiBpc1RydWUudmFsdWUgPT09IHRydWUgPyAnY2hlY2tlZCcgOiB2b2lkIDAsXG4gICAgICAgIG5hbWU6IHByb3BzLm5hbWUsXG4gICAgICAgIHZhbHVlOiBwcm9wcy52YWxcbiAgICAgIH0pXG5cbiAgICAgIHJldHVybiBwcm9wXG4gICAgfSlcblxuICAgIGNvbnN0IGluamVjdEZvcm1JbnB1dCA9IHVzZUZvcm1JbmplY3QoZm9ybUF0dHJzKVxuXG4gICAgZnVuY3Rpb24gb25DbGljayAoZSkge1xuICAgICAgaWYgKGUgIT09IHZvaWQgMCkge1xuICAgICAgICBzdG9wQW5kUHJldmVudChlKVxuICAgICAgICByZWZvY3VzVGFyZ2V0KGUpXG4gICAgICB9XG5cbiAgICAgIGlmIChwcm9wcy5kaXNhYmxlICE9PSB0cnVlICYmIGlzVHJ1ZS52YWx1ZSAhPT0gdHJ1ZSkge1xuICAgICAgICBlbWl0KCd1cGRhdGU6bW9kZWxWYWx1ZScsIHByb3BzLnZhbCwgZSlcbiAgICAgIH1cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBvbktleWRvd24gKGUpIHtcbiAgICAgIGlmIChlLmtleUNvZGUgPT09IDEzIHx8IGUua2V5Q29kZSA9PT0gMzIpIHtcbiAgICAgICAgc3RvcEFuZFByZXZlbnQoZSlcbiAgICAgIH1cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBvbktleXVwIChlKSB7XG4gICAgICBpZiAoZS5rZXlDb2RlID09PSAxMyB8fCBlLmtleUNvZGUgPT09IDMyKSB7XG4gICAgICAgIG9uQ2xpY2soZSlcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvLyBleHBvc2UgcHVibGljIG1ldGhvZHNcbiAgICBPYmplY3QuYXNzaWduKHByb3h5LCB7IHNldDogb25DbGljayB9KVxuXG4gICAgcmV0dXJuICgpID0+IHtcbiAgICAgIGNvbnN0IGNvbnRlbnQgPSBpY29uLnZhbHVlICE9PSBudWxsXG4gICAgICAgID8gW1xuICAgICAgICAgICAgaCgnZGl2Jywge1xuICAgICAgICAgICAgICBrZXk6ICdpY29uJyxcbiAgICAgICAgICAgICAgY2xhc3M6ICdxLXJhZGlvX19pY29uLWNvbnRhaW5lciBhYnNvbHV0ZS1mdWxsIGZsZXggZmxleC1jZW50ZXIgbm8td3JhcCdcbiAgICAgICAgICAgIH0sIFtcbiAgICAgICAgICAgICAgaChRSWNvbiwge1xuICAgICAgICAgICAgICAgIGNsYXNzOiAncS1yYWRpb19faWNvbicsXG4gICAgICAgICAgICAgICAgbmFtZTogaWNvbi52YWx1ZVxuICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgXSlcbiAgICAgICAgICBdXG4gICAgICAgIDogWyBzdmcgXVxuXG4gICAgICBwcm9wcy5kaXNhYmxlICE9PSB0cnVlICYmIGluamVjdEZvcm1JbnB1dChcbiAgICAgICAgY29udGVudCxcbiAgICAgICAgJ3Vuc2hpZnQnLFxuICAgICAgICAnIHEtcmFkaW9fX25hdGl2ZSBxLW1hLW5vbmUgcS1wYS1ub25lJ1xuICAgICAgKVxuXG4gICAgICBjb25zdCBjaGlsZCA9IFtcbiAgICAgICAgaCgnZGl2Jywge1xuICAgICAgICAgIGNsYXNzOiBpbm5lckNsYXNzLnZhbHVlLFxuICAgICAgICAgIHN0eWxlOiBzaXplU3R5bGUudmFsdWVcbiAgICAgICAgfSwgY29udGVudClcbiAgICAgIF1cblxuICAgICAgaWYgKHJlZm9jdXNUYXJnZXRFbC52YWx1ZSAhPT0gbnVsbCkge1xuICAgICAgICBjaGlsZC5wdXNoKHJlZm9jdXNUYXJnZXRFbC52YWx1ZSlcbiAgICAgIH1cblxuICAgICAgY29uc3QgbGFiZWwgPSBwcm9wcy5sYWJlbCAhPT0gdm9pZCAwXG4gICAgICAgID8gaE1lcmdlU2xvdChzbG90cy5kZWZhdWx0LCBbIHByb3BzLmxhYmVsIF0pXG4gICAgICAgIDogaFNsb3Qoc2xvdHMuZGVmYXVsdClcblxuICAgICAgbGFiZWwgIT09IHZvaWQgMCAmJiBjaGlsZC5wdXNoKFxuICAgICAgICBoKCdkaXYnLCB7XG4gICAgICAgICAgY2xhc3M6ICdxLXJhZGlvX19sYWJlbCBxLWFuY2hvci0tc2tpcCdcbiAgICAgICAgfSwgbGFiZWwpXG4gICAgICApXG5cbiAgICAgIHJldHVybiBoKCdkaXYnLCB7XG4gICAgICAgIHJlZjogcm9vdFJlZixcbiAgICAgICAgY2xhc3M6IGNsYXNzZXMudmFsdWUsXG4gICAgICAgIHRhYmluZGV4OiB0YWJpbmRleC52YWx1ZSxcbiAgICAgICAgcm9sZTogJ3JhZGlvJyxcbiAgICAgICAgJ2FyaWEtbGFiZWwnOiBwcm9wcy5sYWJlbCxcbiAgICAgICAgJ2FyaWEtY2hlY2tlZCc6IGlzVHJ1ZS52YWx1ZSA9PT0gdHJ1ZSA/ICd0cnVlJyA6ICdmYWxzZScsXG4gICAgICAgICdhcmlhLWRpc2FibGVkJzogcHJvcHMuZGlzYWJsZSA9PT0gdHJ1ZSA/ICd0cnVlJyA6IHZvaWQgMCxcbiAgICAgICAgb25DbGljayxcbiAgICAgICAgb25LZXlkb3duLFxuICAgICAgICBvbktleXVwXG4gICAgICB9LCBjaGlsZClcbiAgICB9XG4gIH1cbn0pXG4iLCJpbXBvcnQgeyBoLCByZWYsIGNvbXB1dGVkLCBnZXRDdXJyZW50SW5zdGFuY2UgfSBmcm9tICd2dWUnXG5cbmltcG9ydCB1c2VEYXJrLCB7IHVzZURhcmtQcm9wcyB9IGZyb20gJy4uLy4uL2NvbXBvc2FibGVzL3ByaXZhdGUvdXNlLWRhcmsuanMnXG5pbXBvcnQgdXNlU2l6ZSwgeyB1c2VTaXplUHJvcHMgfSBmcm9tICcuLi8uLi9jb21wb3NhYmxlcy9wcml2YXRlL3VzZS1zaXplLmpzJ1xuaW1wb3J0IHVzZVJlZm9jdXNUYXJnZXQgZnJvbSAnLi4vLi4vY29tcG9zYWJsZXMvcHJpdmF0ZS91c2UtcmVmb2N1cy10YXJnZXQuanMnXG5pbXBvcnQgeyB1c2VGb3JtSW5qZWN0LCB1c2VGb3JtUHJvcHMgfSBmcm9tICcuLi8uLi9jb21wb3NhYmxlcy9wcml2YXRlL3VzZS1mb3JtLmpzJ1xuXG5pbXBvcnQgb3B0aW9uU2l6ZXMgZnJvbSAnLi4vLi4vdXRpbHMvcHJpdmF0ZS9vcHRpb24tc2l6ZXMuanMnXG5pbXBvcnQgeyBzdG9wQW5kUHJldmVudCB9IGZyb20gJy4uLy4uL3V0aWxzL2V2ZW50LmpzJ1xuaW1wb3J0IHsgaFNsb3QsIGhNZXJnZVNsb3QgfSBmcm9tICcuLi8uLi91dGlscy9wcml2YXRlL3JlbmRlci5qcydcblxuZXhwb3J0IGNvbnN0IHVzZUNoZWNrYm94UHJvcHMgPSB7XG4gIC4uLnVzZURhcmtQcm9wcyxcbiAgLi4udXNlU2l6ZVByb3BzLFxuICAuLi51c2VGb3JtUHJvcHMsXG5cbiAgbW9kZWxWYWx1ZToge1xuICAgIHJlcXVpcmVkOiB0cnVlLFxuICAgIGRlZmF1bHQ6IG51bGxcbiAgfSxcbiAgdmFsOiB7fSxcblxuICB0cnVlVmFsdWU6IHsgZGVmYXVsdDogdHJ1ZSB9LFxuICBmYWxzZVZhbHVlOiB7IGRlZmF1bHQ6IGZhbHNlIH0sXG4gIGluZGV0ZXJtaW5hdGVWYWx1ZTogeyBkZWZhdWx0OiBudWxsIH0sXG5cbiAgY2hlY2tlZEljb246IFN0cmluZyxcbiAgdW5jaGVja2VkSWNvbjogU3RyaW5nLFxuICBpbmRldGVybWluYXRlSWNvbjogU3RyaW5nLFxuXG4gIHRvZ2dsZU9yZGVyOiB7XG4gICAgdHlwZTogU3RyaW5nLFxuICAgIHZhbGlkYXRvcjogdiA9PiB2ID09PSAndGYnIHx8IHYgPT09ICdmdCdcbiAgfSxcbiAgdG9nZ2xlSW5kZXRlcm1pbmF0ZTogQm9vbGVhbixcblxuICBsYWJlbDogU3RyaW5nLFxuICBsZWZ0TGFiZWw6IEJvb2xlYW4sXG5cbiAgY29sb3I6IFN0cmluZyxcbiAga2VlcENvbG9yOiBCb29sZWFuLFxuICBkZW5zZTogQm9vbGVhbixcblxuICBkaXNhYmxlOiBCb29sZWFuLFxuICB0YWJpbmRleDogWyBTdHJpbmcsIE51bWJlciBdXG59XG5cbmV4cG9ydCBjb25zdCB1c2VDaGVja2JveEVtaXRzID0gWyAndXBkYXRlOm1vZGVsVmFsdWUnIF1cblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gKHR5cGUsIGdldElubmVyKSB7XG4gIGNvbnN0IHsgcHJvcHMsIHNsb3RzLCBlbWl0LCBwcm94eSB9ID0gZ2V0Q3VycmVudEluc3RhbmNlKClcbiAgY29uc3QgeyAkcSB9ID0gcHJveHlcblxuICBjb25zdCBpc0RhcmsgPSB1c2VEYXJrKHByb3BzLCAkcSlcblxuICBjb25zdCByb290UmVmID0gcmVmKG51bGwpXG4gIGNvbnN0IHsgcmVmb2N1c1RhcmdldEVsLCByZWZvY3VzVGFyZ2V0IH0gPSB1c2VSZWZvY3VzVGFyZ2V0KHByb3BzLCByb290UmVmKVxuICBjb25zdCBzaXplU3R5bGUgPSB1c2VTaXplKHByb3BzLCBvcHRpb25TaXplcylcblxuICBjb25zdCBtb2RlbElzQXJyYXkgPSBjb21wdXRlZCgoKSA9PlxuICAgIHByb3BzLnZhbCAhPT0gdm9pZCAwICYmIEFycmF5LmlzQXJyYXkocHJvcHMubW9kZWxWYWx1ZSlcbiAgKVxuXG4gIGNvbnN0IGluZGV4ID0gY29tcHV0ZWQoKCkgPT4gKFxuICAgIG1vZGVsSXNBcnJheS52YWx1ZSA9PT0gdHJ1ZVxuICAgICAgPyBwcm9wcy5tb2RlbFZhbHVlLmluZGV4T2YocHJvcHMudmFsKVxuICAgICAgOiAtMVxuICApKVxuXG4gIGNvbnN0IGlzVHJ1ZSA9IGNvbXB1dGVkKCgpID0+IChcbiAgICBtb2RlbElzQXJyYXkudmFsdWUgPT09IHRydWVcbiAgICAgID8gaW5kZXgudmFsdWUgPiAtMVxuICAgICAgOiBwcm9wcy5tb2RlbFZhbHVlID09PSBwcm9wcy50cnVlVmFsdWVcbiAgKSlcblxuICBjb25zdCBpc0ZhbHNlID0gY29tcHV0ZWQoKCkgPT4gKFxuICAgIG1vZGVsSXNBcnJheS52YWx1ZSA9PT0gdHJ1ZVxuICAgICAgPyBpbmRleC52YWx1ZSA9PT0gLTFcbiAgICAgIDogcHJvcHMubW9kZWxWYWx1ZSA9PT0gcHJvcHMuZmFsc2VWYWx1ZVxuICApKVxuXG4gIGNvbnN0IGlzSW5kZXRlcm1pbmF0ZSA9IGNvbXB1dGVkKCgpID0+XG4gICAgaXNUcnVlLnZhbHVlID09PSBmYWxzZSAmJiBpc0ZhbHNlLnZhbHVlID09PSBmYWxzZVxuICApXG5cbiAgY29uc3QgdGFiaW5kZXggPSBjb21wdXRlZCgoKSA9PiAoXG4gICAgcHJvcHMuZGlzYWJsZSA9PT0gdHJ1ZSA/IC0xIDogcHJvcHMudGFiaW5kZXggfHwgMFxuICApKVxuXG4gIGNvbnN0IGNsYXNzZXMgPSBjb21wdXRlZCgoKSA9PlxuICAgIGBxLSR7IHR5cGUgfSBjdXJzb3ItcG9pbnRlciBuby1vdXRsaW5lIHJvdyBpbmxpbmUgbm8td3JhcCBpdGVtcy1jZW50ZXJgXG4gICAgKyAocHJvcHMuZGlzYWJsZSA9PT0gdHJ1ZSA/ICcgZGlzYWJsZWQnIDogJycpXG4gICAgKyAoaXNEYXJrLnZhbHVlID09PSB0cnVlID8gYCBxLSR7IHR5cGUgfS0tZGFya2AgOiAnJylcbiAgICArIChwcm9wcy5kZW5zZSA9PT0gdHJ1ZSA/IGAgcS0keyB0eXBlIH0tLWRlbnNlYCA6ICcnKVxuICAgICsgKHByb3BzLmxlZnRMYWJlbCA9PT0gdHJ1ZSA/ICcgcmV2ZXJzZScgOiAnJylcbiAgKVxuXG4gIGNvbnN0IGlubmVyQ2xhc3MgPSBjb21wdXRlZCgoKSA9PiB7XG4gICAgY29uc3Qgc3RhdGUgPSBpc1RydWUudmFsdWUgPT09IHRydWUgPyAndHJ1dGh5JyA6IChpc0ZhbHNlLnZhbHVlID09PSB0cnVlID8gJ2ZhbHN5JyA6ICdpbmRldCcpXG4gICAgY29uc3QgY29sb3IgPSBwcm9wcy5jb2xvciAhPT0gdm9pZCAwICYmIChcbiAgICAgIHByb3BzLmtlZXBDb2xvciA9PT0gdHJ1ZVxuICAgICAgfHwgKHR5cGUgPT09ICd0b2dnbGUnID8gaXNUcnVlLnZhbHVlID09PSB0cnVlIDogaXNGYWxzZS52YWx1ZSAhPT0gdHJ1ZSlcbiAgICApXG4gICAgICA/IGAgdGV4dC0keyBwcm9wcy5jb2xvciB9YFxuICAgICAgOiAnJ1xuXG4gICAgcmV0dXJuIGBxLSR7IHR5cGUgfV9faW5uZXIgcmVsYXRpdmUtcG9zaXRpb24gbm9uLXNlbGVjdGFibGUgcS0keyB0eXBlIH1fX2lubmVyLS0keyBzdGF0ZSB9JHsgY29sb3IgfWBcbiAgfSlcblxuICBjb25zdCBmb3JtQXR0cnMgPSBjb21wdXRlZCgoKSA9PiB7XG4gICAgY29uc3QgcHJvcCA9IHsgdHlwZTogJ2NoZWNrYm94JyB9XG5cbiAgICBwcm9wcy5uYW1lICE9PSB2b2lkIDAgJiYgT2JqZWN0LmFzc2lnbihwcm9wLCB7XG4gICAgICAnXmNoZWNrZWQnOiBpc1RydWUudmFsdWUgPT09IHRydWUgPyAnY2hlY2tlZCcgOiB2b2lkIDAsXG4gICAgICBuYW1lOiBwcm9wcy5uYW1lLFxuICAgICAgdmFsdWU6IG1vZGVsSXNBcnJheS52YWx1ZSA9PT0gdHJ1ZVxuICAgICAgICA/IHByb3BzLnZhbFxuICAgICAgICA6IHByb3BzLnRydWVWYWx1ZVxuICAgIH0pXG5cbiAgICByZXR1cm4gcHJvcFxuICB9KVxuXG4gIGNvbnN0IGluamVjdEZvcm1JbnB1dCA9IHVzZUZvcm1JbmplY3QoZm9ybUF0dHJzKVxuXG4gIGNvbnN0IGF0dHJpYnV0ZXMgPSBjb21wdXRlZCgoKSA9PiB7XG4gICAgY29uc3QgYXR0cnMgPSB7XG4gICAgICB0YWJpbmRleDogdGFiaW5kZXgudmFsdWUsXG4gICAgICByb2xlOiAnY2hlY2tib3gnLFxuICAgICAgJ2FyaWEtbGFiZWwnOiBwcm9wcy5sYWJlbCxcbiAgICAgICdhcmlhLWNoZWNrZWQnOiBpc0luZGV0ZXJtaW5hdGUudmFsdWUgPT09IHRydWVcbiAgICAgICAgPyAnbWl4ZWQnXG4gICAgICAgIDogKGlzVHJ1ZS52YWx1ZSA9PT0gdHJ1ZSA/ICd0cnVlJyA6ICdmYWxzZScpXG4gICAgfVxuXG4gICAgaWYgKHByb3BzLmRpc2FibGUgPT09IHRydWUpIHtcbiAgICAgIGF0dHJzWyAnYXJpYS1kaXNhYmxlZCcgXSA9ICd0cnVlJ1xuICAgIH1cblxuICAgIHJldHVybiBhdHRyc1xuICB9KVxuXG4gIGZ1bmN0aW9uIG9uQ2xpY2sgKGUpIHtcbiAgICBpZiAoZSAhPT0gdm9pZCAwKSB7XG4gICAgICBzdG9wQW5kUHJldmVudChlKVxuICAgICAgcmVmb2N1c1RhcmdldChlKVxuICAgIH1cblxuICAgIGlmIChwcm9wcy5kaXNhYmxlICE9PSB0cnVlKSB7XG4gICAgICBlbWl0KCd1cGRhdGU6bW9kZWxWYWx1ZScsIGdldE5leHRWYWx1ZSgpLCBlKVxuICAgIH1cbiAgfVxuXG4gIGZ1bmN0aW9uIGdldE5leHRWYWx1ZSAoKSB7XG4gICAgaWYgKG1vZGVsSXNBcnJheS52YWx1ZSA9PT0gdHJ1ZSkge1xuICAgICAgaWYgKGlzVHJ1ZS52YWx1ZSA9PT0gdHJ1ZSkge1xuICAgICAgICBjb25zdCB2YWwgPSBwcm9wcy5tb2RlbFZhbHVlLnNsaWNlKClcbiAgICAgICAgdmFsLnNwbGljZShpbmRleC52YWx1ZSwgMSlcbiAgICAgICAgcmV0dXJuIHZhbFxuICAgICAgfVxuXG4gICAgICByZXR1cm4gcHJvcHMubW9kZWxWYWx1ZS5jb25jYXQoWyBwcm9wcy52YWwgXSlcbiAgICB9XG5cbiAgICBpZiAoaXNUcnVlLnZhbHVlID09PSB0cnVlKSB7XG4gICAgICBpZiAocHJvcHMudG9nZ2xlT3JkZXIgIT09ICdmdCcgfHwgcHJvcHMudG9nZ2xlSW5kZXRlcm1pbmF0ZSA9PT0gZmFsc2UpIHtcbiAgICAgICAgcmV0dXJuIHByb3BzLmZhbHNlVmFsdWVcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSBpZiAoaXNGYWxzZS52YWx1ZSA9PT0gdHJ1ZSkge1xuICAgICAgaWYgKHByb3BzLnRvZ2dsZU9yZGVyID09PSAnZnQnIHx8IHByb3BzLnRvZ2dsZUluZGV0ZXJtaW5hdGUgPT09IGZhbHNlKSB7XG4gICAgICAgIHJldHVybiBwcm9wcy50cnVlVmFsdWVcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICByZXR1cm4gcHJvcHMudG9nZ2xlT3JkZXIgIT09ICdmdCdcbiAgICAgICAgPyBwcm9wcy50cnVlVmFsdWVcbiAgICAgICAgOiBwcm9wcy5mYWxzZVZhbHVlXG4gICAgfVxuXG4gICAgcmV0dXJuIHByb3BzLmluZGV0ZXJtaW5hdGVWYWx1ZVxuICB9XG5cbiAgZnVuY3Rpb24gb25LZXlkb3duIChlKSB7XG4gICAgaWYgKGUua2V5Q29kZSA9PT0gMTMgfHwgZS5rZXlDb2RlID09PSAzMikge1xuICAgICAgc3RvcEFuZFByZXZlbnQoZSlcbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiBvbktleXVwIChlKSB7XG4gICAgaWYgKGUua2V5Q29kZSA9PT0gMTMgfHwgZS5rZXlDb2RlID09PSAzMikge1xuICAgICAgb25DbGljayhlKVxuICAgIH1cbiAgfVxuXG4gIGNvbnN0IGdldElubmVyQ29udGVudCA9IGdldElubmVyKGlzVHJ1ZSwgaXNJbmRldGVybWluYXRlKVxuXG4gIC8vIGV4cG9zZSBwdWJsaWMgbWV0aG9kc1xuICBPYmplY3QuYXNzaWduKHByb3h5LCB7IHRvZ2dsZTogb25DbGljayB9KVxuXG4gIHJldHVybiAoKSA9PiB7XG4gICAgY29uc3QgaW5uZXIgPSBnZXRJbm5lckNvbnRlbnQoKVxuXG4gICAgcHJvcHMuZGlzYWJsZSAhPT0gdHJ1ZSAmJiBpbmplY3RGb3JtSW5wdXQoXG4gICAgICBpbm5lcixcbiAgICAgICd1bnNoaWZ0JyxcbiAgICAgIGAgcS0keyB0eXBlIH1fX25hdGl2ZSBhYnNvbHV0ZSBxLW1hLW5vbmUgcS1wYS1ub25lYFxuICAgIClcblxuICAgIGNvbnN0IGNoaWxkID0gW1xuICAgICAgaCgnZGl2Jywge1xuICAgICAgICBjbGFzczogaW5uZXJDbGFzcy52YWx1ZSxcbiAgICAgICAgc3R5bGU6IHNpemVTdHlsZS52YWx1ZVxuICAgICAgfSwgaW5uZXIpXG4gICAgXVxuXG4gICAgaWYgKHJlZm9jdXNUYXJnZXRFbC52YWx1ZSAhPT0gbnVsbCkge1xuICAgICAgY2hpbGQucHVzaChyZWZvY3VzVGFyZ2V0RWwudmFsdWUpXG4gICAgfVxuXG4gICAgY29uc3QgbGFiZWwgPSBwcm9wcy5sYWJlbCAhPT0gdm9pZCAwXG4gICAgICA/IGhNZXJnZVNsb3Qoc2xvdHMuZGVmYXVsdCwgWyBwcm9wcy5sYWJlbCBdKVxuICAgICAgOiBoU2xvdChzbG90cy5kZWZhdWx0KVxuXG4gICAgbGFiZWwgIT09IHZvaWQgMCAmJiBjaGlsZC5wdXNoKFxuICAgICAgaCgnZGl2Jywge1xuICAgICAgICBjbGFzczogYHEtJHsgdHlwZSB9X19sYWJlbCBxLWFuY2hvci0tc2tpcGBcbiAgICAgIH0sIGxhYmVsKVxuICAgIClcblxuICAgIHJldHVybiBoKCdkaXYnLCB7XG4gICAgICByZWY6IHJvb3RSZWYsXG4gICAgICBjbGFzczogY2xhc3Nlcy52YWx1ZSxcbiAgICAgIC4uLmF0dHJpYnV0ZXMudmFsdWUsXG4gICAgICBvbkNsaWNrLFxuICAgICAgb25LZXlkb3duLFxuICAgICAgb25LZXl1cFxuICAgIH0sIGNoaWxkKVxuICB9XG59XG4iLCJpbXBvcnQgeyBoLCBjb21wdXRlZCB9IGZyb20gJ3Z1ZSdcblxuaW1wb3J0IFFJY29uIGZyb20gJy4uL2ljb24vUUljb24uanMnXG5cbmltcG9ydCB7IGNyZWF0ZUNvbXBvbmVudCB9IGZyb20gJy4uLy4uL3V0aWxzL3ByaXZhdGUvY3JlYXRlLmpzJ1xuaW1wb3J0IHVzZUNoZWNrYm94LCB7IHVzZUNoZWNrYm94UHJvcHMsIHVzZUNoZWNrYm94RW1pdHMgfSBmcm9tICcuL3VzZS1jaGVja2JveC5qcydcblxuY29uc3QgYmdOb2RlID0gaCgnZGl2Jywge1xuICBrZXk6ICdzdmcnLFxuICBjbGFzczogJ3EtY2hlY2tib3hfX2JnIGFic29sdXRlJ1xufSwgW1xuICBoKCdzdmcnLCB7XG4gICAgY2xhc3M6ICdxLWNoZWNrYm94X19zdmcgZml0IGFic29sdXRlLWZ1bGwnLFxuICAgIHZpZXdCb3g6ICcwIDAgMjQgMjQnLFxuICAgICdhcmlhLWhpZGRlbic6ICd0cnVlJ1xuICB9LCBbXG4gICAgaCgncGF0aCcsIHtcbiAgICAgIGNsYXNzOiAncS1jaGVja2JveF9fdHJ1dGh5JyxcbiAgICAgIGZpbGw6ICdub25lJyxcbiAgICAgIGQ6ICdNMS43MywxMi45MSA4LjEsMTkuMjggMjIuNzksNC41OSdcbiAgICB9KSxcblxuICAgIGgoJ3BhdGgnLCB7XG4gICAgICBjbGFzczogJ3EtY2hlY2tib3hfX2luZGV0JyxcbiAgICAgIGQ6ICdNNCwxNEgyMFYxMEg0J1xuICAgIH0pXG4gIF0pXG5dKVxuXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVDb21wb25lbnQoe1xuICBuYW1lOiAnUUNoZWNrYm94JyxcblxuICBwcm9wczogdXNlQ2hlY2tib3hQcm9wcyxcbiAgZW1pdHM6IHVzZUNoZWNrYm94RW1pdHMsXG5cbiAgc2V0dXAgKHByb3BzKSB7XG4gICAgZnVuY3Rpb24gZ2V0SW5uZXIgKGlzVHJ1ZSwgaXNJbmRldGVybWluYXRlKSB7XG4gICAgICBjb25zdCBpY29uID0gY29tcHV0ZWQoKCkgPT5cbiAgICAgICAgKGlzVHJ1ZS52YWx1ZSA9PT0gdHJ1ZVxuICAgICAgICAgID8gcHJvcHMuY2hlY2tlZEljb25cbiAgICAgICAgICA6IChpc0luZGV0ZXJtaW5hdGUudmFsdWUgPT09IHRydWVcbiAgICAgICAgICAgICAgPyBwcm9wcy5pbmRldGVybWluYXRlSWNvblxuICAgICAgICAgICAgICA6IHByb3BzLnVuY2hlY2tlZEljb25cbiAgICAgICAgICAgIClcbiAgICAgICAgKSB8fCBudWxsXG4gICAgICApXG5cbiAgICAgIHJldHVybiAoKSA9PiAoXG4gICAgICAgIGljb24udmFsdWUgIT09IG51bGxcbiAgICAgICAgICA/IFtcbiAgICAgICAgICAgICAgaCgnZGl2Jywge1xuICAgICAgICAgICAgICAgIGtleTogJ2ljb24nLFxuICAgICAgICAgICAgICAgIGNsYXNzOiAncS1jaGVja2JveF9faWNvbi1jb250YWluZXIgYWJzb2x1dGUtZnVsbCBmbGV4IGZsZXgtY2VudGVyIG5vLXdyYXAnXG4gICAgICAgICAgICAgIH0sIFtcbiAgICAgICAgICAgICAgICBoKFFJY29uLCB7XG4gICAgICAgICAgICAgICAgICBjbGFzczogJ3EtY2hlY2tib3hfX2ljb24nLFxuICAgICAgICAgICAgICAgICAgbmFtZTogaWNvbi52YWx1ZVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICBdXG4gICAgICAgICAgOiBbIGJnTm9kZSBdXG4gICAgICApXG4gICAgfVxuXG4gICAgcmV0dXJuIHVzZUNoZWNrYm94KCdjaGVja2JveCcsIGdldElubmVyKVxuICB9XG59KVxuIiwiaW1wb3J0IHsgaCwgY29tcHV0ZWQgfSBmcm9tICd2dWUnXG5cbmltcG9ydCBRSWNvbiBmcm9tICcuLi9pY29uL1FJY29uLmpzJ1xuXG5pbXBvcnQgdXNlQ2hlY2tib3gsIHsgdXNlQ2hlY2tib3hQcm9wcywgdXNlQ2hlY2tib3hFbWl0cyB9IGZyb20gJy4uL2NoZWNrYm94L3VzZS1jaGVja2JveC5qcydcblxuaW1wb3J0IHsgY3JlYXRlQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vdXRpbHMvcHJpdmF0ZS9jcmVhdGUuanMnXG5cbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZUNvbXBvbmVudCh7XG4gIG5hbWU6ICdRVG9nZ2xlJyxcblxuICBwcm9wczoge1xuICAgIC4uLnVzZUNoZWNrYm94UHJvcHMsXG5cbiAgICBpY29uOiBTdHJpbmcsXG4gICAgaWNvbkNvbG9yOiBTdHJpbmdcbiAgfSxcblxuICBlbWl0czogdXNlQ2hlY2tib3hFbWl0cyxcblxuICBzZXR1cCAocHJvcHMpIHtcbiAgICBmdW5jdGlvbiBnZXRJbm5lciAoaXNUcnVlLCBpc0luZGV0ZXJtaW5hdGUpIHtcbiAgICAgIGNvbnN0IGljb24gPSBjb21wdXRlZCgoKSA9PlxuICAgICAgICAoaXNUcnVlLnZhbHVlID09PSB0cnVlXG4gICAgICAgICAgPyBwcm9wcy5jaGVja2VkSWNvblxuICAgICAgICAgIDogKGlzSW5kZXRlcm1pbmF0ZS52YWx1ZSA9PT0gdHJ1ZSA/IHByb3BzLmluZGV0ZXJtaW5hdGVJY29uIDogcHJvcHMudW5jaGVja2VkSWNvbilcbiAgICAgICAgKSB8fCBwcm9wcy5pY29uXG4gICAgICApXG5cbiAgICAgIGNvbnN0IGNvbG9yID0gY29tcHV0ZWQoKCkgPT4gKGlzVHJ1ZS52YWx1ZSA9PT0gdHJ1ZSA/IHByb3BzLmljb25Db2xvciA6IG51bGwpKVxuXG4gICAgICByZXR1cm4gKCkgPT4gW1xuICAgICAgICBoKCdkaXYnLCB7IGNsYXNzOiAncS10b2dnbGVfX3RyYWNrJyB9KSxcblxuICAgICAgICBoKCdkaXYnLCB7XG4gICAgICAgICAgY2xhc3M6ICdxLXRvZ2dsZV9fdGh1bWIgYWJzb2x1dGUgZmxleCBmbGV4LWNlbnRlciBuby13cmFwJ1xuICAgICAgICB9LCBpY29uLnZhbHVlICE9PSB2b2lkIDBcbiAgICAgICAgICA/IFtcbiAgICAgICAgICAgICAgaChRSWNvbiwge1xuICAgICAgICAgICAgICAgIG5hbWU6IGljb24udmFsdWUsXG4gICAgICAgICAgICAgICAgY29sb3I6IGNvbG9yLnZhbHVlXG4gICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICBdXG4gICAgICAgICAgOiB2b2lkIDBcbiAgICAgICAgKVxuICAgICAgXVxuICAgIH1cblxuICAgIHJldHVybiB1c2VDaGVja2JveCgndG9nZ2xlJywgZ2V0SW5uZXIpXG4gIH1cbn0pXG4iLCJpbXBvcnQgeyBoLCBjb21wdXRlZCwgZ2V0Q3VycmVudEluc3RhbmNlIH0gZnJvbSAndnVlJ1xuXG5pbXBvcnQgUVJhZGlvIGZyb20gJy4uL3JhZGlvL1FSYWRpby5qcydcbmltcG9ydCBRQ2hlY2tib3ggZnJvbSAnLi4vY2hlY2tib3gvUUNoZWNrYm94LmpzJ1xuaW1wb3J0IFFUb2dnbGUgZnJvbSAnLi4vdG9nZ2xlL1FUb2dnbGUuanMnXG5cbmltcG9ydCB7IGNyZWF0ZUNvbXBvbmVudCB9IGZyb20gJy4uLy4uL3V0aWxzL3ByaXZhdGUvY3JlYXRlLmpzJ1xuXG5pbXBvcnQgdXNlRGFyaywgeyB1c2VEYXJrUHJvcHMgfSBmcm9tICcuLi8uLi9jb21wb3NhYmxlcy9wcml2YXRlL3VzZS1kYXJrLmpzJ1xuXG5jb25zdCBjb21wb25lbnRzID0ge1xuICByYWRpbzogUVJhZGlvLFxuICBjaGVja2JveDogUUNoZWNrYm94LFxuICB0b2dnbGU6IFFUb2dnbGVcbn1cblxuY29uc3QgdHlwZVZhbHVlcyA9IE9iamVjdC5rZXlzKGNvbXBvbmVudHMpXG5cbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZUNvbXBvbmVudCh7XG4gIG5hbWU6ICdRT3B0aW9uR3JvdXAnLFxuXG4gIHByb3BzOiB7XG4gICAgLi4udXNlRGFya1Byb3BzLFxuXG4gICAgbW9kZWxWYWx1ZToge1xuICAgICAgcmVxdWlyZWQ6IHRydWVcbiAgICB9LFxuICAgIG9wdGlvbnM6IHtcbiAgICAgIHR5cGU6IEFycmF5LFxuICAgICAgdmFsaWRhdG9yOiBvcHRzID0+IG9wdHMuZXZlcnkob3B0ID0+ICd2YWx1ZScgaW4gb3B0ICYmICdsYWJlbCcgaW4gb3B0KVxuICAgIH0sXG5cbiAgICBuYW1lOiBTdHJpbmcsXG5cbiAgICB0eXBlOiB7XG4gICAgICBkZWZhdWx0OiAncmFkaW8nLFxuICAgICAgdmFsaWRhdG9yOiB2ID0+IHR5cGVWYWx1ZXMuaW5jbHVkZXModilcbiAgICB9LFxuXG4gICAgY29sb3I6IFN0cmluZyxcbiAgICBrZWVwQ29sb3I6IEJvb2xlYW4sXG4gICAgZGVuc2U6IEJvb2xlYW4sXG5cbiAgICBzaXplOiBTdHJpbmcsXG5cbiAgICBsZWZ0TGFiZWw6IEJvb2xlYW4sXG4gICAgaW5saW5lOiBCb29sZWFuLFxuICAgIGRpc2FibGU6IEJvb2xlYW5cbiAgfSxcblxuICBlbWl0czogWyAndXBkYXRlOm1vZGVsVmFsdWUnIF0sXG5cbiAgc2V0dXAgKHByb3BzLCB7IGVtaXQsIHNsb3RzIH0pIHtcbiAgICBjb25zdCB7IHByb3h5OiB7ICRxIH0gfSA9IGdldEN1cnJlbnRJbnN0YW5jZSgpXG5cbiAgICBjb25zdCBhcnJheU1vZGVsID0gQXJyYXkuaXNBcnJheShwcm9wcy5tb2RlbFZhbHVlKVxuXG4gICAgaWYgKHByb3BzLnR5cGUgPT09ICdyYWRpbycpIHtcbiAgICAgIGlmIChhcnJheU1vZGVsID09PSB0cnVlKSB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoJ3Etb3B0aW9uLWdyb3VwOiBtb2RlbCBzaG91bGQgbm90IGJlIGFycmF5JylcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSBpZiAoYXJyYXlNb2RlbCA9PT0gZmFsc2UpIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoJ3Etb3B0aW9uLWdyb3VwOiBtb2RlbCBzaG91bGQgYmUgYXJyYXkgaW4geW91ciBjYXNlJylcbiAgICB9XG5cbiAgICBjb25zdCBpc0RhcmsgPSB1c2VEYXJrKHByb3BzLCAkcSlcblxuICAgIGNvbnN0IGNvbXBvbmVudCA9IGNvbXB1dGVkKCgpID0+IGNvbXBvbmVudHNbIHByb3BzLnR5cGUgXSlcblxuICAgIGNvbnN0IGNsYXNzZXMgPSBjb21wdXRlZCgoKSA9PlxuICAgICAgJ3Etb3B0aW9uLWdyb3VwIHEtZ3V0dGVyLXgtc20nXG4gICAgICArIChwcm9wcy5pbmxpbmUgPT09IHRydWUgPyAnIHEtb3B0aW9uLWdyb3VwLS1pbmxpbmUnIDogJycpXG4gICAgKVxuXG4gICAgY29uc3QgYXR0cnMgPSBjb21wdXRlZCgoKSA9PiB7XG4gICAgICBjb25zdCBhdHRycyA9IHt9XG5cbiAgICAgIGlmIChwcm9wcy50eXBlID09PSAncmFkaW8nKSB7XG4gICAgICAgIGF0dHJzLnJvbGUgPSAncmFkaW9ncm91cCdcblxuICAgICAgICBpZiAocHJvcHMuZGlzYWJsZSA9PT0gdHJ1ZSkge1xuICAgICAgICAgIGF0dHJzWyAnYXJpYS1kaXNhYmxlZCcgXSA9ICd0cnVlJ1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBhdHRyc1xuICAgIH0pXG5cbiAgICBmdW5jdGlvbiBvblVwZGF0ZU1vZGVsVmFsdWUgKHZhbHVlKSB7XG4gICAgICBlbWl0KCd1cGRhdGU6bW9kZWxWYWx1ZScsIHZhbHVlKVxuICAgIH1cblxuICAgIHJldHVybiAoKSA9PiBoKCdkaXYnLCB7XG4gICAgICBjbGFzczogY2xhc3Nlcy52YWx1ZSxcbiAgICAgIC4uLmF0dHJzLnZhbHVlXG4gICAgfSwgcHJvcHMub3B0aW9ucy5tYXAoKG9wdCwgaSkgPT4ge1xuICAgICAgLy8gVE9ETzogKFF2MykgTWFrZSB0aGUgJ29wdCcgYSBzZXBhcmF0ZSBwcm9wZXJ0eSBpbnN0ZWFkIG9mXG4gICAgICAvLyB0aGUgd2hvbGUgc2NvcGUgZm9yIGNvbnNpc3RlbmN5IGFuZCBmbGV4aWJpbGl0eVxuICAgICAgLy8gKGUuZy4geyBvcHQgfSBpbnN0ZWFkIG9mIG9wdClcbiAgICAgIGNvbnN0IGNoaWxkID0gc2xvdHNbICdsYWJlbC0nICsgaSBdICE9PSB2b2lkIDBcbiAgICAgICAgPyAoKSA9PiBzbG90c1sgJ2xhYmVsLScgKyBpIF0ob3B0KVxuICAgICAgICA6IChcbiAgICAgICAgICAgIHNsb3RzLmxhYmVsICE9PSB2b2lkIDBcbiAgICAgICAgICAgICAgPyAoKSA9PiBzbG90cy5sYWJlbChvcHQpXG4gICAgICAgICAgICAgIDogdm9pZCAwXG4gICAgICAgICAgKVxuXG4gICAgICByZXR1cm4gaCgnZGl2JywgW1xuICAgICAgICBoKGNvbXBvbmVudC52YWx1ZSwge1xuICAgICAgICAgIG1vZGVsVmFsdWU6IHByb3BzLm1vZGVsVmFsdWUsXG4gICAgICAgICAgdmFsOiBvcHQudmFsdWUsXG4gICAgICAgICAgbmFtZTogb3B0Lm5hbWUgPT09IHZvaWQgMCA/IHByb3BzLm5hbWUgOiBvcHQubmFtZSxcbiAgICAgICAgICBkaXNhYmxlOiBwcm9wcy5kaXNhYmxlIHx8IG9wdC5kaXNhYmxlLFxuICAgICAgICAgIGxhYmVsOiBjaGlsZCA9PT0gdm9pZCAwID8gb3B0LmxhYmVsIDogbnVsbCxcbiAgICAgICAgICBsZWZ0TGFiZWw6IG9wdC5sZWZ0TGFiZWwgPT09IHZvaWQgMCA/IHByb3BzLmxlZnRMYWJlbCA6IG9wdC5sZWZ0TGFiZWwsXG4gICAgICAgICAgY29sb3I6IG9wdC5jb2xvciA9PT0gdm9pZCAwID8gcHJvcHMuY29sb3IgOiBvcHQuY29sb3IsXG4gICAgICAgICAgY2hlY2tlZEljb246IG9wdC5jaGVja2VkSWNvbixcbiAgICAgICAgICB1bmNoZWNrZWRJY29uOiBvcHQudW5jaGVja2VkSWNvbixcbiAgICAgICAgICBkYXJrOiBvcHQuZGFyayB8fCBpc0RhcmsudmFsdWUsXG4gICAgICAgICAgc2l6ZTogb3B0LnNpemUgPT09IHZvaWQgMCA/IHByb3BzLnNpemUgOiBvcHQuc2l6ZSxcbiAgICAgICAgICBkZW5zZTogcHJvcHMuZGVuc2UsXG4gICAgICAgICAga2VlcENvbG9yOiBvcHQua2VlcENvbG9yID09PSB2b2lkIDAgPyBwcm9wcy5rZWVwQ29sb3IgOiBvcHQua2VlcENvbG9yLFxuICAgICAgICAgICdvblVwZGF0ZTptb2RlbFZhbHVlJzogb25VcGRhdGVNb2RlbFZhbHVlXG4gICAgICAgIH0sIGNoaWxkKVxuICAgICAgXSlcbiAgICB9KSlcbiAgfVxufSlcbiIsIjx0ZW1wbGF0ZT5cbiAgICA8cS1wYWdlIHBhZGRpbmc+XG4gICAgICAgIDxoND5UaGVtZTwvaDQ+XG4gICAgICAgIDxxLW9wdGlvbi1ncm91cCB2LW1vZGVsPVwic2V0dGluZ3MudGhlbWVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgOm9wdGlvbnM9XCJ0aGVtZU9wdGlvbnNcIlxuICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I9XCJwcmltYXJ5XCJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlubGluZVxuICAgICAgICAgICAgICAgICAgICAgICAgQHVwZGF0ZTptb2RlbC12YWx1ZT1cIm9uVGhlbWVDaGFuZ2VcIiAvPlxuICAgICAgICA8aDQ+U3RvcmVzPC9oND5cbiAgICAgICAgPHA+TGlzdCBvZiBzdG9yZXM8L3A+XG4gICAgPC9xLXBhZ2U+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0IGxhbmc9XCJ0c1wiPlxuICAgIGltcG9ydCB7IGRlZmluZUNvbXBvbmVudCB9IGZyb20gJ3Z1ZSc7XG4gICAgaW1wb3J0IHsgU2V0dGluZ1NlcnZpY2UsIFNldHRpbmdzRm9ybSB9IGZyb20gJ3NyYy9zZXJ2aWNlcy9kb21haW4nO1xuXG4gICAgZXhwb3J0IGRlZmF1bHQgZGVmaW5lQ29tcG9uZW50KHtcbiAgICAgICAgbmFtZTogJ0luZGV4UGFnZScsXG4gICAgICAgIGRhdGEoKSB7XG4gICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgIHRoZW1lT3B0aW9uczogW1xuICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICBsYWJlbDogJ1N5c3RlbScsXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogbnVsbFxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICBsYWJlbDogJ0xpZ2h0JyxcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBmYWxzZVxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICBsYWJlbDogJ0RhcmsnLFxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRydWVcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgc2V0dGluZ3M6IHt9IGFzIFNldHRpbmdzRm9ybSxcbiAgICAgICAgICAgIH07XG4gICAgICAgIH0sXG4gICAgICAgIGFzeW5jIGJlZm9yZU1vdW50KCkge1xuICAgICAgICAgICAgYXdhaXQgdGhpcy5nZXRTZXR0aW5ncygpO1xuICAgICAgICB9LFxuICAgICAgICBtZXRob2RzOiB7XG4gICAgICAgICAgICBzZXRUaGVtZSgpIHtcbiAgICAgICAgICAgICAgICB0aGlzLiRxLmRhcmsuc2V0KHRoaXMuc2V0dGluZ3MudGhlbWUgPz8gJ2F1dG8nKTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBvblRoZW1lQ2hhbmdlKCkge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0VGhlbWUoKTtcbiAgICAgICAgICAgICAgICB0aGlzLm9uQ2hhbmdlKCk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgb25DaGFuZ2UoKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTZXR0aW5ncygpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGFzeW5jIGdldFNldHRpbmdzKCkge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0dGluZ3MgPSBhd2FpdCBTZXR0aW5nU2VydmljZS5nZXQoKTtcbiAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHNldFNldHRpbmdzKCkge1xuICAgICAgICAgICAgICAgIFNldHRpbmdTZXJ2aWNlLnNhdmUoeyBib2R5OiB0aGlzLnNldHRpbmdzIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSk7XG48L3NjcmlwdD5cbiJdLCJuYW1lcyI6WyJfY3JlYXRlRWxlbWVudFZOb2RlIiwiX29wZW5CbG9jayIsIl9jcmVhdGVCbG9jayIsIl93aXRoQ3R4IiwiX2NyZWF0ZVZOb2RlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVlLDBCQUFVLE9BQU8sU0FBUztBQUN2QyxRQUFNLGFBQWEsSUFBSSxJQUFJO0FBRTNCLFFBQU0sa0JBQWtCLFNBQVMsTUFBTTtBQUNyQyxRQUFJLE1BQU0sWUFBWSxNQUFNO0FBQzFCLGFBQU87QUFBQSxJQUNSO0FBRUQsV0FBTyxFQUFFLFFBQVE7QUFBQSxNQUNmLEtBQUs7QUFBQSxNQUNMLE9BQU87QUFBQSxNQUNQLFVBQVU7QUFBQSxJQUNoQixDQUFLO0FBQUEsRUFDTCxDQUFHO0FBRUQseUJBQXdCLEdBQUc7QUFDekIsVUFBTSxPQUFPLFFBQVE7QUFFckIsUUFBSSxNQUFNLFVBQVUsRUFBRSxLQUFLLFFBQVEsS0FBSyxNQUFNLEdBQUc7QUFDL0MsVUFDRSxTQUFTLFFBQ04sU0FBUyxrQkFBa0IsUUFDM0IsS0FBSyxTQUFTLFNBQVMsYUFBYSxNQUFNLE1BQzdDO0FBQ0EsYUFBSyxNQUFPO0FBQUEsTUFDYjtBQUFBLElBQ0YsV0FFQyxXQUFXLFVBQVUsUUFDakIsT0FBTSxVQUFXLFNBQVMsUUFBUSxLQUFLLFNBQVMsRUFBRSxNQUFNLE1BQU0sT0FDbEU7QUFDQSxpQkFBVyxNQUFNLE1BQU87QUFBQSxJQUN6QjtBQUFBLEVBQ0Y7QUFFRCxTQUFPO0FBQUEsSUFDTDtBQUFBLElBQ0E7QUFBQSxFQUNEO0FBQ0g7QUN6Q0EsSUFBZSxjQUFBO0FBQUEsRUFDYixJQUFJO0FBQUEsRUFDSixJQUFJO0FBQUEsRUFDSixJQUFJO0FBQUEsRUFDSixJQUFJO0FBQUEsRUFDSixJQUFJO0FBQ047QUNRQSxNQUFNLE1BQU0sRUFBRSxPQUFPO0FBQUEsRUFDbkIsS0FBSztBQUFBLEVBQ0wsT0FBTztBQUFBLEVBQ1AsU0FBUztBQUFBLEVBQ1QsZUFBZTtBQUNqQixHQUFHO0FBQUEsRUFDRCxFQUFFLFFBQVE7QUFBQSxJQUNSLEdBQUc7QUFBQSxFQUNQLENBQUc7QUFBQSxFQUVELEVBQUUsUUFBUTtBQUFBLElBQ1IsT0FBTztBQUFBLElBQ1AsR0FBRztBQUFBLEVBQ1AsQ0FBRztBQUNILENBQUM7QUFFRCxJQUFBLFNBQWUsZ0JBQWdCO0FBQUEsRUFDN0IsTUFBTTtBQUFBLEVBRU4sT0FBTywrREFDRixlQUNBLGVBQ0EsZUFIRTtBQUFBLElBS0wsWUFBWSxFQUFFLFVBQVUsS0FBTTtBQUFBLElBQzlCLEtBQUssRUFBRSxVQUFVLEtBQU07QUFBQSxJQUV2QixPQUFPO0FBQUEsSUFDUCxXQUFXO0FBQUEsSUFFWCxhQUFhO0FBQUEsSUFDYixlQUFlO0FBQUEsSUFFZixPQUFPO0FBQUEsSUFDUCxXQUFXO0FBQUEsSUFDWCxPQUFPO0FBQUEsSUFFUCxTQUFTO0FBQUEsSUFDVCxVQUFVLENBQUUsUUFBUSxNQUFRO0FBQUEsRUFDN0I7QUFBQSxFQUVELE9BQU8sQ0FBRSxtQkFBcUI7QUFBQSxFQUU5QixNQUFPLE9BQU8sRUFBRSxPQUFPLFFBQVE7QUFDN0IsVUFBTSxFQUFFLFVBQVUsbUJBQW9CO0FBRXRDLFVBQU0sU0FBUyxRQUFRLE9BQU8sTUFBTSxFQUFFO0FBQ3RDLFVBQU0sWUFBWSxRQUFRLE9BQU8sV0FBVztBQUU1QyxVQUFNLFVBQVUsSUFBSSxJQUFJO0FBQ3hCLFVBQU0sRUFBRSxpQkFBaUIsa0JBQWtCLGlCQUFpQixPQUFPLE9BQU87QUFFMUUsVUFBTSxTQUFTLFNBQVMsTUFBTSxNQUFNLGVBQWUsTUFBTSxHQUFHO0FBRTVELFVBQU0sVUFBVSxTQUFTLE1BQ3ZCLHNFQUNHLE9BQU0sWUFBWSxPQUFPLGNBQWMsTUFDdkMsUUFBTyxVQUFVLE9BQU8sbUJBQW1CLE1BQzNDLE9BQU0sVUFBVSxPQUFPLG9CQUFvQixNQUMzQyxPQUFNLGNBQWMsT0FBTyxhQUFhLEdBQzVDO0FBRUQsVUFBTSxhQUFhLFNBQVMsTUFBTTtBQUNoQyxZQUFNLFFBQVEsTUFBTSxVQUFVLFVBQzVCLE9BQU0sY0FBYyxRQUNqQixPQUFPLFVBQVUsUUFFbEIsU0FBVSxNQUFNLFVBQ2hCO0FBRUosYUFBTyxvREFDaUIsT0FBTyxVQUFVLE9BQU8sV0FBVyxVQUFZO0FBQUEsSUFDN0UsQ0FBSztBQUVELFVBQU0sT0FBTyxTQUFTLE1BQ25CLFFBQU8sVUFBVSxPQUNkLE1BQU0sY0FDTixNQUFNLGtCQUNMLElBQ047QUFFRCxVQUFNLFdBQVcsU0FBUyxNQUN4QixNQUFNLFlBQVksT0FBTyxLQUFLLE1BQU0sWUFBWSxDQUNqRDtBQUVELFVBQU0sWUFBWSxTQUFTLE1BQU07QUFDL0IsWUFBTSxPQUFPLEVBQUUsTUFBTSxRQUFTO0FBRTlCLFlBQU0sU0FBUyxVQUFVLE9BQU8sT0FBTyxNQUFNO0FBQUEsUUFDM0MsWUFBWSxPQUFPLFVBQVUsT0FBTyxZQUFZO0FBQUEsUUFDaEQsTUFBTSxNQUFNO0FBQUEsUUFDWixPQUFPLE1BQU07QUFBQSxNQUNyQixDQUFPO0FBRUQsYUFBTztBQUFBLElBQ2IsQ0FBSztBQUVELFVBQU0sa0JBQWtCLGNBQWMsU0FBUztBQUUvQyxxQkFBa0IsR0FBRztBQUNuQixVQUFJLE1BQU0sUUFBUTtBQUNoQix1QkFBZSxDQUFDO0FBQ2hCLHNCQUFjLENBQUM7QUFBQSxNQUNoQjtBQUVELFVBQUksTUFBTSxZQUFZLFFBQVEsT0FBTyxVQUFVLE1BQU07QUFDbkQsYUFBSyxxQkFBcUIsTUFBTSxLQUFLLENBQUM7QUFBQSxNQUN2QztBQUFBLElBQ0Y7QUFFRCx1QkFBb0IsR0FBRztBQUNyQixVQUFJLEVBQUUsWUFBWSxNQUFNLEVBQUUsWUFBWSxJQUFJO0FBQ3hDLHVCQUFlLENBQUM7QUFBQSxNQUNqQjtBQUFBLElBQ0Y7QUFFRCxxQkFBa0IsR0FBRztBQUNuQixVQUFJLEVBQUUsWUFBWSxNQUFNLEVBQUUsWUFBWSxJQUFJO0FBQ3hDLGdCQUFRLENBQUM7QUFBQSxNQUNWO0FBQUEsSUFDRjtBQUdELFdBQU8sT0FBTyxPQUFPLEVBQUUsS0FBSyxRQUFPLENBQUU7QUFFckMsV0FBTyxNQUFNO0FBQ1gsWUFBTSxVQUFVLEtBQUssVUFBVSxPQUMzQjtBQUFBLFFBQ0UsRUFBRSxPQUFPO0FBQUEsVUFDUCxLQUFLO0FBQUEsVUFDTCxPQUFPO0FBQUEsUUFDckIsR0FBZTtBQUFBLFVBQ0QsRUFBRSxPQUFPO0FBQUEsWUFDUCxPQUFPO0FBQUEsWUFDUCxNQUFNLEtBQUs7QUFBQSxVQUMzQixDQUFlO0FBQUEsUUFDZixDQUFhO0FBQUEsTUFDRixJQUNELENBQUUsR0FBSztBQUVYLFlBQU0sWUFBWSxRQUFRLGdCQUN4QixTQUNBLFdBQ0Esc0NBQ0Q7QUFFRCxZQUFNLFFBQVE7QUFBQSxRQUNaLEVBQUUsT0FBTztBQUFBLFVBQ1AsT0FBTyxXQUFXO0FBQUEsVUFDbEIsT0FBTyxVQUFVO0FBQUEsUUFDbEIsR0FBRSxPQUFPO0FBQUEsTUFDWDtBQUVELFVBQUksZ0JBQWdCLFVBQVUsTUFBTTtBQUNsQyxjQUFNLEtBQUssZ0JBQWdCLEtBQUs7QUFBQSxNQUNqQztBQUVELFlBQU0sUUFBUSxNQUFNLFVBQVUsU0FDMUIsV0FBVyxNQUFNLFNBQVMsQ0FBRSxNQUFNLEtBQUssQ0FBRSxJQUN6QyxNQUFNLE1BQU0sT0FBTztBQUV2QixnQkFBVSxVQUFVLE1BQU0sS0FDeEIsRUFBRSxPQUFPO0FBQUEsUUFDUCxPQUFPO0FBQUEsTUFDUixHQUFFLEtBQUssQ0FDVDtBQUVELGFBQU8sRUFBRSxPQUFPO0FBQUEsUUFDZCxLQUFLO0FBQUEsUUFDTCxPQUFPLFFBQVE7QUFBQSxRQUNmLFVBQVUsU0FBUztBQUFBLFFBQ25CLE1BQU07QUFBQSxRQUNOLGNBQWMsTUFBTTtBQUFBLFFBQ3BCLGdCQUFnQixPQUFPLFVBQVUsT0FBTyxTQUFTO0FBQUEsUUFDakQsaUJBQWlCLE1BQU0sWUFBWSxPQUFPLFNBQVM7QUFBQSxRQUNuRDtBQUFBLFFBQ0E7QUFBQSxRQUNBO0FBQUEsTUFDRCxHQUFFLEtBQUs7QUFBQSxJQUNUO0FBQUEsRUFDRjtBQUNILENBQUM7QUN4TE0sTUFBTSxtQkFBbUIsK0RBQzNCLGVBQ0EsZUFDQSxlQUgyQjtBQUFBLEVBSzlCLFlBQVk7QUFBQSxJQUNWLFVBQVU7QUFBQSxJQUNWLFNBQVM7QUFBQSxFQUNWO0FBQUEsRUFDRCxLQUFLLENBQUU7QUFBQSxFQUVQLFdBQVcsRUFBRSxTQUFTLEtBQU07QUFBQSxFQUM1QixZQUFZLEVBQUUsU0FBUyxNQUFPO0FBQUEsRUFDOUIsb0JBQW9CLEVBQUUsU0FBUyxLQUFNO0FBQUEsRUFFckMsYUFBYTtBQUFBLEVBQ2IsZUFBZTtBQUFBLEVBQ2YsbUJBQW1CO0FBQUEsRUFFbkIsYUFBYTtBQUFBLElBQ1gsTUFBTTtBQUFBLElBQ04sV0FBVyxPQUFLLE1BQU0sUUFBUSxNQUFNO0FBQUEsRUFDckM7QUFBQSxFQUNELHFCQUFxQjtBQUFBLEVBRXJCLE9BQU87QUFBQSxFQUNQLFdBQVc7QUFBQSxFQUVYLE9BQU87QUFBQSxFQUNQLFdBQVc7QUFBQSxFQUNYLE9BQU87QUFBQSxFQUVQLFNBQVM7QUFBQSxFQUNULFVBQVUsQ0FBRSxRQUFRLE1BQVE7QUFDOUI7QUFFTyxNQUFNLG1CQUFtQixDQUFFLG1CQUFxQjtBQUV4QyxxQkFBVSxNQUFNLFVBQVU7QUFDdkMsUUFBTSxFQUFFLE9BQU8sT0FBTyxNQUFNLFVBQVUsbUJBQW9CO0FBQzFELFFBQU0sRUFBRSxPQUFPO0FBRWYsUUFBTSxTQUFTLFFBQVEsT0FBTyxFQUFFO0FBRWhDLFFBQU0sVUFBVSxJQUFJLElBQUk7QUFDeEIsUUFBTSxFQUFFLGlCQUFpQixrQkFBa0IsaUJBQWlCLE9BQU8sT0FBTztBQUMxRSxRQUFNLFlBQVksUUFBUSxPQUFPLFdBQVc7QUFFNUMsUUFBTSxlQUFlLFNBQVMsTUFDNUIsTUFBTSxRQUFRLFVBQVUsTUFBTSxRQUFRLE1BQU0sVUFBVSxDQUN2RDtBQUVELFFBQU0sUUFBUSxTQUFTLE1BQ3JCLGFBQWEsVUFBVSxPQUNuQixNQUFNLFdBQVcsUUFBUSxNQUFNLEdBQUcsSUFDbEMsRUFDTDtBQUVELFFBQU0sU0FBUyxTQUFTLE1BQ3RCLGFBQWEsVUFBVSxPQUNuQixNQUFNLFFBQVEsS0FDZCxNQUFNLGVBQWUsTUFBTSxTQUNoQztBQUVELFFBQU0sVUFBVSxTQUFTLE1BQ3ZCLGFBQWEsVUFBVSxPQUNuQixNQUFNLFVBQVUsS0FDaEIsTUFBTSxlQUFlLE1BQU0sVUFDaEM7QUFFRCxRQUFNLGtCQUFrQixTQUFTLE1BQy9CLE9BQU8sVUFBVSxTQUFTLFFBQVEsVUFBVSxLQUM3QztBQUVELFFBQU0sV0FBVyxTQUFTLE1BQ3hCLE1BQU0sWUFBWSxPQUFPLEtBQUssTUFBTSxZQUFZLENBQ2pEO0FBRUQsUUFBTSxVQUFVLFNBQVMsTUFDdkIsS0FBTSxtRUFDSCxPQUFNLFlBQVksT0FBTyxjQUFjLE1BQ3ZDLFFBQU8sVUFBVSxPQUFPLE1BQU8sZUFBZ0IsTUFDL0MsT0FBTSxVQUFVLE9BQU8sTUFBTyxnQkFBaUIsTUFDL0MsT0FBTSxjQUFjLE9BQU8sYUFBYSxHQUM1QztBQUVELFFBQU0sYUFBYSxTQUFTLE1BQU07QUFDaEMsVUFBTSxRQUFRLE9BQU8sVUFBVSxPQUFPLFdBQVksUUFBUSxVQUFVLE9BQU8sVUFBVTtBQUNyRixVQUFNLFFBQVEsTUFBTSxVQUFVLFVBQzVCLE9BQU0sY0FBYyxRQUNoQixVQUFTLFdBQVcsT0FBTyxVQUFVLE9BQU8sUUFBUSxVQUFVLFNBRWhFLFNBQVUsTUFBTSxVQUNoQjtBQUVKLFdBQU8sS0FBTSxrREFBb0QsZ0JBQWtCLFFBQVU7QUFBQSxFQUNqRyxDQUFHO0FBRUQsUUFBTSxZQUFZLFNBQVMsTUFBTTtBQUMvQixVQUFNLE9BQU8sRUFBRSxNQUFNLFdBQVk7QUFFakMsVUFBTSxTQUFTLFVBQVUsT0FBTyxPQUFPLE1BQU07QUFBQSxNQUMzQyxZQUFZLE9BQU8sVUFBVSxPQUFPLFlBQVk7QUFBQSxNQUNoRCxNQUFNLE1BQU07QUFBQSxNQUNaLE9BQU8sYUFBYSxVQUFVLE9BQzFCLE1BQU0sTUFDTixNQUFNO0FBQUEsSUFDaEIsQ0FBSztBQUVELFdBQU87QUFBQSxFQUNYLENBQUc7QUFFRCxRQUFNLGtCQUFrQixjQUFjLFNBQVM7QUFFL0MsUUFBTSxhQUFhLFNBQVMsTUFBTTtBQUNoQyxVQUFNLFFBQVE7QUFBQSxNQUNaLFVBQVUsU0FBUztBQUFBLE1BQ25CLE1BQU07QUFBQSxNQUNOLGNBQWMsTUFBTTtBQUFBLE1BQ3BCLGdCQUFnQixnQkFBZ0IsVUFBVSxPQUN0QyxVQUNDLE9BQU8sVUFBVSxPQUFPLFNBQVM7QUFBQSxJQUN2QztBQUVELFFBQUksTUFBTSxZQUFZLE1BQU07QUFDMUIsWUFBTyxtQkFBb0I7QUFBQSxJQUM1QjtBQUVELFdBQU87QUFBQSxFQUNYLENBQUc7QUFFRCxtQkFBa0IsR0FBRztBQUNuQixRQUFJLE1BQU0sUUFBUTtBQUNoQixxQkFBZSxDQUFDO0FBQ2hCLG9CQUFjLENBQUM7QUFBQSxJQUNoQjtBQUVELFFBQUksTUFBTSxZQUFZLE1BQU07QUFDMUIsV0FBSyxxQkFBcUIsYUFBYyxHQUFFLENBQUM7QUFBQSxJQUM1QztBQUFBLEVBQ0Y7QUFFRCwwQkFBeUI7QUFDdkIsUUFBSSxhQUFhLFVBQVUsTUFBTTtBQUMvQixVQUFJLE9BQU8sVUFBVSxNQUFNO0FBQ3pCLGNBQU0sTUFBTSxNQUFNLFdBQVcsTUFBTztBQUNwQyxZQUFJLE9BQU8sTUFBTSxPQUFPLENBQUM7QUFDekIsZUFBTztBQUFBLE1BQ1I7QUFFRCxhQUFPLE1BQU0sV0FBVyxPQUFPLENBQUUsTUFBTSxHQUFHLENBQUU7QUFBQSxJQUM3QztBQUVELFFBQUksT0FBTyxVQUFVLE1BQU07QUFDekIsVUFBSSxNQUFNLGdCQUFnQixRQUFRLE1BQU0sd0JBQXdCLE9BQU87QUFDckUsZUFBTyxNQUFNO0FBQUEsTUFDZDtBQUFBLElBQ0YsV0FDUSxRQUFRLFVBQVUsTUFBTTtBQUMvQixVQUFJLE1BQU0sZ0JBQWdCLFFBQVEsTUFBTSx3QkFBd0IsT0FBTztBQUNyRSxlQUFPLE1BQU07QUFBQSxNQUNkO0FBQUEsSUFDRixPQUNJO0FBQ0gsYUFBTyxNQUFNLGdCQUFnQixPQUN6QixNQUFNLFlBQ04sTUFBTTtBQUFBLElBQ1g7QUFFRCxXQUFPLE1BQU07QUFBQSxFQUNkO0FBRUQscUJBQW9CLEdBQUc7QUFDckIsUUFBSSxFQUFFLFlBQVksTUFBTSxFQUFFLFlBQVksSUFBSTtBQUN4QyxxQkFBZSxDQUFDO0FBQUEsSUFDakI7QUFBQSxFQUNGO0FBRUQsbUJBQWtCLEdBQUc7QUFDbkIsUUFBSSxFQUFFLFlBQVksTUFBTSxFQUFFLFlBQVksSUFBSTtBQUN4QyxjQUFRLENBQUM7QUFBQSxJQUNWO0FBQUEsRUFDRjtBQUVELFFBQU0sa0JBQWtCLFNBQVMsUUFBUSxlQUFlO0FBR3hELFNBQU8sT0FBTyxPQUFPLEVBQUUsUUFBUSxRQUFPLENBQUU7QUFFeEMsU0FBTyxNQUFNO0FBQ1gsVUFBTSxRQUFRLGdCQUFpQjtBQUUvQixVQUFNLFlBQVksUUFBUSxnQkFDeEIsT0FDQSxXQUNBLE1BQU8sMkNBQ1I7QUFFRCxVQUFNLFFBQVE7QUFBQSxNQUNaLEVBQUUsT0FBTztBQUFBLFFBQ1AsT0FBTyxXQUFXO0FBQUEsUUFDbEIsT0FBTyxVQUFVO0FBQUEsTUFDbEIsR0FBRSxLQUFLO0FBQUEsSUFDVDtBQUVELFFBQUksZ0JBQWdCLFVBQVUsTUFBTTtBQUNsQyxZQUFNLEtBQUssZ0JBQWdCLEtBQUs7QUFBQSxJQUNqQztBQUVELFVBQU0sUUFBUSxNQUFNLFVBQVUsU0FDMUIsV0FBVyxNQUFNLFNBQVMsQ0FBRSxNQUFNLEtBQUssQ0FBRSxJQUN6QyxNQUFNLE1BQU0sT0FBTztBQUV2QixjQUFVLFVBQVUsTUFBTSxLQUN4QixFQUFFLE9BQU87QUFBQSxNQUNQLE9BQU8sS0FBTTtBQUFBLElBQ2QsR0FBRSxLQUFLLENBQ1Q7QUFFRCxXQUFPLEVBQUUsT0FBTztBQUFBLE1BQ2QsS0FBSztBQUFBLE1BQ0wsT0FBTyxRQUFRO0FBQUEsT0FDWixXQUFXLFFBSEE7QUFBQSxNQUlkO0FBQUEsTUFDQTtBQUFBLE1BQ0E7QUFBQSxJQUNELElBQUUsS0FBSztBQUFBLEVBQ1Q7QUFDSDtBQ3hPQSxNQUFNLFNBQVMsRUFBRSxPQUFPO0FBQUEsRUFDdEIsS0FBSztBQUFBLEVBQ0wsT0FBTztBQUNULEdBQUc7QUFBQSxFQUNELEVBQUUsT0FBTztBQUFBLElBQ1AsT0FBTztBQUFBLElBQ1AsU0FBUztBQUFBLElBQ1QsZUFBZTtBQUFBLEVBQ25CLEdBQUs7QUFBQSxJQUNELEVBQUUsUUFBUTtBQUFBLE1BQ1IsT0FBTztBQUFBLE1BQ1AsTUFBTTtBQUFBLE1BQ04sR0FBRztBQUFBLElBQ1QsQ0FBSztBQUFBLElBRUQsRUFBRSxRQUFRO0FBQUEsTUFDUixPQUFPO0FBQUEsTUFDUCxHQUFHO0FBQUEsSUFDVCxDQUFLO0FBQUEsRUFDTCxDQUFHO0FBQ0gsQ0FBQztBQUVELElBQUEsWUFBZSxnQkFBZ0I7QUFBQSxFQUM3QixNQUFNO0FBQUEsRUFFTixPQUFPO0FBQUEsRUFDUCxPQUFPO0FBQUEsRUFFUCxNQUFPLE9BQU87QUFDWixzQkFBbUIsUUFBUSxpQkFBaUI7QUFDMUMsWUFBTSxPQUFPLFNBQVMsTUFDbkIsUUFBTyxVQUFVLE9BQ2QsTUFBTSxjQUNMLGdCQUFnQixVQUFVLE9BQ3ZCLE1BQU0sb0JBQ04sTUFBTSxrQkFFVCxJQUNOO0FBRUQsYUFBTyxNQUNMLEtBQUssVUFBVSxPQUNYO0FBQUEsUUFDRSxFQUFFLE9BQU87QUFBQSxVQUNQLEtBQUs7QUFBQSxVQUNMLE9BQU87QUFBQSxRQUN2QixHQUFpQjtBQUFBLFVBQ0QsRUFBRSxPQUFPO0FBQUEsWUFDUCxPQUFPO0FBQUEsWUFDUCxNQUFNLEtBQUs7QUFBQSxVQUM3QixDQUFpQjtBQUFBLFFBQ2pCLENBQWU7QUFBQSxNQUNGLElBQ0QsQ0FBRSxNQUFRO0FBQUEsSUFFakI7QUFFRCxXQUFPLFlBQVksWUFBWSxRQUFRO0FBQUEsRUFDeEM7QUFDSCxDQUFDO0FDMURELElBQUEsVUFBZSxnQkFBZ0I7QUFBQSxFQUM3QixNQUFNO0FBQUEsRUFFTixPQUFPLGlDQUNGLG1CQURFO0FBQUEsSUFHTCxNQUFNO0FBQUEsSUFDTixXQUFXO0FBQUEsRUFDWjtBQUFBLEVBRUQsT0FBTztBQUFBLEVBRVAsTUFBTyxPQUFPO0FBQ1osc0JBQW1CLFFBQVEsaUJBQWlCO0FBQzFDLFlBQU0sT0FBTyxTQUFTLE1BQ25CLFFBQU8sVUFBVSxPQUNkLE1BQU0sY0FDTCxnQkFBZ0IsVUFBVSxPQUFPLE1BQU0sb0JBQW9CLE1BQU0sa0JBQ2pFLE1BQU0sSUFDWjtBQUVELFlBQU0sUUFBUSxTQUFTLE1BQU8sT0FBTyxVQUFVLE9BQU8sTUFBTSxZQUFZLElBQUs7QUFFN0UsYUFBTyxNQUFNO0FBQUEsUUFDWCxFQUFFLE9BQU8sRUFBRSxPQUFPLGtCQUFpQixDQUFFO0FBQUEsUUFFckMsRUFBRSxPQUFPO0FBQUEsVUFDUCxPQUFPO0FBQUEsUUFDakIsR0FBVyxLQUFLLFVBQVUsU0FDZDtBQUFBLFVBQ0UsRUFBRSxPQUFPO0FBQUEsWUFDUCxNQUFNLEtBQUs7QUFBQSxZQUNYLE9BQU8sTUFBTTtBQUFBLFVBQzdCLENBQWU7QUFBQSxRQUNGLElBQ0QsTUFDSDtBQUFBLE1BQ0Y7QUFBQSxJQUNGO0FBRUQsV0FBTyxZQUFZLFVBQVUsUUFBUTtBQUFBLEVBQ3RDO0FBQ0gsQ0FBQztBQ3hDRCxNQUFNLGFBQWE7QUFBQSxFQUNqQixPQUFPO0FBQUEsRUFDUCxVQUFVO0FBQUEsRUFDVixRQUFRO0FBQ1Y7QUFFQSxNQUFNLGFBQWEsT0FBTyxLQUFLLFVBQVU7QUFFekMsSUFBQSxlQUFlLGdCQUFnQjtBQUFBLEVBQzdCLE1BQU07QUFBQSxFQUVOLE9BQU8saUNBQ0YsZUFERTtBQUFBLElBR0wsWUFBWTtBQUFBLE1BQ1YsVUFBVTtBQUFBLElBQ1g7QUFBQSxJQUNELFNBQVM7QUFBQSxNQUNQLE1BQU07QUFBQSxNQUNOLFdBQVcsVUFBUSxLQUFLLE1BQU0sU0FBTyxXQUFXLE9BQU8sV0FBVyxHQUFHO0FBQUEsSUFDdEU7QUFBQSxJQUVELE1BQU07QUFBQSxJQUVOLE1BQU07QUFBQSxNQUNKLFNBQVM7QUFBQSxNQUNULFdBQVcsT0FBSyxXQUFXLFNBQVMsQ0FBQztBQUFBLElBQ3RDO0FBQUEsSUFFRCxPQUFPO0FBQUEsSUFDUCxXQUFXO0FBQUEsSUFDWCxPQUFPO0FBQUEsSUFFUCxNQUFNO0FBQUEsSUFFTixXQUFXO0FBQUEsSUFDWCxRQUFRO0FBQUEsSUFDUixTQUFTO0FBQUEsRUFDVjtBQUFBLEVBRUQsT0FBTyxDQUFFLG1CQUFxQjtBQUFBLEVBRTlCLE1BQU8sT0FBTyxFQUFFLE1BQU0sU0FBUztBQUM3QixVQUFNLEVBQUUsT0FBTyxFQUFFLFNBQVMsbUJBQW9CO0FBRTlDLFVBQU0sYUFBYSxNQUFNLFFBQVEsTUFBTSxVQUFVO0FBRWpELFFBQUksTUFBTSxTQUFTLFNBQVM7QUFDMUIsVUFBSSxlQUFlLE1BQU07QUFDdkIsZ0JBQVEsTUFBTSwyQ0FBMkM7QUFBQSxNQUMxRDtBQUFBLElBQ0YsV0FDUSxlQUFlLE9BQU87QUFDN0IsY0FBUSxNQUFNLG9EQUFvRDtBQUFBLElBQ25FO0FBRUQsVUFBTSxTQUFTLFFBQVEsT0FBTyxFQUFFO0FBRWhDLFVBQU0sWUFBWSxTQUFTLE1BQU0sV0FBWSxNQUFNLEtBQU07QUFFekQsVUFBTSxVQUFVLFNBQVMsTUFDdkIsaUNBQ0csT0FBTSxXQUFXLE9BQU8sNEJBQTRCLEdBQ3hEO0FBRUQsVUFBTSxRQUFRLFNBQVMsTUFBTTtBQUMzQixZQUFNLFNBQVEsQ0FBRTtBQUVoQixVQUFJLE1BQU0sU0FBUyxTQUFTO0FBQzFCLGVBQU0sT0FBTztBQUViLFlBQUksTUFBTSxZQUFZLE1BQU07QUFDMUIsaUJBQU8sbUJBQW9CO0FBQUEsUUFDNUI7QUFBQSxNQUNGO0FBRUQsYUFBTztBQUFBLElBQ2IsQ0FBSztBQUVELGdDQUE2QixPQUFPO0FBQ2xDLFdBQUsscUJBQXFCLEtBQUs7QUFBQSxJQUNoQztBQUVELFdBQU8sTUFBTSxFQUFFLE9BQU87QUFBQSxNQUNwQixPQUFPLFFBQVE7QUFBQSxPQUNaLE1BQU0sUUFDUixNQUFNLFFBQVEsSUFBSSxDQUFDLEtBQUssTUFBTTtBQUkvQixZQUFNLFFBQVEsTUFBTyxXQUFXLE9BQVEsU0FDcEMsTUFBTSxNQUFPLFdBQVcsR0FBSSxHQUFHLElBRTdCLE1BQU0sVUFBVSxTQUNaLE1BQU0sTUFBTSxNQUFNLEdBQUcsSUFDckI7QUFHVixhQUFPLEVBQUUsT0FBTztBQUFBLFFBQ2QsRUFBRSxVQUFVLE9BQU87QUFBQSxVQUNqQixZQUFZLE1BQU07QUFBQSxVQUNsQixLQUFLLElBQUk7QUFBQSxVQUNULE1BQU0sSUFBSSxTQUFTLFNBQVMsTUFBTSxPQUFPLElBQUk7QUFBQSxVQUM3QyxTQUFTLE1BQU0sV0FBVyxJQUFJO0FBQUEsVUFDOUIsT0FBTyxVQUFVLFNBQVMsSUFBSSxRQUFRO0FBQUEsVUFDdEMsV0FBVyxJQUFJLGNBQWMsU0FBUyxNQUFNLFlBQVksSUFBSTtBQUFBLFVBQzVELE9BQU8sSUFBSSxVQUFVLFNBQVMsTUFBTSxRQUFRLElBQUk7QUFBQSxVQUNoRCxhQUFhLElBQUk7QUFBQSxVQUNqQixlQUFlLElBQUk7QUFBQSxVQUNuQixNQUFNLElBQUksUUFBUSxPQUFPO0FBQUEsVUFDekIsTUFBTSxJQUFJLFNBQVMsU0FBUyxNQUFNLE9BQU8sSUFBSTtBQUFBLFVBQzdDLE9BQU8sTUFBTTtBQUFBLFVBQ2IsV0FBVyxJQUFJLGNBQWMsU0FBUyxNQUFNLFlBQVksSUFBSTtBQUFBLFVBQzVELHVCQUF1QjtBQUFBLFFBQ3hCLEdBQUUsS0FBSztBQUFBLE1BQ2hCLENBQU87QUFBQSxJQUNQLENBQUssQ0FBQztBQUFBLEVBQ0g7QUFDSCxDQUFDO0FDL0dHLE1BQUssWUFBYSxnQkFBYTtBQUFBLEVBQzNCLE1BQU07QUFBQSxFQUNOLE9BQU87QUFDSSxXQUFBO0FBQUEsTUFDSCxjQUFjO0FBQUEsUUFDVjtBQUFBLFVBQ0ksT0FBTztBQUFBLFVBQ1AsT0FBTztBQUFBLFFBQ1g7QUFBQSxRQUNBO0FBQUEsVUFDSSxPQUFPO0FBQUEsVUFDUCxPQUFPO0FBQUEsUUFDWDtBQUFBLFFBQ0E7QUFBQSxVQUNJLE9BQU87QUFBQSxVQUNQLE9BQU87QUFBQSxRQUNYO0FBQUEsTUFDSjtBQUFBLE1BQ0EsVUFBVSxDQUFDO0FBQUEsSUFBQTtBQUFBLEVBRW5CO0FBQUEsUUFDTSxjQUFjO0FBQ2hCLFVBQU0sS0FBSztFQUNmO0FBQUEsRUFDQSxTQUFTO0FBQUEsSUFDTCxXQUFXOztBQUNQLFdBQUssR0FBRyxLQUFLLElBQUksV0FBSyxTQUFTLFVBQWQsWUFBdUIsTUFBTTtBQUFBLElBQ2xEO0FBQUEsSUFDQSxnQkFBZ0I7QUFDWixXQUFLLFNBQVM7QUFDZCxXQUFLLFNBQVM7QUFBQSxJQUNsQjtBQUFBLElBQ0EsV0FBVztBQUNQLFdBQUssWUFBWTtBQUFBLElBQ3JCO0FBQUEsVUFDTSxjQUFjO0FBQ1gsV0FBQSxXQUFXLE1BQU0sZUFBZSxJQUFJO0FBQUEsSUFDOUM7QUFBQSxJQUNDLGNBQWM7QUFDVixxQkFBZSxLQUFLLEVBQUUsTUFBTSxLQUFLLFNBQVUsQ0FBQTtBQUFBLElBQy9DO0FBQUEsRUFDSjtBQUNKLENBQUM7QUF6REcsTUFBQSxhQUFBQSxnQ0FBYyxZQUFWLFNBQUssRUFBQTtBQU1ULE1BQUEsYUFBQUEsZ0NBQWUsWUFBWCxVQUFNLEVBQUE7QUFDVixNQUFBLGFBQUFBLGdDQUFxQixXQUFsQixrQkFBYyxFQUFBOztBQVJyQixTQUFBQyxVQUFBLEdBQUFDLFlBU1Msd0JBVE07QUFBQSxJQUFBLFNBQUFDLFFBQ1gsTUFBYztBQUFBLE1BQWQ7QUFBQSxNQUNBQyxZQUlzRCxjQUFBO0FBQUEsUUFBQSxZQUo3QixLQUFTLFNBQUE7QUFBQSxRQUFBLHVCQUFBO0FBQUEsVUFBVCxPQUFBLE1BQUEsUUFBQSxLQUFBLENBQUEsV0FBQSxLQUFBLFNBQVMsUUFBSztBQUFBLFVBSUYsS0FBQTtBQUFBLFFBQUE7QUFBQSxRQUhwQixTQUFTLEtBQUE7QUFBQSxRQUNWLE9BQU07QUFBQSxRQUNOLFFBQUE7QUFBQSxNQUFBLEdBQUEsTUFBQSxHQUFBLENBQUEsY0FBQSxXQUFBLHFCQUFBLENBQUE7QUFBQSxNQUVoQjtBQUFBLE1BQ0E7QUFBQSxJQUFBLENBQUE7QUFBQTs7Ozs7In0=
